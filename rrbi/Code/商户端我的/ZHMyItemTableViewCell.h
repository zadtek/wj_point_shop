//
//  ZHMyItemTableViewCell.h
//  UsersShop
//
//  Created by W_L on 2019/5/5.
//  Copyright © 2019 dwj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZHMyItemTableViewCell : UITableViewCell
//
@property (nonatomic,strong) UIImageView *imgView;
@property (nonatomic,strong) UILabel *titleLB;
@property (nonatomic,strong) UIView *lineView;

@end

NS_ASSUME_NONNULL_END
