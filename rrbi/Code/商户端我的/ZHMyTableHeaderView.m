//
//  ZHMyTableHeaderView.m
//  UsersShop
//
//  Created by W_L on 2019/5/5.
//  Copyright © 2019 dwj. All rights reserved.
//

#import "ZHMyTableHeaderView.h"

@implementation ZHMyTableHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
   self = [super initWithFrame:frame];
    self.backgroundColor = [Config colorWithHexString:@"#313b45" WithAlpha:1];
    UIColor *Textcolor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:1];
    self.headerImgBtn = [UIButton buttonWithType:0];
    self.nameLB = [[UILabel alloc] init];
    self.scoreLB = [[UILabel alloc] init];
    self.scoreImage = [[UIImageView alloc] init];
    self.zongheView = [[UIView alloc] init];
    self.acconunLB = [[UILabel alloc] init];
    
    self.yingyeTimeLB = [[UILabel alloc] init];
    self.yingyeBtn = [UIButton buttonWithType:0];
    self.changeyingyetimeBtn = [UIButton buttonWithType:0];
    self.announcementLB = [[UILabel alloc]init];
    self.announcementBtn = [UIButton buttonWithType:0];
    
    
    [self addSubview:self.headerImgBtn];
    [self addSubview:self.nameLB];
    [self addSubview:self.scoreLB];
    [self addSubview:self.scoreImage];
    [self addSubview:self.zongheView];
    [self addSubview:self.acconunLB];
    [self addSubview:self.yingyeTimeLB];
    [self addSubview:self.yingyeBtn];
    [self addSubview:self.changeyingyetimeBtn];
    [self addSubview:self.announcementLB];
    [self addSubview:self.announcementBtn];
    
    CGFloat padding = kSizeRatio(10);
    [self.headerImgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(kSizeRatio(20));
        make.size.mas_equalTo(CGSizeMake(kSizeRatio(124), kSizeRatio(124)));
        make.top.equalTo(self).offset(kSizeRatio(114));
        //make.bottom.equalTo(self).offset(kSizeRatio(-114));
    }];
    
    self.headerImgBtn.layer.cornerRadius = kSizeRatio(124)/2;
    self.headerImgBtn.layer.masksToBounds = YES;
    
    self.headerImgBtn.layer.borderWidth = 3;
    self.headerImgBtn.layer.borderColor = [Config colorWithHexString:@"#bec3c4" WithAlpha:1].CGColor;
    
    self.headerImgBtn.backgroundColor = [UIColor whiteColor];
    NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[ShopConfig getLogo]]];
    UIImage *image = [UIImage imageWithData:imgData];// 拿到image
    [self.headerImgBtn setBackgroundImage:image forState:UIControlStateNormal];
    
    
    [self.nameLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerImgBtn);
        make.left.equalTo(self.headerImgBtn.mas_right).offset(padding);
//        make.width.mas_equalTo(kSizeRatio(250));
    }];
    self.nameLB.font = [UIFont systemFontOfSize:20];
    self.nameLB.textColor = [UIColor whiteColor];
    self.nameLB.text = [NSString stringWithFormat:@"%@",[ShopConfig getSite_name]];

    
    
    [self.zongheView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLB.mas_bottom).offset(padding*2);
        make.left.equalTo(self.nameLB);
        make.width.mas_equalTo(kSizeRatio(200));
        make.height.mas_equalTo(kSizeRatio(40));
    }];
    
    
    [self.scoreLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.zongheView.mas_bottom).offset(padding-30);
        make.left.equalTo(self.nameLB);
        //        make.width.mas_equalTo(kSizeRatio(200));
    }];
    self.scoreLB.font = [UIFont systemFontOfSize:15];
    self.scoreLB.textColor = Textcolor;
    self.scoreLB.text = [NSString stringWithFormat:@"综合评分: %@分",[ShopConfig getPhone]];
    
    
    
    [self.scoreImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.zongheView.mas_bottom).offset(padding-30);
        make.left.equalTo(self.scoreLB.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(10, 17));
        //        make.width.mas_equalTo(kSizeRatio(200));
    }];
    self.scoreImage.contentMode = UIViewContentModeCenter;
    
    
    [self.acconunLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.zongheView.mas_bottom).offset(padding-10);
        make.left.equalTo(self.nameLB);
//        make.width.mas_equalTo(kSizeRatio(200));
    }];
    self.acconunLB.font = [UIFont systemFontOfSize:15];
    self.acconunLB.textColor = Textcolor;
    self.acconunLB.text = [NSString stringWithFormat:@"账号: %@",[ShopConfig getPhone]];
    
    
    [self.yingyeTimeLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.acconunLB.mas_bottom).offset(padding *4/3);
        make.left.equalTo(self.nameLB);
//        make.width.mas_equalTo(kSizeRatio(250));
    }];
    self.yingyeTimeLB.font = [UIFont systemFontOfSize:19];
    self.yingyeTimeLB.textColor = Textcolor;
    self.yingyeTimeLB.text = [NSString stringWithFormat:@"营业时间: %@~%@",[ShopConfig getOpen_time],[ShopConfig getClose_time]];;
    
    
    [self.yingyeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(kSizeRatio(153));
        make.right.equalTo(self).offset(kSizeRatio(-20));
        make.size.mas_equalTo(CGSizeMake(kSizeRatio(120), kSizeRatio(38)));
    }];
    
    
    if ([[ShopConfig getStatus] isEqualToString:@"1"]) {
        [self.yingyeBtn setTitle:@"营业中" forState:(UIControlStateNormal)];
    } else {
        [self.yingyeBtn setTitle:@"休息中" forState:(UIControlStateNormal)];
    }
    
    [self.yingyeBtn setTitleColor:[Config colorWithHexString:@"#febe21" WithAlpha:1] forState:(UIControlStateNormal)];
    [self.yingyeBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    self.yingyeBtn.layer.borderWidth = 1;
    self.yingyeBtn.layer.borderColor = [Config colorWithHexString:@"#febe21" WithAlpha:1].CGColor;
    self.yingyeBtn.layer.cornerRadius = 5;
    self.yingyeBtn.layer.masksToBounds = YES;
    
    [self.changeyingyetimeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.acconunLB.mas_bottom).offset(padding *4/3);
        make.right.equalTo(self).offset(kSizeRatio(-20));
        make.size.mas_equalTo(CGSizeMake(kSizeRatio(120), kSizeRatio(38)));
    }];
    
    [self.changeyingyetimeBtn setTitle:@"修改" forState:(UIControlStateNormal)];
    [self.changeyingyetimeBtn setTitleColor:[Config colorWithHexString:@"#febe21" WithAlpha:1] forState:(UIControlStateNormal)];
    [self.changeyingyetimeBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    self.changeyingyetimeBtn.layer.borderWidth = 1;
    self.changeyingyetimeBtn.layer.borderColor = [Config colorWithHexString:@"#febe21" WithAlpha:1].CGColor;
    self.changeyingyetimeBtn.layer.cornerRadius = 5;
    self.changeyingyetimeBtn.layer.masksToBounds = YES;
    
    
    
    [self.announcementLB mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.equalTo(self).offset(kSizeRatio(20));
        make.right.equalTo(self).offset(-40);
         make.top.equalTo(self.changeyingyetimeBtn.mas_bottom).offset(20);
    }];
    self.announcementLB.font = [UIFont systemFontOfSize:13];
    self.announcementLB.textColor = Textcolor;
    
    
    [self.announcementBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.changeyingyetimeBtn.mas_bottom).offset(18);
        make.right.equalTo(self).offset(kSizeRatio(-20));
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
    [self.announcementBtn setImage:[UIImage imageNamed:@"修改"] forState:UIControlStateNormal];
    
    
    [self.yingyeBtn addTarget:self action:@selector(yingyeBtnACtion) forControlEvents:UIControlEventTouchUpInside];
    [self.changeyingyetimeBtn addTarget:self action:@selector(changeyingyetimeBtnACtion) forControlEvents:UIControlEventTouchUpInside];
    [self.announcementBtn addTarget:self action:@selector(announcementBtnAciton) forControlEvents:UIControlEventTouchUpInside];
    
    
    return self;
}
- (void)updateHeadData {
    
    if ([[ShopConfig getStatus] isEqualToString:@"1"]) {
        [self.yingyeBtn setTitle:@"营业中" forState:(UIControlStateNormal)];
    } else {
        [self.yingyeBtn setTitle:@"休息中" forState:(UIControlStateNormal)];
    }
    self.yingyeTimeLB.text = [NSString stringWithFormat:@"营业时间: %@~%@",[ShopConfig getOpen_time],[ShopConfig getClose_time]];;
}

- (void)updateNewHeadDataWith:(NSDictionary *)dict {
    if ([[dict objectForKey:@"pf_type"] integerValue] == 1) {
        self.scoreImage.image = [UIImage imageNamed:@"增长-1"];
    } else {
        self.scoreImage.image = [UIImage imageNamed:@"降低-1"];
    }
    self.scoreLB.text = [NSString stringWithFormat:@"综合评分: %@",[dict objectForKey:@"pf"]];
    self.announcementLB.text = [NSString stringWithFormat:@"公告:%@",[dict objectForKey:@"shop_remark"]];
    self.acconunLB.text = [NSString stringWithFormat:@"账号: %@",[dict objectForKey:@"shop_dh"]];
    self.nameLB.text = [NSString stringWithFormat:@"%@",[ShopConfig getSite_name]];
    NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[ShopConfig getLogo]]];
    UIImage *image = [UIImage imageWithData:imgData];// 拿到image
    [self.headerImgBtn setBackgroundImage:image forState:UIControlStateNormal];
}
- (void)yingyeBtnACtion {
    if (self.YingYeButtonBlock) {
        self.YingYeButtonBlock();
    }
}
- (void)changeyingyetimeBtnACtion {
    
    if (self.ChangYingYeTimeButtonBlock) {
        self.ChangYingYeTimeButtonBlock();
    }
}

- (void)announcementBtnAciton {
    
    if (self.announcementButtonBlock) {
        self.announcementButtonBlock();
    }
}

@end
