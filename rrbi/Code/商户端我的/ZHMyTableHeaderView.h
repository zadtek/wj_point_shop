//
//  ZHMyTableHeaderView.h
//  UsersShop
//
//  Created by W_L on 2019/5/5.
//  Copyright © 2019 dwj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZHMyTableHeaderView : UIView

@property (strong,nonatomic) UIButton *headerImgBtn;

@property (strong,nonatomic) UILabel *nameLB;
@property (strong,nonatomic) UILabel *scoreLB;//综合评分
@property (strong,nonatomic) UIImageView *scoreImage;//综合评分箭头
@property (strong,nonatomic) UILabel *announcementLB;//公告
@property (strong,nonatomic) UIView *zongheView;
@property (strong,nonatomic) UILabel *acconunLB;

@property (strong,nonatomic) UILabel *yingyeTimeLB;

@property (strong,nonatomic) UIButton *yingyeBtn;
@property (strong,nonatomic) UIButton *announcementBtn;//公告按钮
@property (strong,nonatomic) UIButton *changeyingyetimeBtn;

@property (nonatomic, copy) void(^YingYeButtonBlock)();

@property (nonatomic, copy) void(^ChangYingYeTimeButtonBlock)();
@property (nonatomic, copy) void(^announcementButtonBlock)();

- (void)updateHeadData; 
- (void)updateNewHeadDataWith:(NSDictionary *)dict;


@end

NS_ASSUME_NONNULL_END
