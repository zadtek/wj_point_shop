//
//  SettlementInformationViewController.m
//  rrbi
//
//  Created by MyMac on 2019/5/13.
//

#import "SettlementInformationViewController.h"
#import "SettlementInformationModel.h"
#import "SettlementInformationCell.h"


@interface SettlementInformationViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tabelView;

@property (nonatomic, strong) NSMutableArray *subArray;


@end

@implementation SettlementInformationViewController

- (NSMutableArray *)subArray {
    if (!_subArray) {
        _subArray  = [NSMutableArray array];
    }
    return _subArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"账号信息";
    
    [self createTAbelView];
    [self getShopData];
}
- (void)createTAbelView {
    self.tabelView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-SafeAreaTopHeight) style:UITableViewStyleGrouped];
    self.tabelView.delegate = self;
    self.tabelView.dataSource = self;
    [self.view addSubview:self.tabelView];
    self.tabelView.showsVerticalScrollIndicator = NO;
    self.tabelView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tabelView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if (@available(iOS 11.0, *))
    {
        self.tabelView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tabelView.estimatedRowHeight = 0;
        self.tabelView.estimatedSectionFooterHeight = 0;
        self.tabelView.estimatedSectionHeaderHeight = 0;
    }
    
    [self.tabelView registerNib:[UINib nibWithNibName:NSStringFromClass([SettlementInformationCell class]) bundle:nil] forCellReuseIdentifier:@"settlementInformationCell"];
    
   
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.subArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSMutableArray *arr = self.subArray[section];
    return arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
        UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 13)];
        headView.backgroundColor = [UIColor clearColor];
        return headView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.00001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 13;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SettlementInformationCell *orderCell = [tableView dequeueReusableCellWithIdentifier:@"settlementInformationCell"];
    
    NSMutableArray *subArr = self.subArray[indexPath.section];
    SettlementInformationModel *model = subArr[indexPath.row];
    [orderCell updataDataWithMainTitleStr:model.mainTStr subTitleStr:model.subTStr];
    
    return orderCell;
    
}
#pragma mark - 数据请求
- (void)getShopData{
    
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{
                          @"ru_id":[UserModel sharedInstanced].ru_Id,
                          @"ince":@"get_bills_info"
                          
                          };
    NSString *URL = [NSString stringWithFormat:@"%@%@",BaseUrl,@"get_bills_info"];
    [mgr POST:URL parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            
            NSDictionary *dict = [responseObject objectForKey:@"data"];
            
            NSMutableArray *subArr0 = [[NSMutableArray alloc]init];
            for (NSInteger i=0; i<1; i++) {
                SettlementInformationModel *model = [[SettlementInformationModel alloc]init];
                model.mainTStr = @"当前账号";
                model.subTStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"dq_account"]];
                [subArr0 addObject:model];
            }
            [self.subArray addObject:subArr0];
            NSMutableArray *subArr = [[NSMutableArray alloc]init];
            for (NSInteger i=0; i<7; i++) {
                SettlementInformationModel *model = [[SettlementInformationModel alloc]init];
                if (i==0) {
                    model.mainTStr = @"提现账号";
                    model.subTStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"bank_card"]];
                } else if (i==1) {
                    model.mainTStr = @"开户行";
                    model.subTStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"bank"]];
                }else if (i==2) {
                    model.mainTStr = @"开户名";
                    model.subTStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"name"]];
                }else if (i==3) {
                    model.mainTStr = @"开户证件";
                    model.subTStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"idtype"]];
                }else if (i==4) {
                    model.mainTStr = @"证件号码";
                    model.subTStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"id_card"]];
                }else if (i==5) {
                    model.mainTStr = @"预留手机号";
                    model.subTStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"phone"]];
                }else if (i==6) {
                    model.mainTStr = @"账号类型";
                    model.subTStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"type"]];
                }
                [subArr addObject:model];
            }
            [self.subArray addObject:subArr];
            NSMutableArray *subArr1 = [[NSMutableArray alloc]init];
            for (NSInteger i=0; i<5; i++) {
                SettlementInformationModel *model = [[SettlementInformationModel alloc]init];
                if (i==0) {
                    model.mainTStr = @"财务负责人";
                    model.subTStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"fz_name"]];
                } else if (i==1) {
                    model.mainTStr = @"负责人电话";
                    model.subTStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"fz_tel"]];
                }else if (i==2) {
                    model.mainTStr = @"结算方式";
                    model.subTStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"js_type"]];
                }else if (i==3) {
                    model.mainTStr = @"结算周期";
                    model.subTStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"js_cycle"]];
                }else if (i==4) {
                    model.mainTStr = @"最低结算金额";
                    model.subTStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"mix_money"]];
                }
                [subArr1 addObject:model];
            }
            [self.subArray addObject:subArr1];
            [self.tabelView reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        [self hidHUD:@"网络异常"];
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
