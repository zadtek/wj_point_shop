//
//  SettlementInformationModel.h
//  rrbi
//
//  Created by MyMac on 2019/5/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettlementInformationModel : NSObject

@property (nonatomic, copy) NSString *mainTStr;
@property (nonatomic, copy) NSString *subTStr;


@end

NS_ASSUME_NONNULL_END
