//
//  SettlementInformationCell.h
//  rrbi
//
//  Created by MyMac on 2019/5/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettlementInformationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mainTitleL;
@property (weak, nonatomic) IBOutlet UILabel *subTitLabel;

- (void)updataDataWithMainTitleStr:(NSString *)mainStr subTitleStr:(NSString *)subTitle;

@end

NS_ASSUME_NONNULL_END
