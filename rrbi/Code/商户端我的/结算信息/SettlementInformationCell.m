//
//  SettlementInformationCell.m
//  rrbi
//
//  Created by MyMac on 2019/5/13.
//

#import "SettlementInformationCell.h"

@implementation SettlementInformationCell


- (void)updataDataWithMainTitleStr:(NSString *)mainStr subTitleStr:(NSString *)subTitle {
    self.mainTitleL.text = mainStr;
    self.subTitLabel.text = subTitle;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
