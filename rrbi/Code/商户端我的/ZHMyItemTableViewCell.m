//
//  ZHMyItemTableViewCell.m
//  UsersShop
//
//  Created by W_L on 2019/5/5.
//  Copyright © 2019 dwj. All rights reserved.
//

#import "ZHMyItemTableViewCell.h"

@implementation ZHMyItemTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupSubview];
    }
    
    return self;
}

- (void)setupSubview{
    self.imgView = [[UIImageView alloc] init];
    self.imgView.contentMode= UIViewContentModeCenter;
    [self.contentView addSubview:self.imgView];
    
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(kSizeRatio(40));
        make.size.mas_equalTo(CGSizeMake(kSizeRatio(60), kSizeRatio(60)));
        make.top.equalTo(self.contentView).offset(kSizeRatio(22));
        make.bottom.equalTo(self.contentView).offset(-kSizeRatio(22));
    }];
    
//    self.imgView.backgroundColor = [UIColor yellowColor];
    
    self.titleLB = [[UILabel alloc] init];
    
    [self.contentView addSubview:self.titleLB];
    
    [self.titleLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.imgView);
        make.left.equalTo(self.imgView.mas_right).offset(kSizeRatio(30));
        make.right.equalTo(self.contentView.mas_right).offset(kSizeRatio(-40));
    }];
    
    self.titleLB.textColor = [Config colorWithHexString:@"#343434" WithAlpha:1];
    self.titleLB.font = [UIFont systemFontOfSize:16];
    
    self.lineView = [[UIView alloc] init];
    
    [self.contentView addSubview:self.lineView];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.titleLB.mas_left);
        make.right.equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
    }];
    self.lineView.backgroundColor = [Config colorWithHexString:@"#ededed" WithAlpha:1];
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
