//
//  MessgerSettingCell.m
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import "MessgerSettingCell.h"

@implementation MessgerSettingCell

- (void)updateDataWithSection:(NSInteger)secton row:(NSInteger)row {
    
    if (secton == 0) {
        if (row == 0) {
            if (![[NSUserDefaults standardUserDefaults] boolForKey:@"XITONGXIAOXI_LINGSHENG"]) {//铃声默认
                self.kaiguanSwitch.on = YES;
            } else {//
                self.kaiguanSwitch.on = NO;
            }
            
            self.biaoTiLabel.text = @"铃声";
        } else {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"XITONGXIAOXI_ZHENDONG"]) {//震动
                self.kaiguanSwitch.on = YES;
            } else {
                self.kaiguanSwitch.on = NO;
            }
            
            self.biaoTiLabel.text = @"震动";
        }
    } else {
        
        if (row == 0) {
            if (![[NSUserDefaults standardUserDefaults] boolForKey:@"DINGDANXIAOXI_LINGSHENG"]) {//铃声默认
                self.kaiguanSwitch.on = YES;
            } else {//
                self.kaiguanSwitch.on = NO;
            }
            
            self.biaoTiLabel.text = @"铃声";
        } else {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"DINGDANXIAOXI_ZHENDONG"]) {//震动
                self.kaiguanSwitch.on = YES;
            } else {
                self.kaiguanSwitch.on = NO;
            }
            
            self.biaoTiLabel.text = @"震动";
        }
    }
}


- (IBAction)kaiguanSwitchAction:(id)sender {
    if (self.kaiGuanSwitch) {
        self.kaiGuanSwitch(self.kaiguanSwitch);
    }
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
