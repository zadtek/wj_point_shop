//
//  MessgerSettingCell.h
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessgerSettingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *biaoTiLabel;
@property (weak, nonatomic) IBOutlet UISwitch *kaiguanSwitch;

- (void)updateDataWithSection:(NSInteger)secton row:(NSInteger)row;
@property (nonatomic, copy) void(^kaiGuanSwitch)(UISwitch *kaiguanSwitch);
@end

NS_ASSUME_NONNULL_END
