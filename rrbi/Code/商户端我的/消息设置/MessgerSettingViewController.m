//
//  MessgerSettingViewController.m
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import "MessgerSettingViewController.h"

#import "MessgerSettingCell.h"
#import <AudioToolbox/AudioToolbox.h>

@interface MessgerSettingViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;

@end

@implementation MessgerSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor lightGrayColor];
    self.title = @"消息设置";

    [self creatSubViews];
}
-(void)creatSubViews{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,  CGRectGetHeight(self.view.frame) -NAVGATIONBAR_HEIGHT) style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.view addSubview:self.tableView];
    
    if (@available(iOS 11.0, *))
    {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MessgerSettingCell class]) bundle:nil] forCellReuseIdentifier:@"messgerSettingCell"];
    
}

#pragma mark - ******* tableview代理方法 *******
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MessgerSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"messgerSettingCell"];
    [cell updateDataWithSection:indexPath.section row:indexPath.row];
    cell.kaiGuanSwitch = ^(UISwitch * _Nonnull kaiguanSwitch) {
        if (indexPath.section == 0 && indexPath.row == 0) {
            if (!kaiguanSwitch.isOn) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"XITONGXIAOXI_LINGSHENG"];
            } else {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"XITONGXIAOXI_LINGSHENG"];
            }
        } else if (indexPath.section == 0 && indexPath.row == 1) {
            if (kaiguanSwitch.isOn) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"XITONGXIAOXI_ZHENDONG"];
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);  // 震动
            } else {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"XITONGXIAOXI_ZHENDONG"];
            }
            
        }  else if (indexPath.section == 1 && indexPath.row == 0) {
            if (!kaiguanSwitch.isOn) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DINGDANXIAOXI_LINGSHENG"];
            } else {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"DINGDANXIAOXI_LINGSHENG"];
            }
            
        } else if (indexPath.section == 1 && indexPath.row == 1) {
            if (kaiguanSwitch.isOn) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DINGDANXIAOXI_ZHENDONG"];
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);  // 震动
            } else {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"DINGDANXIAOXI_ZHENDONG"];
            }
            
        }
    };
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 0) {
        UIView *footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
        footView.backgroundColor = [UIColor clearColor];
        
        UIView *baiView= [[UIView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 30)];
        baiView.backgroundColor = [UIColor whiteColor];
        [footView addSubview:baiView];
        UILabel *biaoLabel =[[ UILabel alloc]initWithFrame:CGRectMake(15, 0, 100, 30)];
        biaoLabel.text = @"订单消息";
        biaoLabel.textColor = [UIColor blackColor];
        biaoLabel.font = [UIFont systemFontOfSize:13];
        [baiView addSubview:biaoLabel];
        
        return footView;
    } else {
        return nil;
    }
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        UIView *footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
        footView.backgroundColor = [UIColor clearColor];
        
        UIView *baiView= [[UIView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 30)];
        baiView.backgroundColor = [UIColor whiteColor];
        [footView addSubview:baiView];
        UILabel *biaoLabel =[[ UILabel alloc]initWithFrame:CGRectMake(15, 0, 100, 30)];
        biaoLabel.text = @"系统消息";
        biaoLabel.textColor = [UIColor blackColor];
        biaoLabel.font = [UIFont systemFontOfSize:13];
        [baiView addSubview:biaoLabel];
        return footView;
    } else {
        return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return 40;
    } else {
        return 0.000001;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 40;
    } else {
        return 0.000001;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
