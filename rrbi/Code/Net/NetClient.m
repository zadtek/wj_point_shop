//
//  NetClient.m
//  KYRR
//
//  Created by kyjun on 16/6/1.
//
//

#import "NetClient.h"

//static NSString * const AFAppDotNetAPIBaseURLString = @"http://shop.xyrr.com/";
//static NSString * const AFAppDotNetAPIBaseURLString = @"http://shop.zadtek.com/";
static NSString * const AFAppDotNetAPIBaseURLString = @"http://xmshop.zadtek.com/mobile/Api/index";


@implementation NetClient

static NetClient *sharedClient;

+ (instancetype)sharedClient {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClient = [[NetClient alloc] initWithBaseURL:[NSURL URLWithString:AFAppDotNetAPIBaseURLString]];
        sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    return sharedClient;
}

-(instancetype)init{
    self = [super init];
    if(self){
        
    }
    return self;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        if(sharedClient == nil)
        {
            sharedClient = [super allocWithZone:zone];
        }
    });
    return sharedClient;
}

-(id)copy{
    return self;
}

-(id)mutableCopy{
    return self;
}

@end
