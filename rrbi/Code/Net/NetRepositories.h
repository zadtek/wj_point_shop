//
//  NetRepositories.h
//  KYRR
//
//  Created by kyjun on 16/6/1.
//
//

#import <Foundation/Foundation.h>
#import "NetPage.h"
#import "NetClient.h"
#import "Model.h"
#import <AFNetworking/AFNetworking.h>

/**
 *  数据请求结果枚举值
 */
typedef NS_ENUM(NSInteger,EnumNetResponse) {
    /**
     *  请求成功 200
     */
    NetResponseSuccess = 200,
    /**
     *  请求返回错误 0
     */
    NetResponseFail =  0,
    /**
     *  请求异常 400
     */
    NetResponseException = 400
};

/**
 *  数据请求响应结果集 block
 *
 *  @param react  EnumNetResponse 表示当前请求数据的状态 例如:失败(0)
 *  @param list    NSArray  标示当前请求放回的结果集 如果请求失败是nill
 *  @param message NSString 标示请求返回的消息 如果请求失败会返回失败原因
 */
typedef void(^responseListBlock)(EnumNetResponse react, NSArray* list, NSString* message, NSDictionary *mainDict);

typedef void(^responseDictBlock)(EnumNetResponse react,NSDictionary* response,NSString* message);

typedef void(^responseListAndOtherBlock)(EnumNetResponse react, NSArray* list,id model, NSString* message);

/**
 *  数据请求响应结果 block
 *
 *  @param react  EnumNetResponse 表示当前请求数据的状态 例如:失败(0)
 *  @param item    id  标示当前请求放回的结果 如果请求失败是nill
 *  @param message NSString 标示请求返回的消息 如果请求失败会返回失败原因
 */
typedef void(^responseItemBlock)(EnumNetResponse react, id obj ,NSString* message);

typedef void(^responseBlock)(EnumNetResponse react,NSString* message);

@interface NetRepositories : NSObject
+(void)netSingleConfirm:(NSDictionary *)arg complete:(responseDictBlock)complete;
+(void)netConfirm:(NSDictionary*)arg complete:(responseDictBlock)complete;
/** * 上传单张图片 */
+(void)requestAFURL:(NSDictionary *)arg
          imageData:(NSData *)imageData
           complete:(responseDictBlock)complete;

+(void)netUploadImage:(NSDictionary *)arg
       imageDataArray:(NSArray *)imageDataArray
             complete:(responseDictBlock)complete;
@end

FOUNDATION_EXTERN NSString* const AppNetSubPath;

@interface NetRepositories (Goods)
+(void)queryGoods:(NSDictionary*)arg complete:(responseListBlock)complete;
+(void)queryGoods:(NSDictionary*)arg page:(NetPage*)page complete:(responseListBlock)complete;
+(void)searchGoods:(NSDictionary*)arg complete:(responseListBlock)complete;
+(void)updtaeGoods:(NSDictionary*)arg complete:(responseBlock)complete;
//平台分类
+(void)searchPlatformGoods:(NSDictionary*)arg complete:(responseListBlock)complete;

@end
/**
 *  店铺的数据操作方法
 */
@interface NetRepositories (Store)

-(void)queryStore:(NSDictionary*)arg complete:(responseListBlock)complete;
-(void)queryStore:(NSDictionary*)arg page:(NetPage*)page complete:(responseListBlock)complete;
-(void)searchStore:(NSDictionary*)arg complete:(responseItemBlock)complete;
+(void)deleteStore:(NSDictionary*)arg complete:(responseItemBlock)complete;
+(void)addStore:(NSDictionary*)arg complete:(responseItemBlock)complete;

@end
/**
 *  促销的数据操作方法
 */
@interface NetRepositories (Promotion)
+(void)queryPromotion:(NSDictionary*)arg complete:(responseListBlock)complete;
+(void)queryPromotion:(NSDictionary*)arg page:(NetPage*)page complete:(responseListBlock)complete;
+(void)deletePromotion:(NSDictionary*)arg complete:(responseItemBlock)complete;
+(void)addPromotion:(NSDictionary*)arg complete:(responseItemBlock)complete;
+(void)changePromotion:(NSDictionary*)arg complete:(responseItemBlock)complete;

@end


@interface NetRepositories (Comment)

-(void)queryComment:(NSDictionary*)arg complete:(responseListAndOtherBlock)complete;
-(void)queryComment:(NSDictionary*)arg page:(NetPage*)page complete:(responseListAndOtherBlock)complete;
-(void)updateComment:(NSDictionary*)arg complete:(responseItemBlock)complete;

@end

/**
 *  用户的数据操作方法
 */
@interface NetRepositories (UserInfo)
-(void)login:(NSDictionary*)arg complete:(responseItemBlock)complete;
-(void)register:(NSDictionary*)arg complete:(responseItemBlock)complete;
-(void)searchUserInfo:(NSDictionary*)arg complete:(responseItemBlock)complete;
/**
 *  账户明细
 *
 *  @param arg      NSDictionary
 *  @param page     NetPage
 *  @param complete block
 */
-(void)queryAccount:(NSDictionary*)arg page:(NetPage*)page complete:(responseListBlock)complete;
/**
 *  提现明细
 *
 *  @param arg      NSDictionary
 *  @param page     NetPage
 *  @param complete block
 */
-(void)queryCash:(NSDictionary*)arg page:(NetPage*)page complete:(responseListBlock)complete;

@end



