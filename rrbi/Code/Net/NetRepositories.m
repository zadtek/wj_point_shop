//
//  NetRepositories.m
//  KYRR
//
//  Created by kyjun on 16/6/1.
//
//

#import "NetRepositories.h"


NSString* const AppNetSubPath = @"mobile/Api/index";

@implementation NetRepositories

+(void)netSingleConfirm:(NSDictionary *)arg complete:(responseDictBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSDictionary* empty = nil;
        if(![WMHelper isNULLOrnil:responseObject]){
            empty = responseObject;
        }

        complete(flag,empty,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常");
    }];
}


+(void)netConfirm:(NSDictionary *)arg complete:(responseDictBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSDictionary* empty = nil;
        if(flag == NetResponseSuccess){
            message = [responseObject objectForKey:@"msg"];
            if(![WMHelper isNULLOrnil:responseObject]){
                empty = responseObject;
            }
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常");
    }];
}
/** * 上传单张图片 */
+(void)requestAFURL:(NSDictionary *)arg
          imageData:(NSData *)imageData
           complete:(responseDictBlock)complete{

    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        // 在网络开发中，上传文件时，是文件不允许被覆盖，文件重名
        // 要解决此问题，
        // 可以在上传时使用当前的系统事件作为文件名
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyyMMddHHmmssSSS";
        // 设置时间格式
        NSString *str = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
        //将得到的二进制图片拼接到表单中 /** data,指定上传的二进制流;name,服务器端所需参数名*/
        [formData appendPartWithFileData:imageData name:@"img" fileName:fileName mimeType:@"image/jpg/png/jpeg"];
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
//        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSDictionary* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            if(![WMHelper isNULLOrnil:responseObject]){
                empty = responseObject;
            }
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常");

    }];
}
/** * AF网络请求 */
+(void)netUploadImage:(NSDictionary *)arg
       imageDataArray:(NSArray *)imageDataArray
             complete:(responseDictBlock)complete{
    
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i = 0; i<imageDataArray.count; i++){
            //               NSData *imageData =imageDataArray[i];
            
            UIImage *image = imageDataArray[i];
            NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
            
            
            // 在网络开发中，上传文件时，是文件不允许被覆盖，文件重名
            // 要解决此问题，
            // 可以在上传时使用当前的系统事件作为文件名
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            // 设置时间格式
            formatter.dateFormat = @"yyyyMMddHHmmss";
            NSString *str = [formatter stringFromDate:[NSDate date]];
            NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
            NSString *name = [NSString stringWithFormat:@"img%d",i ];
            //将得到的二进制图片拼接到表单中 /** data,指定上传的二进制流;name,服务器端所需参数名*/
            [formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:@"image/jpg/png/jpeg"];
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSInteger flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSDictionary* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            if(![WMHelper isNULLOrnil:responseObject]){
                empty = responseObject;
            }
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        complete(NetResponseException,nil,@"网络异常");

    }];
    
    
}




@end


@implementation NetRepositories (Goods)

+(void)queryGoods:(NSDictionary *)arg complete:(responseListBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSMutableArray* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            empty =[[NSMutableArray alloc] init];
            NSArray* emptyArray = [responseObject objectForKey:@"info"];
            if(![WMHelper isNULLOrnil:emptyArray])
                [emptyArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    MGoods* item = [[MGoods alloc]init];
                    [item setValuesForKeysWithDictionary:obj];
                    [empty addObject:item];
                }];
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常",nil);
    }];
    
}

+(void)queryGoods:(NSDictionary *)arg page:(NetPage *)page complete:(responseListBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSMutableArray* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            empty =[[NSMutableArray alloc] init];
            NSArray* emptyArray = [responseObject objectForKey:@"list"];
            if(![WMHelper isNULLOrnil:page]){
                page.recordCount = [[responseObject objectForKey:@"count"]integerValue];
                if(![WMHelper isNULLOrnil:[responseObject objectForKey:@"number"]]){
                    page.pageSize = [[responseObject objectForKey:@"number"] integerValue];
                }else{
                    page.pageSize = emptyArray.count;
                }
                if(![WMHelper isNULLOrnil:[responseObject objectForKey:@"total_page"]]){
                    page.pageCount = [[responseObject objectForKey:@"total_page"] integerValue];
                }else if (![WMHelper isNULLOrnil:[responseObject objectForKey:@"totalpage"]]){
                    page.pageCount = [[responseObject objectForKey:@"totalpage"] integerValue];
                }else{
                    if(page.recordCount%page.pageSize==0){
                        page.pageCount = page.recordCount/page.pageSize;
                    }else{
                        page.pageCount = (page.recordCount+page.pageSize)/page.pageSize;
                    }
                }
            }
            if(![WMHelper isNULLOrnil:emptyArray])
                [emptyArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    MGoods* item = [[MGoods alloc]init];
                    [item setValuesForKeysWithDictionary:obj];
                    [empty addObject:item];
                }];
            
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常",nil);
    }];
}

+(void)searchGoods:(NSDictionary *)arg complete:(responseListBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSMutableArray* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            empty =[[NSMutableArray alloc] init];
            NSArray* emptyArray = [responseObject objectForKey:@"data"];
            for (NSDictionary *dic in emptyArray) {
                MStore* model = [MStore InitStoreWithJsonData:dic];
                 [empty addObject:model];
            
            }
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常",nil);
    }];
}

+(void)updtaeGoods:(NSDictionary *)arg complete:(responseBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        if(flag == NetResponseSuccess){
            message = @"";
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,@"网络异常");
    }];
    
}
+(void)searchPlatformGoods:(NSDictionary*)arg complete:(responseListBlock)complete{
    
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
//        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSMutableArray* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            empty = [NSMutableArray arrayWithArray:[responseObject objectForKey:@"data"]];
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常",nil);
    }];
    
}


@end

#pragma mark =====================================================  Store
@implementation NetRepositories (Store)
-(void)queryStore:(NSDictionary *)arg complete:(responseListBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSMutableArray* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            empty =[[NSMutableArray alloc] init];
            NSArray* emptyArray = [responseObject objectForKey:@"info"];
            if(![WMHelper isNULLOrnil:emptyArray])
                [emptyArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    MStore* item = [[MStore alloc]init];
                    [item setValuesForKeysWithDictionary:obj];
                    [empty addObject:item];
                }];
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常",nil);
    }];
    
}

-(void)queryStore:(NSDictionary *)arg page:(NetPage *)page complete:(responseListBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSMutableArray* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            empty =[[NSMutableArray alloc] init];
            NSArray* emptyArray = [responseObject objectForKey:@"list"];
            if(![WMHelper isNULLOrnil:emptyArray])
                [emptyArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                    MStore* item = [[MStore alloc]init];
                    //                    [item setValuesForKeysWithDictionary:obj];

                     MStore* item = [MStore InitStoreWithJsonData:obj];
                    [empty addObject:item];
                }];
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常",nil);
    }];
}

-(void)searchStore:(NSDictionary *)arg complete:(responseItemBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        MStore* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            empty =[[MStore alloc] init];
            NSDictionary* emptyDict = [responseObject objectForKey:@"info"];
            if(![WMHelper isNULLOrnil:emptyDict])
                [empty setValuesForKeysWithDictionary:emptyDict];
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常");
    }];
}
+(void)deleteStore:(NSDictionary*)arg complete:(responseItemBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        MStore* empty = nil;
        if(flag == NetResponseSuccess){
            message = [responseObject objectForKey:@"msg"];;
//            empty =[[MStore alloc] init];
//            NSDictionary* emptyDict = [responseObject objectForKey:@"info"];
//            if(![WMHelper isNULLOrnil:emptyDict])
//                [empty setValuesForKeysWithDictionary:emptyDict];
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常");
    }];
    
}
+(void)addStore:(NSDictionary*)arg complete:(responseItemBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        MStore* empty = nil;
        if(flag == NetResponseSuccess){
            message = [responseObject objectForKey:@"msg"];;
            //            empty =[[MStore alloc] init];
            //            NSDictionary* emptyDict = [responseObject objectForKey:@"info"];
            //            if(![WMHelper isNULLOrnil:emptyDict])
            //                [empty setValuesForKeysWithDictionary:emptyDict];
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常");
    }];
    
}

@end

@implementation NetRepositories (Promotion)

+(void)queryPromotion:(NSDictionary *)arg complete:(responseListBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSMutableArray* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            empty =[[NSMutableArray alloc] init];
            NSArray* emptyArray = [responseObject objectForKey:@"info"];
            if(![WMHelper isNULLOrnil:emptyArray])
                [emptyArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    MStore* item = [[MStore alloc]init];
                    [item setValuesForKeysWithDictionary:obj];
                    [empty addObject:item];
                }];
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常",nil);
    }];
    
}

+(void)queryPromotion:(NSDictionary *)arg page:(NetPage *)page complete:(responseListBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSMutableArray* empty = nil;
        if(flag == NetResponseSuccess){
            NSDictionary* dataDic = [responseObject objectForKey:@"data"];
            message = dataDic[@"new_custom"];
            empty =[[NSMutableArray alloc] init];
            NSArray* emptyArray = [dataDic objectForKey:@"manjian"];
            if(![WMHelper isNULLOrnil:emptyArray])
                [emptyArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    MStore* item = [MStore InitStoreWithJsonData:obj];
                    [empty addObject:item];
                }];
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常",nil);
    }];
}


+(void)deletePromotion:(NSDictionary*)arg complete:(responseItemBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        MStore* empty = nil;
        if(flag == NetResponseSuccess){
            message = [responseObject objectForKey:@"msg"];
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常");
    }];
    
}
+(void)addPromotion:(NSDictionary*)arg complete:(responseItemBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        MStore* empty = nil;
        if(flag == NetResponseSuccess){
            message = [responseObject objectForKey:@"msg"];
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常");
    }];
    
}

+(void)changePromotion:(NSDictionary*)arg complete:(responseItemBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        MStore* empty = nil;
        if(flag == NetResponseSuccess){
            message = [responseObject objectForKey:@"msg"];;
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常");
    }];
    
}


@end

@implementation NetRepositories (Comment)

-(void)queryComment:(NSDictionary*)arg complete:(responseListAndOtherBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSMutableArray* empty = nil;
        MStoreComment* other = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            empty =[[NSMutableArray alloc] init];
            NSArray* emptyArray = [responseObject objectForKey:@"list"];
            if(![WMHelper isNULLOrnil:emptyArray]){
                [emptyArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    MComment* item = [[MComment alloc]init];
                    [item setValuesForKeysWithDictionary:obj];
                    [empty addObject:item];
                }];
            }
            NSDictionary* emptyDict = [responseObject objectForKey:@"num"];
            if(![WMHelper isNULLOrnil:emptyDict]){
                other = [[MStoreComment alloc]init];
                other.totalNum = [emptyDict objectForKey:@"all"];
                other.badNum = [emptyDict objectForKey:@"score_bad"];
                other.goodNum = [emptyDict objectForKey:@"score_good"];
                other.bestNum = [emptyDict objectForKey:@"score_best"];
                
                other.totalComment = [responseObject objectForKey:@"score"];
                other.foodComment = [responseObject objectForKey:@"score1"];
                other.shipComment = [responseObject objectForKey:@"score2"];
            }
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,other,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,nil,@"网络异常");
    }];
    
}
-(void)queryComment:(NSDictionary*)arg page:(NetPage*)page complete:(responseListAndOtherBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSMutableArray* empty = nil;
        MStoreComment* other = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            
            empty =[[NSMutableArray alloc] init];
            NSArray* emptyArray = [responseObject objectForKey:@"list"];
            if(![WMHelper isNULLOrnil:page]){
                if(![WMHelper isNULLOrnil:[responseObject objectForKey:@"number"]]){
                    page.pageSize = [[responseObject objectForKey:@"number"] integerValue];
                }else{
                    page.pageSize = emptyArray.count;
                }
                page.recordCount = [[responseObject objectForKey:@"num"]integerValue];
                if(![WMHelper isNULLOrnil:[responseObject objectForKey:@"total_page"]]){
                    page.pageCount = [[responseObject objectForKey:@"total_page"] integerValue];
                }else if (![WMHelper isNULLOrnil:[responseObject objectForKey:@"totalpage"]]){
                    page.pageCount = [[responseObject objectForKey:@"totalpage"] integerValue];
                }else{
                    if(page.recordCount%page.pageSize==0){
                        page.pageCount = page.recordCount/page.pageSize;
                    }else{
                        page.pageCount = (page.recordCount+page.pageSize)/page.pageSize;
                    }

                }
            }
            
            if(![WMHelper isNULLOrnil:emptyArray]){
                [emptyArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    MComment* item = [[MComment alloc]init];
                    [item setValuesForKeysWithDictionary:obj];
                    [empty addObject:item];
                }];
            }
            
            other = [[MStoreComment alloc]init];
            other.totalNum = [responseObject objectForKey:@"num"];
            
            other.badNum = [responseObject objectForKey:@"score"];
            other.goodNum = [responseObject objectForKey:@"score1"];
            other.bestNum = [responseObject objectForKey:@"score2"];
            
            other.totalComment = [responseObject objectForKey:@"score"];
            other.foodComment = [responseObject objectForKey:@"score1"];
            other.shipComment = [responseObject objectForKey:@"score2"];
            
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,other,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,nil,@"网络异常");
    }];
}
-(void)updateComment:(NSDictionary*)arg complete:(responseItemBlock)complete{
    
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        MMember* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常");
    }];
}

@end


@implementation NetRepositories (UserInfo)
-(void)login:(NSDictionary *)arg complete:(responseItemBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        MMember* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            empty =[[MMember alloc] init];
            NSDictionary* emptyDict = [responseObject objectForKey:@"info"];
            if(![WMHelper isNULLOrnil:emptyDict])
                [empty setValuesForKeysWithDictionary:emptyDict];
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常");
    }];
    
}

-(void)register:(NSDictionary *)arg complete:(responseItemBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        MMember* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常");
    }];
}

-(void)searchUserInfo:(NSDictionary *)arg complete:(responseItemBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        MMember* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            empty =[[MMember alloc] init];
            NSDictionary* emptyDict = [responseObject objectForKey:@"info"];
            if(![WMHelper isNULLOrnil:emptyDict])
                [empty setValuesForKeysWithDictionary:emptyDict];
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常");
    }];
    
}

-(void)queryAccount:(NSDictionary *)arg page:(NetPage *)page complete:(responseListBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSMutableArray* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            empty =[[NSMutableArray alloc] init];
            NSArray* emptyArray = [responseObject objectForKey:@"list"];
            if(![WMHelper isNULLOrnil:page]){
                page.recordCount = [[responseObject objectForKey:@"total"]integerValue];
                if(![WMHelper isNULLOrnil:[responseObject objectForKey:@"number"]]){
                    page.pageSize = [[responseObject objectForKey:@"number"] integerValue];
                }else{
                    page.pageSize = emptyArray.count;
                }
                if(![WMHelper isNULLOrnil:[responseObject objectForKey:@"total_page"]]){
                    page.pageCount = [[responseObject objectForKey:@"total_page"] integerValue];
                }else if (![WMHelper isNULLOrnil:[responseObject objectForKey:@"totalpage"]]){
                    page.pageCount = [[responseObject objectForKey:@"totalpage"] integerValue];
                }else{
                    if(page.recordCount%page.pageSize==0){
                        page.pageCount = page.recordCount/page.pageSize;
                    }else{
                        page.pageCount = (page.recordCount+page.pageSize)/page.pageSize;
                    }
                }
            }
            if(![WMHelper isNULLOrnil:emptyArray])
                [emptyArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    MIncome* item = [[MIncome alloc]init];
                    [item setValuesForKeysWithDictionary:obj];
                    [empty addObject:item];
                }];
            
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常",nil);
    }];
}

-(void)queryCash:(NSDictionary *)arg page:(NetPage *)page complete:(responseListBlock)complete{
    [[NetClient sharedClient] POST:AppNetSubPath parameters:arg progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        EnumNetResponse flag = [[responseObject objectForKey:@"ret"] integerValue];
        [WMHelper outPutJsonString:responseObject];
        NSString* message = nil;
        NSMutableArray* empty = nil;
        if(flag == NetResponseSuccess){
            message = @"";
            empty =[[NSMutableArray alloc] init];
            NSArray* emptyArray = [responseObject objectForKey:@"list"];
            if(![WMHelper isNULLOrnil:page]){
                page.recordCount = [[responseObject objectForKey:@"total"]integerValue];
                if(![WMHelper isNULLOrnil:[responseObject objectForKey:@"number"]]){
                    page.pageSize = [[responseObject objectForKey:@"number"] integerValue];
                }else{
                    page.pageSize = emptyArray.count;
                }
                if(![WMHelper isNULLOrnil:[responseObject objectForKey:@"total_page"]]){
                    page.pageCount = [[responseObject objectForKey:@"total_page"] integerValue];
                }else if (![WMHelper isNULLOrnil:[responseObject objectForKey:@"totalpage"]]){
                    page.pageCount = [[responseObject objectForKey:@"totalpage"] integerValue];
                }else{
                    if(page.recordCount%page.pageSize==0){
                        page.pageCount = page.recordCount/page.pageSize;
                    }else{
                        page.pageCount = (page.recordCount+page.pageSize)/page.pageSize;
                    }
                }
            }
            if(![WMHelper isNULLOrnil:emptyArray])
                [emptyArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    MIncome* item = [[MIncome alloc]init];
                    [item setValuesForKeysWithDictionary:obj];
                    [empty addObject:item];
                }];
            
        }else{
            message = [responseObject objectForKey:@"msg"];
        }
        complete(flag,empty,message,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NetResponseException,nil,@"网络异常",nil);
    }];
}

@end


