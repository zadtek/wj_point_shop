//
//  TBPoiSearchView.m
//  poi-gaode
//
//  Created by W_L on 2018/10/24.
//  Copyright © 2018 W_L. All rights reserved.
//

#import "TBPoiSearchView.h"

@interface TBPoiSearchView ()<UISearchBarDelegate>

@end

@implementation TBPoiSearchView

- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self =   [super initWithFrame:frame]) {
        [self updataUI];
    }

    return self;
}
-(void)updataUI{
    
    self.cityBtn = [UIButton buttonWithType:0];
    
    self.cityBtn.frame = CGRectMake(10, 0, 40, 60);
    
    [self.cityBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    
    [self.cityBtn addTarget:self action:@selector(cityBtnHandler:) forControlEvents:(UIControlEventTouchUpInside)];
    
    
    
    [self.cityBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    
    [self.cityBtn setTitle:@"沈阳" forState:(UIControlStateNormal)];
    
    [self addSubview:self.cityBtn];
    
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.cityBtn.frame) + 10, 0, self.frame.size.width - (CGRectGetMaxX(self.cityBtn.frame) + 10), self.frame.size.height)];
    self.searchBar.barTintColor = [UIColor whiteColor];
    
    self.searchBar.searchBarStyle = UISearchBarStyleDefault;
    
    [self.searchBar setBackgroundImage:[UIImage new]];
    
    self.searchBar.delegate = self;
    
    //先设置barTintColor 才能设置textfield背景颜色
    UITextField *textfield = [[[self.searchBar.subviews firstObject] subviews] lastObject];
    textfield.backgroundColor =[UIColor groupTableViewBackgroundColor]
    ;
    
    self.searchBar.placeholder = @"请输入收货地址";
    self.searchBar.showsCancelButton = NO;
    
    [self addSubview:self.searchBar];
    
}

#pragma mark --

- (void)cityBtnHandler:(UIButton *)button{
    if ([self.delegate respondsToSelector:@selector(tb_TapCityBtnhHandler:)]) {
        [self.delegate tb_TapCityBtnhHandler:button];
    }
    
}

#pragma mark ---
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
 
    if ([self.delegate respondsToSelector:@selector(tb_SearchBarShouldBeginEditing:)]) {
        
        [self.delegate tb_SearchBarShouldBeginEditing:searchBar];
        
    }
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    if ([self.delegate respondsToSelector:@selector(tb_SearchBarCancelButtonClicked:)]) {
        
        [self.delegate tb_SearchBarCancelButtonClicked:searchBar];
        
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ([self.delegate respondsToSelector:@selector(tb_SearchBar:textDidChange:)]) {
        
        [self.delegate tb_SearchBar:searchBar textDidChange:searchText];
        
    }
    
    
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    if ([self.delegate respondsToSelector:@selector(tb_SearchBarSearchButtonClicked:)]) {
        
        [self.delegate tb_SearchBarSearchButtonClicked:searchBar];
        
    }

    
    
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
