//
//  POIRequest.m
//  poi-gaode
//
//  Created by W_L on 2018/10/19.
//  Copyright © 2018 W_L. All rights reserved.
//

#import "POIRequest.h"

@interface POIRequest ()<AMapSearchDelegate>
@property (strong, nonatomic) AMapSearchAPI *search;


@end


@implementation POIRequest

- (instancetype)init{
    
    if (self = [super init]) {
        //搜索poi初始化
        self.search = [[AMapSearchAPI alloc] init];
        self.search.delegate = self;
        
    }
    return self;
}


- (void)searchRequestCLLocationCoordinate2D:(CLLocationCoordinate2D )cLLocationCoordinate2D
                                   keyWords:(NSString *)keyWords
                                   cityName:(NSString *)cityName
                                 complation:(ReturnData)complation
{
    
    AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
    
    request.location            = [AMapGeoPoint locationWithLatitude:cLLocationCoordinate2D.latitude longitude:cLLocationCoordinate2D.longitude];
    
    request.keywords            = keyWords;
    
    request.city = cityName;
    /* 按照距离排序. */
    request.sortrule            = 0;
    request.requireExtension    = YES;
    request.radius           = 2000;
    //    request.requireSubPOIs      = YES;
    [self.search AMapPOIAroundSearch:request];
    
     self.returnData = complation;
    
}
/* POI 搜索回调. */
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    if (response.pois.count == 0)
    {
        return;
    }
    
    //解析response获取POI信息，具体解析见 Demo
    
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:response.pois.count];
    
    [response.pois enumerateObjectsUsingBlock:^(AMapPOI *obj, NSUInteger idx, BOOL *stop) {
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:0];
        
        dic[@"poi_name"] = obj.name;
        dic[@"poi_address"] = [NSString stringWithFormat:@"%@%@%@%@",obj.province,obj.city,obj.district,obj.address];
        dic[@"poi_province"] = obj.province;
        dic[@"poi_city"] = obj.city;
        dic[@"poi_district"] = obj.district;

        dic[@"poi_latitude"] = [NSString stringWithFormat:@"%f",obj.location.latitude];//经纬度
        dic[@"poi_longitude"] = [NSString stringWithFormat:@"%f",obj.location.longitude];//经纬度

        [arr addObject:dic];
        
        //        [poiAnnotations addObject:[[POIAnnotation alloc] initWithPOI:obj]];
        NSLog(@"%@ ==== %@",obj.name,dic[@"poi_address"] );
    }];
    
    self.returnData(arr);
//    [self.listView updataDataSource:arr];
    
    
    
}
@end
