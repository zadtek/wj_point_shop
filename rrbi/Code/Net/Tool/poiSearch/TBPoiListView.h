//
//  TBPoiListView.h
//  poi-gaode
//
//  Created by W_L on 2018/10/19.
//  Copyright © 2018 W_L. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@protocol TBPoiListViewDelegate <NSObject>

- (void)poiListScrollViewDidScroll:(UIScrollView *)scrollView;

- (void)tb_ListViewDidselectPoiList:(NSDictionary *)dic;

@end


@interface TBPoiListView : UIView

@property (weak, nonatomic) id <TBPoiListViewDelegate> delegate;

- (void)updataDataSource:(NSArray *)dataSource;

@end

NS_ASSUME_NONNULL_END
