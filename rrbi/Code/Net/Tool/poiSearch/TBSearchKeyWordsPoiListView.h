//
//  TBSearchKeyWordsPoiListView.h
//  poi-gaode
//
//  Created by W_L on 2018/10/19.
//  Copyright © 2018 W_L. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TBSearchKeyWordsPoiListViewDelegate <NSObject>

- (void)tb_KeyWordsDidselectPoiList:(NSDictionary *)dic;

@end

@interface TBSearchKeyWordsPoiListView : UIView
- (void)updataDataSource:(NSArray *)dataSource;

@property (weak, nonatomic)id <TBSearchKeyWordsPoiListViewDelegate> delegate;


@end

NS_ASSUME_NONNULL_END
