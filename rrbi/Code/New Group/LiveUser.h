//
//  LiveUser.h
//  iphoneLive
//
//  Created by cat on 16/3/9.
//  Copyright © 2016年 cat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LiveUser : NSObject
@property (nonatomic, copy)NSString *site_name;
@property (nonatomic, copy)NSString *status;
@property (nonatomic, copy)NSString *open_time;
@property (nonatomic, copy)NSString *is_real;
@property (nonatomic, copy)NSString *hxid;
@property (nonatomic, copy)NSString *balance;
@property (nonatomic, copy)NSString *owner_name;
@property (nonatomic, copy)NSString *ru_id;
@property (nonatomic, copy)NSString *uid;

@property (nonatomic, copy)NSString *status_remark;
@property (nonatomic, copy)NSString *phone;
@property (nonatomic, copy)NSString *close_time;
@property (nonatomic, copy)NSString *sale_sub;
@property (nonatomic, copy)NSString *sale_full;
@property (nonatomic, copy)NSString *mobile;
@property (nonatomic, copy)NSString *freeship_amount;
@property (nonatomic, copy)NSString *send;
@property (nonatomic, copy)NSString *logo;
@property (nonatomic, copy)NSString *New_user_sale;
@property (nonatomic, copy)NSString *address;
@property (nonatomic, copy)NSString *shop_sale;

//vip_type

-(void)setValue:(id)value forUndefinedKey:(NSString *)key;
-(instancetype)initWithDic:(NSDictionary *) dic;
+(instancetype)modelWithDic:(NSDictionary *) dic;

@end
