

#import <Foundation/Foundation.h>
#import "LiveUser.h"

@interface ShopConfig : NSObject
+ (void)saveProfile:(LiveUser *)user;
+ (void)updateProfile:(LiveUser *)user;
+ (void)clearProfile;
+ (LiveUser *)myProfile;
+(NSString *)getSite_name;
+(NSString *)getStatus;
+(NSString *)getOpen_time;
+(NSString *)getIs_real;
+(NSString *)getHxid;
+(NSString *)getBalance;
+(NSString *)getOwner_name;
+(NSString *)getRu_id;
+(NSString *)getUid;
+(NSString *)getStatus_remark;
+(NSString *)getPhone;
+(NSString *)getClose_time;
+(NSString *)getSale_sub;
+(NSString *)getSale_full;
+(NSString *)getMobile;
+(NSString *)getFreeship_amount;
+(NSString *)getSend;
+(NSString *)getLogo;
+(NSString *)getNew_user_sale;
+(NSString *)getAddress;
+(NSString *)getShop_sale;



@end
