//
//  UserModel.m
//  BaseFrame
//
//  Created by 孟学浩 on 2017/4/17.
//  Copyright © 2017年 com.mxh. All rights reserved.
//

#import "UserModel.h"

static NSString * const KEY_RU_ID     = @"KEY_RU_ID";
static NSString * const KEY_USER_ID     = @"B2B_USER_ID";
static NSString * const KEY_USER_MOBILE = @"B2B_USER_MOBILE";
static NSString * const KEY_USER_PASSWORD = @"B2B_USER_PASSWORD";
static NSString * const KEY_USER_HEADIMG = @"B2B_USER_HEADIMG";
static NSString * const KEY_SEARCH_LIST = @"B2B_SEARCH_LIST";
static NSString * const KEY_BUY_CAR_LIST = @"B2B_BUY_CAR_LIST";
static NSString * const KEY_JPUSH_REGISTERATIONID = @"KEY_JPUSH_REGISTERATIONID";
static NSString * const KEY_TOKEN = @"KEY_TOKEN";
static NSString * const KEY_LOCATION_ADDRESS = @"KEY_LOCATION_ADDRESS";

@implementation UserModel

+ (UserModel*)sharedInstanced{
    static UserModel *_userLogin = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _userLogin = [[UserModel alloc] init];
    });
    return _userLogin;
}

#pragma mark - handMove
- (NSString *)handMove
{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"handMove":@""}];
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"handMove"];
}

- (void)setHandMove:(NSString *)handMove
{
    if (!handMove) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"handMove"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    //获取本地数据
    NSString *currentUserID = [[NSUserDefaults standardUserDefaults] objectForKey:@"handMove"];
    //判断是否需要更新
    if ([currentUserID isEqualToString:handMove]) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setObject:handMove forKey:@"handMove"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

#pragma mark - sID

- (NSString *)s_id{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"s_id":@""}];
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"s_id"];
}

- (void)setS_id:(NSString *)s_id{
    if (!s_id) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"s_id"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    //获取本地数据
    NSString *currentUserID = [[NSUserDefaults standardUserDefaults] objectForKey:@"s_id"];
    //判断是否需要更新
    if ([currentUserID isEqualToString:s_id]) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setObject:s_id forKey:@"s_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

#pragma mark - address

- (NSString *)address{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"address":@""}];
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"address"];
}

/**
 收获地址
 
 @param address address description
 */
- (void)setAddress:(NSString *)address{
    if (!address) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"address"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    //获取本地数据
    NSString *currentUserID = [[NSUserDefaults standardUserDefaults] objectForKey:@"address"];
    //判断是否需要更新
    if ([currentUserID isEqualToString:address]) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setObject:address forKey:@"address"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - longitude

- (NSString *)longitude{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"longitude":@""}];
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
}

/**
 经度
 
 @param longitude 经度
 */
- (void)setlongitude:(NSString *)longitude{
    if (!longitude) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"longitude"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    //获取本地数据
    NSString *currentUserID = [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
    //判断是否需要更新
    if ([currentUserID isEqualToString:longitude]) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setObject:longitude forKey:@"longitude"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - latitude

- (NSString *)latitude{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"latitude":@""}];
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
}
/**
 纬度
 
 @param latitude 纬度
 */
- (void)setlatitude:(NSString *)latitude{
    if (!latitude) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"latitude"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    //获取本地数据
    NSString *currentUserID = [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
    //判断是否需要更新
    if ([currentUserID isEqualToString:latitude]) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setObject:latitude forKey:@"latitude"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - locality

- (NSString *)locality{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"locality":@""}];
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"locality"];
}

/**
 城市
 
 @param locality 城市
 */
- (void)setlocality:(NSString *)locality{
    if (!locality) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"locality"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    //获取本地数据
    NSString *currentUserID = [[NSUserDefaults standardUserDefaults] objectForKey:@"locality"];
    //判断是否需要更新
    if ([currentUserID isEqualToString:locality]) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setObject:locality forKey:@"locality"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - placeName

- (NSString *)placeName{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"placeName":@""}];
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"placeName"];
}

/**
 位置
 
 @param placeName 位置
 */
- (void)setPlaceName:(NSString *)placeName{
    if (!placeName) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"placeName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    //获取本地数据
    NSString *currentUserID = [[NSUserDefaults standardUserDefaults] objectForKey:@"placeName"];
    //判断是否需要更新
    if ([currentUserID isEqualToString:placeName]) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setObject:placeName forKey:@"placeName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
/**
 token 一般做判断有效期用
 
 @param
 */
- (void)setToken:(NSString *)token
{
    if (!token) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:KEY_TOKEN];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    NSString *tokenId = [[NSUserDefaults standardUserDefaults] valueForKey:KEY_TOKEN];
    if (tokenId == token) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setValue:token forKey:KEY_TOKEN];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)token
{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{KEY_TOKEN:@""}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:KEY_TOKEN];
}
/**
 极光推送 别名
 
 @param RegistrationID
 */
- (void)setJpushRegistrationID:(NSString *)jpushRegistrationID
{
    if (!jpushRegistrationID) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:KEY_JPUSH_REGISTERATIONID];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    NSString *preUserId = [[NSUserDefaults standardUserDefaults] valueForKey:KEY_JPUSH_REGISTERATIONID];
    if (preUserId == jpushRegistrationID) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setValue:jpushRegistrationID forKey:KEY_JPUSH_REGISTERATIONID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)jpushRegistrationID
{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{KEY_JPUSH_REGISTERATIONID:@""}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:KEY_JPUSH_REGISTERATIONID];
}


/**
 用户id
 
 @param userId 用户id
 */
- (void)setUserId:(NSString *)userId{
    if (!userId) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:KEY_USER_ID];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    NSString *preUserId = [[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ID];
    if (preUserId == userId) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setValue:userId forKey:KEY_USER_ID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userId{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{KEY_USER_ID:@""}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ID];
}


/**
 店铺id
 
 @param ru_Id 店铺id
 */
- (void)setRu_Id:(NSString *)ru_Id
{
    if (!ru_Id) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:KEY_RU_ID];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    NSString *preUserId = [[NSUserDefaults standardUserDefaults] valueForKey:KEY_RU_ID];
    if (preUserId == ru_Id) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setValue:ru_Id forKey:KEY_RU_ID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)ru_Id
{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{KEY_RU_ID:@""}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:KEY_RU_ID];
}
/**
 头像

 @param headImg 头像
 */
- (void)setHeadImg:(NSString *)headImg{
    if (!headImg) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:KEY_USER_HEADIMG];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    NSString *preUserId = [[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_HEADIMG];
    if (preUserId == headImg) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setValue:headImg forKey:KEY_USER_HEADIMG];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)headImg{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{KEY_USER_HEADIMG:@""}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_HEADIMG];
}

/**
 电话
 
 @param mobile 电话
 */
- (void)setMobile:(NSString *)mobile{
    if (!mobile) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:KEY_USER_MOBILE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    NSString *preMobile= [[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_MOBILE];
    if (preMobile == mobile) {
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:mobile forKey:KEY_USER_MOBILE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 用户名
 
 */
- (NSString *)nameNick{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"nameNick":@"未知昵称"}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"nameNick"];
}

/**
 用户名
 
 @param nameNick nameNick description
 */
- (void)setNameNick:(NSString *)nameNick{
    if (!nameNick) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"nameNick"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    NSString *preMobile= [[NSUserDefaults standardUserDefaults] valueForKey:@"nameNick"];
    if (preMobile == nameNick) {
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:nameNick forKey:@"nameNick"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


/**
 广告路径
 */
- (NSString *)advertImg
{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"advertImg":@""}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"advertImg"];
}

/**
 广告路径

 */
- (void)setAdvertImg:(NSString *)advertImg
{
    if (!advertImg) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"advertImg"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    NSString *advertImgStr= [[NSUserDefaults standardUserDefaults] valueForKey:@"advertImg"];
    if (advertImgStr == advertImg) {
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:advertImg forKey:@"advertImg"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 广告时长
 */
- (NSInteger)advertTimeInt
{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"advertTimeInt":@""}];
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"advertTimeInt"];
}

/**
 广告时长
 
 */
- (void)setAdvertTimeInt:(NSInteger)advertTimeInt
{
    if (!advertTimeInt) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"advertTimeInt"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    NSInteger advertTimeInta= [[NSUserDefaults standardUserDefaults] integerForKey:@"advertTimeInt"];
    if (advertTimeInta == advertTimeInt) {
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:advertTimeInt forKey:@"advertTimeInt"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)mobile{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{KEY_USER_MOBILE:@""}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_MOBILE];
}

/**
 密码
 
 @param mobile 密码
 */
- (void)setPassword:(NSString *)password{
    if (!password) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:KEY_USER_PASSWORD];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    NSString *preMobile= [[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_PASSWORD];
    if (preMobile == password) {
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:password forKey:KEY_USER_PASSWORD];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)password{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{KEY_USER_PASSWORD:@""}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_PASSWORD];
}

/**
 搜索店铺
 
 @return 导航历史搜索数组
 */
- (NSArray *)searchShopArray{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"searchShopArray":@[]}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"searchShopArray"];
}

+ (void)addSearchShopHistory:(NSString *)keyword{
    NSMutableArray *array = [UserModel sharedInstanced].searchShopArray.mutableCopy;
    if ([[UserModel sharedInstanced].searchShopArray containsObject:keyword]) {
        [array removeObject:keyword];
    }
    [array insertObject:keyword atIndex:0];
    [[NSUserDefaults standardUserDefaults] setValue:[NSArray arrayWithArray:array] forKey:@"searchShopArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)removeShopHistory:(NSString *)keyword{
    NSMutableArray *array = [UserModel sharedInstanced].searchShopArray.mutableCopy;
    if ([[UserModel sharedInstanced].searchShopArray containsObject:keyword]) {
        [array removeObject:keyword];
    }
    [[NSUserDefaults standardUserDefaults] setValue:[NSArray arrayWithArray:array] forKey:@"searchShopArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


/**
 搜索商品
 
 @return 导航历史搜索数组
 */
- (NSArray *)searchGoodsArray{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"searchGoodsArray":@[]}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"searchGoodsArray"];
}

+ (void)addSearchGoodsHistory:(NSString *)keyword{
    NSMutableArray *array = [UserModel sharedInstanced].searchGoodsArray.mutableCopy;
    if ([[UserModel sharedInstanced].searchGoodsArray containsObject:keyword]) {
        [array removeObject:keyword];
    }
    [array insertObject:keyword atIndex:0];
    [[NSUserDefaults standardUserDefaults] setValue:[NSArray arrayWithArray:array] forKey:@"searchGoodsArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)removeGoodsHistory:(NSString *)keyword{
    NSMutableArray *array = [UserModel sharedInstanced].searchGoodsArray.mutableCopy;
    if ([[UserModel sharedInstanced].searchGoodsArray containsObject:keyword]) {
        [array removeObject:keyword];
    }
    [[NSUserDefaults standardUserDefaults] setValue:[NSArray arrayWithArray:array] forKey:@"searchGoodsArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 购物车
 
 @return 购物车数组
 */
- (NSArray *)buyCarArray{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{KEY_BUY_CAR_LIST:@[]}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:KEY_BUY_CAR_LIST];
}

+ (void)addToBuyCar:(NSDictionary *)keyword{
    NSMutableArray *array = [UserModel sharedInstanced].buyCarArray.mutableCopy;
    [array insertObject:keyword atIndex:0];
    [[NSUserDefaults standardUserDefaults] setValue:array forKey:KEY_BUY_CAR_LIST];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setBuyCarArray:(NSArray *)buyCarArray{
    if (!buyCarArray) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:KEY_BUY_CAR_LIST];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    
    NSArray *preBuyCarArray = [[NSUserDefaults standardUserDefaults] valueForKey:KEY_BUY_CAR_LIST];
    if (preBuyCarArray == buyCarArray) {
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:buyCarArray forKey:KEY_BUY_CAR_LIST];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
/**
 用户等级
 
 */

- (NSString *)userType{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"userType":@"0"}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"userType"];
}

/**
 用户等级
 
 @param userType userType description
 */
- (void)setUserType:(NSString *)userType{
    if (!userType) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"userType"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    
    NSString *preBuyCarArray = [[NSUserDefaults standardUserDefaults] valueForKey:@"userType"];
    if ([preBuyCarArray isEqualToString: userType]) {
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:userType forKey:@"userType"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

/**
 定位详细地址
 
 */
- (void)setLocationAddress:(NSString *)locationAddress{
    if (!locationAddress) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:KEY_LOCATION_ADDRESS];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    NSString *preLocationAddress = [[NSUserDefaults standardUserDefaults] valueForKey:KEY_LOCATION_ADDRESS];
    if (preLocationAddress == locationAddress) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setValue:locationAddress forKey:KEY_LOCATION_ADDRESS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)locationAddress{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{KEY_LOCATION_ADDRESS:@""}];
    return [[NSUserDefaults standardUserDefaults] valueForKey:KEY_LOCATION_ADDRESS];
}
@end
