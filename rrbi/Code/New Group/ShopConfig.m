//
//  Config.m
//  iphoneLive
//
//  Created by cat on 16/3/9.
//  Copyright © 2016年 cat. All rights reserved.
//
#import "ShopConfig.h"
NSString * const KKSite_name = @"site_name";
NSString * const KStatus = @"status";
NSString * const KOpen_time = @"open_time";
NSString * const KIs_real = @"is_real";
NSString * const KHxid = @"hxid";
NSString * const KBalance = @"balance";
NSString * const KOwner_name = @"owner_name";
NSString * const KRu_id = @"ru_id";
NSString * const KUid = @"uid";
NSString * const KStatus_remark = @"status_remark";
NSString * const KPhone = @"phone";
NSString * const KClose_time = @"close_time";
NSString * const KSale_sub = @"sale_sub";
NSString * const KSale_full = @"sale_full";
NSString * const KMobile = @"mobile";
NSString * const KFreeship_amount = @"freeship_amount";
NSString * const KSend = @"send";
NSString * const KLogo = @"logo";
NSString * const KNew_user_sale = @"New_user_sale";
NSString * const KAddress = @"address";
NSString * const KShop_sale = @"shop_sale";


@implementation ShopConfig
#pragma mark - user profile


+ (void)saveProfile:(LiveUser *)user
{
    
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    
    [userDefaults setObject:user.site_name forKey:KKSite_name];
    [userDefaults setObject:user.status forKey:KStatus];
    [userDefaults setObject:user.open_time forKey:KOpen_time];
    [userDefaults setObject:user.is_real forKey:KIs_real];
    [userDefaults setObject:user.hxid forKey:KHxid];
    [userDefaults setObject:user.balance forKey:KBalance];
    [userDefaults setObject:user.owner_name forKey:KOwner_name];
    [userDefaults setObject:user.ru_id forKey:KRu_id];
    [userDefaults setObject:user.uid forKey:KUid];
    [userDefaults setObject:user.status_remark forKey:KStatus_remark];
    [userDefaults setObject:user.phone forKey:KPhone];
    [userDefaults setObject:user.close_time forKey:KClose_time];
    [userDefaults setObject:user.sale_sub forKey:KSale_sub];
    [userDefaults setObject:user.sale_full forKey:KSale_full];
    [userDefaults setObject:user.mobile forKey:KMobile];
    [userDefaults setObject:user.freeship_amount forKey:KFreeship_amount];
    [userDefaults setObject:user.send forKey:KSend];
    [userDefaults setObject:user.logo forKey:KLogo];
    [userDefaults setObject:user.New_user_sale forKey:KNew_user_sale];
     [userDefaults setObject:user.address forKey:KAddress];
    [userDefaults setObject:user.shop_sale forKey:KShop_sale];


    [userDefaults synchronize];
    
    
}
+ (void)updateProfile:(LiveUser *)user
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    if(user.site_name != nil) [userDefaults setObject:user.site_name forKey:KKSite_name];
    if(user.status != nil) [userDefaults setObject:user.status forKey:KStatus];
    if(user.open_time!=nil) [userDefaults setObject:user.open_time forKey:KOpen_time];
    if(user.is_real!=nil) [userDefaults setObject:user.is_real forKey:KIs_real];
    if(user.hxid!=nil) [userDefaults setObject:user.hxid forKey:KHxid];
    if(user.balance!=nil) [userDefaults setObject:user.balance forKey:KBalance];
    if(user.owner_name!=nil) [userDefaults setObject:user.owner_name forKey:KOwner_name];
    if(user.ru_id!=nil) [userDefaults setObject:user.ru_id forKey:KRu_id];
    if(user.uid!=nil) [userDefaults setObject:user.uid forKey:KUid];
    if(user.status_remark!=nil) [userDefaults setObject:user.status_remark forKey:KStatus_remark];
    if(user.phone!=nil) [userDefaults setObject:user.phone forKey:KPhone];
    if(user.close_time!=nil) [userDefaults setObject:user.close_time forKey:KClose_time];
    if (user.sale_sub!=nil)[userDefaults setObject:user.sale_sub forKey:KSale_sub];
    if (user.sale_full!=nil)[userDefaults setObject:user.sale_full forKey:KSale_full];
    if (user.mobile!=nil)[userDefaults setObject:user.mobile forKey:KMobile];
    if (user.freeship_amount!=nil)[userDefaults setObject:user.freeship_amount forKey:KFreeship_amount];
    if (user.send!=nil)[userDefaults setObject:user.send forKey:KSend];
    if (user.logo!=nil)[userDefaults setObject:user.logo forKey:KLogo];
    if (user.New_user_sale!=nil)[userDefaults setObject:user.New_user_sale forKey:KNew_user_sale];
    if (user.address!=nil)[userDefaults setObject:user.address forKey:KAddress];
    if (user.shop_sale!=nil)[userDefaults setObject:user.shop_sale forKey:KShop_sale];
    
    [userDefaults synchronize];
}

+ (void)clearProfile
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
     [userDefaults setObject:nil forKey:KKSite_name];
    [userDefaults setObject:nil forKey:KStatus];
    [userDefaults setObject:nil forKey:KOpen_time];
    [userDefaults setObject:nil forKey:KIs_real];
    [userDefaults setObject:nil forKey:KHxid];
    [userDefaults setObject:nil forKey:KBalance];
    [userDefaults setObject:nil forKey:KOwner_name];
    [userDefaults setObject:nil forKey:KRu_id];
    [userDefaults setObject:nil forKey:KUid];
    [userDefaults setObject:nil forKey:KStatus_remark];
    [userDefaults setObject:nil forKey:KPhone];
    [userDefaults setObject:nil forKey:KClose_time];
    [userDefaults setObject:nil forKey:KSale_sub];
    [userDefaults setObject:nil forKey:KSale_full];
    [userDefaults setObject:nil forKey:KMobile];
    [userDefaults setObject:nil forKey:KFreeship_amount];
    [userDefaults setObject:nil forKey:KSend];
    [userDefaults setObject:nil forKey:KLogo];
    [userDefaults setObject:nil forKey:KNew_user_sale];
    [userDefaults setObject:nil forKey:KAddress];
    [userDefaults setObject:nil forKey:KShop_sale];
    
    [userDefaults synchronize];
}

+ (LiveUser *)myProfile
{
    
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    
    LiveUser *user = [[LiveUser alloc] init];
    user.site_name = [userDefaults objectForKey:KKSite_name];
    user.status = [userDefaults objectForKey:KStatus ];
    user.open_time = [userDefaults objectForKey:KOpen_time ];
    user.is_real = [userDefaults objectForKey:KIs_real ];
    user.hxid = [userDefaults objectForKey:KHxid];
    user.balance = [userDefaults objectForKey:KBalance ];
    user.owner_name = [userDefaults objectForKey:KOwner_name ];
    user.ru_id = [userDefaults objectForKey:KRu_id ];
    user.uid = [userDefaults objectForKey:KUid];
    user.status_remark = [userDefaults objectForKey:KStatus_remark];
    user.phone = [userDefaults objectForKey:KPhone];
    user.close_time = [userDefaults objectForKey:KClose_time];
    user.sale_sub = [userDefaults objectForKey:KSale_sub];
    user.sale_full = [userDefaults objectForKey:KSale_full];
    user.mobile = [userDefaults objectForKey:KMobile];
    user.freeship_amount = [userDefaults objectForKey:KFreeship_amount];
    user.send = [userDefaults objectForKey:KSend];
    user.logo = [userDefaults objectForKey:KLogo];
    user.New_user_sale = [userDefaults objectForKey:KNew_user_sale];
    user.address = [userDefaults objectForKey:KAddress];
    user.shop_sale = [userDefaults objectForKey:KShop_sale];
    
    return user;
}
+ (NSString *)getSite_name {
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString* enterID = [userDefaults objectForKey: KKSite_name];
    return enterID;
}
+(NSString *)getStatus
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString* user_type_sub = [userDefaults objectForKey: KStatus];
    return user_type_sub;
}
+(NSString *)getOpen_time
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString* user_type = [userDefaults objectForKey: KOpen_time];
    return user_type;
}
+(NSString *)getIs_real
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString* ID = [userDefaults objectForKey: KIs_real];
    return ID;
}

+(NSString *)getHxid
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString* nicename = [userDefaults objectForKey: KHxid];
    return nicename;
}

+(NSString *)getBalance
{
    
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString *token = [userDefaults objectForKey:KBalance];
    return token;
}

+(NSString *)getOwner_name
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString *signature = [userDefaults objectForKey:KOwner_name];
    return signature;
}
+(NSString *)getRu_id
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString *avatar = [NSString stringWithFormat:@"%@",[userDefaults objectForKey:KRu_id]];
    return avatar;
}
+(NSString *)getUid
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString *signature = [userDefaults objectForKey:KUid];
    return signature;
}
+(NSString *)getStatus_remark
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString *level = [userDefaults objectForKey:KStatus_remark];
    return level;
}
+(NSString *)getPhone
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString *city = [userDefaults objectForKey:KPhone];
    return city;
}
+(NSString *)getClose_time
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString *lcity = [userDefaults objectForKey:KClose_time];
    return lcity;
}
+(NSString *)getSale_sub
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString *birth = [userDefaults objectForKey:KSale_sub];
    return birth;
}
+(NSString *)getSale_full
{
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init ];
    NSString *sex = [userDefaults objectForKey:KSale_full];
    return sex;
}
+(NSString *)getMobile
{
    NSUserDefaults *userDefults = [NSUserDefaults standardUserDefaults];
    NSString *level_anchors = [userDefults objectForKey:KMobile];
    return level_anchors;
}
+(NSString *)getFreeship_amount{
    NSUserDefaults *userDefults = [NSUserDefaults standardUserDefaults];
    NSString *ages = [userDefults objectForKey:KFreeship_amount];
    return ages;
}
+(NSString *)getSend{
    NSUserDefaults *userDefults = [NSUserDefaults standardUserDefaults];
    NSString *mobiles = [userDefults objectForKey:KSend];
    return mobiles;
}
+(NSString *)getLogo{
    NSUserDefaults *userDefults = [NSUserDefaults standardUserDefaults];
    NSString *qqnums = [userDefults objectForKey:KLogo];
    return qqnums;
}
+(NSString *)getNew_user_sale{
    NSUserDefaults *userDefults = [NSUserDefaults standardUserDefaults];
    NSString *jiguan = [userDefults objectForKey:KNew_user_sale];
    return jiguan;
}
+(NSString *)getAddress{
    NSUserDefaults *userDefults = [NSUserDefaults standardUserDefaults];
    NSString *adress = [userDefults objectForKey:KAddress];
    return adress;
}
+ (NSString *)getShop_sale {
    NSUserDefaults *userDefults = [NSUserDefaults standardUserDefaults];
    NSString *Sstatus = minstr([userDefults objectForKey:KShop_sale]);
    return Sstatus;
}


@end
