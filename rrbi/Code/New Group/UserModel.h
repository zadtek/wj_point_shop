//
//  UserModel.h
//  BaseFrame
//
//  Created by 孟学浩 on 2017/4/17.
//  Copyright © 2017年 com.mxh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject
+ (UserModel*)sharedInstanced;
@property (nonatomic,strong) NSString *mobile;         // 手机号
@property (nonatomic,strong) NSString *userId;         // 管理员ID
@property (nonatomic,strong) NSString *ru_Id;         // 店铺id
@property (nonatomic,strong) NSString *jpushRegistrationID; // 极光推送 别名
@property (nonatomic,strong) NSString *token;
@property (nonatomic,strong) NSString *password;       // 密码
@property (nonatomic,strong) NSString *headImg;        // 头像
@property (nonatomic,strong) NSString *advertImg;        // 广告路径
@property (nonatomic,assign) NSInteger advertTimeInt;        // 广告时间
@property (nonatomic,strong) NSArray *buyCarArray;     // 购物车
Strong NSArray *searchShopArray;   // 搜索店铺数组
Strong NSArray *searchGoodsArray;// 搜索商品数组
Strong NSString *nameNick;
Strong NSString *userType; // 用户等级
Strong NSString *address;  // 收获地址
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *locality;
@property (nonatomic, strong) NSString *placeName;
@property (nonatomic, strong) NSString *s_id;
@property (nonatomic, strong) NSString *handMove; // 手势
@property (nonatomic,strong) NSString *locationAddress; // 定位地址

- (void)setHandMove:(NSString *)handMove;

- (void)setS_id:(NSString *)s_id;


/**
 收获地址

 @param address address description
 */
- (void)setAddress:(NSString *)address;

/**
 经度
 
 @param longitude 经度
 */
- (void)setlongitude:(NSString *)longitude;
/**
 纬度
 
 @param latitude 纬度
 */
- (void)setlatitude:(NSString *)latitude;

/**
 城市
 
 @param locality 城市
 */
- (void)setlocality:(NSString *)locality;


/**
 位置
 
 @param placeName 位置
 */
- (void)setPlaceName:(NSString *)placeName;

/**
 用户名

 @param nameNick nameNick description
 */
- (void)setNameNick:(NSString *)nameNick;

/**
 1为用户2为商家

 @param userType userType description
 */
- (void)setUserType:(NSString *)userType;

/**
 添加店铺

 @param keyword keyword description
 */
+ (void)addSearchShopHistory:(NSString *)keyword;

/**
 删除店铺

 @param keyword keyword description
 */
+ (void)removeShopHistory:(NSString *)keyword;

/**
 添加商品

 @param keyword keyword description
 */
+ (void)addSearchGoodsHistory:(NSString *)keyword;

/**
 删除商品

 @param keyword keyword description
 */
+ (void)removeGoodsHistory:(NSString *)keyword;

/**
 添加到购物车

 @param keyword 购物车
 */
+ (void)addToBuyCar:(NSDictionary *)keyword;

@end
