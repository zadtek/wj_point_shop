//
//  ProcessedOrderChild.h
//  rrbi
//
//  Created by mac book on 2019/3/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProcessedOrderChild : BaseViewController

@property (nonatomic, copy) NSString *pass_id;

-(void)refreshDataSource;

@end

NS_ASSUME_NONNULL_END
