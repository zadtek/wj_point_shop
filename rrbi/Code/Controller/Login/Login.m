                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            //
//  Login.m
//  RRQS
//
//  Created by kyjun on 16/3/29.
//
//

#import "Login.h"
#import "JPUSHService.h"

@interface Login ()

@property(nonatomic,strong) UIView* headerView;
@property(nonatomic,strong) UITextField* txtUserName;
@property(nonatomic,strong) UITextField* txtPassword;


@property(nonatomic,strong) UIView* footerView;
@property(nonatomic,strong) UIButton* btnConfirm;
@property(nonatomic,strong) UILabel* promptTextLabel;


@end

@implementation Login

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"登录";
    [self layoutUI];
    [self layoutConstratints];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark =====================================================  User interface layout
-(void)layoutUI{
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 200.f);
    [self.headerView addSubview:self.txtUserName];
    [self.headerView addSubview:self.txtPassword];
    self.tableView.tableHeaderView = self.headerView;
    self.footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 200.f);
    [self.footerView addSubview:self.btnConfirm];
    [self.footerView addSubview:self.promptTextLabel];
    self.tableView.tableFooterView = self.footerView;
}
-(void)layoutConstratints{
    self.txtUserName.translatesAutoresizingMaskIntoConstraints = NO;
    self.txtPassword.translatesAutoresizingMaskIntoConstraints = NO;
    self.btnConfirm.translatesAutoresizingMaskIntoConstraints = NO;
    self.promptTextLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    CGFloat width = SCREEN_WIDTH - 30.f;
    CGFloat height = 45.f;
    
    [self.txtUserName addConstraint:[NSLayoutConstraint constraintWithItem:self.txtUserName attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:width]];
    [self.txtUserName addConstraint:[NSLayoutConstraint constraintWithItem:self.txtUserName attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:height]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.txtUserName attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeTop multiplier:1.0 constant:90]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.txtUserName attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.f]];
    
    [self.txtPassword addConstraint:[NSLayoutConstraint constraintWithItem:self.txtPassword attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:width]];
    [self.txtPassword addConstraint:[NSLayoutConstraint constraintWithItem:self.txtPassword attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:height]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.txtPassword attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.txtUserName attribute:NSLayoutAttributeBottom multiplier:1.0 constant:20.f]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.txtPassword attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.f]];
    
    [self.btnConfirm addConstraint:[NSLayoutConstraint constraintWithItem:self.btnConfirm attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:width]];
    [self.btnConfirm addConstraint:[NSLayoutConstraint constraintWithItem:self.btnConfirm attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:height]];
    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnConfirm attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.footerView attribute:NSLayoutAttributeTop multiplier:1.0 constant:20.f]];
    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnConfirm attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.footerView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.f]];
    
    [self.promptTextLabel addConstraint:[NSLayoutConstraint constraintWithItem:self.promptTextLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:width]];
    [self.promptTextLabel addConstraint:[NSLayoutConstraint constraintWithItem:self.promptTextLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:height]];
    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.promptTextLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.btnConfirm attribute:NSLayoutAttributeBottom multiplier:1.0 constant:20.f]];
    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.promptTextLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.footerView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.f]];
    
}

#pragma mark =====================================================  SEL
-(IBAction)btnConfirmTouch:(UIButton*)sender{
    [self.view endEditing:YES];
    if([self checkForm]){
        self.HUD = [[MBProgressHUD alloc] initWithView:self.view.window];
        [self.view.window addSubview:self.HUD];
        self.HUD.delegate = self;
        self.HUD.minSize = CGSizeMake(135.f, 135.f);
        self.HUD.label.font = [UIFont systemFontOfSize:14.f];
        self.HUD.label.text = @"正在登录!";
        [self.HUD showAnimated:YES];
        
         AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
        NSDictionary* arg = @{@"ince":@"login",@"username":self.txtUserName.text,@"password":self.txtPassword.text};
        [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            [WMHelper outPutJsonString:responseObject];
            NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
            
            if(flag ==200){
                NSDictionary* emptyDict = [responseObject objectForKey:@"data"];
                [UserModel sharedInstanced].userId = emptyDict[@"uid"];
                [UserModel sharedInstanced].ru_Id = emptyDict[@"ru_id"];
                LiveUser *userInfo = [[LiveUser alloc] initWithDic:emptyDict];
                [ShopConfig saveProfile:userInfo];
                MMember* entity = [[MMember alloc]initWithItem:emptyDict];
                entity.isLogin = YES;
                Boolean flag= [NSKeyedArchiver archiveRootObject:entity toFile:[WMHelper archiverPath]];
                if(flag){
                    [self registerTagsWithAlias:entity.userID];
                }else
                    NSLog(@"%@",@"归档失败!");
//                [self hidHUD:@"登录成功" success:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:NotifciationReLoadUserInfo object:nil];
                [self hidHUD: @"登录成功" success:YES complete:^{
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
                
            }
            else {
                [self hidHUD:[responseObject objectForKey:@"msg"]];
            }
            
        } failure:^(NSURLSessionDataTask *task, NSError *error)  {
            [self hidHUD:@"网络异常" success:NO];
        }];
    }
}

#pragma mark =====================================================  private method
-(BOOL)checkForm{
    if([WMHelper isEmptyOrNULLOrnil:self.txtUserName.text]){
        [self alertHUD:@"用户名不能为空"];
        return NO;
    }else if([WMHelper isEmptyOrNULLOrnil:self.txtPassword.text]){
        [self alertHUD:@"密码不能为空"];
        return NO;
    }
    return YES;

}

-(void)registerTagsWithAlias:(NSString*)userID{
 
//     __autoreleasing NSMutableSet *tags = [NSMutableSet set];
//    __autoreleasing NSString *alias = userID;
//    [self analyseInput:&alias tags:&tags];
    
    [JPUSHService setAlias:userID completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
        NSLog(@"消息推送%tu,%@,%tu",iResCode,iAlias,seq);
        
        if(iResCode == 0){
            
        }else{
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:nil message:@"消息推送注册失败,请退出后再登录!" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alert show];
        }
    } seq:0];
  
    
    
//    [JPUSHService setTags:tags alias:alias fetchCompletionHandle:^(int iResCode, NSSet *iTags, NSString *iAlias) {
//               NSLog(@"TagsAlias回调:\n iResCode:%d  iAlias:%@",iResCode,iAlias);
//        if(iResCode == 0){
//
//        }else{
//            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:nil message:@"消息推送注册失败,请退出后再登录!" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//            [alert show];
//        }
//    }];
}

- (void)setTags:(NSMutableSet **)tags addTag:(NSString *)tag {
    [*tags addObject:tag];
}

- (void)analyseInput:(NSString **)alias tags:(NSSet **)tags {
    // alias analyse
    if (![*alias length]) {
        // ignore alias
        *alias = nil;
    }
    // tags analyse
    if (![*tags count]) {
        *tags = nil;
    } else {
        __block int emptyStringCount = 0;
        [*tags enumerateObjectsUsingBlock:^(NSString *tag, BOOL *stop) {
            if ([tag isEqualToString:@""]) {
                emptyStringCount++;
            } else {
                emptyStringCount = 0;
                *stop = YES;
            }
        }];
        if (emptyStringCount == [*tags count]) {
            *tags = nil;
        }
    }
}

#pragma mark =====================================================  propert package
-(UIView *)headerView{
    if(!_headerView){
        _headerView = [[UIView alloc]init];
    }
    return _headerView;
}

-(UITextField *)txtUserName{
    if(!_txtUserName){
       UILabel* label = [[UILabel alloc]init];
        label.frame = CGRectMake(0, 0, 5, 45);
        _txtUserName = [[UITextField alloc]init];
        _txtUserName.placeholder = @"请输入您的用户名";
        _txtUserName.backgroundColor = [UIColor whiteColor];
        _txtUserName.borderStyle=UITextBorderStyleNone;
        _txtUserName.layer.borderColor =[UIColor colorWithRed:231/255.f green:231/255.f blue:231/255.f alpha:1.0].CGColor;
        _txtUserName.layer.borderWidth = 1.0f;
        _txtUserName.layer.masksToBounds = YES;
        _txtUserName.layer.cornerRadius = 5.f;
        _txtUserName.font = [UIFont systemFontOfSize:15.f];
        _txtUserName.leftView = label;
        _txtUserName.leftViewMode =UITextFieldViewModeAlways;
    }
    return _txtUserName;
}
-(UITextField *)txtPassword{
    if(!_txtPassword){
        UILabel* label = [[UILabel alloc]init];
        label.frame = CGRectMake(0, 0, 5, 45);
        _txtPassword = [[UITextField alloc]init];
        _txtPassword.placeholder = @"请输入您的登录密码";
        _txtPassword.backgroundColor = [UIColor whiteColor];
        _txtPassword.borderStyle=UITextBorderStyleNone;
        _txtPassword.layer.borderColor =[UIColor colorWithRed:231/255.f green:231/255.f blue:231/255.f alpha:1.0].CGColor;
        _txtPassword.layer.borderWidth = 1.0f;
        _txtPassword.layer.masksToBounds = YES;
        _txtPassword.layer.cornerRadius = 5.f;
        _txtPassword.font = [UIFont systemFontOfSize:15.f];
        _txtPassword.secureTextEntry = YES;
        _txtPassword.leftView = label;
        _txtPassword.leftViewMode =UITextFieldViewModeAlways;
    }
    return _txtPassword;
}

-(UIView *)footerView{
    if(!_footerView){
        _footerView = [[UIView alloc]init];
    }
    return _footerView;
}

-(UIButton *)btnConfirm{
    if(!_btnConfirm){
        _btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnConfirm.backgroundColor = [UIColor colorWithRed:248/255.f green:159/255.f blue:39/255.f alpha:1.0];
        [_btnConfirm setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnConfirm setTitle:@"登录" forState:UIControlStateNormal];
        [_btnConfirm addTarget:self action:@selector(btnConfirmTouch:) forControlEvents:UIControlEventTouchUpInside];
        _btnConfirm.layer.masksToBounds = YES;
        _btnConfirm.layer.cornerRadius = 5.f;
        _btnConfirm.layer.borderColor = [UIColor colorWithRed:231/255.f green:231/255.f blue:231/255.f alpha:1.0].CGColor;

    }
    return _btnConfirm;
}

- (UILabel *)promptTextLabel{
    if (!_promptTextLabel) {
        _promptTextLabel = [[UILabel alloc]init];
        _promptTextLabel.textColor = getColor(@"979797");
        _promptTextLabel.font = kFont(12);
        _promptTextLabel.text = @"提示：如需更改密码请联系您的业务负责人";
        _promptTextLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _promptTextLabel;
}

@end
