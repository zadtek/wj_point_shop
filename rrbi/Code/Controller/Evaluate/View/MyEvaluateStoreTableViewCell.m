//
//  MyEvaluateStoreTableViewCell.m
//  UsersShop
//
//  Created by mac book on 2018/10/31.
//  Copyright © 2018年 dwj. All rights reserved.
//

#import "MyEvaluateStoreTableViewCell.h"
#import "JWStarView.h"


#define TextView_h   120


@interface MyEvaluateStoreTableViewCell ()<UITextViewDelegate,UINavigationControllerDelegate>

@property (strong, nonatomic)  UIView *backView;

@property (nonatomic,strong)UIView *personView;


@property (nonatomic,strong)UIImageView *headerImageView;
@property (strong, nonatomic)  UILabel *nameLabel;
@property (strong, nonatomic)  UILabel *timeLabel;



@property (nonatomic,strong) JWStarView * merchant_starView;//商家评价











@end

@implementation MyEvaluateStoreTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];

        self.merchant_starNum  = 0;
        
        [self createSubviews];
    }
    
    return self;
}

#pragma mark - 创建子试图
-(void)createSubviews{
    
    self.backView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, SCREEN_WIDTH-30, 300)];
    self.backView.backgroundColor = [UIColor whiteColor];
    self.backView.layer.cornerRadius = 3;
    [self.contentView addSubview:self.backView];
  

    
    
    self.personView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.backView.frame.size.width, 80)];
    self.personView.backgroundColor = [UIColor whiteColor];
    [self.backView addSubview:self.personView];
    
    
    float image_wh = self.personView.frame.size.height - 20;
    self.headerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, image_wh, image_wh)];
    self.headerImageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.headerImageView.layer.cornerRadius = image_wh/2;
    self.headerImageView.layer.masksToBounds =YES;
    [self.personView addSubview:self.headerImageView];

    
    self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.headerImageView.frame)+10, CGRectGetMinY(self.headerImageView.frame),self.personView.frame.size.width -CGRectGetMaxX(self.headerImageView.frame) ,image_wh/2)];
    self.nameLabel.font = [UIFont systemFontOfSize:13];
    self.nameLabel.textColor = getColor(blackColor);
    [self.personView addSubview:self.nameLabel];
    
    
    self.timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame),CGRectGetWidth(self.nameLabel.frame) ,CGRectGetHeight(self.nameLabel.frame))];
    self.timeLabel.font = [UIFont systemFontOfSize:13];
    self.timeLabel.textColor = getColor(blackColor);
    [self.personView addSubview:self.timeLabel];
    
    
    
    
    
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.personView.frame), self.backView.frame.size.width-20, 1)];
    line.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.backView addSubview:line];
    


    //商家评价🌟
    float merchant_y = CGRectGetMaxY(line.frame)+20;
    float merchant_w = self.backView.frame.size.width *0.6;
    float merchant_x = (self.backView.frame.size.width - merchant_w)/2;
    float merchant_h = 35;
    self.merchant_starView = [[JWStarView alloc]initWithFrame:CGRectMake(merchant_x, merchant_y, merchant_w, merchant_h) finish:^(CGFloat currentScore) {
//        NSLog(@"current-----%f",currentScore);
        self.merchant_starNum = currentScore;
       
    }];

    self.merchant_starView.backgroundColor = [UIColor whiteColor];
    [self.backView addSubview:self.merchant_starView];
    

    

    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.merchant_starView.frame)+20, CGRectGetWidth(line.frame), TextView_h)]; //初始化大小并自动释放
    _textView = textView;
    textView.textColor = [UIColor blackColor];//设置字体颜色
    textView.font = [UIFont systemFontOfSize:13];//设置 字体 和 大小
    textView.delegate = self;// 设置控制器为 textView 的代理方法
    textView.backgroundColor = [UIColor groupTableViewBackgroundColor];//设置它的背景颜色
    textView.returnKeyType = UIReturnKeyDefault;//返回键的类型
    textView.keyboardType = UIKeyboardTypeDefault;//键盘类型
    textView.layer.cornerRadius = 3;
    textView.scrollEnabled = YES;//是否可以拖动
    [self.backView addSubview:textView];
    
    
    UILabel *placeHoldLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, textView.frame.size.width, 25)];
    self.placeHoldLabel = placeHoldLabel;
    placeHoldLabel.enabled = NO;
    placeHoldLabel.text = @"亲，对骑手满意吗？";
    placeHoldLabel.font = [UIFont systemFontOfSize:13];
    [self.textView addSubview:placeHoldLabel];
    
    
    
    
    


    
}

-(void)setDataDic:(NSDictionary *)dataDic{
    
    
    _dataDic = dataDic;
    
    self.nameLabel.text = dataDic[@"rider_name"];
    self.timeLabel.text = dataDic[@"order_desc"];
    
    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:dataDic[@"rider_img"]]];

}


#pragma mark - textView delegate
-(void)textViewDidChange:(UITextView *)textView
{
    if ([self.textView.text length] == 0) {
        [self.placeHoldLabel setHidden:NO];
        
    }else{
        [self.placeHoldLabel setHidden:YES];
    }
    NSInteger maxFontNum = 200;//最大输入限制
    NSInteger length = self.textView.text.length;
    NSString *toBeString = textView.text;
    // 获取键盘输入模式
    NSString *lang = [[UIApplication sharedApplication] textInputMode].primaryLanguage;
    if ([lang isEqualToString:@"zh-Hans"]) { // zh-Hans代表简体中文输入，包括简体拼音，健体五笔，简体手写
        UITextRange *selectedRange = [textView markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textView positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > maxFontNum) {
                textView.text = [toBeString substringToIndex:maxFontNum];//超出限制则截取最大限制的文本
                self.noticeLabel.text = [NSString stringWithFormat:@"%ld/200",maxFontNum];
            } else {
                self.noticeLabel.text = [NSString stringWithFormat:@"%ld/200",toBeString.length];
            }
        }
    } else {// 中文输入法以外的直接统计
        if (toBeString.length > maxFontNum) {
            textView.text = [toBeString substringToIndex:maxFontNum];
            self.noticeLabel.text = [NSString stringWithFormat:@"%ld/200",maxFontNum];
        } else {
            self.noticeLabel.text = [NSString stringWithFormat:@"%ld/200",toBeString.length];
        }
    }
    
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if([text isEqualToString:@"\n"]){
        
        [textView resignFirstResponder];
        
        return NO;
    }
    
    if (range.location>=200){
        
        return  NO;
    } else {
        
        return YES;
    }
}




//获取View所在的Viewcontroller方法
- (UIViewController *)viewController {
    
    for (UIView* next = [self superview]; next; next = next.superview) {
        
        UIResponder *nextResponder = [next nextResponder];
        
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            
            return (UIViewController *)nextResponder;
            
        }
        
    }
    
    return nil;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
