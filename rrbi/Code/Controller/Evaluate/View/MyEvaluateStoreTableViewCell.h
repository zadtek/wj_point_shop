//
//  MyEvaluateStoreTableViewCell.h
//  UsersShop
//
//  Created by mac book on 2018/10/31.
//  Copyright © 2018年 dwj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class MyEvaluateStoreTableViewCell;

@interface MyEvaluateStoreTableViewCell : UITableViewCell

@property (nonatomic, assign) NSInteger merchant_starNum;//商家评价星数

@property (nonatomic,strong)NSDictionary *dataDic;

/** textView */
@property (nonatomic, weak) UITextView *textView;

/** placeHoldLabel */
@property (nonatomic, weak) UILabel *placeHoldLabel;

/** noticeLabel */
@property (nonatomic, weak) UILabel *noticeLabel;

@end

NS_ASSUME_NONNULL_END
