//
//  KnightEvaluateViewController.m
//  rrbi
//
//  Created by mac book on 2019/5/16.
//

#import "KnightEvaluateViewController.h"
#import "MyEvaluateStoreTableViewCell.h"

#define FooterButton_Height    50/HEIGHT_6S_SCALE


@interface KnightEvaluateViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property(nonatomic,strong)UITableView *tableView;

@property (nonatomic,strong)UIButton *commitbtn;
@property (nonatomic, strong) UITextView *textView;

@property (nonatomic,strong)NSDictionary *dataDic;


@end

@implementation KnightEvaluateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"评价骑手";
    [self creatSubViews];
    
    [self getData];

    
}

-(void)getData{
    
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{@"ince":@"get_comment_info",
                          @"sid":[UserModel sharedInstanced].ru_Id,
                          @"order_sn":self.order_sn

                          };
    [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            NSDictionary* dict = [responseObject objectForKey:@"data"];
            
            self.dataDic = dict;
            
            [self.tableView reloadData];
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        [self hidHUD:@"网络异常"];
    }];
    
}


#pragma mark -  子试图
-(void)creatSubViews{
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.tableView];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    if (@available(iOS 11.0, *))
    {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tableView.estimatedRowHeight = 0;
    }
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.view);
    
    }];
    self.tableView.tableFooterView = [self creatFooterView];

    [self.tableView registerClass:[MyEvaluateStoreTableViewCell class] forCellReuseIdentifier:@"MyEvaluateStoreTableViewCell"];
    
    
}
#pragma mark -  UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return  1;

}

//tableView代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return  1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MyEvaluateStoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyEvaluateStoreTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.dataDic = self.dataDic;
    
    self.textView = cell.textView;
    
    
    return cell;
    
    
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 310;
    
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc] init];
    return headerView;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section

{
    return 0.01f;
}
#pragma mark 提交按钮
-(UIView *)creatFooterView{
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, FooterButton_Height)];
    
    self.commitbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.commitbtn addTarget:self action:@selector(commitBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    self.commitbtn.layer.masksToBounds = YES;
    self.commitbtn.layer.cornerRadius = 5;
    self.commitbtn.titleLabel.font= [UIFont systemFontOfSize:15];
    [footerView addSubview:self.commitbtn];
    [self.commitbtn setTitle:@"提交" forState:UIControlStateNormal];
    self.commitbtn.backgroundColor=  getColor(mainColor);
    [self.commitbtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.commitbtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(45);
        make.width.equalTo(footerView).multipliedBy(0.5);
        make.top.mas_equalTo(footerView).mas_offset(10);
        make.centerX.equalTo(footerView);

    }];
    
    
    return footerView;
}
-(void)commitBtnAction:(UIButton *)button{
    
    MyEvaluateStoreTableViewCell *storeCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    if (storeCell.merchant_starNum == 0) {        
        ShowMessage(@"请对骑手进行评价");
        return;
    }
    
    
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{@"ince":@"post_evaluate",
                          @"ru_id":[UserModel sharedInstanced].ru_Id,
                          @"star_num":@(storeCell.merchant_starNum),
                          @"qs_msg":self.textView.text,
                          @"emp_id":self.dataDic[@"emp_id"],
                          @"order_id":self.dataDic[@"order_id"],
                          
                          };

    [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        ShowMessage(responseObject[@"msg"]);
        if(flag == 200){
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        [self hidHUD:@"网络异常"];
    }];
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
