//
//  OrderStateHeadView.h
//  rrbi
//
//  Created by MyMac on 2019/5/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderStateHeadView : UIView

@property (weak, nonatomic) IBOutlet UILabel *oneLAbel;
@property (weak, nonatomic) IBOutlet UILabel *erLabel;
@property (weak, nonatomic) IBOutlet UILabel *sanLabel;
@property (weak, nonatomic) IBOutlet UILabel *siLabel;
@property (weak, nonatomic) IBOutlet UILabel *wuLabel;
@property (weak, nonatomic) IBOutlet UIView *yiView;
@property (weak, nonatomic) IBOutlet UIView *erView;
@property (weak, nonatomic) IBOutlet UIView *sanView;
@property (weak, nonatomic) IBOutlet UIView *siView;

@property (weak, nonatomic) IBOutlet UILabel *yiMLabel;
@property (weak, nonatomic) IBOutlet UILabel *yiTLabel;
@property (weak, nonatomic) IBOutlet UILabel *erMLabel;
@property (weak, nonatomic) IBOutlet UILabel *erTLAbel;
@property (weak, nonatomic) IBOutlet UILabel *sanMLabel;
@property (weak, nonatomic) IBOutlet UILabel *sanTLabel;
@property (weak, nonatomic) IBOutlet UILabel *siMLabel;
@property (weak, nonatomic) IBOutlet UILabel *siTLabel;
@property (weak, nonatomic) IBOutlet UILabel *wuMLabel;
@property (weak, nonatomic) IBOutlet UILabel *wuTLabel;


- (void)updataWithArray:(NSArray *)arr;

@end

NS_ASSUME_NONNULL_END
