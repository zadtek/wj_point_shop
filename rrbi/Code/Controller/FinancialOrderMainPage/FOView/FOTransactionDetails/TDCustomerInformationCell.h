//
//  TDCustomerInformationCell.h
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TDCustomerInformationCell : UITableViewCell

@property (nonatomic, copy) NSString *ues_tel;
@property (weak, nonatomic) IBOutlet UILabel *xingMingLabel;
@property (weak, nonatomic) IBOutlet UILabel *dizhiLabel;
@property (weak, nonatomic) IBOutlet UILabel *beizhuLabel;

@property (weak, nonatomic) IBOutlet UILabel *lianxiGuKeLabel;
@property (weak, nonatomic) IBOutlet UIButton *daDianHuaButton;
@property (weak, nonatomic) IBOutlet UIButton *touMingButt;

- (void)updataWithDict:(NSDictionary *)dict type:(NSInteger)type;
@end

NS_ASSUME_NONNULL_END
