//
//  TDShippingInformationCell.h
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TDShippingInformationCell : UITableViewCell

@property (nonatomic, copy) NSString *emp_tel;

@property (weak, nonatomic) IBOutlet UILabel *peiSongFuwuLabel;
@property (weak, nonatomic) IBOutlet UILabel *xingmingLabel;
@property (weak, nonatomic) IBOutlet UILabel *peisongFangSLabel;

@property (weak, nonatomic) IBOutlet UILabel *lianXiQiShouLabel;
- (void)updataWithDict:(NSDictionary *)dict type:(NSInteger)type;
@property (weak, nonatomic) IBOutlet UIButton *daDianHuaButton;
@property (weak, nonatomic) IBOutlet UIButton *toumingButton;
@end

NS_ASSUME_NONNULL_END
