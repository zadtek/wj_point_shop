//
//  TDOrderInformationCell.h
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TDOrderInformationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *xiaDanTImeLabel;
@property (weak, nonatomic) IBOutlet UILabel *songdaLabel;
@property (weak, nonatomic) IBOutlet UILabel *zhifuFSLAbel;
- (void)updataWithDict:(NSDictionary *)dict;
@end

NS_ASSUME_NONNULL_END
