//
//  TDShippingInformationCell.m
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import "TDShippingInformationCell.h"

@implementation TDShippingInformationCell

- (void)updataWithDict:(NSDictionary *)dict type:(NSInteger)type{
    
    self.emp_tel = [NSString stringWithFormat:@"%@",dict[@"emp_tel"]];
    self.peiSongFuwuLabel.text = [NSString stringWithFormat:@"%@",dict[@"peisong"]];
    self.xingmingLabel.text = [NSString stringWithFormat:@"%@",dict[@"emp_user"]];
    self.peisongFangSLabel.text = [NSString stringWithFormat:@"%@",dict[@"qw_time"]];
    
    if (type == 5) {
        self.daDianHuaButton.hidden = YES;
        self.lianXiQiShouLabel.hidden = YES;
        self.toumingButton.hidden = YES;
    }
    
}

- (IBAction)daDianHuaButtonAction:(id)sender {
    NSMutableString * string = [[NSMutableString alloc] initWithFormat:@"tel:%@",self.emp_tel];
    UIWebView * callWebview = [[UIWebView alloc] init];
    [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:string]]];
    [self addSubview:callWebview];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
