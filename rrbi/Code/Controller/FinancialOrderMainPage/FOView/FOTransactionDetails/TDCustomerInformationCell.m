//
//  TDCustomerInformationCell.m
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import "TDCustomerInformationCell.h"

@implementation TDCustomerInformationCell

- (void)updataWithDict:(NSDictionary *)dict type:(NSInteger)type {
    
    self.ues_tel = [NSString stringWithFormat:@"%@",dict[@"user_tel"]];
    
    
    self.xingMingLabel.text = [NSString stringWithFormat:@"%@",dict[@"user"]];
    self.dizhiLabel.text = [NSString stringWithFormat:@"%@",dict[@"address"]];
    self.beizhuLabel.text = [NSString stringWithFormat:@"%@",dict[@"reamrk"]];
    
    if (type == 5) {
        self.daDianHuaButton.hidden = YES;
        self.lianxiGuKeLabel.hidden = YES;
        self.touMingButt.hidden = YES;
    }
    
}

- (IBAction)daDianHuaButtonAction:(id)sender {
    NSMutableString * string = [[NSMutableString alloc] initWithFormat:@"tel:%@",self.ues_tel];
    UIWebView * callWebview = [[UIWebView alloc] init];
    [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:string]]];
    [self addSubview:callWebview];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
