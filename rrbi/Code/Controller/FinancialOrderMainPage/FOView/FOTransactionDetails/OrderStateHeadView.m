//
//  OrderStateHeadView.m
//  rrbi
//
//  Created by MyMac on 2019/5/7.
//

#import "OrderStateHeadView.h"

@implementation OrderStateHeadView



- (void)updataWithArray:(NSArray *)arr {
    
    for (NSInteger i=0; i<arr.count; i++) {
        
        
        
        if (i == 0) {
            
            NSDictionary *dict = arr[0];
            self.yiMLabel.text = [dict objectForKey:@"tk_describe"];
            NSString *timeSt= [dict objectForKey:@"tk_time"];
            self.yiTLabel.text =timeSt;
            
            self.oneLAbel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            
        } else if (i == 1) {
            
            NSDictionary *dict = arr[1];
            self.erMLabel.text = [dict objectForKey:@"tk_describe"];
            NSString *timeSt= [dict objectForKey:@"tk_time"];
            
            self.erTLAbel.text =timeSt;
            
            
            
            
            self.oneLAbel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.yiView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.erLabel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            
            
        }else if (i == 2) {
            NSDictionary *dict = arr[2];
            self.sanMLabel.text = [dict objectForKey:@"tk_describe"];
            NSString *timeSt= [dict objectForKey:@"tk_time"];
            NSString *hourS = timeSt;
            self.sanTLabel.text =hourS;
            
            
            
            self.oneLAbel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.yiView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.erLabel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.erView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.sanLabel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            
            
        }else if (i == 3) {
            NSDictionary *dict = arr[3];
            self.siMLabel.text = [dict objectForKey:@"tk_describe"];
            NSString *timeSt= [dict objectForKey:@"tk_time"];
            NSString *hourS = [timeSt substringWithRange:NSMakeRange(10, 6)];
            self.siTLabel.text =hourS;
            
            
            
            
            self.oneLAbel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.yiView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.erLabel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.erView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.sanLabel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.sanView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.siLabel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            
        } else if (i == 4) {
            NSDictionary *dict = arr[4];
            self.wuMLabel.text = [dict objectForKey:@"tk_describe"];
            NSString *timeSt= [dict objectForKey:@"tk_time"];
            NSString *hourS = [timeSt substringWithRange:NSMakeRange(10, 6)];
            self.wuTLabel.text =hourS;
            
            
            
            self.oneLAbel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.yiView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.erLabel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.erView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.sanLabel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.sanView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.siLabel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.siView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            self.wuLabel.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:199.0/255.0 blue:0.0/255.0 alpha:1];
            
            
        }
        
    }
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.oneLAbel.layer.cornerRadius = 5;
    self.erLabel.layer.cornerRadius = 5;
    self.sanLabel.layer.cornerRadius = 5;
    self.siLabel.layer.cornerRadius = 5;
    self.wuLabel.layer.cornerRadius = 5;
   
    self.oneLAbel.layer.masksToBounds = YES;
    self.erLabel.layer.masksToBounds = YES;
    self.sanLabel.layer.masksToBounds = YES;
    self.siLabel.layer.masksToBounds = YES;
    self.wuLabel.layer.masksToBounds = YES;
    
}



@end
