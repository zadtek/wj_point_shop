//
//  TDGoodsListCell.h
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import <UIKit/UIKit.h>

#import "FinancaialOrderModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface TDGoodsListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *listBackView;
@property (weak, nonatomic) IBOutlet UILabel *zongjiaLabel;
@property (weak, nonatomic) IBOutlet UILabel *huiYuanJiaLabel;
@property (weak, nonatomic) IBOutlet UILabel *fuwuFEiLabel;
@property (weak, nonatomic) IBOutlet UILabel *shifuLabel;
@property (weak, nonatomic) IBOutlet UILabel *yujiLabel;
@property (weak, nonatomic) IBOutlet UILabel *xiaoSHouCHengBenLabel;
@property (weak, nonatomic) IBOutlet UILabel *peisongFeiLabel;
@property (weak, nonatomic) IBOutlet UILabel *huodongZhiChuLabel;
@property (weak, nonatomic) IBOutlet UILabel *manJianHuoLabel;
@property (weak, nonatomic) IBOutlet UILabel *pingTaiBuTieLabel;
@property (weak, nonatomic) IBOutlet UIImageView *mainJianImage;
@property (weak, nonatomic) IBOutlet UILabel *mainJianTitleLabel;


@property (nonatomic, strong) NSMutableArray *mainArray;


- (void)updataWithDict:(NSDictionary *)dict;
@end

NS_ASSUME_NONNULL_END
