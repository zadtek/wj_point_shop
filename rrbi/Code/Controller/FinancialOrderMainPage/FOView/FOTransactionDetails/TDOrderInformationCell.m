//
//  TDOrderInformationCell.m
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import "TDOrderInformationCell.h"

@implementation TDOrderInformationCell
- (void)updataWithDict:(NSDictionary *)dict {
    
    self.orderNumLabel.text = [NSString stringWithFormat:@"%@",dict[@"order_sn"]];
    
    self.xiaDanTImeLabel.text = [NSString stringWithFormat:@"%@",[self timeStrConversionC:dict[@"add_time"]]];
    self.songdaLabel.text = [NSString stringWithFormat:@"%@",[self timeStrConversionC:dict[@"yj_time"]]];
    self.zhifuFSLAbel.text = [NSString stringWithFormat:@"%@",dict[@"pay_name"]];
}

- (NSString *)timeStrConversionC:(NSString *)timeStr {
    NSString *timeStampString  = timeStr;
    // iOS 生成的时间戳是10位
    NSTimeInterval interval    =[timeStampString doubleValue];
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *dateString       = [formatter stringFromDate: date];
    return dateString;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
