//
//  TDGoodsListCell.m
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import "TDGoodsListCell.h"

@implementation TDGoodsListCell
- (NSMutableArray *)mainArray {
    if (!_mainArray) {
        _mainArray  = [NSMutableArray array];
    }
    return _mainArray;
}

- (void)updataWithDict:(NSDictionary *)dict {
    
    self.zongjiaLabel.text = [NSString stringWithFormat:@"总价¥%@",dict[@"goods_aount"]];
    self.huiYuanJiaLabel.text = [NSString stringWithFormat:@"-¥%@",dict[@"vip_price"]];
    self.fuwuFEiLabel.text = [NSString stringWithFormat:@"-¥%@",dict[@"fw_price"]];
    self.shifuLabel.text = [NSString stringWithFormat:@"¥%@",dict[@"should_pay"]];
    self.yujiLabel.text = [NSString stringWithFormat:@"¥%@",dict[@"yj_price"]];
    
    self.peisongFeiLabel.text = [NSString stringWithFormat:@"¥%@",dict[@"peisong_price"]];
    self.huodongZhiChuLabel.text = [NSString stringWithFormat:@"-¥%@",dict[@"hd_price"]];
    self.pingTaiBuTieLabel.text = [NSString stringWithFormat:@"¥%@",dict[@"bt_price"]];
    self.xiaoSHouCHengBenLabel.text = [NSString stringWithFormat:@"-¥%@",dict[@"cb_price"]];
    
    [self.mainJianImage sd_setImageWithURL:[NSURL URLWithString:dict[@""]] placeholderImage:[UIImage imageNamed:@"减"]];
    self.mainJianTitleLabel.text = [NSString stringWithFormat:@"%@",@"满减活动"];
    self.manJianHuoLabel.text = [NSString stringWithFormat:@"-¥%@",dict[@"jian"]];
    
    
    [self createList:dict];
}

- (void)createList:(NSDictionary *)mainDict {
    [self.mainArray removeAllObjects];
    for(UIView *view in [self.listBackView subviews])
    {
        [view removeFromSuperview];
    }
    self.mainArray = [FinancaialOrderModel mj_objectArrayWithKeyValuesArray:[mainDict objectForKey:@"goods_list"]];
    
    CGFloat H = 70;
    CGFloat space = 10;
    for (NSInteger i=0; i<self.mainArray.count; i++) {
        FinancaialOrderModel *model = self.mainArray[i];
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, i*H, SCREEN_WIDTH-20-30, H)];
        view.backgroundColor = [UIColor whiteColor];
        [self.listBackView addSubview:view];
        
        
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, space, H-space*2, H-space*2)];
        [imageView sd_setImageWithURL:[NSURL URLWithString:model.goods_thumb] placeholderImage:[UIImage imageNamed:@""]];
        [view addSubview:imageView];
        
        UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(imageView.bounds.size.width+10, space, (view.bounds.size.width-(H-space*2)-10)-100, 12)];
        nameLabel.font = [UIFont systemFontOfSize:12];
        nameLabel.textColor = [UIColor blackColor];
        nameLabel.text = model.goods_name;
        [view addSubview:nameLabel];
        
        
        UILabel *countLabel = [[UILabel alloc]initWithFrame:CGRectMake(imageView.bounds.size.width+10, view.bounds.size.height-space-12, (view.bounds.size.width-(H-space*2)-10)-150, 12)];
        countLabel.font = [UIFont systemFontOfSize:12];
        countLabel.textColor = [UIColor blackColor];
        countLabel.text = model.goods_number;
        [view addSubview:countLabel];
        
        UILabel *priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(view.bounds.size.width-100, space,100, 12)];
        priceLabel.font = [UIFont systemFontOfSize:12];
        priceLabel.textAlignment = NSTextAlignmentRight;
        priceLabel.textColor = [UIColor blackColor];
        priceLabel.text = [NSString stringWithFormat:@"¥%@", model.goods_price];
        [view addSubview:priceLabel];
        
    }
    
    
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
