//
//  ProductDataListCell.h
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import <UIKit/UIKit.h>

#import "FinancaialOrderModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface ProductDataListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *yiwanChengButton;
@property (weak, nonatomic) IBOutlet UILabel *bianMALAbel;
@property (weak, nonatomic) IBOutlet UILabel *mingChengLabel;
@property (weak, nonatomic) IBOutlet UILabel *jinjiaLabel;
@property (weak, nonatomic) IBOutlet UILabel *shouJiaLabel;
@property (weak, nonatomic) IBOutlet UILabel *shuliangLabel;
@property (weak, nonatomic) IBOutlet UILabel *lirunLabel;
@property (weak, nonatomic) IBOutlet UILabel *bianhaoLabel;
@property (weak, nonatomic) IBOutlet UILabel *zongeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dengjiLabel;




@property (nonatomic, copy) void(^FinancaialOrderBlock)();
- (void)updataDataWithModel:(FinancaialOrderModel *)model;


@end

NS_ASSUME_NONNULL_END
