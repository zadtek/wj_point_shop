//
//  FinancaialOrderCell.m
//  rrbi
//
//  Created by MyMac on 2019/5/5.
//

#import "FinancaialOrderCell.h"

@implementation FinancaialOrderCell


- (void)updataDataWithModel:(FinancaialOrderModel *)model {
    self.timeLAbel.text = model.time;
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@",model.money];
}

- (IBAction)xiangQingButtonAction:(UIButton *)sender {
    if (self.FinancaialOrderBlock) {
        self.FinancaialOrderBlock();
    }
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
