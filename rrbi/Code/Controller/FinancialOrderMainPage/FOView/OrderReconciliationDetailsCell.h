//
//  OrderReconciliationDetailsCell.h
//  rrbi
//
//  Created by MyMac on 2019/5/5.
//

#import <UIKit/UIKit.h>

#import "FinancaialOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderReconciliationDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *bianHaoLabel;
@property (weak, nonatomic) IBOutlet UILabel *liushuiHaolabel;
@property (weak, nonatomic) IBOutlet UILabel *zongJiaLabel;
@property (weak, nonatomic) IBOutlet UILabel *baoZhuangLabel;
@property (weak, nonatomic) IBOutlet UILabel *huodongLabel;
@property (weak, nonatomic) IBOutlet UILabel *pingtaiLabel;
@property (weak, nonatomic) IBOutlet UILabel *jiesuanLabel;
@property (weak, nonatomic) IBOutlet UIButton *waiMaiButton;

@property (nonatomic, copy) void(^FinancaialOrderBlock)();
- (void)updataDataWithModel:(FinancaialOrderModel *)model;


@end

NS_ASSUME_NONNULL_END
