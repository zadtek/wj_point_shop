//
//  FinancaialBalanceStateCell.m
//  rrbi
//
//  Created by MyMac on 2019/5/5.
//

#import "FinancaialBalanceStateCell.h"

@implementation FinancaialBalanceStateCell


- (void)updataDataWithModel:(FinancaialOrderModel *)model {
    self.timeLabel.text = model.days;
    self.tixianLabel.text = model.status;
    self.priceLabel.text = model.money;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
