//
//  OrderReconciliationDetailsCell.m
//  rrbi
//
//  Created by MyMac on 2019/5/5.
//

#import "OrderReconciliationDetailsCell.h"

@implementation OrderReconciliationDetailsCell

- (void)updataDataWithModel:(FinancaialOrderModel *)model {
    self.timeLabel.text =model.time;
    self.bianHaoLabel.text = model.order_number;
    self.liushuiHaolabel.text = model.liushui;
    self.zongJiaLabel.text = [NSString stringWithFormat:@"¥%@",model.total_price];
    self.baoZhuangLabel.text = [NSString stringWithFormat:@"¥%@",model.packing_price];
    self.huodongLabel.text = [NSString stringWithFormat:@"¥%@",model.activity];
    self.pingtaiLabel.text = [NSString stringWithFormat:@"¥%@",model.platform];
    self.jiesuanLabel.text = [NSString stringWithFormat:@"¥%@",model.price];
    [self.waiMaiButton setTitle:model.order_type forState:UIControlStateNormal];
    
}
- (IBAction)waiMaiButtonAction:(id)sender {
    
    if (self.FinancaialOrderBlock) {
        self.FinancaialOrderBlock();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
