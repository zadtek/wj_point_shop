//
//  TimeSectionView.m
//  rrbi
//
//  Created by MyMac on 2019/5/7.
//

#import "TimeSectionView.h"



@implementation TimeSectionView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self =[super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

-(void)createUI {
    
    
    UIView *backView= [[UIView alloc]initWithFrame:CGRectMake(0, self.bounds.size.height - 300, SCREEN_WIDTH, 40)];
    backView.backgroundColor =[UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1];
    [self addSubview:backView];
    
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeSystem];
    btnDone.frame = CGRectMake(SCREEN_WIDTH -50-8, 8, 50, 30);
    [btnDone setTitle:@"完成" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(pickDone) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:btnDone];
    
    UILabel *kaishiLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 8, SCREEN_WIDTH*0.5, 30)];
    kaishiLabel.text = @"开始时间";
    kaishiLabel.font = [UIFont systemFontOfSize:14];
    kaishiLabel.textColor = [UIColor blackColor];
    kaishiLabel.textAlignment = NSTextAlignmentCenter;
    [backView addSubview:kaishiLabel];
    
    UILabel *jiesuLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.5, 8, SCREEN_WIDTH*0.5, 30)];
    jiesuLabel.text = @"结束时间";
    jiesuLabel.font = [UIFont systemFontOfSize:14];
    jiesuLabel.textColor = [UIColor blackColor];
    jiesuLabel.textAlignment = NSTextAlignmentCenter;
    [backView addSubview:jiesuLabel];
    
    
    
        self.picker1 = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 260, SCREEN_WIDTH*0.5, 260)];
        self.picker1.tag= 100;
    self.picker1.backgroundColor = [UIColor whiteColor];
    self.picker1.datePickerMode = UIDatePickerModeTime;
    [self.picker1 addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:self.picker1];
    
    
    
    
        self.picker2 = [[UIDatePicker alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*0.5, self.bounds.size.height - 260, SCREEN_WIDTH*0.5, 260)];
        self.picker2.backgroundColor = [UIColor whiteColor];
        self.picker2.tag= 200;
    self.picker2.datePickerMode = UIDatePickerModeTime;
    [self.picker2 addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:self.picker2];
    
    NSDate *date= [NSDate date];
    NSDateFormatter *fm = [[NSDateFormatter alloc] init];
    fm.dateFormat = @"HH:mm";
    self.staTime = [fm stringFromDate:date];
    self.endTime = [fm stringFromDate:date];

}

- (void)valueChanged:(UIDatePicker *)picker{
    NSDateFormatter *fm = [[NSDateFormatter alloc] init];
    fm.dateFormat = @"HH:mm";
    if (picker.tag == 100) {
        self.staTime = [fm stringFromDate:picker.date];
    } else {
        self.endTime = [fm stringFromDate:picker.date];
    }
    
}


- (void)pickDone{
    if ([self.delegate respondsToSelector:@selector(pickerStaTime:endTime:)]) {
        [self.delegate pickerStaTime:self.staTime endTime:self.endTime];
    }
    self.hidden = YES;
}

@end
