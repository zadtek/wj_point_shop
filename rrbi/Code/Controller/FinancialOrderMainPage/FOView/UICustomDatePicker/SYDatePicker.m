//
//  SYDatePicker.m
//  DatePickerDemo
//
//  Created by Apple on 16/3/8.
//  Copyright © 2016年 Apple. All rights reserved.
//

#import "SYDatePicker.h"

@implementation SYDatePicker

- (instancetype)initWithFrame:(CGRect)frame {
    if (self =[super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        
        if(!self.picker1){
            self.picker1 = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
            self.picker1.tag= 100;
        }
        
        self.picker1.datePickerMode = UIDatePickerModeTime;
        [self.picker1 addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:self.picker1];
        
        
        
        if(!self.picker2){
            self.picker2 = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
            self.picker2.tag= 200;
        }
        
        self.picker2.datePickerMode = UIDatePickerModeTime;
        [self.picker2 addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:self.picker1];
    }
    return self;
}

- (void)dismiss{
    [UIView animateWithDuration:0.3 animations:^{
         self.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 0);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)valueChanged:(UIDatePicker *)picker{
    if([self.delegate respondsToSelector:@selector(picker:ValueChanged:)]){
        [self.delegate picker:picker ValueChanged:picker.date];
    }
}

@end
