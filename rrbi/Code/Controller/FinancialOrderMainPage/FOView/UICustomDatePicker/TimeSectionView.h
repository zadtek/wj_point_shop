//
//  TimeSectionView.h
//  rrbi
//
//  Created by MyMac on 2019/5/7.
//

#import <UIKit/UIKit.h>

#import "SYDatePicker.h"
NS_ASSUME_NONNULL_BEGIN

@protocol TimeSectionViewDelegate <NSObject>

- (void)pickerStaTime:(NSString *)staTime endTime:(NSString *)endTime;

@end

@interface TimeSectionView : UIView <SYDatePickerDelegate>

@property  (weak, nonatomic) id<TimeSectionViewDelegate> delegate;
@property (nonatomic, copy) NSString *staTime;
@property (nonatomic, copy) NSString *endTime;

//@property SYDatePicker *picker1;
//@property SYDatePicker *picker2;


@property  (strong, nonatomic) UIDatePicker *picker1;
@property  (strong, nonatomic) UIDatePicker *picker2;


@end

NS_ASSUME_NONNULL_END
