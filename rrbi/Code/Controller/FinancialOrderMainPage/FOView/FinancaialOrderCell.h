//
//  FinancaialOrderCell.h
//  rrbi
//
//  Created by MyMac on 2019/5/5.
//

#import <UIKit/UIKit.h>

#import "FinancaialOrderModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface FinancaialOrderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeLAbel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *xiangQingButton;

@property (nonatomic, copy) void(^FinancaialOrderBlock)();
- (void)updataDataWithModel:(FinancaialOrderModel *)model;

@end

NS_ASSUME_NONNULL_END
