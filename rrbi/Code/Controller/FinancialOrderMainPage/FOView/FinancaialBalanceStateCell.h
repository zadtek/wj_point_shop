//
//  FinancaialBalanceStateCell.h
//  rrbi
//
//  Created by MyMac on 2019/5/5.
//

#import <UIKit/UIKit.h>

#import "FinancaialOrderModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface FinancaialBalanceStateCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *tixianLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

- (void)updataDataWithModel:(FinancaialOrderModel *)model;

@end

NS_ASSUME_NONNULL_END
