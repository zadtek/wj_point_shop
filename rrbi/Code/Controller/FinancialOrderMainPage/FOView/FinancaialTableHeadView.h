//
//  FinancaialTableHeadView.h
//  rrbi
//
//  Created by MyMac on 2019/5/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FinancaialTableHeadView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *fuzeRenLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic, copy) void(^HeadViewBlock)();

@end

NS_ASSUME_NONNULL_END
