//
//  ProductDataListCell.m
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import "ProductDataListCell.h"

@implementation ProductDataListCell

- (void)updataDataWithModel:(FinancaialOrderModel *)model {
    self.timeLabel.text =model.time;
    self.bianMALAbel.text = model.goods_number;
    self.mingChengLabel.text = model.goods_name;
    self.jinjiaLabel.text = [NSString stringWithFormat:@"¥%@",model.cost_price];
    self.shouJiaLabel.text = [NSString stringWithFormat:@"¥%@",model.selling_price];
    self.shuliangLabel.text = [NSString stringWithFormat:@"%@",model.number];
    self.lirunLabel.text = [NSString stringWithFormat:@"¥%@",model.profit];
    self.bianhaoLabel.text = [NSString stringWithFormat:@"%@",model.order_number];
    self.zongeLabel.text = [NSString stringWithFormat:@"¥%@",model.order_money];
    self.dengjiLabel.text = [NSString stringWithFormat:@"%@",model.level];
    [self.yiwanChengButton setTitle:model.status forState:UIControlStateNormal];
    
}
- (IBAction)yiwanChengButtonAction:(id)sender {
    
    if (self.FinancaialOrderBlock) {
        self.FinancaialOrderBlock();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
