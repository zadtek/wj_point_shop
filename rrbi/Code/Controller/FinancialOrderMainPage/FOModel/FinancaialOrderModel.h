//
//  FinancaialOrderModel.h
//  rrbi
//
//  Created by MyMac on 2019/5/5.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FinancaialOrderModel : NSObject

@property (nonatomic, copy) NSString *days;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *money;


@property (nonatomic, copy) NSString *time_count;
@property (nonatomic, copy) NSString *money_count;
@property (nonatomic, strong) NSArray *list;

@property (nonatomic, copy) NSString *order_type;
@property (nonatomic, copy) NSString *order_number;
@property (nonatomic, copy) NSString *liushui;
@property (nonatomic, copy) NSString *total_price;
@property (nonatomic, copy) NSString *packing_price;
@property (nonatomic, copy) NSString *activity;
@property (nonatomic, copy) NSString *platform;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *delivery_price;

@property (nonatomic, copy) NSString *goods_number;
@property (nonatomic, copy) NSString *goods_name;
@property (nonatomic, copy) NSString *cost_price;
@property (nonatomic, copy) NSString *selling_price;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *profit;
@property (nonatomic, copy) NSString *order_money;
@property (nonatomic, copy) NSString *level;

@property (nonatomic, copy) NSString *goods_price;
@property (nonatomic, copy) NSString *goods_thumb;
@property (nonatomic, copy) NSString *goods_amount;
@property (nonatomic, copy) NSString *pack_fee;


@property (nonatomic, copy) NSString *estimated_revenue;

@property (nonatomic, copy) NSString *platform_subsidy;
@property (nonatomic, copy) NSString *business_subsidy;
@property (nonatomic, copy) NSString *order_duration;
@property (nonatomic, copy) NSString *completion_time;
@property (nonatomic, copy) NSString *activity_type;

@end

NS_ASSUME_NONNULL_END
