//
//  OrderReconciliationViewController.m
//  rrbi
//
//  Created by MyMac on 2019/5/5.
//

#import "OrderReconciliationViewController.h"
#import "FinancaialOrderModel.h"
#import "FinancaialOrderCell.h"
#import "OrderReconciliationDetailsViewController.h"

@interface OrderReconciliationViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tabelView;
@property (nonatomic, strong) NSMutableArray *mainArray;
@property (nonatomic, strong) NSMutableArray *subArray;

@property (nonatomic, copy) NSString *sttaus;

@end

@implementation OrderReconciliationViewController
- (NSMutableArray *)subArray {
    if (!_subArray) {
        _subArray  = [NSMutableArray array];
    }
    return _subArray;
}

- (NSMutableArray *)mainArray {
    if (!_mainArray) {
        _mainArray  = [NSMutableArray array];
    }
    return _mainArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"账单对账";
    self.sttaus = @"1";
    [self createTAbelView];
    
    [self getShopData];
}
- (void)createTAbelView {
    self.tabelView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-SafeAreaTopHeight) style:UITableViewStyleGrouped];
    self.tabelView.delegate = self;
    self.tabelView.dataSource = self;
    [self.view addSubview:self.tabelView];
    self.tabelView.showsVerticalScrollIndicator = NO;
    self.tabelView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tabelView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if (@available(iOS 11.0, *))
    {
        self.tabelView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tabelView.estimatedRowHeight = 0;
        self.tabelView.estimatedSectionFooterHeight = 0;
        self.tabelView.estimatedSectionHeaderHeight = 0;
    }
    
    [self.tabelView registerNib:[UINib nibWithNibName:NSStringFromClass([FinancaialOrderCell class]) bundle:nil] forCellReuseIdentifier:@"financaialOrderCell"];
    
    [self createHeadTabelView];
}

- (void)createHeadTabelView {
    
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    headView.backgroundColor = [UIColor whiteColor];
    
    UIButton *button1 = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH*0.5, 48)];
    [button1 setTitle:@"已结账单" forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont systemFontOfSize:15];
    [button1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button1 addTarget:self action:@selector(button1Action) forControlEvents:UIControlEventTouchUpInside];
    [headView addSubview:button1];
    
    UIButton *button2 = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.5, 0, SCREEN_WIDTH*0.5, 48)];
    button2.titleLabel.font = [UIFont systemFontOfSize:15];
    [button2 setTitle:@"未结账单" forState:UIControlStateNormal];
    [button2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(button2Action) forControlEvents:UIControlEventTouchUpInside];
    [headView addSubview:button2];
    
    UIView *lineView= [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/4-130*0.5, 48, 130, 2)];
    lineView.tag = 100;
    lineView.backgroundColor = [UIColor blackColor];
    [headView addSubview:lineView];
    
    self.tabelView.tableHeaderView = headView;
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.mainArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
        NSMutableArray *arr = self.subArray[section];
        return arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    FinancaialOrderModel *model = self.mainArray[section];
    
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    headView.backgroundColor = [UIColor whiteColor];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, (SCREEN_WIDTH-30-13)*0.5, 30)];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
    label.text = model.time_count;
    label.textColor = [UIColor blackColor];
    [headView addSubview:label];
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-((SCREEN_WIDTH-30-13)*0.5)-13, 0, (SCREEN_WIDTH-30-13)*0.5, 30)];
    label1.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
    label1.text = [NSString stringWithFormat:@"¥%@已存入余额",model.money_count];
    label1.textAlignment = NSTextAlignmentRight;
    label1.textColor = [UIColor blackColor];
    [headView addSubview:label1];
    
    return headView;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
   
        UIView *footView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 15)];
        footView.backgroundColor = [UIColor whiteColor];
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 5)];
        lineView.backgroundColor = [UIColor colorWithRed:234.0/255.0 green:235.0/255.0 blue:236/255.0 alpha:1];
        [footView addSubview:lineView];
        return footView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 15;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    FinancaialOrderCell *orderCell = [tableView dequeueReusableCellWithIdentifier:@"financaialOrderCell"];
    
    NSMutableArray *subArr = self.subArray[indexPath.section];
    FinancaialOrderModel *model = subArr[indexPath.row];
    [orderCell updataDataWithModel:model];
    __weak typeof(self) weakSelf = self;
    orderCell.FinancaialOrderBlock = ^{
        OrderReconciliationDetailsViewController *VC = [[OrderReconciliationDetailsViewController alloc]init];
        VC.timeStr = model.time;
        [weakSelf.navigationController pushViewController:VC animated:YES];
    };
        return orderCell;
    
}
#pragma mark - 数据请求
- (void)getShopData{
    
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{
                          @"ru_id":[UserModel sharedInstanced].ru_Id,
                          @"ince":@"bill_finance",
                          @"status":self.sttaus,
                          
                          };
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",BaseUrl,@"bill_finance"];
    [mgr POST:URL parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            [self.mainArray removeAllObjects];
            [self.subArray removeAllObjects];
            self.mainArray = [FinancaialOrderModel mj_objectArrayWithKeyValuesArray:[responseObject objectForKey:@"list_all"]];
            
            NSMutableArray *arr = [NSMutableArray array];
            for (NSInteger i=0; i<self.mainArray.count; i++) {
                FinancaialOrderModel *model = self.mainArray[i];
                arr = [FinancaialOrderModel mj_objectArrayWithKeyValuesArray:model.list];
                [self.subArray addObject:arr];
            }
            
            
            [self.tabelView reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        [self hidHUD:@"网络异常"];
    }];
    
}


- (void)button1Action {
    UIView *lineView = [self.view viewWithTag:100];
    lineView.frame = CGRectMake(SCREEN_WIDTH/4-130*0.5, 48, 130, 2);
    self.sttaus = @"1";
    [self getShopData];
}
- (void)button2Action {
    UIView *lineView = [self.view viewWithTag:100];
    lineView.frame = CGRectMake(SCREEN_WIDTH/4*3-130*0.5, 48, 130, 2);
    self.sttaus = @"2";
    [self getShopData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
