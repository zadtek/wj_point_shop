//
//  FBOrderDetalisViewController.m
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import "FBOrderDetalisViewController.h"
#import "TDOrderInformationCell.h"
#import "TDGoodsListCell.h"
#import "TDCustomerInformationCell.h"
#import "TDShippingInformationCell.h"

#import "FinancaialOrderModel.h"
#import "OrderStateHeadView.h"

@interface FBOrderDetalisViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tabelView;
@property (nonatomic, strong) NSMutableArray *mainArray;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSDictionary *mainDict;
@property (nonatomic, strong)  UILabel *liushuiLabel;
@property (nonatomic, strong)  UIImageView *jianTouImage;
@property (nonatomic, strong)  UILabel *stateLabel;
@property (nonatomic, strong) OrderStateHeadView * stateHeadView;

@end

@implementation FBOrderDetalisViewController

- (NSMutableArray *)mainArray {
    if (!_mainArray) {
        _mainArray  = [NSMutableArray array];
    }
    return _mainArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.order_type == 1) {
        self.title = @"新订单详情";
    } else if (self.order_type == 2) {
        if (self.isGuanLi) {
            self.title = @"订单详情";
        } else {
            self.title = @"异常配送";
        }
    } else if (self.order_type == 3) {
        self.title = @"催单详情";
    }else if (self.order_type == 4) {
        self.title = @"退款";
    }else if (self.order_type == 5) {
        self.title = @"交易详情";
    }

   // self.view.backgroundColor = [UIColor whiteColor];
    
    [self createTAbelView];
    
    [self getData];
}

- (void)createNoStateTabelHeadView {
    
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
    headView.backgroundColor = [UIColor clearColor];
    self.tabelView.tableHeaderView = headView;
    
    UIView *liushuiView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
    liushuiView.backgroundColor = [UIColor clearColor];
    [headView addSubview:liushuiView];
    
    self.liushuiLabel = [[UILabel alloc]init];
    self.liushuiLabel.textColor = [UIColor blackColor];
    self.liushuiLabel.font =  [UIFont boldSystemFontOfSize:20];
    [liushuiView addSubview:self.liushuiLabel];
    
    
    self.jianTouImage = [[UIImageView alloc]init];
    self.jianTouImage.image = [UIImage imageNamed:@"store_operation_enter"];
    self.jianTouImage.contentMode = UIViewContentModeCenter;
    [liushuiView addSubview:self.jianTouImage];
    
    self.stateLabel = [[UILabel alloc]init];
    self.stateLabel.font = [UIFont systemFontOfSize:12];
    self.stateLabel.textColor = [UIColor lightGrayColor];
    [liushuiView addSubview:self.stateLabel];
}


- (void)createHaveStateTabelHeadView {
    
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60+120)];
    headView.backgroundColor = [UIColor clearColor];
    self.tabelView.tableHeaderView = headView;
    
    UIView *liushuiView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
    liushuiView.backgroundColor = [UIColor clearColor];
    [headView addSubview:liushuiView];
    
    self.liushuiLabel = [[UILabel alloc]init];
    self.liushuiLabel.textColor = [UIColor blackColor];
    self.liushuiLabel.font =  [UIFont boldSystemFontOfSize:20];
    [liushuiView addSubview:self.liushuiLabel];
    
    
    self.jianTouImage = [[UIImageView alloc]init];
    self.jianTouImage.image = [UIImage imageNamed:@"store_operation_enter"];
    self.jianTouImage.contentMode = UIViewContentModeCenter;
    [liushuiView addSubview:self.jianTouImage];
    
    self.stateLabel = [[UILabel alloc]init];
    self.stateLabel.font = [UIFont systemFontOfSize:12];
    self.stateLabel.textColor = [UIColor lightGrayColor];
    [liushuiView addSubview:self.stateLabel];
    
    
    self.stateHeadView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([OrderStateHeadView class]) owner:self options:nil].lastObject;
    self.stateHeadView.frame = CGRectMake(0, 60, SCREEN_WIDTH, 120);
    [headView addSubview:self.stateHeadView];
    
}




- (void)createTAbelView {
    
    self.tabelView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-SafeAreaTopHeight) style:UITableViewStyleGrouped];
    self.tabelView.delegate = self;
    self.tabelView.dataSource = self;
    [self.view addSubview:self.tabelView];
    self.tabelView.showsVerticalScrollIndicator = NO;
    self.tabelView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tabelView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if (@available(iOS 11.0, *))
    {
        self.tabelView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tabelView.estimatedRowHeight = 0;
        self.tabelView.estimatedSectionFooterHeight = 0;
        self.tabelView.estimatedSectionHeaderHeight = 0;
    }
    
    [self.tabelView registerNib:[UINib nibWithNibName:NSStringFromClass([TDShippingInformationCell class]) bundle:nil] forCellReuseIdentifier:@"tDShippingInformationCell"];
    [self.tabelView registerNib:[UINib nibWithNibName:NSStringFromClass([TDCustomerInformationCell class]) bundle:nil] forCellReuseIdentifier:@"tDCustomerInformationCell"];
    [self.tabelView registerNib:[UINib nibWithNibName:NSStringFromClass([TDGoodsListCell class]) bundle:nil] forCellReuseIdentifier:@"tDGoodsListCell"];
    [self.tabelView registerNib:[UINib nibWithNibName:NSStringFromClass([TDOrderInformationCell class]) bundle:nil] forCellReuseIdentifier:@"tDOrderInformationCell"];
    [self.tabelView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
    if (self.order_type == 1) {
        [self createNoStateTabelHeadView];
    } else if (self.order_type == 2) {
        [self createHaveStateTabelHeadView];
    } else if (self.order_type == 3) {
       [self createHaveStateTabelHeadView];
    }else if (self.order_type == 4) {
        [self createHaveStateTabelHeadView];
    }else if (self.order_type == 5) {
        
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 300 + self.mainArray.count*70;
    } else if (indexPath.row == 1) {
        
        if (self.order_type == 1) {
            return 0.00001;
        } else {
            return 120;
        }
        
    } else if (indexPath.row == 2) {
        return 130;
    } else {
        return 145;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.00001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.00001;;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TDGoodsListCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"tDGoodsListCell"];
    TDShippingInformationCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"tDShippingInformationCell"];
    TDCustomerInformationCell *cell3 = [tableView dequeueReusableCellWithIdentifier:@"tDCustomerInformationCell"];
    TDOrderInformationCell *cell4 = [tableView dequeueReusableCellWithIdentifier:@"tDOrderInformationCell"];
    
    UITableViewCell *Cell6 = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    
    
    if (indexPath.row == 0) {
        [cell1 updataWithDict:self.mainDict];
        return cell1;
    } else if (indexPath.row == 1) {
        if (self.order_type == 1) {
            return Cell6;
        } else {
        [cell2 updataWithDict:self.mainDict type:self.order_type];
        return cell2;
        }
    } else if (indexPath.row == 2) {
        [cell3 updataWithDict:self.mainDict type:self.order_type];
        return cell3;
    }else {
        [cell4 updataWithDict:self.mainDict];
        return cell4;
    }
}

- (void)getData {
    
    NSString *ty=@"";
    if (self.order_type == 1) {
       ty = @"1";
    } else if (self.order_type == 2) {
        ty = @"3";
    } else if (self.order_type == 3) {
       ty = @"2";
    }else if (self.order_type == 4) {
        ty = @"4";
    }else if (self.order_type == 5) {
        ty = @"";
    }
    
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{
                          @"type":ty,
                          @"ince":@"seller_order_info",
                          @"order_id":self.order_id
                          };
    NSString *URL = [NSString stringWithFormat:@"%@%@",BaseUrl,@"seller_order_info"];
    [mgr POST:URL parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            self.mainDict = responseObject;
            if (self.order_type == 2) {
                self.jianTouImage.hidden = YES;
                self.stateLabel.hidden = YES;
            }
            self.liushuiLabel.text = [NSString stringWithFormat:@"流水号#%@",[responseObject objectForKey:@"liushui"] ];
            self.stateLabel.text = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"order_describe"]];
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:20],};
            CGSize textSize = [self.liushuiLabel.text boundingRectWithSize:CGSizeMake(300, 60) options:NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
            self.liushuiLabel.frame = CGRectMake(12, 0, textSize.width, 60);
            self.jianTouImage.frame  = CGRectMake(12+textSize.width+5, 0, 10, 60);
            self.stateLabel.frame = CGRectMake(12+textSize.width+5+10+5, 0, 150, 60);
            
            [self.stateHeadView updataWithArray:[responseObject objectForKey:@"log"]];
            self.mainArray = [FinancaialOrderModel mj_objectArrayWithKeyValuesArray:[responseObject objectForKey:@"goods_list"]];
            [self.tabelView reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        NSLog(@"%@",error);
        [self hidHUD:@"网络异常"];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
