//
//  FBOrderDetalisViewController.h
//  rrbi
//
//  Created by MyMac on 2019/5/6.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBOrderDetalisViewController : BaseViewController

@property (nonatomic, copy) NSString *order_id;
@property (nonatomic, assign) NSInteger order_type;//1新订单2异常3催单4退款5交易详情
@property (nonatomic, assign) BOOL isGuanLi;

@end

NS_ASSUME_NONNULL_END
