//
//  OrderReconciliationDetailsViewController.m
//  rrbi
//
//  Created by MyMac on 2019/5/5.
//

#import "OrderReconciliationDetailsViewController.h"

#import "FinancaialOrderModel.h"
#import "OrderReconciliationDetailsCell.h"

#import "FBOrderDetalisViewController.h"


@interface OrderReconciliationDetailsViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tabelView;
@property (nonatomic, strong) NSMutableArray *mainArray;
@property (nonatomic, strong) UILabel *timeLabel;


@end

@implementation OrderReconciliationDetailsViewController
- (NSMutableArray *)mainArray {
    if (!_mainArray) {
        _mainArray  = [NSMutableArray array];
    }
    return _mainArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"账单详情";
    
    [self createTAbelView];
    
    [self getShopData];
}

- (void)createTAbelView {
    self.tabelView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-SafeAreaTopHeight) style:UITableViewStyleGrouped];
    self.tabelView.delegate = self;
    self.tabelView.dataSource = self;
    [self.view addSubview:self.tabelView];
    self.tabelView.showsVerticalScrollIndicator = NO;
    self.tabelView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tabelView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if (@available(iOS 11.0, *))
    {
        self.tabelView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tabelView.estimatedRowHeight = 0;
        self.tabelView.estimatedSectionFooterHeight = 0;
        self.tabelView.estimatedSectionHeaderHeight = 0;
    }
    
    [self.tabelView registerNib:[UINib nibWithNibName:NSStringFromClass([OrderReconciliationDetailsCell class]) bundle:nil] forCellReuseIdentifier:@"orderReconciliationDetailsCell"];
    
    [self createHeadTabelView];
}

- (void)createHeadTabelView {
    
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    headView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:243.0/255.0 blue:244/255.0 alpha:1];
    
    self.timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, 50)];
    self.timeLabel.font = [UIFont systemFontOfSize:15];
    self.timeLabel.textColor = [UIColor blackColor];
    [headView addSubview:self.timeLabel];
    self.tabelView.tableHeaderView = headView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
   return self.mainArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 230;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.00001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.00001;;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderReconciliationDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"orderReconciliationDetailsCell"];
    
    FinancaialOrderModel *model = self.mainArray[indexPath.row];
    [cell updataDataWithModel:model];
    
    
    __weak typeof(self) weakSelf = self;
    cell.FinancaialOrderBlock = ^{
        NSLog(@"%@",model);
        FBOrderDetalisViewController *VC = [[FBOrderDetalisViewController alloc]init];
        VC.order_id = model.order_number;
        VC.order_type = 5;
        [weakSelf.navigationController pushViewController:VC animated:YES];
    };
    return cell;
}


#pragma mark - 数据请求
- (void)getShopData{
    
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{
                          @"ru_id":[UserModel sharedInstanced].ru_Id,
                          @"ince":@"bill_info",
                          @"time":self.timeStr
                          };
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",BaseUrl,@"bill_info"];
    [mgr POST:URL parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            self.timeLabel.text = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"count_time"]];
            
            self.mainArray = [FinancaialOrderModel mj_objectArrayWithKeyValuesArray:[responseObject objectForKey:@"list"]];
            
            [self.tabelView reloadData];
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        [self hidHUD:@"网络异常"];
    }];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
