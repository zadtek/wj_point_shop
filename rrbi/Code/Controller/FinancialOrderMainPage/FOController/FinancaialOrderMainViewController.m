//
//  FinancaialOrderMainViewController.m
//  rrbi
//
//  Created by MyMac on 2019/5/5.
//

#import "FinancaialOrderMainViewController.h"

#import "FinancaialBalanceStateCell.h"
#import "FinancaialOrderCell.h"
#import "FinancaialTableHeadView.h"
#import "FinancaialOrderModel.h"
#import "OrderReconciliationViewController.h"
#import "OrderReconciliationDetailsViewController.h"
#import "FinancaialBalanceAllViewController.h"

@interface FinancaialOrderMainViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tabelView;
@property (nonatomic, strong) FinancaialTableHeadView *tableHeadView;
@property (nonatomic, strong) UILabel *yuELabel;

@property (nonatomic, strong) NSMutableArray *yuEArray;
@property (nonatomic, strong) NSMutableArray *daiJieArray;

@property (nonatomic, strong) NSMutableArray *mainArray;
@property (nonatomic, strong) NSMutableArray *subArray;


@end

@implementation FinancaialOrderMainViewController


- (NSMutableArray *)subArray {
    if (!_subArray) {
        _subArray  = [NSMutableArray array];
    }
    return _subArray;
}


- (NSMutableArray *)mainArray {
    if (!_mainArray) {
        _mainArray  = [NSMutableArray array];
    }
    return _mainArray;
}
- (NSMutableArray *)yuEArray {
    if (!_yuEArray) {
        _yuEArray  = [NSMutableArray array];
    }
    return _yuEArray;
}
- (NSMutableArray *)daiJieArray {
    if (!_daiJieArray) {
        _daiJieArray  = [NSMutableArray array];
    }
    return _daiJieArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"对账首页";
    self.view.backgroundColor = [UIColor whiteColor];
    [self createTAbelView];
    
    [self getShopData];
}

- (void)createTAbelView {
    self.tabelView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-SafeAreaTopHeight) style:UITableViewStyleGrouped];
    self.tabelView.delegate = self;
    self.tabelView.dataSource = self;
    [self.view addSubview:self.tabelView];
    self.tabelView.showsVerticalScrollIndicator = NO;
    self.tabelView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tabelView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if (@available(iOS 11.0, *))
    {
        self.tabelView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tabelView.estimatedRowHeight = 0;
        self.tabelView.estimatedSectionFooterHeight = 0;
        self.tabelView.estimatedSectionHeaderHeight = 0;
    }

    [self.tabelView registerNib:[UINib nibWithNibName:NSStringFromClass([FinancaialBalanceStateCell class]) bundle:nil] forCellReuseIdentifier:@"financaialBalanceStateCell"];
    [self.tabelView registerNib:[UINib nibWithNibName:NSStringFromClass([FinancaialOrderCell class]) bundle:nil] forCellReuseIdentifier:@"financaialOrderCell"];
    
    [self createHeadTabelView];
}

- (void)createHeadTabelView {
    
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
    headView.backgroundColor = [UIColor whiteColor];
    
    self.tableHeadView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FinancaialTableHeadView class]) owner:self options:nil].lastObject;
    self.tableHeadView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 170);
   // backView.backgroundColor = [UIColor colorWithRed:59.0/255.0 green:195.0/255.0 blue:60/255.0 alpha:1];
    self.tableHeadView.headImageView.layer.cornerRadius = 33;
    self.tableHeadView.headImageView.layer.masksToBounds = YES;
    [headView addSubview:self.tableHeadView];
    
    __weak typeof(self) weakSelf = self;
    self.tableHeadView.HeadViewBlock = ^{
        //账单对账
        NSLog(@"账单对账");
        OrderReconciliationViewController *vc = [[OrderReconciliationViewController alloc]init];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    
    
    UIView *yuanjiaoView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*.5-150*0.5, headView.bounds.size.height-5-50, 150, 50)];
    yuanjiaoView.layer.cornerRadius = 25;
    yuanjiaoView.backgroundColor = [UIColor whiteColor];
    yuanjiaoView.layer.shadowColor = [UIColor blackColor].CGColor;
    yuanjiaoView.layer.shadowRadius = 5;
    yuanjiaoView.layer.shadowOffset = CGSizeMake(0, 0);
    yuanjiaoView.layer.shadowOpacity = 0.4;
    [headView addSubview:yuanjiaoView];
    
    
    UILabel *yueLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 50)];
    yueLabel.text = @"余额";
    yueLabel.textColor = [UIColor colorWithRed:59.0/255.0 green:195.0/255.0 blue:60/255.0 alpha:1];
    yueLabel.font = [UIFont systemFontOfSize:13];
    yueLabel.textAlignment = NSTextAlignmentCenter;
    [yuanjiaoView addSubview:yueLabel];
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(yueLabel.bounds.size.width, 15, 0.5, 20)];
    lineView.backgroundColor = [UIColor colorWithRed:59.0/255.0 green:195.0/255.0 blue:60/255.0 alpha:1];
    [yuanjiaoView addSubview:lineView];
    
    self.yuELabel = [[UILabel alloc]initWithFrame:CGRectMake(61, 0, 150-1-60, 50)];
    self.yuELabel.textColor = [UIColor colorWithRed:59.0/255.0 green:195.0/255.0 blue:60/255.0 alpha:1];
    self.yuELabel.font = [UIFont systemFontOfSize:13];
    self.yuELabel.textAlignment = NSTextAlignmentCenter;
    [yuanjiaoView addSubview:self.yuELabel];
    
    self.tabelView.tableHeaderView = headView;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2+self.subArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.yuEArray.count;
    } else if (section == 1) {
        return self.daiJieArray.count;
    } else {
        NSMutableArray *arr = self.subArray[section-2];
        return arr.count;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
        headView.backgroundColor = [UIColor whiteColor];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 170, 40)];
        label.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
        label.text = @"最近余额流水状态";
        label.textColor = [UIColor blackColor];
        [headView addSubview:label];
        
        UIButton *lookAllButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH- 13-100, 0, 100, 40)];
        [lookAllButton setTitle:@"查看全部" forState:UIControlStateNormal];
        [lookAllButton setTitleColor:[UIColor colorWithRed:59.0/255.0 green:195.0/255.0 blue:60/255.0 alpha:1] forState:UIControlStateNormal];
        lookAllButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
        lookAllButton.contentHorizontalAlignment =  UIControlContentHorizontalAlignmentRight;
        [lookAllButton addTarget:self action:@selector(lookAllButtonACtion) forControlEvents:UIControlEventTouchUpInside];
        [headView addSubview:lookAllButton];
        return headView;
    } else if (section == 1) {
        UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)];
        headView.backgroundColor = [UIColor whiteColor];
        return headView;
    } else {
        FinancaialOrderModel *model = self.mainArray[section-2];
        
        UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
        headView.backgroundColor = [UIColor whiteColor];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, (SCREEN_WIDTH-30-13)*0.5, 30)];
        label.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        label.text = model.time_count;
        label.textColor = [UIColor blackColor];
        [headView addSubview:label];
        UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-((SCREEN_WIDTH-30-13)*0.5)-13, 0, (SCREEN_WIDTH-30-13)*0.5, 30)];
        label1.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        label1.text = [NSString stringWithFormat:@"¥%@已存入余额",model.money_count];
        label1.textAlignment = NSTextAlignmentRight;
        label1.textColor = [UIColor blackColor];
        [headView addSubview:label1];
        
        return headView;
    }
    
    

}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 0) {
        UIView *footView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
        footView.backgroundColor = [UIColor whiteColor];
        
        
        UIView *huiseView =[[UIView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 50)];
        huiseView.backgroundColor  = [UIColor colorWithRed:234.0/255.0 green:235.0/255.0 blue:236/255.0 alpha:1];
        [footView addSubview:huiseView];
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(15, 20, 3, 10)];
        lineView.backgroundColor = [UIColor colorWithRed:101.0/255.0 green:102.0/255.0 blue:103/255.0 alpha:1];
        [huiseView addSubview:lineView];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, 200, 50)];
        label.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        label.text = @"待算结账单";
        label.textColor = [UIColor blackColor];
        [huiseView addSubview:label];
        
        return footView;
    } else if (section == 1) {
        UIView *footView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
        footView.backgroundColor = [UIColor whiteColor];
        
        UIView *huiseView =[[UIView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 50)];
        huiseView.backgroundColor  = [UIColor colorWithRed:234.0/255.0 green:235.0/255.0 blue:236/255.0 alpha:1];
        [footView addSubview:huiseView];
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(15, 20, 3, 10)];
        lineView.backgroundColor = [UIColor colorWithRed:101.0/255.0 green:102.0/255.0 blue:103/255.0 alpha:1];
        [huiseView addSubview:lineView];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, 200, 50)];
        label.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        label.text = @"已算结账单";
        label.textColor = [UIColor blackColor];
        [huiseView addSubview:label];
        
        return footView;
    } else {
        UIView *footView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 15)];
        footView.backgroundColor = [UIColor whiteColor];
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 5)];
        lineView.backgroundColor = [UIColor colorWithRed:234.0/255.0 green:235.0/255.0 blue:236/255.0 alpha:1];
        [footView addSubview:lineView];
        return footView;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return 60;
    } else if (section == 1) {
        return 60;
    } else {
        return 15;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 40;
    } else if (section == 1) {
        return 10;
    } else {
        return 30;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FinancaialBalanceStateCell *stateCell = [tableView dequeueReusableCellWithIdentifier:@"financaialBalanceStateCell"];
    FinancaialOrderCell *orderCell = [tableView dequeueReusableCellWithIdentifier:@"financaialOrderCell"];
    __weak typeof(self) weakSelf = self;
    if (indexPath.section == 0) {
        
        FinancaialOrderModel *model = self.yuEArray[indexPath.row];
        [stateCell updataDataWithModel:model];
        return stateCell;
    } else if (indexPath.section == 1) {
        FinancaialOrderModel *model = self.daiJieArray[indexPath.row];
        [orderCell updataDataWithModel:model];
        
        orderCell.FinancaialOrderBlock = ^{
            NSLog(@"%@",model.time);
            OrderReconciliationDetailsViewController *VC = [[OrderReconciliationDetailsViewController alloc]init];
            VC.timeStr = model.time;
            [weakSelf.navigationController pushViewController:VC animated:YES];
        };
        return orderCell;
    } else {
        NSMutableArray *subArr = self.subArray[indexPath.section-2];
        FinancaialOrderModel *model = subArr[indexPath.row];
        [orderCell updataDataWithModel:model];
        orderCell.FinancaialOrderBlock = ^{
            OrderReconciliationDetailsViewController *VC = [[OrderReconciliationDetailsViewController alloc]init];
            VC.timeStr = model.time;
            [weakSelf.navigationController pushViewController:VC animated:YES];
        };
        return orderCell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
}


#pragma mark - 数据请求
- (void)getShopData{
    
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{
                          @"ru_id":[UserModel sharedInstanced].ru_Id,
                          @"ince":@"finance_reconciliation_home",
                          };
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",BaseUrl,@"finance_reconciliation_home"];
    [mgr POST:URL parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            [self.tableHeadView.headImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"headimg"]]] placeholderImage:[UIImage imageNamed:@""]];
            self.tableHeadView.fuzeRenLabel.text = [NSString stringWithFormat:@"财务负责人: %@",[responseObject objectForKey:@"name"]];
            self.tableHeadView.phoneNumLabel.text = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"phone"]];
            self.tableHeadView.timeLabel.text = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"zhouqi"]];
            self.yuELabel.text = [NSString stringWithFormat:@"¥%@",[responseObject objectForKey:@"price"]];
            
            self.yuEArray = [FinancaialOrderModel mj_objectArrayWithKeyValuesArray:[responseObject objectForKey:@"liushui"]];
            self.daiJieArray = [FinancaialOrderModel mj_objectArrayWithKeyValuesArray:[responseObject objectForKey:@"daijiesuan"]];
            
            self.mainArray = [FinancaialOrderModel mj_objectArrayWithKeyValuesArray:[responseObject objectForKey:@"yijiesuan"]];
            NSMutableArray *arr = [NSMutableArray array];
            for (NSInteger i=0; i<self.mainArray.count; i++) {
                FinancaialOrderModel *model = self.mainArray[i];
                arr = [FinancaialOrderModel mj_objectArrayWithKeyValuesArray:model.list];
                [self.subArray addObject:arr];
            }
            
            [self.tabelView reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        [self hidHUD:@"网络异常"];
    }];
    
}
//查看全部
- (void)lookAllButtonACtion {
    
    FinancaialBalanceAllViewController *VC= [[FinancaialBalanceAllViewController alloc]init];
    
    [self.navigationController pushViewController:VC animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
