//
//  ProcessedOrder.m
//  RRBI
//
//  Created by kyjun on 16/4/26.
//
//

#import "ProcessedOrder.h"
#import "Login.h"
#import "SDCursorView.h"
#import "ProcessedOrderChild.h"
#import "ProcessedOrderSearch.h"


#define ButtonView_Height    35


@interface ProcessedOrder ()
{
    
    UIImageView *navBarHairlineImageView;
}

@property (nonatomic, strong) ZHMenuPageScrollView *navSliderBar;
@property (nonatomic, strong) NSMutableArray *pagesArray;
@property (nonatomic, strong) NSMutableArray *dataArray;


@property(nonatomic,assign) BOOL isFirstLoad;


@end

@implementation ProcessedOrder
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!self.Identity.userInfo.isLogin){
        UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:[[Login alloc]init]];
        [self presentViewController:nav animated:YES completion:nil];
    }else{
        [self RequestData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"點吧商户";

    self.view.backgroundColor = [UIColor whiteColor];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_search"] style:UIBarButtonItemStylePlain target:self action:@selector(searchButtonClick)];
    
//    [self RequestData];
    
    self.isFirstLoad = YES;
    
}
-(void)searchButtonClick{
    
    ProcessedOrderSearch *seachVC = [[ProcessedOrderSearch alloc] init];
    [self.navigationController pushViewController:seachVC animated:YES];
}
- (void)setUpNavSliderBar {
    self.navSliderBar.pageMenus = self.pagesArray;
    [self.navSliderBar updateSelectedPage];
}

- (ZHMenuPageScrollView *)navSliderBar {
    if (!_navSliderBar) {
        NSMutableArray *contrors = [NSMutableArray array];
        for (NSDictionary *dic in self.dataArray) {
            ProcessedOrderChild *controller = [[ProcessedOrderChild alloc]init];
            controller.pass_id = [NSString stringWithFormat:@"%@",dic[@"status"]];
            [contrors addObject:controller];
        }
        _navSliderBar = [[ZHMenuPageScrollView alloc]initWithParentController:self MenusPages:contrors isShowLine:YES];
        _navSliderBar.menuPageControllers = [contrors copy];
        [self.view addSubview:_navSliderBar];
        [_navSliderBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.view);
            make.width.equalTo(self.view);
            make.height.mas_equalTo(40);
            make.top.equalTo(self.view).offset(0);
        }];
        MASAttachKeys(_navSliderBar);
    }
    return _navSliderBar;
}


- (void)RequestData {
    
    self.dataArray = [NSMutableArray array];
    self.pagesArray = [NSMutableArray array];
    
    [self showHUD];
    NSDictionary* arg = @{@"ince":@"get_order_status",@"sid":[UserModel sharedInstanced].ru_Id, @"uid":self.Identity.userInfo.userID};
    [NetRepositories netConfirm:arg complete:^(EnumNetResponse react, NSDictionary *response, NSString *message) {
        NSLog(@"===%@",response);
        if(react == NetResponseSuccess){
            [self hidHUD];
            
            self.dataArray = response[@"data"];
            for (NSDictionary *dic in self.dataArray) {
                [self.pagesArray addObject:dic[@"value"]];
            }
            
            if (self.pagesArray.count > 0) {
                
                if (self.isFirstLoad) {
                    [self setUpNavSliderBar];
                    self.isFirstLoad = NO;
                }else{

                    [self.navSliderBar refreshPage];
                    if (_navSliderBar.menuPageControllers.count >0) {
                        for (ProcessedOrderChild *controller in _navSliderBar.menuPageControllers) {
                            [controller refreshDataSource];
                        }
                    }
                  

                }
                
            }
            
            
            
        }else if (react == NetResponseFail){
            [self hidHUD:message];
        }else{
            [self hidHUD:message];
        }
    }];
    
}


@end
