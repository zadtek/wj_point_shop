//
//  OrderManagementViewController.h
//  rrbi
//
//  Created by MyMac on 2019/5/16.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderManagementViewController : BaseViewController

@property (nonatomic, strong) NSString *startStr;
@property (nonatomic, strong) NSString *endStr;

@end

NS_ASSUME_NONNULL_END
