//
//  OrderManagementModel.h
//  rrbi
//
//  Created by MyMac on 2019/5/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderManagementModel : NSObject

@property (nonatomic, copy) NSString *status_txt;
@property (nonatomic, copy) NSString *add_time;
@property (nonatomic, copy) NSString *order_sn;
@property (nonatomic, copy) NSString *day_sn;
@property (nonatomic, copy) NSString *order_id;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *order_profits;
@property (nonatomic, copy) NSString *address1;
@property (nonatomic, copy) NSString *consignee;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, strong) NSArray *delivery;//phone     realname    count  骑手信息数组
@property (nonatomic, copy) NSString *pj_status;//1显示评价2不显示评价
//顶部d菜单
@property (nonatomic, copy) NSString *reminder_num;
@property (nonatomic, copy) NSString *value;

@end

NS_ASSUME_NONNULL_END
