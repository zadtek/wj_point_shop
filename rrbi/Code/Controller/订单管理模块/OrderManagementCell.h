//
//  OrderManagementCell.h
//  rrbi
//
//  Created by MyMac on 2019/5/16.
//

#import <UIKit/UIKit.h>
#import "OrderManagementModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface OrderManagementCell : UITableViewCell

@property (nonatomic, copy) NSString *emp_tel;
@property (nonatomic, copy) NSString *user_tel;

@property (weak, nonatomic) IBOutlet UIButton *quPingJiaButton;
@property (weak, nonatomic) IBOutlet UIButton *xiangQingButton;
@property (weak, nonatomic) IBOutlet UILabel *xiadanTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dingDanBianhaoLable;
@property (weak, nonatomic) IBOutlet UILabel *liuShuiHaoLabel;
@property (weak, nonatomic) IBOutlet UILabel *yujiShouRuLabel;
@property (weak, nonatomic) IBOutlet UILabel *gukeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *gukeShouJiHaoLabel;
@property (weak, nonatomic) IBOutlet UILabel *qiShouNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *qiShouDianHuaLabel;
@property (weak, nonatomic) IBOutlet UILabel *lianxiGuKeLabel;
@property (weak, nonatomic) IBOutlet UILabel *gukeDiZhiLabel;
@property (weak, nonatomic) IBOutlet UILabel *lianxiQiShouLabel;
@property (weak, nonatomic) IBOutlet UIButton *lianxiQiShouButton;

@property (weak, nonatomic) IBOutlet UILabel *dingDanStateLabel;
- (void)updataDataWithModel:(OrderManagementModel *)model;

@property (nonatomic, copy) void (^xianQingButtonBlock)();
@property (nonatomic, copy) void (^evaluationButtonBlock)();

@end

NS_ASSUME_NONNULL_END
