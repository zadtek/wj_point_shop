//
//  OrderManagementCell.m
//  rrbi
//
//  Created by MyMac on 2019/5/16.
//

#import "OrderManagementCell.h"

@implementation OrderManagementCell


- (void)updataDataWithModel:(OrderManagementModel *)model {
    
    self.dingDanStateLabel.text = [NSString stringWithFormat:@"%@",model.status_txt];
    self.xiadanTimeLabel.text = [NSString stringWithFormat:@"%@",model.add_time];
    self.dingDanBianhaoLable.text = [NSString stringWithFormat:@"%@",model.order_sn];
    self.liuShuiHaoLabel.text = [NSString stringWithFormat:@"#%@",model.day_sn];
    self.yujiShouRuLabel.text = [NSString stringWithFormat:@"¥%@",model.order_profits];
    self.gukeNameLabel.text = [NSString stringWithFormat:@"%@",model.consignee];
    self.gukeShouJiHaoLabel.text = [NSString stringWithFormat:@"%@",model.mobile];
    self.gukeDiZhiLabel.text = [NSString stringWithFormat:@"%@",model.address1];
    self.user_tel =  [NSString stringWithFormat:@"%@",model.mobile];
    
    if (model.delivery.count <= 0) {
        self.qiShouNameLabel.hidden = YES;
        self.qiShouDianHuaLabel.hidden = YES;
        self.lianxiQiShouLabel.hidden = YES;
        self.lianxiQiShouButton.hidden = YES;
    } else {
        self.qiShouNameLabel.hidden = NO;
        self.qiShouDianHuaLabel.hidden = NO;
        self.lianxiQiShouLabel.hidden = NO;
         self.lianxiQiShouButton.hidden = NO;
        self.qiShouNameLabel.text = [NSString stringWithFormat:@"%@",model.delivery[0][@"realname"]];
        self.qiShouDianHuaLabel.text = [NSString stringWithFormat:@"%@",model.delivery[0][@"phone"]];
        self.emp_tel = [NSString stringWithFormat:@"%@",model.delivery[0][@"phone"]];
    }
    
    if ([model.pj_status isEqualToString:@"1"]) {//已完成
        self.quPingJiaButton.hidden = NO;
    } else {
        self.quPingJiaButton.hidden = YES;
    }
    
    
}
- (IBAction)xiangQingButtonAction:(id)sender {
    if (self.xianQingButtonBlock) {
        self.xianQingButtonBlock();
    }
}
- (IBAction)quPingJiaButtonAction:(id)sender {
    if (self.evaluationButtonBlock) {
        self.evaluationButtonBlock();
    }
}
- (IBAction)lianXiQiShouButtonAction:(id)sender {
    
    NSMutableString * string = [[NSMutableString alloc] initWithFormat:@"tel:%@",self.emp_tel];
    UIWebView * callWebview = [[UIWebView alloc] init];
    [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:string]]];
    [self addSubview:callWebview];
}
- (IBAction)lianXiGuKeButtonAction:(id)sender {
    
    NSMutableString * string = [[NSMutableString alloc] initWithFormat:@"tel:%@",self.user_tel];
    UIWebView * callWebview = [[UIWebView alloc] init];
    [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:string]]];
    [self addSubview:callWebview];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.quPingJiaButton.layer.borderWidth = 1;
    self.quPingJiaButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.quPingJiaButton.layer.cornerRadius = 5;
    self.quPingJiaButton.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
