//
//  StroeMessageViewController.m
//  rrbi
//
//  Created by mac book on 2018/12/14.
//

#import "StroeMessageViewController.h"
#import "MStore.h"
#import "POISearchViewController.h"



#define FirstView_Height    50/HEIGHT_6S_SCALE
#define SecondView_Height    80/HEIGHT_6S_SCALE

#define FooterButton_Height    80/HEIGHT_6S_SCALE


@interface StroeMessageViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property(nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *dataArray;

@property(nonatomic,strong)UITextField *codeTF;
@property(nonatomic,strong)UITextField *personTF;

@property(nonatomic,strong)UITextField *shopNameTF;
@property(nonatomic,strong)UITextField *shopAddressTF;
@property(nonatomic,strong)UITextField *shopPhoneTF;

@property(nonatomic,strong)UITextField *accountTF;

@property(nonatomic,strong)UITextField *openBankTF;
@property(nonatomic,strong)UITextField *openPersonTF;
@property(nonatomic,strong)UITextField *locationTF;


@property(nonatomic,strong)UIView *logoView;
@property(nonatomic,strong)UIView *logoZhaoView;
@property(nonatomic,strong)UIView *businessView;
@property(nonatomic,strong)UIView *licenceView;



@property(nonatomic,strong)UIImageView *logoImageView;
@property(nonatomic,strong)UIImageView *logoZhaoImageView;
@property(nonatomic,strong)UIImageView *businessImageView;
@property(nonatomic,strong)UIImageView *licenceImageView;


@property(nonatomic,strong)NSString *logoImageStr;
@property(nonatomic,strong)NSString *logoZhaoStr;
@property(nonatomic,strong)NSString *businessStr;
@property(nonatomic,strong)NSString *licenceStr;


@property(nonatomic,strong) UIButton *commitbtn;

@property(nonatomic,strong) MStore *storeModel;
@property(nonatomic,strong) NSMutableDictionary *dataDic;
@property(nonatomic,assign) NSInteger selectedIndex;

@end

@implementation StroeMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.navigationItem.title =  @"门店管理";
    
    [self creatSubViews];
    
    
}
-(void)creatSubViews{
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,  CGRectGetHeight(self.view.frame) -NAVGATIONBAR_HEIGHT) style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.tableFooterView = [self creatFooterView];
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:self.tableView];
    
    if (@available(iOS 11.0, *))
    {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
    }
    
    

    [self getData];
}


- (void)getData{
    
    //"ince":"getshopinfo","sid":"101","rrbl_shop_id":"101"

    [self showprogressHUD];
    NSDictionary* arg = @{@"ince":@"getshopinfo",@"shop_id":[UserModel sharedInstanced].ru_Id};
    [NetRepositories netConfirm:arg complete:^(EnumNetResponse react, NSDictionary *response, NSString *message) {
        if(react == NetResponseSuccess){
            [self hiddenProgressHUD];

            NSDictionary *dic = response[@"data"];
            self.storeModel = [MStore InitStoreWithJsonData:dic];
            
            [self initTalbeViewSubview];
            [self.tableView reloadData];

        }else if (react == NetResponseFail){
            [self hiddenProgressHUD];
        }else{
            [self hiddenProgressHUD];
        }
    }];
    
    
}

-(void)initTalbeViewSubview{
    
    float textWidth =  220/WIDTH_6S_SCALE;
    float textX = SCREEN_WIDTH - textWidth - 10;
    float textHeigth = FirstView_Height;
    
    
    
    _codeTF = [[UITextField alloc] initWithFrame:CGRectMake(textX, 0, textWidth, textHeigth)];
    _codeTF.returnKeyType = UIReturnKeyDone;
    [_codeTF setFont:kFontNameSize(14)];
    //_codeTF.placeholder = @"请输入业务员代码";
    _codeTF.delegate = self;
    _codeTF.textAlignment = NSTextAlignmentLeft;
    _codeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    
    _personTF = [[UITextField alloc] initWithFrame:CGRectMake(textX, 0, textWidth, textHeigth)];
    _personTF.returnKeyType = UIReturnKeyDone;
    [_personTF setFont:kFontNameSize(14)];
    //_personTF.placeholder=@"请输入负责人";
    _personTF.delegate = self;
    _personTF.textAlignment = NSTextAlignmentLeft;
    _personTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    

    _shopNameTF = [[UITextField alloc] initWithFrame:CGRectMake(textX, 0, textWidth, textHeigth)];
    _shopNameTF.returnKeyType = UIReturnKeyDone;
    [_shopNameTF setFont:kFontNameSize(14)];
    //_shopNameTF.placeholder=@"请输入店铺名称";
    _shopNameTF.delegate = self;
    _shopNameTF.textAlignment = NSTextAlignmentLeft;
    _shopNameTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    
    _shopAddressTF = [[UITextField alloc] initWithFrame:CGRectMake(textX, 0, textWidth, textHeigth)];
    _shopAddressTF.returnKeyType = UIReturnKeyDone;
    [_shopAddressTF setFont:kFontNameSize(14)];
    //_shopAddressTF.placeholder=@"店铺地址";
    _shopAddressTF.delegate = self;
    _shopAddressTF.textAlignment = NSTextAlignmentLeft;
    _shopAddressTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    
    _shopPhoneTF = [[UITextField alloc] initWithFrame:CGRectMake(textX, 0, textWidth, textHeigth)];
    _shopPhoneTF.returnKeyType = UIReturnKeyDone;
    [_shopPhoneTF setFont:kFontNameSize(14)];
    //_shopPhoneTF.placeholder=@"店铺电话";
    _shopPhoneTF.delegate = self;
    _shopPhoneTF.textAlignment = NSTextAlignmentLeft;
    _shopPhoneTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    

    
    
    _accountTF = [[UITextField alloc] initWithFrame:CGRectMake(textX, 0, textWidth, textHeigth)];
    [_accountTF setFont:kFontNameSize(14)];
    //_accountTF.placeholder=@"请输入提现账号";
    _accountTF.delegate = self;
    _accountTF.textAlignment = NSTextAlignmentLeft;
    _accountTF.keyboardType = UIKeyboardTypeNumberPad;
    _accountTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    
 
    
    _openBankTF = [[UITextField alloc] initWithFrame:CGRectMake(textX, 0, textWidth, textHeigth)];
    [_openBankTF setFont:kFontNameSize(14)];
    //_openBankTF.placeholder=@"请输入开户行";
    _openBankTF.delegate = self;
    _openBankTF.textAlignment = NSTextAlignmentLeft;
    _openBankTF.clearButtonMode = UITextFieldViewModeWhileEditing;




    _openPersonTF = [[UITextField alloc] initWithFrame:CGRectMake(textX, 0, textWidth, textHeigth)];
    [_openPersonTF setFont:kFontNameSize(14)];
    //_openPersonTF.placeholder=@"请输入开户人姓名";
    _openPersonTF.delegate = self;
    _openPersonTF.textAlignment = NSTextAlignmentLeft;
    _openPersonTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    
    
    
    self.locationTF = [[UITextField alloc] initWithFrame:CGRectMake(textX, 0, 80, textHeigth)];
    self.locationTF.placeholder = @"点击定位";
    [self.locationTF setFont:kFontNameSize(14)];
    self.locationTF.delegate = self;
    self.locationTF.textAlignment = NSTextAlignmentLeft;
    self.locationTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    UIImageView *leftImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_dingwei_black"]];
    leftImage.frame =CGRectMake(0, 0, 20, 20);
    self.locationTF.leftViewMode = UITextFieldViewModeAlways;
    self.locationTF.leftView = leftImage;
    self.locationTF.userInteractionEnabled = NO;

    
    
    
    _logoView = [[UIView alloc] initWithFrame:CGRectMake(textX, 0, textWidth,SecondView_Height)];
    
    float arrow_wh = 20;
    float image_wh = SecondView_Height - 10*2;
    
    float image_x = CGRectGetWidth(_logoView.frame) - arrow_wh ;
    float image_y = (CGRectGetHeight(_logoView.frame) - arrow_wh)/2 ;
    
    UIImageView *arrow1 = [[UIImageView alloc] initWithFrame:CGRectMake(image_x,image_y, arrow_wh, arrow_wh)];
    arrow1.image = [UIImage imageNamed:@"icon_arrowright"];
    
    if (self.showtType != 1) {
        [self.logoView addSubview:arrow1];
    }

    float logo_x = CGRectGetMinX(arrow1.frame) - image_wh - 10;
    _logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(logo_x, 10, image_wh, image_wh)];
//    _logoImageView.backgroundColor = [UIColor yellowColor];
    [self.logoView addSubview:_logoImageView];
    
    
    
    
    _logoZhaoView = [[UIView alloc] initWithFrame:CGRectMake(textX, 0, textWidth,SecondView_Height)];
    UIImageView *arrow2 = [[UIImageView alloc] initWithFrame:CGRectMake(image_x,image_y, arrow_wh, arrow_wh)];
    arrow2.image = [UIImage imageNamed:@"icon_arrowright"];
    
    if (self.showtType != 1) {
        [self.logoZhaoView addSubview:arrow2];
    }
    
    
    _logoZhaoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(logo_x, 10, image_wh, image_wh)];
//    _logoZhaoImageView.backgroundColor = [UIColor yellowColor];
    [self.logoZhaoView addSubview:_logoZhaoImageView];
    
    
    
    
    _businessView = [[UIView alloc] initWithFrame:CGRectMake(textX, 0, textWidth,SecondView_Height)];
    UIImageView *arrow3 = [[UIImageView alloc] initWithFrame:CGRectMake(image_x,image_y, arrow_wh, arrow_wh)];
    arrow3.image = [UIImage imageNamed:@"icon_arrowright"];
    
    if (self.showtType != 1) {
        [self.businessView addSubview:arrow3];
    }
    
    _businessImageView = [[UIImageView alloc] initWithFrame:CGRectMake(logo_x, 10, image_wh, image_wh)];
//    _businessImageView.backgroundColor = [UIColor yellowColor];
    [self.businessView addSubview:_businessImageView];
    
    
    
    
    _licenceView = [[UIView alloc] initWithFrame:CGRectMake(textX, 0, textWidth,SecondView_Height)];
    UIImageView *arrow4 = [[UIImageView alloc] initWithFrame:CGRectMake(image_x,image_y, arrow_wh, arrow_wh)];
    arrow4.image = [UIImage imageNamed:@"icon_arrowright"];
    
    if (self.showtType != 1) {
        [self.licenceView addSubview:arrow4];
    }
    
    _licenceImageView = [[UIImageView alloc] initWithFrame:CGRectMake(logo_x, 10, image_wh, image_wh)];
//    _licenceImageView.backgroundColor = [UIColor yellowColor];
    [self.licenceView addSubview:_licenceImageView];
    
    
    _codeTF.textAlignment = NSTextAlignmentRight;
    _personTF.textAlignment = NSTextAlignmentRight;
    _shopNameTF.textAlignment = NSTextAlignmentRight;
    _shopAddressTF.textAlignment = NSTextAlignmentRight;
    _shopPhoneTF.textAlignment = NSTextAlignmentRight;
    _accountTF.textAlignment = NSTextAlignmentRight;
    _openBankTF.textAlignment = NSTextAlignmentRight;
    _locationTF.textAlignment = NSTextAlignmentRight;
    
    
    
    
    _codeTF.text = self.storeModel.agent_code;
    _personTF.text = self.storeModel.shop_owner;
    _shopNameTF.text = self.storeModel.shop_name;
    _shopAddressTF.text = self.storeModel.shop_address;
    _shopPhoneTF.text = self.storeModel.shop_tel;
    _accountTF.text = self.storeModel.bank_num;
    _openBankTF.text = [NSString stringWithFormat:@"%@km",self.storeModel.shop_range];//配送范围
    _openPersonTF.text = self.storeModel.bank_host;
    _codeTF.userInteractionEnabled = NO;
    _personTF.userInteractionEnabled = NO;
    _shopNameTF.userInteractionEnabled = NO;
    _shopAddressTF.userInteractionEnabled = NO;
    _shopPhoneTF.userInteractionEnabled = NO;
    _accountTF.userInteractionEnabled = NO;
    _openBankTF.userInteractionEnabled = NO;
    _openPersonTF.userInteractionEnabled = NO;
    
    
    
    [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:self.storeModel.shop_logo] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.logoZhaoImageView sd_setImageWithURL:[NSURL URLWithString:self.storeModel.shop_back] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.businessImageView sd_setImageWithURL:[NSURL URLWithString:self.storeModel.license] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.licenceImageView sd_setImageWithURL:[NSURL URLWithString:self.storeModel.other_cert] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    
    self.dataDic = [NSMutableDictionary dictionary];
    [self.dataDic setObject :self.storeModel.latitude forKey:@"poi_latitude"];
    [self.dataDic setObject :self.storeModel.longitude forKey:@"poi_longitude"];
                                               
}


#pragma mark 提交按钮
-(UIView *)creatFooterView{
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, FooterButton_Height)];
    
    self.commitbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.commitbtn addTarget:self action:@selector(commitBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    self.commitbtn.layer.masksToBounds = YES;
    self.commitbtn.layer.cornerRadius = 2;
    self.commitbtn.titleLabel.font= kFontNameSize(16);
    [footerView addSubview:self.commitbtn];
    if (self.showtType == 1) {
        [self.commitbtn setTitle:@"联系业务经理" forState:UIControlStateNormal];
    }else{
        [self.commitbtn setTitle:@"联系业务经理" forState:UIControlStateNormal];
    }
    
    self.commitbtn.backgroundColor=  [UIColor colorWithRed:215.0/255.0 green:96.0/255.0 blue:3.0/255.0 alpha:1];
    [self.commitbtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.commitbtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(FooterButton_Height/2);
        make.left.mas_equalTo(footerView).mas_offset(40);
        make.right.mas_equalTo(footerView).mas_offset(-40);
        make.centerY.mas_equalTo(footerView);
    }];
    
    
    return footerView;
}



#pragma mark -  UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

//tableView代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 12;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"CELL_ID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType  = UITableViewCellAccessoryNone;

    cell.textLabel.font = kFontNameSize(14);
    cell.textLabel.textColor = [UIColor grayColor];
    if (cell.contentView.subviews.count > 0) {
        
        for (UIView *views in cell.contentView.subviews) {
            
            [views removeFromSuperview];
        }
    }
    
    if (self.showtType == 1) {///只做信息展示不能编辑
        cell.userInteractionEnabled = NO;
    }
    
    
    if (indexPath.row == 0){
        
        cell.textLabel.text = @"业务员代码";
        cell.accessoryView = _codeTF;
        
    }else if (indexPath.row == 1){
        
        cell.textLabel.text = @"负责人";
        cell.accessoryView = _personTF;
        
    }else if (indexPath.row == 2){
        
        cell.textLabel.text = @"店名";
        cell.accessoryView = _shopNameTF;
        
    }else if (indexPath.row == 3){
        
        cell.textLabel.text = @"店址";
        cell.accessoryView = _shopAddressTF;
        
    }else if (indexPath.row == 4){
        
        cell.textLabel.text = @"店铺电话";
        cell.accessoryView = _shopPhoneTF;
        
    }else if (indexPath.row == 5){
        cell.textLabel.text = @"地图定位";
        cell.accessoryView = _locationTF;
    }else if (indexPath.row == 6){
        
        cell.textLabel.text = @"配送范围";
        cell.accessoryView = _openBankTF;
        
    } else if (indexPath.row == 7){
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 13)];
        lineView.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1];
        [cell.contentView addSubview:lineView];
    } else if (indexPath.row == 8){
        
        cell.textLabel.text = @"店铺logo";
        cell.accessoryView = _logoView;
        
    }else if (indexPath.row == 9){
        
        cell.textLabel.text = @"店招";
        cell.accessoryView = _logoZhaoView;
        
    } else if (indexPath.row == 10){
        
        cell.textLabel.text = @"营业许可证";
        cell.accessoryView = _businessView;
        
    }else {
        
        cell.textLabel.text = @"特种许可证";
        cell.accessoryView = _licenceView;
        
    }
    
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    return;
    if (self.showtType == 1) {///只做信息展示，不能编辑
        return;
    }
    
    
    if (indexPath.row == 8) {
        
        POISearchViewController *vc = [[POISearchViewController alloc] init];
        
        vc.title = @"选择地址";
        __weak typeof(self) weakSelf = self;
        
        vc.returnPoiData = ^(NSDictionary *dic) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            
            strongSelf.dataDic = [NSMutableDictionary dictionaryWithDictionary:dic];
            
            strongSelf.locationTF.text =dic[@"poi_address"];

        };
        
        
        [self.navigationController pushViewController:vc animated:YES];
        
        
    }
    if (indexPath.row > 8) {
        
        self.selectedIndex = indexPath.row;
        
        [self updateAvatarImage];
        
    }
    
}




-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    if (indexPath.row >= 8) {
        
        return SecondView_Height;
        
    }else if (indexPath.row == 7) {
        
        return 13;
    } else {
        return FirstView_Height;
    }
    
}

#pragma mark -  UITextField
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

//-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    [self.view endEditing:YES];
//}

//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    [self.view endEditing:YES];
//
//}



#pragma mark - 点击提交评价按钮
-(void)commitBtnAction:(UIButton *)button{
    
    
    
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{@"ince":@"get_shop_info",@"sid":[UserModel sharedInstanced].ru_Id};
    [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            NSDictionary* dict = [responseObject objectForKey:@"data"];
            
            NSMutableString * string = [[NSMutableString alloc] initWithFormat:@"tel:%@",dict[@"tel"]];
            UIWebView * callWebview = [[UIWebView alloc] init];
            [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:string]]];
            [self.view addSubview:callWebview];
            
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        [self hidHUD:@"网络异常"];
    }];
    
    
    
    
//
//    if (self.showtType == 1) {
//        ShowMessage(@"请联系业务更改");
//        return;
//    }
//
//
//    if (self.codeTF.text.length == 0) {
//        ShowMessage(@"请填写业务员代码");
//        return;
//    }
//    if (self.personTF.text.length == 0) {
//        ShowMessage(@"请填写负责人");
//        return;
//    }
//
//    if (self.shopNameTF.text.length == 0) {
//        ShowMessage(@"请填写店铺名称");
//        return;
//    }
//
//    if (self.shopAddressTF.text.length == 0) {
//        ShowMessage(@"请填写店铺地址");
//        return;
//    }
//
//    if (self.shopPhoneTF.text.length == 0) {
//        ShowMessage(@"请填写店铺电话");
//        return;
//    }
//
//    if (self.accountTF.text.length == 0) {
//        ShowMessage(@"请填写店提现账号");
//        return;
//    }
//
//
//    if (self.openBankTF.text.length == 0) {
//        ShowMessage(@"请填写开户行");
//        return;
//    }
//
//
//    if (self.openPersonTF.text.length == 0) {
//        ShowMessage(@"请填写开户人姓名");
//        return;
//    }
//
//    [self commitData];
    
}


#pragma mark - 上传头像
- (void)updateAvatarImage
{
    
    self.navigationController.navigationBar.hidden = NO;
    UIAlertController *alertSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.navigationBar.translucent = NO;
    imagePickerController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.allowsEditing = YES;
    
    // 判断是否支持相机
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            // 设置数据源
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }];
        [alertSheet addAction:cameraAction];
    }
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"从手机相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }];
        [alertSheet addAction:photoAction];
    }
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alertSheet addAction:cancelAction];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertSheet animated:YES completion:nil];
    });
    
    
}



#pragma mark UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"])        // 被选中的图片
    {
        // 获取照片
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        [picker dismissViewControllerAnimated:YES completion:nil];          // 隐藏视图
        
        [self commitUserAvatarDataWithImage:image];
    }
}

#pragma mark - 提交用户头像
-(void)commitUserAvatarDataWithImage:(UIImage *)image{
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);

    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setValue:@"arr" forKey:@"ince"];
    [dic setValue:[UserModel sharedInstanced].ru_Id forKey:@"sid"];
    
    [NetRepositories requestAFURL:dic imageData:imageData complete:^(EnumNetResponse react, NSDictionary *response, NSString *message) {
        
         NSLog(@"response = %@",response);
        ShowMessage(response[@"msg"]);
        
//        NSString *original = response[@"original_img"];
//        NSString *url = response[@"url"];
//        NSString *goods = response[@"goods_img"];
        
        if (self.selectedIndex == 9) {
            
            self.logoImageView.image = image;
            self.logoImageStr = response[@"url"];
            self.storeModel.shop_logo =self.logoImageStr ;
            
        }else if (self.selectedIndex == 10){
            
            self.logoZhaoImageView.image = image;
            self.logoZhaoStr = response[@"url"];
            self.storeModel.shop_back =self.logoZhaoStr ;

            
        }else if (self.selectedIndex == 10){
            
            self.businessImageView.image = image;
            self.businessStr = response[@"url"];
            self.storeModel.license =self.businessStr ;

            
        }else{
            
            self.licenceImageView.image = image;
            self.licenceStr = response[@"url"];
            self.storeModel.other_cert =self.licenceStr ;

        }

        
    }];
    
}




-(void)commitData{
    
//    ince":"openshop",
//    "ru_id":"101","token":"101",
//    "agent_code":"1",
//    "shop_name":"\u6b63\u5b97\u4e5d\u9f99\u5305",
//    "shop_tel":"6227936",
//    "shop_back":"http:\/\/xmshop.zadtek.com\/Public\/Uploads\/thumb\/1537756858.img",
//    "other_cert":"http:\/\/xmshop.zadtek.com\/Public\/Uploads\/thumb\/1544772138.img",
//    "bank_name":"\u4e2d\u56fd\u94f6\u884c",
//    "is_update":"1",
//    "shop_owner":"\u9648\u5148\u5e73",
//    "shop_addr":"\u8fbd\u6cb3\u5927\u88574\u53f7",
//    "shop_logo":"http:\/\/xmshop.zadtek.com\/Public\/Uploads\/thumb\/1537756849.img",
//    "license":"http:\/\/xmshop.zadtek.com\/Public\/Uploads\/thumb\/1537756862.img",
//    "bank_num":"6217900400000284460",
//    "bank_host":"\u9648\u5148\u5e73",
//    "lat_lnt":"41.677287,123.465009"
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"openshop" forKey:@"ince"];
    [dic setValue:[UserModel sharedInstanced].ru_Id forKey:@"ru_id"];
    
    [dic setValue:self.codeTF.text forKey:@"agent_code"];
    [dic setValue:self.personTF.text forKey:@"shop_owner"];
    [dic setValue:self.shopNameTF.text forKey:@"shop_name"];
    [dic setValue:self.shopAddressTF.text forKey:@"shop_addr"];
    [dic setValue:self.shopPhoneTF.text forKey:@"shop_tel"];
    [dic setValue:@"1" forKey:@"is_update"];

    [dic setValue:self.accountTF.text forKey:@"bank_num"];
    [dic setValue:self.openBankTF.text forKey:@"bank_name"];
    [dic setValue:self.openPersonTF.text forKey:@"bank_host"];

    
    [dic setValue:self.storeModel.shop_logo forKey:@"shop_logo"];
    [dic setValue:self.storeModel.shop_back forKey:@"shop_back"];
    [dic setValue:self.storeModel.license forKey:@"license"];
    [dic setValue:self.storeModel.other_cert forKey:@"other_cert"];

    
    NSArray *arr = @[self.dataDic[@"poi_latitude"],self.dataDic[@"poi_longitude"]];
    NSString *string = [arr componentsJoinedByString:@","];//,为分隔
    [dic setValue:string forKey:@"lat_lnt"];
    
    
    NSLog(@"dic = %@",dic);
    [NetRepositories netConfirm:dic complete:^(EnumNetResponse react, NSDictionary *response, NSString *message) {
        NSLog(@"response = %@",response);
        ShowMessage(response[@"msg"]);
        [self performSelector:@selector(popView) withObject:nil afterDelay:1.5];
        
    }];
    
    
}
-(void)popView{
    [self.navigationController popViewControllerAnimated:YES];
}


//限制输入
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
//    if (textField == self.accountTF) {
//        if (string.length == 0){
//            return YES;
//        }else{
//            NSInteger existedLength = textField.text.length;
//            NSInteger selectedLength = range.length;
//            NSInteger replaceLength = string.length;
//            if (existedLength - selectedLength + replaceLength > 11) {
//                return NO;
//            }
//        }
//
//    }
//
    
    NSMutableString *str = [[NSMutableString alloc] initWithString:textField.text];
    [str insertString:string atIndex:range.location];
    
    if (textField == self.accountTF) {
        
        if (string.length != 0 ){
            
            //限制输入
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
            NSString*filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            BOOL basicTest = [string isEqualToString:filtered];
            if (!basicTest) {
                return NO;
            }else{
                return YES;
            }
            
        }
        
        
    }
    
    return YES;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
