//
//  ReceivingOrderViewController.m
//  rrbi
//
//  Created by mac book on 2019/3/8.
//

#import "ReceivingOrderViewController.h"
#import "LinkNumberViewController.h"


@interface ReceivingOrderViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataArray;

@property(nonatomic,strong)UISwitch  *receiveSwitch;


@end

@implementation ReceivingOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.title = @"接单设置";
    
    [self creatSubViews];

}

-(void)creatSubViews{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,  CGRectGetHeight(self.view.frame) -NAVGATIONBAR_HEIGHT) style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.view addSubview:self.tableView];
    
    if (@available(iOS 11.0, *))
    {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
    }
    
    self.receiveSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 50, 10, 50, 40)];
    // 当控件值变化时触发changeColor方法
    [self.receiveSwitch addTarget:self action:@selector(changeReserveStatus:) forControlEvents:UIControlEventValueChanged];
    self.receiveSwitch.backgroundColor = [UIColor lightGrayColor];
    self.receiveSwitch.tintColor = [UIColor lightGrayColor];
    self.receiveSwitch.layer.masksToBounds = YES;
    self.receiveSwitch.layer.cornerRadius = self.receiveSwitch.bounds.size.height/2.0;
    
    
    if ([self.Identity.userInfo.confirm_order_way isEqualToString:@"0"]) {
        [self.receiveSwitch setOn:NO];

    }else{
        [self.receiveSwitch setOn:YES];

    }
    
}
-(void)changeReserveStatus:(UISwitch *)swi{
    if(swi.isOn){
        
        [self changeMessageWithValue:@"1" withField:@"confirm_order_way"];

    }else{
        [self changeMessageWithValue:@"0" withField:@"confirm_order_way"];
    }
}



#pragma mark - ******* tableview代理方法 *******
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *cellID = @"CELL_ID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1  reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
        if (self.isJieDanSet) {
            cell.textLabel.text = @"自动接单";
            cell.accessoryView = self.receiveSwitch;
        } else {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.text = @"打印联数";
            if (self.Identity.userInfo.print_num == nil) {
                cell.detailTextLabel.text = [NSString stringWithFormat:@"0联"];
            } else {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@联",self.Identity.userInfo.print_num];
            }
        }
    
    return cell;
    
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

        if (!self.isJieDanSet) {
        LinkNumberViewController* controller = [[LinkNumberViewController alloc] init];
        [self.navigationController pushViewController:controller animated:YES];
        controller.numberBlock = ^(NSString * _Nonnull number) {
            
            [self changeMessageWithValue:number withField:@"print_num"];

            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@联",number];
        };
        }
    
}

-(void)changeMessageWithValue:(NSString *)value withField:(NSString *)fieldname{
    
    [self showHUD];
    NSDictionary* arg = @{
                          @"ince":@"edit_shop_info",
                          @"sid":[UserModel sharedInstanced].ru_Id,
                          @"value":value,
                          @"fieldname":fieldname
                          };
    [NetRepositories netConfirm:arg complete:^(EnumNetResponse react, NSDictionary *response, NSString *message) {
        if(react == NetResponseSuccess){
            [self hidHUD:message];
        }else if (react == NetResponseFail){
            [self hidHUD:message];
        }else{
            [self hidHUD:message];
        }
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
