//
//  LinkNumberViewController.h
//  rrbi
//
//  Created by mac book on 2019/3/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LinkNumberViewController : BaseViewController

@property (copy, nonatomic) void(^numberBlock)(NSString *number);
@end

NS_ASSUME_NONNULL_END
