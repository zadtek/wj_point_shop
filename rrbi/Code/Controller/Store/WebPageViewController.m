//
//  WebPageViewController.m
//  TheBankPro
//
//  Created by 杜文杰 on 2017/9/7.
//  Copyright © 2017年 dwj. All rights reserved.
//

#import "WebPageViewController.h"
#import <WebKit/WebKit.h>


@interface WebPageViewController ()<WKNavigationDelegate,UIScrollViewDelegate,WKUIDelegate,WKScriptMessageHandler>

@property (nonatomic, strong) WKWebView *webView;

@end

@implementation WebPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.titleName;

    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.preferences = [[WKPreferences alloc] init];
    config.preferences.minimumFontSize = 10;
    config.preferences.javaScriptEnabled = YES;
    config.preferences.javaScriptCanOpenWindowsAutomatically = NO;
    config.userContentController = [[WKUserContentController alloc] init];
    config.processPool = [[WKProcessPool alloc] init];
    self.webView = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) configuration:config];
    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;
    [self.view addSubview:self.webView];
     [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_webPageUrlStr]]];
    
    
    // **************** 此处划重点 **************** //
     [config.userContentController addScriptMessageHandler:self name:@"gotoValuePage"];

}
-(void)reload{
    
     [self.webView reload];
}
#pragma mark - webview
//接收js 传值
- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
    //找到对应js端的方法名,获取messge.body
    
    if ([message.name isEqualToString:@"gotoValuePage"]) {
        NSLog(@"message.body = %@", message.body);
        NSDictionary *bodyDic = (NSDictionary*)message.body;
        NSString *string = [bodyDic objectForKey:@"body"];
        NSArray *array = [string componentsSeparatedByString:@"="]; //从字符A中分隔成2个元素的数组
        NSLog(@"lastObject:%@",array.lastObject);

        [self.webView goForward];
    }

}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    
    [self showprogressHUD];
    
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [self hiddenProgressHUD];

}
//加载失败
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    [self hiddenProgressHUD];
    
}
// 在收到响应后，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    [self hiddenProgressHUD];
    //    DLog(@"%@",webView);
    //    DLog(@"%@",navigationResponse);
    WKNavigationResponsePolicy actionPolicy = WKNavigationResponsePolicyAllow;
    //这句是必须加上的，不然会异常
    decisionHandler(actionPolicy);
    
}
// 在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    
    [self hiddenProgressHUD];
 
    NSURL *URL = navigationAction.request.URL;
    NSString *scheme = [URL scheme];
    if ([scheme isEqualToString:@"tel"]) {
        NSString *resourceSpecifier = [URL resourceSpecifier];
        NSMutableString* str=[[NSMutableString alloc] initWithFormat:@"tel:%@",resourceSpecifier];
        UIWebView * callWebview = [[UIWebView alloc] init];
        [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
        [self.view addSubview:callWebview];
        
    }
    
    
    WKNavigationActionPolicy actionPolicy = WKNavigationActionPolicyAllow;

    //这句是必须加上的，不然会异常
    decisionHandler(actionPolicy);
    
    
}

@end
