//
//  AddNotice.m
//  RRBI
//
//  Created by kuyuZJ on 16/7/5.
//
//

#import "AddNotice.h"

@interface AddNotice ()
@property(nonatomic,strong) UIView* headerView;
@property(nonatomic,strong) UITextView* txtView;
@property(nonatomic,strong) UIButton* btnConfirm;
@property(nonatomic,strong) UIButton* btnCancel;
@property(nonatomic,copy) NSString* notice;

@end

@implementation AddNotice

-(instancetype)initWithNotice:(NSString *)notice{
    self = [super init];
    if(self){
        _notice = notice;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title =  @"店铺公告";
    
    [self layoutUI];
    [self layoutConstraints];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.txtView.text = self.notice;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark =====================================================  user interface layout
-(void)layoutUI{
//    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 300.f);
//    [self.headerView addSubview:self.txtView];
//    [self.headerView addSubview:self.btnConfirm];
//    [self.headerView addSubview:self.btnCancel];
//    self.tableView.tableHeaderView = self.headerView;
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 300.f);
    [self.view addSubview:self.headerView];
    [self.headerView addSubview:self.txtView];
    [self.headerView addSubview:self.btnConfirm];
    [self.headerView addSubview:self.btnCancel];
}

-(void)layoutConstraints{
    self.txtView.translatesAutoresizingMaskIntoConstraints = NO;
    self.btnConfirm.translatesAutoresizingMaskIntoConstraints = NO;
    self.btnCancel.translatesAutoresizingMaskIntoConstraints =NO;
    
    [self.txtView addConstraint:[NSLayoutConstraint constraintWithItem:self.txtView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:150.f]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.txtView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeTop multiplier:1.0 constant:20.f]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.txtView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:15.f]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.txtView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-15.f]];
    
    [self.btnCancel addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCancel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40.f]];
    [self.btnCancel addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCancel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:100.f]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCancel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.txtView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:20.f]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCancel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.self.headerView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-15.f]];
    
    [self.btnConfirm addConstraint:[NSLayoutConstraint constraintWithItem:self.btnConfirm attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40.f]];
    [self.btnConfirm addConstraint:[NSLayoutConstraint constraintWithItem:self.btnConfirm attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:100.f]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnConfirm attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.txtView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:20.f]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnConfirm attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.btnCancel attribute:NSLayoutAttributeLeft multiplier:1.0 constant:-15.f]];
    
}

#pragma mark =====================================================  SEL
-(IBAction)confirmTouch:(id)sender{
    [self showHUD];
    NSDictionary* arg = @{ @"ince": @"set_shop_notice", @"sid":[UserModel sharedInstanced].ru_Id, @"notice":self.txtView.text};
    [NetRepositories netConfirm:arg complete:^(EnumNetResponse react, NSDictionary *response, NSString *message) {
        if(react == NetResponseSuccess){
            [self hidHUD: @"提交成功!"];
            [[NSNotificationCenter defaultCenter] postNotificationName:NotifciationReLoadUserInfo object:nil];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else if (react == NetResponseException){
            [self hidHUD:message];
        }else{
            [self hidHUD:message];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

-(IBAction)cancelTouch:(id)sender{
   [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark =====================================================  property package
-(UIView *)headerView{
    if(!_headerView){
        _headerView = [[UIView alloc]init];
    }
    return _headerView;
}

-(UITextView *)txtView{
    if(!_txtView){
        _txtView = [[UITextView alloc]init];
        _txtView.layer.masksToBounds = YES;
        _txtView.layer.cornerRadius = 5.f;
        _txtView.backgroundColor = [UIColor whiteColor];
        _txtView.layer.borderWidth = 1.f;
        _txtView.layer.borderColor = theme_line_color.CGColor;
    }
    return _txtView;
}

-(UIButton *)btnConfirm{
    if(!_btnConfirm){
        _btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnConfirm setBackgroundImage:[UIImage imageNamed: @"bg-store-btn-red"] forState:UIControlStateNormal];
        [_btnConfirm setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnConfirm setTitle: @"确认修改" forState:UIControlStateNormal];
        [_btnConfirm addTarget:self action:@selector(confirmTouch:) forControlEvents:UIControlEventTouchUpInside];
        _btnConfirm.layer.masksToBounds = YES;
        _btnConfirm.layer.cornerRadius = 5.f;
        _btnConfirm.layer.borderColor = theme_line_color.CGColor;
        _btnConfirm.layer.borderWidth = 1.f;
    }
    return _btnConfirm;
}

-(UIButton *)btnCancel{
    if(!_btnCancel){
        _btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnCancel setBackgroundImage:[UIImage imageNamed: @"bg-store-btn-gray"] forState:UIControlStateNormal];
        [_btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnCancel setTitle: @"取消" forState:UIControlStateNormal];
        [_btnCancel addTarget:self action:@selector(cancelTouch:) forControlEvents:UIControlEventTouchUpInside];
        _btnCancel.layer.masksToBounds = YES;
        _btnCancel.layer.cornerRadius = 5.f;
        _btnCancel.layer.borderColor = theme_line_color.CGColor;
        _btnCancel.layer.borderWidth = 1.f;
    }
    return _btnCancel;
}


@end
