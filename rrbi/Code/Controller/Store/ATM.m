//
//  ATM.m
//  RRQS
//
//  Created by kyjun on 16/4/23.
//
//

#import "ATM.h"

@interface ATM ()<UITextFieldDelegate,UIAlertViewDelegate>

@property(nonatomic,strong) UIView* headerView;
@property(nonatomic,strong) UILabel* labelTitle;
@property(nonatomic,strong) UITextField* txtCard;
@property(nonatomic,strong) UITextField* txtMoney;
@property(nonatomic,strong) UIView* footerView;
@property(nonatomic,strong) UIButton* btnConfirm;
@property(nonatomic,strong) UILabel* labelMark;

@end

@implementation ATM



- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"申请提现";
    [self layoutUI];
    [self layoutConstraints];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.txtMoney.text = self.Identity.userInfo.balance;
    NSLog(@"top_note = %@",self.Identity.userInfo.top_note);

    
    _labelTitle.text = [NSString stringWithFormat:@"%@",self.Identity.userInfo.top_note];
    
    NSString* str = self.Identity.userInfo.bottom_note;
    NSMutableAttributedString* attributeStr =[[NSMutableAttributedString alloc]initWithString:str];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [attributeStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    _labelMark.attributedText =attributeStr;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark =====================================================  user interface layout
-(void)layoutUI{
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 200.f);
    self.footerView.frame = CGRectMake(0, CGRectGetMaxY(self.headerView.frame), SCREEN_WIDTH, 200.f);
    [self.view addSubview:self.headerView];
    [self.view addSubview:self.footerView];

//    self.tableView.tableHeaderView = self.headerView;
//    self.tableView.tableFooterView = self.footerView;
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.headerView addSubview:self.labelTitle];
    [self.headerView addSubview:self.txtCard];
    [self.headerView addSubview:self.txtMoney];
    
    [self.footerView addSubview:self.btnConfirm];
    [self.footerView addSubview:self.labelMark];
}

-(void)layoutConstraints{
    self.labelTitle.translatesAutoresizingMaskIntoConstraints  = NO;
    self.txtCard.translatesAutoresizingMaskIntoConstraints = NO;
    self.txtMoney.translatesAutoresizingMaskIntoConstraints = NO;
    self.btnConfirm.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelMark.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.labelTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTitle attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:50.f]];
    [self.labelTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTitle attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH-20]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTitle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTitle attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:15.f]];
    
    [self.txtCard addConstraint:[NSLayoutConstraint constraintWithItem:self.txtCard attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
    [self.txtCard addConstraint:[NSLayoutConstraint constraintWithItem:self.txtCard attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:50.f]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.txtCard attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelTitle attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.txtCard attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
    
    [self.txtMoney addConstraint:[NSLayoutConstraint constraintWithItem:self.txtMoney attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
    [self.txtMoney addConstraint:[NSLayoutConstraint constraintWithItem:self.txtMoney attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:50.f]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.txtMoney attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.txtCard attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.txtMoney attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
    
    
    [self.btnConfirm addConstraint:[NSLayoutConstraint constraintWithItem:self.btnConfirm attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH-20.f]];
    [self.btnConfirm addConstraint:[NSLayoutConstraint constraintWithItem:self.btnConfirm attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:50.f]];
    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnConfirm attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.footerView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnConfirm attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.footerView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.f]];
    
    [self.labelMark addConstraint:[NSLayoutConstraint constraintWithItem:self.labelMark attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH-20.f]];
    [self.labelMark addConstraint:[NSLayoutConstraint constraintWithItem:self.labelMark attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:50.f]];
    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelMark attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.btnConfirm attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10.f]];
    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelMark attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.footerView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:15.f]];
    
}

#pragma mark =====================================================  SEL
-(IBAction)btnConfirmTouch:(id)sender{
    
    if (_txtMoney.text.length == 0) {
        ShowMessage(@"请输入提现金额");
        return ;
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"确认提现吗？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    
        [self showHUD];
        NSDictionary* arg = @{@"ince":@"set_shop_deposit",@"sid":[UserModel sharedInstanced].ru_Id, @"uid":self.Identity.userInfo.userID,@"amount":self.txtMoney.text};
        [NetRepositories netConfirm:arg complete:^(EnumNetResponse react, NSDictionary *response, NSString *message) {
            if(react == NetResponseSuccess){
                [self hidHUD];
                [[NSNotificationCenter defaultCenter] postNotificationName:NotifciationReLoadUserInfo object:nil];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else if (react == NetResponseFail){
                [self hidHUD:message];
            }else{
                [self hidHUD:message];
            }
        }];
        
    }];
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alertController addAction:defaultAction];
    [alertController addAction:cancleAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}


#pragma mark =====================================================  property package
-(UIView *)headerView{
    if(!_headerView){
        _headerView = [[UIView alloc]init];
        _headerView.backgroundColor = [UIColor colorWithRed:242/255.f green:242/255.f blue:242/255.f alpha:1.0];
    }
    return _headerView;
}

-(UILabel *)labelTitle{
    if(!_labelTitle){
        _labelTitle = [[UILabel alloc]init];
        _labelTitle.font = [UIFont systemFontOfSize:12.f];
        _labelTitle.textColor = [UIColor colorWithRed:64/255.f green:64/255.f blue:64/255.f alpha:1.0];
//        _labelTitle.text = [NSString stringWithFormat:@"每个月两次提现机会,当前可提现余额%@元",self.Identity.userInfo.balance];
    }
    return _labelTitle;
}

-(UITextField *)txtCard{
    if(!_txtCard){
        _txtCard = [[UITextField alloc]init];
        _txtCard.backgroundColor = [UIColor whiteColor];
        _txtCard.borderStyle=UITextBorderStyleNone;
        _txtCard.layer.borderColor =[UIColor colorWithRed:231/255.f green:231/255.f blue:231/255.f alpha:1.0].CGColor;
        _txtCard.layer.borderWidth = 1.0f;
        _txtCard.layer.masksToBounds = YES;
        _txtCard.userInteractionEnabled = NO;
        _txtCard.font = [UIFont systemFontOfSize:14.f];
        _txtCard.textColor = [UIColor colorWithRed:64/255.f green:64/255.f blue:64/255.f alpha:1.0];
        _txtCard.text = [NSString stringWithFormat:@"%@ : %@",self.Identity.userInfo.cardPerson,self.Identity.userInfo.bankCard];// self.Identity.userInfo.bankCard;
        UILabel* label = [[UILabel alloc]init];
        label.frame = CGRectMake(0, 0, 15, 50);
        label.font = [UIFont systemFontOfSize:14.f];
        label.textColor = [UIColor colorWithRed:64/255.f green:64/255.f blue:64/255.f alpha:1.0];
        label.textAlignment = NSTextAlignmentCenter;
        _txtCard.leftView = label;
        _txtCard.leftViewMode =UITextFieldViewModeAlways;
    }
    return _txtCard;
}

-(UITextField *)txtMoney{
    if(!_txtMoney){
        _txtMoney = [[UITextField alloc]init];
        _txtMoney.backgroundColor = [UIColor whiteColor];
        _txtMoney.borderStyle=UITextBorderStyleNone;
        _txtMoney.font = [UIFont systemFontOfSize:14.f];
        _txtMoney.keyboardType = UIKeyboardTypeNumberPad;
//        _txtMoney.userInteractionEnabled = NO;
        _txtMoney.placeholder =@"请输入提现金额";
        _txtMoney.textColor = [UIColor colorWithRed:64/255.f green:64/255.f blue:64/255.f alpha:1.0];
        UIView* leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 90, 50.f)];
        UILabel* label = [[UILabel alloc]init];
        label.frame = CGRectMake(15, 0, 70, 50);
        label.font = [UIFont systemFontOfSize:14.f];
        label.textColor =[UIColor colorWithRed:64/255.f green:64/255.f blue:64/255.f alpha:1.0];
        label.text = @"提现金额";
        label.textAlignment = NSTextAlignmentLeft;
        [leftView addSubview:label];
        label = [[UILabel alloc]initWithFrame:CGRectMake(90-8, 15, 2, 20.f)];
        label.backgroundColor =[UIColor colorWithRed:248/255.f green:159/255.f blue:39/255.f alpha:1.0];
        [leftView addSubview:label];
        _txtMoney.leftView = leftView;
        _txtMoney.leftViewMode =UITextFieldViewModeAlways;
    }
    return _txtMoney;
}

-(UIView *)footerView{
    if(!_footerView){
        _footerView = [[UIView alloc]init];
        _footerView.backgroundColor = [UIColor colorWithRed:242/255.f green:242/255.f blue:242/255.f alpha:1.0];
    }
    return _footerView;
}

-(UIButton *)btnConfirm{
    if(!_btnConfirm){
        _btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnConfirm.backgroundColor = [UIColor colorWithRed:248/255.f green:159/255.f blue:39/255.f alpha:1.0];
        [_btnConfirm setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnConfirm setTitle:@"确认提现" forState:UIControlStateNormal];
        [_btnConfirm addTarget:self action:@selector(btnConfirmTouch:) forControlEvents:UIControlEventTouchUpInside];
        _btnConfirm.layer.masksToBounds = YES;
        _btnConfirm.layer.cornerRadius = 5.f;
        _btnConfirm.layer.borderColor = [UIColor colorWithRed:231/255.f green:231/255.f blue:231/255.f alpha:1.0].CGColor;
    }
    return _btnConfirm;
}

-(UILabel *)labelMark{
    if(!_labelMark){
        _labelMark = [[UILabel alloc]init];
        _labelMark.font = [UIFont systemFontOfSize:12.f];
        _labelMark.textColor = [UIColor colorWithRed:64/255.f green:64/255.f blue:64/255.f alpha:1.0];
        _labelMark.numberOfLines = 0;
        _labelMark.lineBreakMode = NSLineBreakByWordWrapping;
//        NSString* str = @"温馨提示:根据银行情况1-3个工作日到账";
//        NSMutableAttributedString* attributeStr =[[NSMutableAttributedString alloc]initWithString:str];
//        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//        [paragraphStyle setLineSpacing:5];
//        [attributeStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
//        _labelMark.attributedText =attributeStr;
    }
    return _labelMark;
}


@end
