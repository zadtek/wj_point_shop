//
//  Account.m
//  RRQS
//
//  Created by kyjun on 16/3/30.
//
//

#import "Account.h"
 
#import "AccountDetail.h"
#import "ATM.h"
//#import "CashRecord.h"//提现记录页面


@interface Account ()
//@property(nonatomic,strong) UITableView* tableView;
@property(nonatomic,strong) UIView* headerView;
@property(nonatomic,strong) UIImageView* iconMoney;
@property(nonatomic,strong) UILabel* labelTitle;
@property(nonatomic,strong) UILabel* labelMoney;
@property(nonatomic,strong) UIButton* btnDeail;
@property(nonatomic,strong) UIButton* btnAppCash;


@property(nonatomic,strong) UILabel*  frozenTitle;
@property(nonatomic,strong) UILabel*  frozenMoney;

@end

@implementation Account

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = theme_default_color;
    self.title = @"财务信息";
    [self layoutUI];
    [self layoutConstraints];
     }

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    _frozenTitle.text = self.Identity.userInfo.frozen_money_desc;
//    _labelTitle.text = self.Identity.userInfo.balanc_desc;
//
//
//
//    self.labelMoney.text = [NSString stringWithFormat:@"%.2f",[self.Identity.userInfo.balance floatValue]];
//
//    self.frozenMoney.text = [NSString stringWithFormat:@"%.2f",[self.Identity.userInfo.frozen_money floatValue]];
//
    
    _frozenTitle.text = self.dataDic[@"frozen_money_desc"];
    _labelTitle.text = self.dataDic[@"balanc_desc"];
    
    self.labelMoney.text = [NSString stringWithFormat:@"%.2f",[self.dataDic[@"balance"] floatValue]];
    
    self.frozenMoney.text = [NSString stringWithFormat:@"%.2f",[self.dataDic[@"frozen_money"] floatValue]];
    
    

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark =====================================================  user interface layout
-(void)layoutUI{
    // [self.view addSubview:self.btnAppCash];
    [self.view addSubview:self.headerView];

//    [self.view addSubview:self.tableView];
//    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 100);
//    self.tableView.tableHeaderView = self.headerView;
    
    
    
    [self.headerView addSubview:self.iconMoney];
    [self.headerView addSubview:self.labelTitle];
    [self.headerView addSubview:self.labelMoney];
    [self.headerView addSubview:self.btnDeail];
    
    [self.headerView addSubview:self.frozenMoney];
    [self.headerView addSubview:self.frozenTitle];

}

-(void)layoutConstraints{
    
    
    
//    [self.btnAppCash mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.mas_equalTo(50.f);
//        make.right.equalTo(self.view);
//        make.left.equalTo(self.view);
//        make.bottom.equalTo(self.view);
//    }];
    
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.right.equalTo(self.view);
        make.left.equalTo(self.view);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
   
    
    
    [self.iconMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(self.view);
        make.top.mas_equalTo(100.0);
        make.size.mas_equalTo(CGSizeMake(75, 75));
    }];
    
    [self.labelMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.iconMoney.mas_bottom).mas_offset(20);
        make.width.equalTo(self.headerView).multipliedBy(0.5);
        make.left.equalTo(self.headerView);
        make.height.mas_equalTo(40);
      
    }];
    
    
    [self.labelTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelMoney.mas_bottom);
        make.left.right.equalTo(self.labelMoney);
        make.height.equalTo(self.labelMoney);
    }];
    
    
    [self.frozenMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.width.equalTo(self.labelMoney);
        make.left.equalTo(self.labelMoney.mas_right);
        
    }];
    
    
    [self.labelTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelMoney.mas_bottom);
        make.left.right.equalTo(self.labelMoney);
        make.height.equalTo(self.labelMoney);
    }];
    
    [self.frozenTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.width.equalTo(self.labelTitle);
        make.left.equalTo(self.labelTitle.mas_right);
        
    }];
    
    
   
    
    [self.btnDeail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelTitle.mas_bottom).mas_offset(20);
        make.left.right.equalTo(self.headerView);
        make.height.equalTo(self.labelTitle);
    }];
    
    
//    self.btnAppCash.translatesAutoresizingMaskIntoConstraints = NO;
//    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
//    self.iconMoney.translatesAutoresizingMaskIntoConstraints = NO;
//    self.labelTitle.translatesAutoresizingMaskIntoConstraints = NO;
//    self.labelMoney.translatesAutoresizingMaskIntoConstraints  = NO;
//    self.btnDeail.translatesAutoresizingMaskIntoConstraints = NO;
//
//    [self.btnAppCash addConstraint:[NSLayoutConstraint constraintWithItem:self.btnAppCash attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:50.f]];
//    [self.btnAppCash addConstraint:[NSLayoutConstraint constraintWithItem:self.btnAppCash attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.btnAppCash attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.btnAppCash attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
//
//    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.btnAppCash attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
//
//    [self.iconMoney addConstraint:[NSLayoutConstraint constraintWithItem:self.iconMoney attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:75.f]];
//    [self.iconMoney addConstraint:[NSLayoutConstraint constraintWithItem:self.iconMoney attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:75.f]];
//    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.iconMoney attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeTop multiplier:1.0 constant:100.f]];
//    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.iconMoney attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.f]];
//
//    [self.labelTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTitle attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
//    [self.labelTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTitle attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:60.f]];
//    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTitle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.iconMoney attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
//    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTitle attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.f]];
//
//    [self.labelMoney addConstraint:[NSLayoutConstraint constraintWithItem:self.labelMoney attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
//    [self.labelMoney addConstraint:[NSLayoutConstraint constraintWithItem:self.labelMoney attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:60.f]];
//    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelMoney attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelTitle attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
//    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelMoney attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.f]];
//
//    [self.btnDeail addConstraint:[NSLayoutConstraint constraintWithItem:self.btnDeail attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
//    [self.btnDeail addConstraint:[NSLayoutConstraint constraintWithItem:self.btnDeail attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:60.f]];
//    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnDeail attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelMoney attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
//    [self.headerView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnDeail attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.f]];
    
    
}


#pragma mark =====================================================  SEL
-(IBAction)detailTouch:(id)sender{
//    CashRecord* controller = [[CashRecord alloc]init];
//    [self.navigationController pushViewController:controller animated:YES];
    
    AccountDetail* controller = [[AccountDetail alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
}
-(IBAction)appCashTouch:(id)sender{
    ATM* controller = [[ATM alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark =====================================================  property package 

//-(UITableView *)tableView{
//    if(!_tableView){
//        _tableView = [[UITableView alloc]init];
//        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    }
//    return _tableView;
//}

-(UIView *)headerView{
    if(!_headerView){
        _headerView = [[UIView alloc]init];
        _headerView.backgroundColor = [UIColor whiteColor];
        
    }
    return _headerView;
}

-(UIImageView *)iconMoney{
    if(!_iconMoney){
        _iconMoney = [[UIImageView alloc]init];
        [_iconMoney setImage:[UIImage imageNamed:@"icon-cash"]];
    }
    return _iconMoney;
}

-(UILabel *)labelTitle{
    if(!_labelTitle){
        _labelTitle = [[UILabel alloc]init];
        _labelTitle.text = @"账户余额(元)";
        _labelTitle.textAlignment = NSTextAlignmentCenter;
        _labelTitle.textColor = [UIColor colorWithRed:156/255.f green:156/244.f blue:156/255.f alpha:1.0];
        _labelTitle.font = [UIFont systemFontOfSize:15.f];
    }
    return _labelTitle;
}

-(UILabel *)frozenTitle{
    if(!_frozenTitle){
        _frozenTitle = [[UILabel alloc]init];
        _frozenTitle.text = @"冻结金额(元)";
        _frozenTitle.textAlignment = NSTextAlignmentCenter;
        _frozenTitle.textColor = [UIColor colorWithRed:156/255.f green:156/244.f blue:156/255.f alpha:1.0];
        _frozenTitle.font = [UIFont systemFontOfSize:15.f];
    }
    return _frozenTitle;
    
}

- (UILabel *)frozenMoney{
    
    if(!_frozenMoney){
        _frozenMoney = [[UILabel alloc]init];
        _frozenMoney.textAlignment = NSTextAlignmentCenter;
        _frozenMoney.textColor = [UIColor colorWithRed:57/255.f green:174/244.f blue:222/255.f alpha:1.0];
        _frozenMoney.font = [UIFont boldSystemFontOfSize:24.f];
    }
    return _frozenMoney;
}

-(UILabel *)labelMoney{
    if(!_labelMoney){
        _labelMoney = [[UILabel alloc]init];
        _labelMoney.textAlignment = NSTextAlignmentCenter;
        _labelMoney.textColor = [UIColor colorWithRed:57/255.f green:174/244.f blue:222/255.f alpha:1.0];
        _labelMoney.font = [UIFont boldSystemFontOfSize:24.f];
    }
    return _labelMoney;
}

-(UIButton *)btnDeail{
    if(!_btnDeail){
        _btnDeail = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnDeail setTitleColor:[UIColor colorWithRed:57/255.f green:174/244.f blue:222/255.f alpha:1.0] forState:UIControlStateNormal];
        _btnDeail.titleLabel.font =[UIFont systemFontOfSize:17.f];
        [_btnDeail setTitle:@"查看余额明细" forState:UIControlStateNormal];
        _btnDeail.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [_btnDeail addTarget:self action:@selector(detailTouch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnDeail;
}

-(UIButton *)btnAppCash{
    if(!_btnAppCash){
//        _btnAppCash = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_btnAppCash setTitleColor:[UIColor colorWithRed:117/255.f green:117/244.f blue:117/255.f alpha:1.0] forState:UIControlStateNormal];
//        [_btnAppCash setTitle:@"申请提现" forState:UIControlStateNormal];
//        _btnAppCash.titleLabel.font =[UIFont systemFontOfSize:17.f];
//        CALayer* border = [[CALayer alloc]init];
//        border.frame = CGRectMake(0, 0.f, SCREEN_WIDTH, 1.f);
//        border.backgroundColor = theme_line_color.CGColor;
//        [_btnAppCash.layer addSublayer:border];
//        [_btnAppCash addTarget:self action:@selector(appCashTouch:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _btnAppCash;
}

@end
