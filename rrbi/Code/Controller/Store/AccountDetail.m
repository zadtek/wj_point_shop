//
//  AccountDetail.m
//  RRQS
//
//  Created by kyjun on 16/4/22.
//
//

#import "AccountDetail.h"
#import "InputOrOut.h"
#import "CashRecord.h"

@interface AccountDetail ()
@property(nonatomic,strong) UIView* topView;
@property(nonatomic,strong) UIButton* btnInputOrOut;
@property(nonatomic,strong) UIButton* btnCash;
@property(nonatomic,strong) UILabel* labelMark;
@property(nonatomic,strong) UIScrollView* mainScroll;
@property(nonatomic,strong) InputOrOut* inputOrOutController;
@property(nonatomic,strong) CashRecord* cashController;

@end

@implementation AccountDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"余额明细";
    [self layoutUI];
    [self layoutConstraints];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.mainScroll.contentSize = CGSizeMake(SCREEN_WIDTH*2, CGRectGetHeight(self.mainScroll.frame));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark =====================================================  user interface layout
-(void)layoutUI{
    [self.view addSubview:self.topView];
    [self.topView addSubview:self.btnInputOrOut];
    [self.topView addSubview:self.btnCash];
    [self.topView addSubview:self.labelMark];
    [self.view addSubview:self.mainScroll];
    self.inputOrOutController = [[InputOrOut alloc]init];
    [self addChildViewController:self.inputOrOutController];
    self.cashController = [[CashRecord alloc]init];
    [self addChildViewController:self.cashController];
    
    [self.mainScroll addSubview:self.inputOrOutController.view];
    [self.mainScroll addSubview:self.cashController.view];
}

-(void)layoutConstraints{
    self.topView.translatesAutoresizingMaskIntoConstraints = NO;
    self.btnInputOrOut.translatesAutoresizingMaskIntoConstraints = NO;
    self.btnCash.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelMark.translatesAutoresizingMaskIntoConstraints = NO;
    self.mainScroll.translatesAutoresizingMaskIntoConstraints = NO;
    self.inputOrOutController.view.translatesAutoresizingMaskIntoConstraints = NO;
    self.cashController.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.topView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45.f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.topView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.topLayoutGuide attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.topView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.f]];
    
    [self.btnInputOrOut addConstraint:[NSLayoutConstraint constraintWithItem:self.btnInputOrOut attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/2]];
    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnInputOrOut attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnInputOrOut attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnInputOrOut attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
    
    [self.btnCash addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCash attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/2]];
    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCash attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCash attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCash attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.btnInputOrOut attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.f]];
   
    [self.labelMark addConstraint:[NSLayoutConstraint constraintWithItem:self.labelMark attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/2]];
    [self.labelMark addConstraint:[NSLayoutConstraint constraintWithItem:self.labelMark attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:2.f]];
    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelMark attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-1.f]];
    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelMark attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.btnInputOrOut attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
    
    [self.mainScroll addConstraint:[NSLayoutConstraint constraintWithItem:self.mainScroll attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.mainScroll attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.mainScroll attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.mainScroll attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    
    [self.inputOrOutController.view addConstraint:[NSLayoutConstraint constraintWithItem:self.inputOrOutController.view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
    [self.mainScroll addConstraint:[NSLayoutConstraint constraintWithItem:self.inputOrOutController.view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.mainScroll attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.mainScroll addConstraint:[NSLayoutConstraint constraintWithItem: self.inputOrOutController.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.mainScroll attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.mainScroll addConstraint:[NSLayoutConstraint constraintWithItem:self.inputOrOutController.view attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.mainScroll attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
    
    [self.cashController.view addConstraint:[NSLayoutConstraint constraintWithItem:self.cashController.view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
    [self.mainScroll addConstraint:[NSLayoutConstraint constraintWithItem:self.cashController.view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.mainScroll attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.mainScroll addConstraint:[NSLayoutConstraint constraintWithItem: self.cashController.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.mainScroll attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.mainScroll addConstraint:[NSLayoutConstraint constraintWithItem:self.cashController.view attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.inputOrOutController.view attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.f]];

}


#pragma mark =====================================================  SEL
-(IBAction)changeOptionTouch:(UIButton*)sender{
    if(self.btnInputOrOut == sender){
        self.btnInputOrOut.selected = YES;
        self.btnCash.selected = NO;
    }else{
        self.btnInputOrOut.selected = NO;
        self.btnCash.selected = YES;
    }
    [self changeMarkPosition:sender.tag];
}
#pragma mark =====================================================  Private method
-(void)changeMarkPosition:(NSInteger)tag{
    [UIView animateWithDuration:0.5 animations:^{
        self.labelMark.frame = CGRectMake(SCREEN_WIDTH/2*tag, CGRectGetHeight(self.topView.frame)-2, SCREEN_WIDTH/2, 2.f);
        CGRect arect =CGRectMake(SCREEN_WIDTH*tag, 0, SCREEN_WIDTH, CGRectGetHeight(self.mainScroll.frame));
        [self.mainScroll scrollRectToVisible:arect animated:YES];
    }];
}

#pragma mark =====================================================  property package
-(UIView *)topView{
    if(!_topView){
        _topView = [[UIView alloc]init];
        CALayer* border = [[CALayer alloc]init];
        border.frame = CGRectMake(0, 44.f, SCREEN_WIDTH, 1.f);
        border.backgroundColor = [UIColor colorWithRed:223/255.f green:223/255.f blue:223/255.f alpha:1.0].CGColor;
        [_topView.layer addSublayer:border];
    }
    return _topView;
}

-(UIButton *)btnInputOrOut{
    if(!_btnInputOrOut){
        _btnInputOrOut = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnInputOrOut setTitleColor:[UIColor colorWithRed:97/255.f green:97/255.f blue:97/255.f alpha:1.0] forState:UIControlStateNormal];
        [_btnInputOrOut setTitleColor:[UIColor colorWithRed:247/255.f green:157/255.f blue:34/255.f alpha:1.0] forState:UIControlStateSelected];
        [_btnInputOrOut setTitle:@"收入明细" forState:UIControlStateNormal];
        [_btnInputOrOut addTarget:self action:@selector(changeOptionTouch:) forControlEvents:UIControlEventTouchUpInside];
        _btnInputOrOut.tag = 0;
        _btnInputOrOut.selected = YES;
        _btnInputOrOut.titleLabel.font = [UIFont systemFontOfSize:15.f];
    }
    return _btnInputOrOut;
}

-(UIButton *)btnCash{
    if(!_btnCash){
        _btnCash  = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnCash setTitleColor:[UIColor colorWithRed:97/255.f green:97/255.f blue:97/255.f alpha:1.0] forState:UIControlStateNormal];
        [_btnCash setTitleColor:[UIColor colorWithRed:247/255.f green:157/255.f blue:34/255.f alpha:1.0] forState:UIControlStateSelected];
        [_btnCash setTitle:@"提现记录" forState:UIControlStateNormal];
        [_btnCash addTarget:self action:@selector(changeOptionTouch:) forControlEvents:UIControlEventTouchUpInside];
        _btnCash.tag = 1;
        _btnCash.titleLabel.font = [UIFont systemFontOfSize:15.f];
    }
    return _btnCash;
}

-(UIScrollView *)mainScroll{
    if(!_mainScroll){
        _mainScroll = [[UIScrollView alloc]init];
        _mainScroll.bounces = YES;
        _mainScroll.pagingEnabled = YES;
        _mainScroll.userInteractionEnabled = YES;
        _mainScroll.showsHorizontalScrollIndicator = NO;
        _mainScroll.backgroundColor=[UIColor clearColor];
        _mainScroll.scrollEnabled=NO;
    }
    return _mainScroll;
}

-(UILabel *)labelMark{
    if(!_labelMark){
        _labelMark = [[ UILabel alloc]init];
        _labelMark.backgroundColor = [UIColor colorWithRed:247/255.f green:157/255.f blue:34/255.f alpha:1.0];
    }
    return _labelMark;
}
@end
