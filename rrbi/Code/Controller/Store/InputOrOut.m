//
//  InputOrOut.m
//  RRQS
//
//  Created by kyjun on 16/4/22.
//
//

#import "InputOrOut.h"
#import "Pager.h"
#import "InputOrOutCell.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "WebPageViewController.h"


@interface InputOrOut ()<DZNEmptyDataSetDelegate,DZNEmptyDataSetSource>

@property(nonatomic,strong) NetPage* page;
@property(nonatomic,strong) NSString* cellIdentifier;
@property(nonatomic,strong) NSMutableArray* arrayData;

@end

@implementation InputOrOut



- (void)viewDidLoad {
    [super viewDidLoad];
    self.cellIdentifier = @"InputOrOutCell";
    [self.tableView registerClass:[InputOrOutCell  class] forCellReuseIdentifier:self.cellIdentifier];
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    if (@available(iOS 11.0, *))
    {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
    }
    [self refreshDataSource];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark =====================================================  Data source
-(void)queryData{
    NSDictionary* arg = @{@"ince":@"get_shop_accountlog",@"sid":self.Identity.userInfo.userID,@"page":[WMHelper integerConvertToString:self.page.pageIndex]};
    NetRepositories* repositories = [[NetRepositories alloc]init];
    [repositories queryAccount:arg page:self.page complete:^(EnumNetResponse react, NSArray *list, NSString *message, NSDictionary *mainDict) {
        if(self.page.pageIndex == 1){
            [self.arrayData removeAllObjects];
        }
        if(react == NetResponseSuccess){
            [list enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [self.arrayData addObject:obj];
            }];
        }else if(react == NetResponseException){
           // [self alertHUD:message];
        }else{
           // [self alertHUD:message];
        }
        
        [self.tableView reloadData];
        if(self.page.pageCount<=self.page.pageIndex){
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            [self.tableView.mj_footer endRefreshing];
        }
        if(self.page.pageIndex==1){
            [self.tableView.mj_header endRefreshing];
        }
    }];
}

-(void)refreshDataSource{
    __weak typeof(self) weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page.pageIndex = 1;
        [weakSelf queryData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.page.pageIndex++;
        [weakSelf queryData];
    }];
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark =====================================================  <UITableViewDataSource>
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrayData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    InputOrOutCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier forIndexPath:indexPath];
    cell.entity =self.arrayData[indexPath.row];
    return cell;
}

#pragma mark =====================================================  <UITableVeiwDelegate>
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45.f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MIncome *entity =self.arrayData[indexPath.row];

    if (entity.h5_url.length != 0) {
        WebPageViewController *VC = [[WebPageViewController alloc] init];
        VC.webPageUrlStr = entity.h5_url;
        VC.titleName = entity.page_title;
        [self.navigationController pushViewController:VC animated:YES];
    }
}
#pragma mark =====================================================  DZEmptyData 协议实现
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    return [[NSAttributedString alloc] initWithString:@"暂无收支记录" attributes:@{NSFontAttributeName :[UIFont boldSystemFontOfSize:17.0],NSForegroundColorAttributeName:[UIColor grayColor]}];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView
{
    return roundf(self.tableView.frame.size.height/10.0);
}
- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}


#pragma mark =====================================================  property package
-(NSMutableArray *)arrayData{
    if(!_arrayData){
        _arrayData = [[NSMutableArray alloc]init];
    }
    return _arrayData;
}
-(NetPage *)page{
    if(!_page){
        _page = [[NetPage alloc]init];
        _page.pageIndex = 1;
    }
    return _page;
}

@end
