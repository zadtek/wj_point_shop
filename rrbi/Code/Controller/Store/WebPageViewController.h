//
//  WebPageViewController.h
//  TheBankPro
//
//  Created by 杜文杰 on 2017/9/7.
//  Copyright © 2017年 dwj. All rights reserved.
//

#import "BaseViewController.h"

@interface WebPageViewController : BaseViewController

@property (nonatomic, strong) NSString *webPageUrlStr;
@property (nonatomic, strong) NSString *titleName;

@end
