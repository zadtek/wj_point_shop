//
//  Setting.m
//  RRBI
//
//  Created by kyjun on 16/4/26.
//
//

#import "Setting.h"
#import "JPUSHService.h"


#import "Account.h"
#import "ReceivingOrderViewController.h"
#import "StroeMessageViewController.h"
@interface Setting ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong) UIView* footerView;
@property(nonatomic,strong) UIButton* btnLoginOut;

@property(nonatomic,strong) NSString* cellIdentifier;
@property(nonatomic,strong) NSArray* array;

@end

@implementation Setting

//-(instancetype)init{
//    self = [super init];
//    if(self){
//        [self.tabBarItem setImage:[UIImage imageNamed:@"tab-setting-default"]];
//        [self.tabBarItem setSelectedImage:[[UIImage imageNamed:@"tab-setting-enter"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//        [self.tabBarItem setTitle:@"设置"];
//        self.tabBarItem.tag=3;
//    }
//    return self;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"设置";
    self.cellIdentifier = @"UITableViewCell";
    self.array = @[@"门店信息",@"营业状态",@"门店设置",@"接单设置",@"消息设置",@"我的钱包",@"联系业务"];
    [self layoutUI];
    [self layoutConstraints];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark =====================================================  user interface layout
-(void)layoutUI{
    self.footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 150.f);
    [self.footerView addSubview:self.btnLoginOut];
    self.tableView.tableFooterView = self.footerView;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:self.cellIdentifier];
}

-(void)layoutConstraints{
    self.btnLoginOut.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.btnLoginOut addConstraint:[NSLayoutConstraint constraintWithItem:self.btnLoginOut attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH-20]];
//    [self.btnLoginOut addConstraint:[NSLayoutConstraint constraintWithItem:self.btnLoginOut attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40.f]];
    [self.btnLoginOut addConstraint:[NSLayoutConstraint constraintWithItem:self.btnLoginOut attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:50.f]];
    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnLoginOut attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.footerView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.f]];
    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnLoginOut attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.footerView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.f]];
}

#pragma mark =====================================================  <UITableViewDataSource>
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return 1;
    return self.array.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = self.array[indexPath.row];
    
    if (indexPath.row != 4) {
        UIImageView *arrow4 = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-30,15, 20, 20)];
        arrow4.image = [UIImage imageNamed:@"icon_arrowright"];
        cell.accessoryView = arrow4;
    }
    
    if (indexPath.row == 4) {
        
        UISwitch * switch1 = [[UISwitch alloc] init];
        [switch1 addTarget:self action:@selector(changeReserveStatus:) forControlEvents:UIControlEventValueChanged];
        switch1.backgroundColor = [UIColor lightGrayColor];
        switch1.tintColor = [UIColor lightGrayColor];
        switch1.layer.masksToBounds = YES;
        switch1.layer.cornerRadius = switch1.bounds.size.height/2.0;
        [switch1 setOn:YES];
//        if ([self.Identity.userInfo.confirm_order_way isEqualToString:@"0"]) {
//            [switch1 setOn:NO];
//
//        }else{
//            [switch1 setOn:YES];
//        }
        switch1.frame = CGRectMake(SCREEN_WIDTH-51-20, (50-31)/2, 51, 31);
        [cell.contentView addSubview:switch1];
    }
//    cell.textLabel.text = @"版本号";
//    UILabel* label = [[UILabel alloc]init];
//    label.textAlignment = NSTextAlignmentRight;
//    label.frame = CGRectMake(SCREEN_WIDTH-SCREEN_WIDTH/3-10, 0, SCREEN_WIDTH/3, 40.f);
//    NSDictionary *bundleDic = [[NSBundle mainBundle] infoDictionary];
//    NSString* releaseVersion = [bundleDic objectForKey:@"CFBundleShortVersionString"];
//    label.text = [NSString stringWithFormat:@"V%@",releaseVersion];
//    [cell.contentView addSubview:label];
    
    return cell;
}
#pragma mark - 监听消息设置
-(void)changeReserveStatus:(UISwitch *)swi{
    if(swi.isOn){
        NSLog(@"开启");
//        [self changeMessageWithValue:@"1" withField:@"confirm_order_way"];
        [self hidHUD:@"消息开启成功"];
    }else{
        NSLog(@"关闭");
        [self hidHUD];
        [self hidHUD:@"消息关闭成功"];
//        [self changeMessageWithValue:@"0" withField:@"confirm_order_way"];
    }
}

#pragma mark =====================================================  <UITableViewDelegate>
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.f;
//    return 40.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
            case 0://门店信息
            {
                StroeMessageViewController* controller = [[StroeMessageViewController alloc]init];
                controller.showtType = 1;
                [self.navigationController pushViewController:controller animated:YES];
            }
            break;
            
            case 1://营业状态
            {
                
            }
            break;
            
            case 2://门店设置
            {
                
            }
            break;
            
            case 3://接单设置
            {
                ReceivingOrderViewController* controller = [[ReceivingOrderViewController alloc] init];
                [self.navigationController pushViewController:controller animated:YES];
            }
            break;
            
            case 4://消息设置
            {
                
            }
            break;
            
            case 5://我的钱包
            {
                Account* controller = [[Account alloc]init];
                [self.navigationController pushViewController:controller animated:YES];
            }
            break;
            
            case 6://联系业务
            {
//                NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"tel:%@",self.entity.tel];
//                UIWebView *callWebview = [[UIWebView alloc] init];
//                [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
//                [self.view addSubview:callWebview];
            }
            
            break;
        default:
            break;
    }

}

//- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
//    return  NO;
//}


#pragma mark =====================================================  SEL
-(IBAction)loginOutTouch:(UIButton*)sender{
    MMember* entity = self.Identity.userInfo;
    entity.isLogin = NO;
    Boolean flag= [NSKeyedArchiver archiveRootObject:entity toFile:[WMHelper archiverPath]];
    if(flag){
        [self logoutJPush:entity.userID];
        self.tabBarController.selectedIndex = 0; 
    }else{
        
    }
}

#pragma mark =====================================================  private method
-(void)logoutJPush:(NSString*)userID{
    
    [JPUSHService deleteAlias:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
        NSLog(@"消息推送退出%tu,%@,%tu",iResCode,iAlias,seq);
        if(iResCode == 0){
            
        }else{
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:nil message:@"消息推送退出失败!" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alert show];
        }
    } seq:1];
    
    
//    __autoreleasing NSMutableSet *tags = [NSMutableSet set];
//    __autoreleasing NSString *alias = @"";
//    
//    [JPUSHService setTags:tags alias:alias fetchCompletionHandle:^(int iResCode, NSSet *iTags, NSString *iAlias) {
//        NSLog(@"TagsAlias回调:\n iResCode:%d  iAlias:%@",iResCode,iAlias);
//        if(iResCode == 0){
//            
//        }else{
//            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:nil message:@"消息推送退出失败!" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//            [alert show];
//        }
//    }];
}

#pragma mark =====================================================  property package

-(UIView *)footerView{
    if(!_footerView){
        _footerView = [[UIView alloc]init];
    }
    return _footerView;
}

-(UIButton *)btnLoginOut{
    if(!_btnLoginOut){
        _btnLoginOut = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnLoginOut.backgroundColor = [UIColor colorWithRed:213/255.f green:213/255.f blue:213/255.f alpha:1.0];
        [_btnLoginOut setTitleColor:[UIColor colorWithRed:109/255.f green:109/255.f blue:109/255.f alpha:1.0] forState:UIControlStateNormal];
        [_btnLoginOut setTitle:@"退出" forState:UIControlStateNormal];
        _btnLoginOut.layer.masksToBounds = YES;
        _btnLoginOut.layer.cornerRadius = 5.f;
        [_btnLoginOut addTarget:self action:@selector(loginOutTouch:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _btnLoginOut;
}

@end
