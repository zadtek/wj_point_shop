//
//  NewOrderListModel.h
//  rrbi
//
//  Created by 赵桂安 on 2019/5/3.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewOrderListModel : NSObject
/**
 * 订单编号
 */
@property(nonatomic,strong) NSString* order_sn;


@property(nonatomic,copy) NSString* refound_reason;
@property(nonatomic,copy) NSString* refound_time;
@property(nonatomic,copy) NSString* hurry_time;
@property(nonatomic,copy) NSString* abnormal_time;
@property (nonatomic, copy) NSString *abnormal_reason;

/**
 *  下单时间
 */
@property(nonatomic,strong) NSString* add_time;

/**
 *  顾客姓名
 */
@property(nonatomic,strong) NSString* custom_name;
/**
 *  顾客地址
 */
@property(nonatomic,strong) NSString* custom_addr;

/**
 *  顾客电话
 */
@property(nonatomic,strong) NSString* custom_tel;
/**
 *  骑手电话
 */
@property(nonatomic,strong) NSString* rider_tel;
/**
 *  催单次数
 */
@property(nonatomic,strong) NSString* hurry_times;
/**
 *  订单id
 */
@property(nonatomic,strong) NSString* order_id;
/**
 *  流水号
 */
@property(nonatomic,strong) NSString* serial_num;
/**
 *  本单收益
 */
@property(nonatomic,strong) NSString* benifit;

-(instancetype)initWithItem:(NSDictionary*)item;



@end

NS_ASSUME_NONNULL_END
