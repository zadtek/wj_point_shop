//
//  NewOrderListModel.m
//  rrbi
//
//  Created by 赵桂安 on 2019/5/3.
//

#import "NewOrderListModel.h"

@implementation NewOrderListModel

-(instancetype)initWithItem:(NSDictionary *)item{
    self = [super init];
    if(self){
        self.order_sn = [item objectForKey:@"order_sn"];
        self.add_time = [item objectForKey:@"add_time"];
        self.custom_name = [item objectForKey:@"custom_name"];
        self.custom_addr = [item objectForKey:@"custom_addr"];
        self.custom_tel = [item objectForKey:@"custom_tel"];
        self.rider_tel = [item objectForKey:@"rider_tel"];
        self.hurry_times = [item objectForKey:@"hurry_times"];
        self.order_id = [item objectForKey:@"order_id"];
        self.serial_num = [item objectForKey:@"serial_num"];
        self.benifit = [item objectForKey:@"benifit"];
        
        
        self.refound_reason = [item objectForKey:@"refound_reason"];
        self.refound_time = [item objectForKey:@"refound_time"];
        self.hurry_time = [item objectForKey:@"hurry_time"];
        self.abnormal_time = [item objectForKey:@"abnormal_time"];
        self.abnormal_reason = [item objectForKey:@"abnormal_reason"];
        
        
        
    }
    return self;
    
}

@end
