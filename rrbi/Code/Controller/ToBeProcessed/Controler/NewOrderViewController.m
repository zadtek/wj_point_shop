//
//  NewOrderViewController.m
//  rrbi
//
//  Created by 赵桂安 on 2019/5/2.
//  待处理订单

#import "NewOrderViewController.h"
#import "NewOrderTableViewCell.h"
#import "NewOrderListModel.h"
#import "CustomAlertView.h"
#import "Login.h"
#import "FBOrderDetalisViewController.h"
#import <AudioToolbox/AudioToolbox.h>

@interface NewOrderViewController ()<UITableViewDelegate,UITableViewDataSource,NewOrderTableViewCellDelegate>

@property (nonatomic, strong) UIView *btnView;
@property (nonatomic, strong) UITableView *freshTableView;
@property (nonatomic, strong) UITableView *abnormaltTableView;
@property (nonatomic, strong) UITableView *rushTableView;
@property (nonatomic, strong) UITableView *refundTableView;
///新订单
@property (nonatomic, strong) UIButton *freshOrderBtn;
///异常配送
@property (nonatomic, strong) UIButton *abnormalOrderBtn;
///催单
@property (nonatomic, strong) UIButton *rushOrderBtn;
///退款
@property (nonatomic, strong) UIButton *refundOrderBtn;
///按钮下线
@property (nonatomic, strong) UIView *lineView;
///订单数据类型 1.新订单 2.异常配送 3.催单 4.退款
@property (nonatomic, assign) NSInteger orderType;

///新订单数据
@property (nonatomic, strong) NSMutableArray *freshOrderArray;
///异常配送
@property (nonatomic, strong) NSMutableArray *abnormalOrderArray;
///催单
@property (nonatomic, strong) NSMutableArray *rushOrderArray;
///退款
@property (nonatomic, strong) NSMutableArray *refundOrderArray;

@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation NewOrderViewController

- (NSMutableArray *)freshOrderArray{
    if (!_freshOrderArray) {
        _freshOrderArray = [NSMutableArray array];
    }
    return _freshOrderArray;
}
- (NSMutableArray *)abnormalOrderArray{
    if (!_abnormalOrderArray) {
        _abnormalOrderArray = [NSMutableArray array];
    }
    return _abnormalOrderArray;
}
- (NSMutableArray *)rushOrderArray{
    if (!_rushOrderArray) {
        _rushOrderArray = [NSMutableArray array];
    }
    return _rushOrderArray;
}
- (NSMutableArray *)refundOrderArray{
    if (!_refundOrderArray) {
        _refundOrderArray = [NSMutableArray array];
    }
    return _refundOrderArray;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!self.Identity.userInfo.isLogin){
        UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:[[Login alloc]init]];
        [self presentViewController:nav animated:YES completion:nil];
    }else{
        [self clickFreshOrderBtn];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(frontDeskRefresh) name:@"FFRONTDeskRefresh" object:nil];
    [self initTopBtn];
    
    [self.view addSubview:self.freshTableView];
    [self.view addSubview:self.abnormaltTableView];
    [self.view addSubview:self.rushTableView];
    [self.view addSubview:self.refundTableView];
    [self.freshTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.btnView.mas_bottom);
        make.left.right.bottom.equalTo(self.view);
    }];
    [self.abnormaltTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.btnView.mas_bottom);
        make.left.right.bottom.equalTo(self.view);
    }];
    [self.rushTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.btnView.mas_bottom);
        make.left.right.bottom.equalTo(self.view);
    }];
    [self.refundTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.btnView.mas_bottom);
        make.left.right.bottom.equalTo(self.view);
    }];
    self.view.backgroundColor = kMyColor(240, 240, 240);
    [self.freshTableView registerClass:[NewOrderTableViewCell class] forCellReuseIdentifier:@"NewOrderTableViewCell"];
    [self.abnormaltTableView registerClass:[NewOrderTableViewCell class] forCellReuseIdentifier:@"NewOrderTableViewCell"];
    [self.rushTableView registerClass:[NewOrderTableViewCell class] forCellReuseIdentifier:@"NewOrderTableViewCell"];
    [self.refundTableView registerClass:[NewOrderTableViewCell class] forCellReuseIdentifier:@"NewOrderTableViewCell"];
    
//    if (@available(iOS 11.0, *))
//    {
//        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
//        self.tableView.estimatedRowHeight = 0;
//        self.tableView.estimatedSectionFooterHeight = 0;
//        self.tableView.estimatedSectionHeaderHeight = 0;
//    }
//
    
    
}

- (void)frontDeskRefresh {
    [self clickFreshOrderBtn];
}

-(void)refreshDataSource{
    __weak typeof(self) weakSelf = self;
    self.freshTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //WJ
        [weakSelf queryData];
        [weakSelf loadListData];
    }];
    self.abnormaltTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //WJ
        [weakSelf queryData];
        [weakSelf loadListData];
    }];
    self.rushTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //WJ
        [weakSelf queryData];
        [weakSelf loadListData];
    }];
    self.refundTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //WJ
        [weakSelf queryData];
        [weakSelf loadListData];
    }];
    if (self.orderType == 1) {
        [self.freshTableView.mj_header beginRefreshing];
    }else if (self.orderType == 2){
        [self.abnormaltTableView.mj_header beginRefreshing];
    }else if (self.orderType == 3){
        [self.rushTableView.mj_header beginRefreshing];
    }else if (self.orderType == 4){
        [self.refundTableView.mj_header beginRefreshing];
    }
}

-(void)queryData{
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{@"ince":@"get_order_num",@"ru_id":[UserModel sharedInstanced].ru_Id};
    [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            NSDictionary * dic = [responseObject objectForKey:@"data"];
            if(![WMHelper isNULLOrnil:dic]){
                [self.freshOrderBtn setTitle:[NSString stringWithFormat:@"新订单(%@)",dic[@"new_num"]] forState:(UIControlStateNormal)];
                [self.abnormalOrderBtn setTitle:[NSString stringWithFormat:@"异常配送(%@)",dic[@"abnormal_num"]] forState:(UIControlStateNormal)];
                [self.rushOrderBtn setTitle:[NSString stringWithFormat:@"催单(%@)",dic[@"hurry_num"]] forState:(UIControlStateNormal)];
                [self.refundOrderBtn setTitle:[NSString stringWithFormat:@"退款(%@)",dic[@"refund_num"]] forState:(UIControlStateNormal)];
            }
        }else{
            [self alertHUD:[responseObject objectForKey:@"msg"]];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        [self alertHUD:@"网络异常"];
    }];
    
}

- (void)loadListData{
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{@"ince":@"get_order_list_new",@"ru_id":[UserModel sharedInstanced].ru_Id,@"order_status":[NSString stringWithFormat:@"%ld",(long)self.orderType]};
    [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            if (self.orderType == 1) {
                [self.freshOrderArray removeAllObjects];
            }else if (self.orderType == 2){
                [self.abnormalOrderArray removeAllObjects];
            }else if (self.orderType == 3){
                [self.rushOrderArray removeAllObjects];
            }else if (self.orderType == 4){
                [self.refundOrderArray removeAllObjects];
            }
            NSArray* array = [[responseObject objectForKey:@"data"] objectForKey:@"order_list"];
            if(![WMHelper isNULLOrnil:array]){
                [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NewOrderListModel* item = [[NewOrderListModel alloc]initWithItem:obj];
                    if (self.orderType == 1) {
                        [self.freshOrderArray addObject:item];
                    }else if (self.orderType == 2){
                        [self.abnormalOrderArray addObject:item];
                    }else if (self.orderType == 3){
                        [self.rushOrderArray addObject:item];
                    }else if (self.orderType == 4){
                        [self.refundOrderArray addObject:item];
                    }
                }];
            }
            if (self.orderType == 1) {
                [self.freshTableView reloadData];
            }else if (self.orderType == 2){
                [self.abnormaltTableView reloadData];
            }else if (self.orderType == 3){
                [self.rushTableView reloadData];
            }else if (self.orderType == 4){
                [self.refundTableView reloadData];
            }
            
        }else{
            [self alertHUD:[responseObject objectForKey:@"msg"]];
        }
        if (self.orderType == 1) {
            [self.freshTableView.mj_header endRefreshing];
        }else if (self.orderType == 2){
            [self.abnormaltTableView.mj_header endRefreshing];
        }else if (self.orderType == 3){
            [self.rushTableView.mj_header endRefreshing];
        }else if (self.orderType == 4){
            [self.refundTableView.mj_header endRefreshing];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        [self alertHUD:@"网络异常"];
        if (self.orderType == 1) {
            [self.freshTableView.mj_header endRefreshing];
        }else if (self.orderType == 2){
            [self.abnormaltTableView.mj_header endRefreshing];
        }else if (self.orderType == 3){
            [self.rushTableView.mj_header endRefreshing];
        }else if (self.orderType == 4){
            [self.refundTableView.mj_header endRefreshing];
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (self.orderType == 1) {
        return self.freshOrderArray.count;
    }else if (self.orderType == 2){
        return self.abnormalOrderArray.count;
    }else if (self.orderType == 3){
        return self.rushOrderArray.count;
    }else if (self.orderType == 4){
        return self.refundOrderArray.count;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NewOrderTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"NewOrderTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;///取消点击效果
    cell.delegate = self;
    cell.orderType = self.orderType;
    if (self.orderType == 1) {
        cell.tag = 100;
        cell.model = self.freshOrderArray[indexPath.section];
    }else if (self.orderType == 2){
        cell.tag = 200;
        cell.model = self.abnormalOrderArray[indexPath.section];
    }else if (self.orderType == 3){
        cell.tag = 300;
        cell.model = self.rushOrderArray[indexPath.section];
    }else if (self.orderType == 4){
        cell.tag = 400;
        cell.model = self.refundOrderArray[indexPath.section];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.orderType == 1) {
        return 276 + 70;
    } else if (self.orderType == 3) {
        return 276 + 70+50;
    }
    return 276 + 70+80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView * view = [[UIView alloc]init];
    view.backgroundColor = kMyColor(240, 240, 240);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

- (void)didDetailsBtnindex:(NSInteger)index model:(NewOrderListModel *)model {
    NSLog(@"详情      %@",model.order_sn);
        FBOrderDetalisViewController *VC= [[FBOrderDetalisViewController alloc]init];
        VC.order_id = model.order_sn;
        VC.order_type = index;
        [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark - cell 按钮点击回调方法
- (void)didBtnindex:(DidBtnType)index model:(NewOrderListModel *)model{

    switch (index) {
        case DidBtnTypeRefusedOrder:///拒绝接单
        {
            
            CustomAlertView *alertView = [[CustomAlertView alloc] initTextViewAlert];
            alertView.textBlock = ^(NSString *textStr) {
                NSLog(@"textStr = %@",textStr);
                [self showHUD];
                AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
                NSDictionary* arg = @{@"ince":@"cancel_shop_order",
                                      @"sid":[UserModel sharedInstanced].ru_Id,
                                      @"order_id":model.order_id,
                                      @"cancel_res":textStr
                                      };
                [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
                    [WMHelper outPutJsonString:responseObject];
                    NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
                    if(flag == 200){
                        [self hidHUD:[responseObject objectForKey:@"msg"]];
                        [self.freshTableView.mj_header beginRefreshing];
                    }else{
                        [self hidHUD:[responseObject objectForKey:@"msg"]];
                    }
                } failure:^(NSURLSessionDataTask *task, NSError *error)  {
                    [self hidHUD:@"网络异常"];
                }];
                
            };
            [alertView showAlertView];
        }
            break;
        case DidBtnTypeConfirmOrder:///确认接单
        {
            [self showHUD];
            AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
            NSDictionary* arg = @{@"ince":@"confirm_shop_order",@"sid":[ShopConfig getRu_id],@"order_id":model.order_id};
            [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
                [WMHelper outPutJsonString:responseObject];
                NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
                if(flag == 200){
                    
                    FBOrderDetalisViewController *VC= [[FBOrderDetalisViewController alloc]init];
                    VC.order_id = model.order_sn;
                    VC.order_type = 1;
                    [self.navigationController pushViewController:VC animated:YES];
                    
                    [self hidHUD:[responseObject objectForKey:@"msg"]];
                    [self.freshTableView.mj_header beginRefreshing];
                }else{
                    [self hidHUD:[responseObject objectForKey:@"msg"]];
                }
            } failure:^(NSURLSessionDataTask *task, NSError *error)  {
                
                NSLog( @"%@",error);
                [self hidHUD:@"网络异常"];
            }];
        }
            break;
        case DidBtnTypeCustomer:///联系客户
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",model.custom_tel]]];
        }
            
            break;
        case DidBtnTypeRider:///联系骑手
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",model.rider_tel]]];
        }
            break;
        case DidBtnTypeKnow:///知道了
        {
            CustomAlertView *alertView = [[CustomAlertView alloc] initTextViewAlert];
            alertView.titleString = @"请输入内容";
            alertView.textBlock = ^(NSString *textStr) {
                NSLog(@"textStr = %@",textStr);
                [self showHUD];
                AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
                NSDictionary* arg = @{@"ince":@"response_to_reminder",
                                      @"sid":[UserModel sharedInstanced].ru_Id,
                                      @"order_id":model.order_id,
                                      @"conten":textStr
                                      };
                [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
                    [WMHelper outPutJsonString:responseObject];
                    NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
                    if(flag == 200){
                        [self hidHUD:[responseObject objectForKey:@"msg"]];
                        [self.rushTableView.mj_header beginRefreshing];
                    }else{
                        [self hidHUD:[responseObject objectForKey:@"msg"]];
                    }
                } failure:^(NSURLSessionDataTask *task, NSError *error)  {
                    [self hidHUD:@"网络异常"];
                }];
                
            };
            [alertView showAlertView];
        }
            break;
        case DidBtnTypePart:///拒绝退款
        {
            
            [self showHUD];
            AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
            NSDictionary* arg = @{@"ince":@"agree_refund",
                                  @"sid":[UserModel sharedInstanced].ru_Id,
                                  @"order_id":model.order_id,
                                  @"refund_amount":@"-2"
                                  };
            [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
                [WMHelper outPutJsonString:responseObject];
                NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
                if(flag == 200){
                    [self hidHUD:[responseObject objectForKey:@"msg"]];
                    [self.refundTableView.mj_header beginRefreshing];
                }else{
                    [self hidHUD:[responseObject objectForKey:@"msg"]];
                }
            } failure:^(NSURLSessionDataTask *task, NSError *error)  {
                [self hidHUD:@"网络异常"];
            }];
//            CustomAlertView *alertView = [[CustomAlertView alloc] initMoneyAlert];
//            alertView.textBlock = ^(NSString *textStr) {
//                NSLog(@"textStr = %@",textStr);
//                [self showHUD];
//                AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
//                NSDictionary* arg = @{@"ince":@"agree_refund",
//                                      @"sid":[UserModel sharedInstanced].ru_Id,
//                                      @"order_id":model.order_id,
//                                      @"refund_amount":@"-2"
//                                      };
//                [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//                    [WMHelper outPutJsonString:responseObject];
//                    NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
//                    if(flag == 200){
//                        [self hidHUD:[responseObject objectForKey:@"msg"]];
//                        [self.refundTableView.mj_header beginRefreshing];
//                    }else{
//                        [self hidHUD:[responseObject objectForKey:@"msg"]];
//                    }
//                } failure:^(NSURLSessionDataTask *task, NSError *error)  {
//                    [self hidHUD:@"网络异常"];
//                }];
//            };
//            [alertView showAlertView];
            
        }
            break;
        case DidBtnTypeAll:///全额退款
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"请确认全额退款？" preferredStyle:UIAlertControllerStyleAlert];
            //创建提示按钮
            UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
            
            UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self showHUD];
                AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
                NSDictionary* arg = @{@"ince":@"agree_refund",
                                      @"sid":[UserModel sharedInstanced].ru_Id,
                                      @"order_id":model.order_id,
                                      @"refund_amount":@"-1"
                                      };
                [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
                    [WMHelper outPutJsonString:responseObject];
                    NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
                    if(flag == 200){
                        [self hidHUD:[responseObject objectForKey:@"msg"]];
                        [self.refundTableView.mj_header beginRefreshing];
                    }else{
                        [self hidHUD:[responseObject objectForKey:@"msg"]];
                    }
                } failure:^(NSURLSessionDataTask *task, NSError *error)  {
                    [self hidHUD:@"网络异常"];
                }];
            }];
            [alertController addAction:action1];
            [alertController addAction:action2];
            [self presentViewController:alertController animated:YES completion:nil];
            
        }
            break;
        default:
            break;
    }
}

///新订单
- (void)clickFreshOrderBtn{
    self.freshTableView.hidden = NO;
    self.abnormaltTableView.hidden = YES;
    self.rushTableView.hidden = YES;
    self.refundTableView.hidden = YES;
    
    [self.freshOrderBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [self.abnormalOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    [self.rushOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    [self.refundOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    CGRect frame = self.lineView.frame;
    frame.origin.x = 0;
    self.lineView.frame = frame;
    self.orderType = 1;
    [self.freshTableView.mj_header endRefreshing];
    [self.abnormaltTableView.mj_header endRefreshing];
    [self.rushTableView.mj_header endRefreshing];
    [self.refundTableView.mj_header endRefreshing];
    [self refreshDataSource];
}
///异常配送
- (void)clickAbnormalOrderBtn{
    self.freshTableView.hidden = YES;
    self.abnormaltTableView.hidden = NO;
    self.rushTableView.hidden = YES;
    self.refundTableView.hidden = YES;
    
    [self.abnormalOrderBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [self.freshOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    [self.rushOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    [self.refundOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    CGRect frame = self.lineView.frame;
    frame.origin.x = CGRectGetMaxX(self.freshOrderBtn.frame);
    self.lineView.frame = frame;
    self.orderType = 2;
    
    
    
    [self.freshTableView.mj_header endRefreshing];
    [self.abnormaltTableView.mj_header endRefreshing];
    [self.rushTableView.mj_header endRefreshing];
    [self.refundTableView.mj_header endRefreshing];
    [self refreshDataSource];
}
///催单
- (void)clickRushOrderBtn{
    self.freshTableView.hidden = YES;
    self.abnormaltTableView.hidden = YES;
    self.rushTableView.hidden = NO;
    self.refundTableView.hidden = YES;
    
    [self.rushOrderBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [self.abnormalOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    [self.freshOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    [self.refundOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    CGRect frame = self.lineView.frame;
    frame.origin.x = CGRectGetMaxX(self.abnormalOrderBtn.frame);
    self.lineView.frame = frame;
    self.orderType = 3;
    [self.freshTableView.mj_header endRefreshing];
    [self.abnormaltTableView.mj_header endRefreshing];
    [self.rushTableView.mj_header endRefreshing];
    [self.refundTableView.mj_header endRefreshing];
    [self refreshDataSource];
}
///退款
- (void)clickRefundOrderBtn{
    self.freshTableView.hidden = YES;
    self.abnormaltTableView.hidden = YES;
    self.rushTableView.hidden = YES;
    self.refundTableView.hidden = NO;
    
    [self.refundOrderBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [self.abnormalOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    [self.rushOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    [self.freshOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    CGRect frame = self.lineView.frame;
    frame.origin.x = CGRectGetMaxX(self.rushOrderBtn.frame);
    self.lineView.frame = frame;
    self.orderType = 4;
    [self.freshTableView.mj_header endRefreshing];
    [self.abnormaltTableView.mj_header endRefreshing];
    [self.rushTableView.mj_header endRefreshing];
    [self.refundTableView.mj_header endRefreshing];
    [self refreshDataSource];
}

- (void)initTopBtn{
    CGFloat width = SCREEN_WIDTH/4;
    CGFloat height = 41;
    self.btnView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 42)];
    self.btnView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.btnView];
    
    self.freshOrderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.freshOrderBtn.frame = CGRectMake(0, 0, width, height);
    [self.freshOrderBtn setTitle:@"新订单" forState:(UIControlStateNormal)];
    [self.freshOrderBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    self.freshOrderBtn.titleLabel.font = kFont(15);
    [self.freshOrderBtn addTarget:self action:@selector(clickFreshOrderBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [self.btnView addSubview:self.freshOrderBtn];
    
    self.abnormalOrderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.abnormalOrderBtn.frame = CGRectMake(CGRectGetMaxX(self.freshOrderBtn.frame), 0, width, height);
    [self.abnormalOrderBtn setTitle:@"异常配送" forState:(UIControlStateNormal)];
    [self.abnormalOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    self.abnormalOrderBtn.titleLabel.font = kFont(15);
    [self.abnormalOrderBtn addTarget:self action:@selector(clickAbnormalOrderBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [self.btnView addSubview:self.abnormalOrderBtn];
    
    self.rushOrderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rushOrderBtn.frame = CGRectMake(CGRectGetMaxX(self.abnormalOrderBtn.frame), 0, width, height);
    [self.rushOrderBtn setTitle:@"催单" forState:(UIControlStateNormal)];
    [self.rushOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    self.rushOrderBtn.titleLabel.font = kFont(15);
    [self.rushOrderBtn addTarget:self action:@selector(clickRushOrderBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [self.btnView addSubview:self.rushOrderBtn];
    
    
    self.refundOrderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.refundOrderBtn.frame = CGRectMake(CGRectGetMaxX(self.rushOrderBtn.frame), 0, width, height);
    [self.refundOrderBtn setTitle:@"退款" forState:(UIControlStateNormal)];
    [self.refundOrderBtn setTitleColor:kMyColor(204, 204, 204) forState:(UIControlStateNormal)];
    self.refundOrderBtn.titleLabel.font = kFont(15);
    [self.refundOrderBtn addTarget:self action:@selector(clickRefundOrderBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [self.btnView addSubview:self.refundOrderBtn];
    
    self.lineView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.freshOrderBtn.frame), width, 1)];
    self.lineView.backgroundColor = kMyColor(92, 170, 113);
    [self.btnView addSubview:self.lineView];
}

- (UITableView *)freshTableView{
    if (!_freshTableView) {
        _freshTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _freshTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _freshTableView.delegate = self;
        _freshTableView.dataSource = self;
    }
    return _freshTableView;
}
- (UITableView *)abnormaltTableView{
    if (!_abnormaltTableView) {
        _abnormaltTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _abnormaltTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _abnormaltTableView.delegate = self;
        _abnormaltTableView.dataSource = self;
    }
    return _abnormaltTableView;
}
- (UITableView *)rushTableView{
    if (!_rushTableView) {
        _rushTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _rushTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _rushTableView.delegate = self;
        _rushTableView.dataSource = self;
    }
    return _rushTableView;
}
- (UITableView *)refundTableView{
    if (!_refundTableView) {
        _refundTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _refundTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _refundTableView.delegate = self;
        _refundTableView.dataSource = self;
    }
    return _refundTableView;
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
