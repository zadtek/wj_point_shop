//
//  NewOrderListDelegate.h
//  rrbi
//
//  Created by 赵桂安 on 2019/5/2.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol NewOrderListDelegate <NSObject>
- (void)newOrderListindex:(NSInteger)index;
@end
NS_ASSUME_NONNULL_END
