//
//  NewOrderTableViewCell.m
//  rrbi
//
//  Created by 赵桂安 on 2019/5/2.
//

#import "NewOrderTableViewCell.h"

@interface NewOrderTableViewCell ()

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UILabel *tagLabel;
@property (nonatomic, strong) UILabel *tagNameLabel;
@property (nonatomic, strong) UIImageView *arrowImgView;
@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) UILabel *timeNameLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *orderNumNameLabel;
@property (nonatomic, strong) UILabel *orderNumLabel;
@property (nonatomic, strong) UILabel *merchantsNumNameLabel;
@property (nonatomic, strong) UILabel *merchantsNumLabel;
@property (nonatomic, strong) UILabel *incomeNameLabel;
@property (nonatomic, strong) UILabel *incomeLabel;
@property (nonatomic, strong) UILabel *reasonNameLabel;
@property (nonatomic, strong) UILabel *reasonLabel;
@property (nonatomic, strong) UILabel *nonTimeNameLabel;
@property (nonatomic, strong) UILabel *nonTimeLabel;

@property (nonatomic, strong) UIView *customView;
@property (nonatomic, strong) UIView *customLineView;
@property (nonatomic, strong) UILabel *customName;
@property (nonatomic, strong) UILabel *customTel;
@property (nonatomic, strong) UILabel *contactCustom;
@property (nonatomic, strong) UILabel *customAdderss;
@property (nonatomic, strong) UIButton *customButton;

@property (nonatomic, strong) UIView *footerView;
@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) UIButton *centerButton;
@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) UIButton *xiangQingButton;


@end

@implementation NewOrderTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self layoutUI];
        [self layoutConstraints];
        //圆角设置
        self.tagLabel.layer.cornerRadius = 12.5;
        self.tagLabel.clipsToBounds = YES;
//        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.tagLabel.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(12.5, 12.5)];
//        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//        maskLayer.frame = self.tagLabel.bounds;
//        maskLayer.path = maskPath.CGPath;
//        self.tagLabel.layer.mask = maskLayer;
    }
    return self;
}

- (void)setModel:(NewOrderListModel *)model{
    _model = model;
    self.timeLabel.text = model.add_time;
    self.orderNumLabel.text = model.order_sn;
    self.merchantsNumLabel.text = [NSString stringWithFormat:@"#%@",model.serial_num];
    self.incomeLabel.text = model.benifit;
    self.customName.text = model.custom_name;
    self.customAdderss.text = model.custom_addr;
    self.customTel.text = model.custom_tel;
    
    
    if (self.tag == 100) {
        self.nonTimeLabel.text = @"";
        self.reasonLabel.text = @"";
    } else if (self.tag == 200) {
        self.nonTimeLabel.text = model.abnormal_time;
        self.reasonLabel.text = model.abnormal_reason;
    } else if (self.tag == 300) {
        self.nonTimeLabel.text = model.hurry_time;
        self.reasonLabel.text = @"";
    } else {
        self.nonTimeLabel.text = model.refound_time;
        self.reasonLabel.text = model.refound_reason;
    }
    
}

- (void)setOrderType:(NSInteger)orderType{
    _orderType = orderType;
    ///订单数据类型 1.新订单 2.异常配送 3.催单 4.退款
    switch (orderType) {
        case 1:
        {
            self.tagLabel.text = @" 新";
            self.tagLabel.backgroundColor = kMyColor(92, 170, 113);
            self.tagNameLabel.text = @"新订单";
            self.leftButton.backgroundColor = kMyColor(102, 102, 102);
            [self.leftButton setTitle:@"拒绝接单" forState:(UIControlStateNormal)];
            self.rightButton.backgroundColor = kMyColor(92, 170, 113);
            [self.rightButton setTitle:@"确认接单" forState:(UIControlStateNormal)];
            self.centerButton.hidden = YES;
            self.leftButton.hidden = NO;
            self.rightButton.hidden = NO;
            self.reasonLabel.hidden = YES;
            self.reasonNameLabel.hidden = YES;
            self.nonTimeNameLabel.hidden = YES;
            self.nonTimeLabel.hidden = YES;
        }
            break;
        case 2:
        {
            self.tagLabel.text = @" 异";
            self.tagLabel.backgroundColor = kMyColor(153, 153, 153);
            self.tagNameLabel.text = @"异常配送";
            self.leftButton.backgroundColor = kMyColor(153, 153, 153);
            [self.leftButton setTitle:@"联系客服" forState:(UIControlStateNormal)];
            self.rightButton.backgroundColor = kMyColor(153, 153, 153);
            [self.rightButton setTitle:@"联系骑手" forState:(UIControlStateNormal)];
            self.nonTimeNameLabel.text = @"异常时间";
            self.reasonNameLabel.text = @"异常原因";
            self.centerButton.hidden = YES;
            self.leftButton.hidden = NO;
            self.rightButton.hidden = NO;
            self.reasonLabel.hidden = NO;
            self.reasonNameLabel.hidden = NO;
            self.nonTimeNameLabel.hidden = NO;
            self.nonTimeLabel.hidden = NO;
        }
            break;
        case 3:
        {
            self.tagLabel.text = @" 催";
            self.tagLabel.backgroundColor = kMyColor(245, 187, 65);
            self.tagNameLabel.text = @"催单";
            self.centerButton.backgroundColor = kMyColor(245, 187, 65);
            [self.centerButton setTitle:@"知道了" forState:(UIControlStateNormal)];
            self.nonTimeNameLabel.text = @"催单时间";
            self.reasonNameLabel.text = @"";
            self.centerButton.hidden = NO;
            self.leftButton.hidden = YES;
            self.rightButton.hidden = YES;
            self.reasonLabel.hidden = YES;
            self.reasonNameLabel.hidden = YES;
            self.nonTimeNameLabel.hidden = NO;
            self.nonTimeLabel.hidden = NO;
        }
            break;
        case 4:
        {
            self.tagLabel.text = @" 退";
            self.tagLabel.backgroundColor = kMyColor(188, 65, 68);
            self.tagNameLabel.text = @"退款";
            self.leftButton.backgroundColor = kMyColor(188, 65, 68);
            [self.leftButton setTitle:@"拒绝退款" forState:(UIControlStateNormal)];
            self.rightButton.backgroundColor = kMyColor(188, 65, 68);
            [self.rightButton setTitle:@"全额退款" forState:(UIControlStateNormal)];
            self.nonTimeNameLabel.text = @"退款时间";
            self.reasonNameLabel.text = @"退款理由";
            self.centerButton.hidden = YES;
            self.leftButton.hidden = NO;
            self.rightButton.hidden = NO;
            self.reasonLabel.hidden = NO;
            self.reasonNameLabel.hidden = NO;
            self.nonTimeNameLabel.hidden = NO;
            self.nonTimeLabel.hidden = NO;
        }
            break;
            
        default:
            break;
    }
}

- (void)clickLeftBtn{
    if ([self.delegate respondsToSelector:@selector(didBtnindex:model:)]) {
        if (self.orderType == 1) {
            [self.delegate didBtnindex:DidBtnTypeRefusedOrder model:_model];
        }else if (self.orderType == 2){
            [self.delegate didBtnindex:DidBtnTypeCustomer model:_model];
        }else if (self.orderType == 4){
            [self.delegate didBtnindex:DidBtnTypePart model:_model];
        }
    }
}
- (void)clickCenterBtn{
    if ([self.delegate respondsToSelector:@selector(didBtnindex:model:)]) {
        [self.delegate didBtnindex:DidBtnTypeKnow model:_model];
    }
}

- (void)clickCustomBtn{
    if ([self.delegate respondsToSelector:@selector(didBtnindex:model:)]) {
        [self.delegate didBtnindex:DidBtnTypeCustomer model:_model];
    }
}
- (void)clickRightBtn{
    if ([self.delegate respondsToSelector:@selector(didBtnindex:model:)]) {
        if (self.orderType == 1){
            [self.delegate didBtnindex:DidBtnTypeConfirmOrder model:_model];
        }else if (self.orderType == 2){
            [self.delegate didBtnindex:DidBtnTypeRider model:_model];
        }else if (self.orderType == 4){
            [self.delegate didBtnindex:DidBtnTypeAll model:_model];
        }
    }
}

- (void)clickXaingQingBtn {
    
    if ([self.delegate respondsToSelector:@selector(didDetailsBtnindex:model:)]) {
        [self.delegate didDetailsBtnindex:self.orderType model:_model];
    }
}

-(void)layoutUI{
    
    [self.contentView addSubview:self.headerView];
    [self.headerView addSubview:self.tagLabel];
    [self.headerView addSubview:self.tagNameLabel];
    [self.headerView addSubview:self.arrowImgView];
    [self.headerView addSubview:self.lineView];
    [self.headerView addSubview:self.xiangQingButton];
    
    [self.contentView addSubview:self.timeNameLabel];
    [self.contentView addSubview:self.timeLabel];
    [self.contentView addSubview:self.orderNumNameLabel];
    [self.contentView addSubview:self.orderNumLabel];
    [self.contentView addSubview:self.merchantsNumNameLabel];
    [self.contentView addSubview:self.merchantsNumLabel];
    [self.contentView addSubview:self.incomeNameLabel];
    [self.contentView addSubview:self.incomeLabel];
    [self.contentView addSubview:self.reasonNameLabel];
    [self.contentView addSubview:self.reasonLabel];
    [self.contentView addSubview:self.nonTimeNameLabel];
    [self.contentView addSubview:self.nonTimeLabel];
    
    [self.contentView addSubview:self.customView];
    [self.customView addSubview:self.customLineView];
    [self.customView addSubview:self.customName];
    [self.customView addSubview:self.customTel];
    [self.customView addSubview:self.contactCustom];
    [self.customView addSubview:self.customAdderss];
    [self.customView addSubview:self.customButton];
    
    [self.contentView addSubview:self.footerView];
    [self.footerView addSubview:self.leftButton];
    [self.footerView addSubview:self.centerButton];
    [self.footerView addSubview:self.rightButton];
    
    self.tagLabel.text = @"新";
    self.tagNameLabel.text = @"新订单";
    self.timeNameLabel.text = @"下单时间";
//    self.timeLabel.text = @"2019-05-04 12:00:00";
    self.orderNumNameLabel.text = @"订单编号";
//    self.orderNumLabel.text = @"2000000000000000";
    self.merchantsNumNameLabel.text = @"商家流水号";
//    self.merchantsNumLabel.text = @"#20";
    self.incomeNameLabel.text = @"预计收入";
    
//    self.incomeLabel.text = @"￥20";
//    self.leftButton.titleLabel.text = @"拒接接单";
//    self.leftButton.backgroundColor = kMyColor(102, 102, 102);
//    self.centerButton.titleLabel.text = @"知道了";
//    self.rightButton.titleLabel.text = @"确认接单";
//    self.rightButton.backgroundColor = kMyColor(92, 170, 113);
    
//    self.customName.text = @"王**(先生)";
//    self.customTel.text = @"12000000000";
    self.contactCustom.text = @"联系顾客";
//    self.customAdderss.text = @"地址z地址地址";
    
}

-(void)layoutConstraints{
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.height.mas_equalTo(40);
    }];
    
    [self.tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.equalTo(self.headerView.mas_left).mas_offset(-10);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(25);
    }];
    [self.xiangQingButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.equalTo(self.headerView.mas_left);
        make.width.mas_equalTo(self.headerView.mas_width);
        make.height.mas_equalTo(self.headerView.mas_height);
        make.right.equalTo(self.headerView.mas_right);
    }];
    
    [self.tagNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.tagLabel.mas_right).mas_offset(10);
        make.centerY.equalTo(self.tagLabel.mas_centerY);
    }];
    [self.arrowImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.equalTo(self.tagLabel.mas_centerY);
        make.width.mas_equalTo(20);
        make.height.mas_equalTo(20);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0);
        make.left.equalTo(self.headerView.mas_left);
        make.right.equalTo(self.headerView.mas_right);
        make.height.mas_equalTo(0.5f);
    }];
    [self.timeNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView.mas_bottom).mas_equalTo(18);
        make.left.mas_equalTo(12);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timeNameLabel.mas_top);
        make.right.mas_equalTo(-12);
    }];
    [self.orderNumNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timeNameLabel.mas_bottom).mas_offset(18);
        make.left.equalTo(self.timeNameLabel.mas_left);
    }];
    [self.orderNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.orderNumNameLabel.mas_top);
        make.right.equalTo(self.timeLabel.mas_right);
    }];
    
    [self.merchantsNumNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.orderNumNameLabel.mas_bottom).mas_offset(18);
        make.left.equalTo(self.timeNameLabel.mas_left);
    }];
    
    [self.merchantsNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.merchantsNumNameLabel.mas_top);
        make.right.equalTo(self.timeLabel.mas_right);
    }];
    
    [self.incomeNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.merchantsNumNameLabel.mas_bottom).mas_offset(18);
        make.left.equalTo(self.timeNameLabel.mas_left);
    }];
    
    [self.incomeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.incomeNameLabel.mas_top);
        make.right.equalTo(self.timeLabel.mas_right);
    }];
    
    
    
    
    
    [self.nonTimeNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.incomeNameLabel.mas_bottom).mas_offset(18);
        make.left.equalTo(self.timeNameLabel.mas_left);
    }];
    
    [self.nonTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nonTimeNameLabel.mas_top);
        make.right.equalTo(self.timeLabel.mas_right);
    }];
    
    
    [self.reasonNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nonTimeNameLabel.mas_bottom).mas_offset(18);
        make.left.equalTo(self.timeNameLabel.mas_left);
    }];
    
    [self.reasonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.reasonNameLabel.mas_top);
        make.right.equalTo(self.timeLabel.mas_right);
    }];
    
    
    
    [self.customView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom).mas_offset(-70);
        make.left.mas_equalTo(self);
        make.right.mas_equalTo(self);
        make.height.mas_equalTo(70);
    }];
    [self.customLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.customView);
        make.left.right.mas_equalTo(self.customView);
        make.height.mas_equalTo(.5f);
    }];
    [self.customName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.customView).mas_offset(12);
        make.left.mas_equalTo(12);
    }];
    [self.customTel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.customName.mas_bottom).mas_offset(5);
        make.left.equalTo(self.customName);
    }];
    [self.contactCustom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.customName.mas_bottom).mas_offset(19);
        make.right.mas_offset(-12);
    }];
    [self.customAdderss mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contactCustom.mas_bottom).mas_offset(5);
        make.left.equalTo(self.customTel.mas_right).mas_offset(10);
        make.right.equalTo(self.contactCustom);
    }];
    [self.customButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.customView);
        make.bottom.equalTo(self.customView).mas_offset(-18);
    }];
    
    [self.footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.customView.mas_bottom);
        make.left.mas_equalTo(self);
        make.right.mas_equalTo(self);
        make.height.mas_equalTo(60);
    }];
    CGFloat width = (SCREEN_WIDTH - 24 -35)/2;
    
    [self.leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.footerView.mas_top);
        make.left.mas_equalTo(12);
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(40);
    }];
    
    [self.centerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.footerView.mas_top);
        make.centerX.equalTo(self.footerView.mas_centerX);
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(40);
    }];
    
    [self.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.footerView.mas_top);
        make.right.mas_equalTo(-12);
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(40);
    }];
}

-(UIView *)headerView{
    if(!_headerView){
        _headerView = [[UIView alloc]init];
        _headerView.backgroundColor = [UIColor whiteColor];
    }
    return _headerView;
}
-(UILabel *)tagLabel{
    if(!_tagLabel){
        _tagLabel = [[UILabel alloc]init];
        _tagLabel.backgroundColor = kMyColor(92, 170, 113);
        _tagLabel.textColor = [UIColor whiteColor];
        _tagLabel.font = kFont(20);
        _tagLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _tagLabel;
}
-(UILabel *)tagNameLabel{
    if(!_tagNameLabel){
        _tagNameLabel = [[UILabel alloc]init];
        _tagNameLabel.textColor = kMyColor(52, 52, 52);
    }
    return _tagNameLabel;
}
-(UIImageView *)arrowImgView{
    if(!_arrowImgView){
        _arrowImgView = [[UIImageView alloc]init];
        _arrowImgView.image = [UIImage imageNamed:@"icon_arrowright"];
    }
    return _arrowImgView;
}
-(UIView *)lineView{
    if(!_lineView){
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = kMyColor(204, 204, 204);
    }
    return _lineView;
}
-(UILabel *)timeNameLabel{
    if(!_timeNameLabel){
        _timeNameLabel = [[UILabel alloc]init];
        _timeNameLabel.textColor = kMyColor(52, 52, 52);
    }
    return _timeNameLabel;
}
-(UILabel *)timeLabel{
    if(!_timeLabel){
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.textColor = kMyColor(52, 52, 52);
    }
    return _timeLabel;
}
-(UILabel *)orderNumNameLabel{
    if(!_orderNumNameLabel){
        _orderNumNameLabel = [[UILabel alloc]init];
        _orderNumNameLabel.textColor = kMyColor(52, 52, 52);
    }
    return _orderNumNameLabel;
}
-(UILabel *)orderNumLabel{
    if(!_orderNumLabel){
        _orderNumLabel = [[UILabel alloc]init];
        _orderNumLabel.textColor = kMyColor(52, 52, 52);
    }
    return _orderNumLabel;
}
-(UILabel *)merchantsNumNameLabel{
    if(!_merchantsNumNameLabel){
        _merchantsNumNameLabel = [[UILabel alloc]init];
        _merchantsNumNameLabel.textColor = kMyColor(52, 52, 52);
    }
    return _merchantsNumNameLabel;
}
-(UILabel *)merchantsNumLabel{
    if(!_merchantsNumLabel){
        _merchantsNumLabel = [[UILabel alloc]init];
        _merchantsNumLabel.textColor = kMyColor(52, 52, 52);
    }
    return _merchantsNumLabel;
}
-(UILabel *)incomeNameLabel{
    if(!_incomeNameLabel){
        _incomeNameLabel = [[UILabel alloc]init];
        _incomeNameLabel.textColor = kMyColor(52, 52, 52);
    }
    return _incomeNameLabel;
}
-(UILabel *)incomeLabel{
    if(!_incomeLabel){
        _incomeLabel = [[UILabel alloc]init];
        _incomeLabel.textColor = kMyColor(236, 72, 62);
    }
    return _incomeLabel;
}

-(UILabel *)nonTimeNameLabel{
    if(!_nonTimeNameLabel){
        _nonTimeNameLabel = [[UILabel alloc]init];
        _nonTimeNameLabel.textColor = kMyColor(52, 52, 52);
    }
    return _nonTimeNameLabel;
}
-(UILabel *)nonTimeLabel{
    if(!_nonTimeLabel){
        _nonTimeLabel = [[UILabel alloc]init];
        _nonTimeLabel.textColor = kMyColor(52, 52, 52);
    }
    return _nonTimeLabel;
}

-(UILabel *)reasonNameLabel{
    if(!_reasonNameLabel){
        _reasonNameLabel = [[UILabel alloc]init];
        _reasonNameLabel.textColor = kMyColor(52, 52, 52);
    }
    return _reasonNameLabel;
}
-(UILabel *)reasonLabel{
    if(!_reasonLabel){
        _reasonLabel = [[UILabel alloc]init];
        _reasonLabel.textColor = kMyColor(52, 52, 52);
    }
    return _reasonLabel;
}


-(UIView *)customView{
    if(!_customView){
        _customView = [[UIView alloc]init];
        _customView.backgroundColor = [UIColor whiteColor];
    }
    return _customView;
}
-(UIView *)customLineView{
    if(!_customLineView){
        _customLineView = [[UIView alloc]init];
        _customLineView.backgroundColor = kMyColor(204, 204, 204);
    }
    return _customLineView;
}
-(UILabel *)customName{
    if(!_customName){
        _customName = [[UILabel alloc]init];
        _customName.textColor = [UIColor blackColor];
        _customName.font = [UIFont boldSystemFontOfSize:17];
    }
    return _customName;
}
-(UILabel *)customTel{
    if(!_customTel){
        _customTel = [[UILabel alloc]init];
        _customTel.textColor = kMyColor(183, 130, 43);
        _customTel.font = kFont(13);
    }
    return _customTel;
}
-(UILabel *)contactCustom{
    if(!_contactCustom){
        _contactCustom = [[UILabel alloc]init];
        _contactCustom.textColor = kMyColor(77, 77, 77);
        _contactCustom.font = kFont(13);
    }
    return _contactCustom;
}
-(UILabel *)customAdderss{
    if(!_customAdderss){
        _customAdderss = [[UILabel alloc]init];
        _customAdderss.textColor = kMyColor(77, 77, 77);
        _customAdderss.font = kFont(13);
        _customAdderss.textAlignment = NSTextAlignmentLeft;
    }
    return _customAdderss;
}
-(UIButton *)customButton{
    if(!_customButton){
        _customButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _customButton.titleLabel.textColor = [UIColor whiteColor];
        [_customButton addTarget:self action:@selector(clickCustomBtn) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _customButton;
}
-(UIView *)footerView{
    if(!_footerView){
        _footerView = [[UIView alloc]init];
        _footerView.backgroundColor = [UIColor whiteColor];
    }
    return _footerView;
}
-(UIButton *)leftButton{
    if(!_leftButton){
        _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftButton.titleLabel.textColor = [UIColor whiteColor];
        [_leftButton addTarget:self action:@selector(clickLeftBtn) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _leftButton;
}
-(UIButton *)centerButton{
    if(!_centerButton){
        _centerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _centerButton.titleLabel.textColor = [UIColor whiteColor];
        [_centerButton addTarget:self action:@selector(clickCenterBtn) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _centerButton;
}
-(UIButton *)rightButton{
    if(!_rightButton){
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightButton.titleLabel.textColor = [UIColor whiteColor];
        [_rightButton addTarget:self action:@selector(clickRightBtn) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _rightButton;
}

- (UIButton *)xiangQingButton {
    if(!_xiangQingButton){
        _xiangQingButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _xiangQingButton.titleLabel.textColor = [UIColor whiteColor];
        [_xiangQingButton addTarget:self action:@selector(clickXaingQingBtn) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _xiangQingButton;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
