//
//  NewOrderTableViewCell.h
//  rrbi
//
//  Created by 赵桂安 on 2019/5/2.
//

#import <UIKit/UIKit.h>
#import "NewOrderListModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, DidBtnType) {
    ///拒绝接单
    DidBtnTypeRefusedOrder,
    ///确认接单
    DidBtnTypeConfirmOrder,
    ///联系客户
    DidBtnTypeCustomer,
    ///联系骑手
    DidBtnTypeRider,
    ///知道了
    DidBtnTypeKnow,
    ///部分退款
    DidBtnTypePart,
    ///全额退款
    DidBtnTypeAll,
};

@protocol NewOrderTableViewCellDelegate <NSObject>

///点击按钮
- (void)didBtnindex:(DidBtnType)index model:(NewOrderListModel *)model;
- (void)didDetailsBtnindex:(NSInteger)index model:(NewOrderListModel *)model;

@end


@interface NewOrderTableViewCell : UITableViewCell

///订单数据类型 1.新订单 2.异常配送 3.催单 4.退款
@property (nonatomic, assign) NSInteger orderType;

@property (nonatomic, strong) NewOrderListModel * model;

@property (nonatomic, weak) id<NewOrderTableViewCellDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
