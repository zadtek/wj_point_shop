//
//  StoreMange.m
//  RRBI
//
//  Created by kyjun on 16/4/26.
//
//

#import "StoreMange.h"
#import "MStore.h"
#import "GoodsList.h"
#import "CommentList.h"
#import "AddNotice.h"
#import "Account.h"
#import "StoreManageCollectionReusableView.h"
#import "StoreManageCollectionViewCell.h"

#import "ShopListViewController.h"
#import "PromotionSettingViewController.h"
#import "StroeMessageViewController.h"
#import "ReceivingOrderViewController.h"

#import "ProductDataListViewController.h"


#define KImageMargin 10.f
#define Header_H 250.f
#define Item_H 100.f


@interface StoreMange ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UINavigationControllerDelegate>

@property (nonatomic, strong) UICollectionView  *collectionView;


//@property(nonatomic,strong) UIView* topView;
//@property(nonatomic,strong) UIImageView* iconLogo;
//@property(nonatomic,strong) UILabel* labelStoreName;
//@property(nonatomic,strong) UILabel* labelStoreSummary;
//@property(nonatomic,strong) UIButton* btnChangeStatus;
//@property(nonatomic,strong) UIView* bottomView;
//@property(nonatomic,strong) UILabel* labelTodaySale;
//@property(nonatomic,strong) UILabel* labelSumSale;

//@property(nonatomic,strong) UIView* contentView;
//@property(nonatomic,strong) UIButton* btnComment;
//@property(nonatomic,strong) UIButton* btnMoney;
//@property(nonatomic,strong) UIButton* btnGoods;
//@property(nonatomic,strong) UIButton* btnNotice;

@property(nonatomic,strong)  NSArray* arrayTitle;
@property(nonatomic,strong) NSArray* arrayImg;

@property(nonatomic,strong) MStore* entity;


@end

@implementation StoreMange


//-(instancetype)init{
//    self = [super init];
//    if(self){
//        [self.tabBarItem setImage:[UIImage imageNamed:@"tab-store-default"]];
//        [self.tabBarItem setSelectedImage:[[UIImage imageNamed:@"tab-store-enter"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//        [self.tabBarItem setTitle:@"门店管理"];
//        self.tabBarItem.tag=2;
//    }
//    return self;
//}
#pragma mark - UINavigationControllerDelegate
// 将要显示控制器
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // 判断要显示的控制器是否是自己
    BOOL isShowPersonPage = [viewController isKindOfClass:[self class]];
    [self.navigationController setNavigationBarHidden:isShowPersonPage animated:animated];

}
//设置字体颜色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;//白色
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"";
    self.navigationController.delegate = self;
    self.navigationController.navigationBar.hidden = YES;
    
    self.arrayTitle=  @[@"用户评价",@"财务信息",@"商品管理",@"店铺公告",@"店铺分类",@"促销设置",@"联系业务员",@"接单设置"];
    self.arrayImg =   @[@"icon-comment",@"icon-money",@"icon-goods",@"icon-notice",@"icon_dianpufenlei",@"icon_cuxiao",@"icon_lianxikefu",@"icon-receivingorder"];
    
    [self creatSubViews];
//    [self layoutUI];
//    [self layoutConstraints];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self queryData];
//    self.navigationController.navigationBarHidden = YES;
}
//
//-(void)viewWillDisappear:(BOOL)animated{
//    [super viewWillDisappear:YES];
//    self.navigationController.navigationBarHidden =NO;
//}
-(void)creatSubViews{
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT- TABBAR_HEIGHT) collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.collectionView];
    
    [self.collectionView registerClass:[StoreManageCollectionViewCell class] forCellWithReuseIdentifier:@"CollectionView"];
    [self.collectionView registerClass:[StoreManageCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CollectionViewHeader"];
}
#pragma mark - ******* collectionview代理方法 *******
//设置分区数（必须实现）
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

//设置每个分区的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.arrayTitle.count;
    
    
}
//设置item大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(SCREEN_WIDTH/2, Item_H);
    
}
//设置返回每个item的属性必须实现
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StoreManageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionView" forIndexPath:indexPath];
    [cell setDataWithImage:self.arrayImg[indexPath.row] title:self.arrayTitle[indexPath.row]];
    
    CALayer* border = [[CALayer alloc]init];
    border.frame = CGRectMake(0,Item_H - 1, SCREEN_WIDTH/2, 1.f);
    border.backgroundColor = [UIColor colorWithRed:236/255.f green:237/255.f blue:239/255.f alpha:1.0].CGColor;
    [cell.layer addSublayer:border];
    if (indexPath.row%2==0) {//如果是偶数
        border = [[CALayer alloc]init];
        border.frame = CGRectMake(SCREEN_WIDTH/2-1, 0.f, 1.f, Item_H);
        border.backgroundColor = [UIColor colorWithRed:236/255.f green:237/255.f blue:239/255.f alpha:1.0].CGColor;
        [cell.layer addSublayer:border];
    }
    
    
    
    return cell;
}

//行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0.01;
}

//列间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
//分区缩进
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


//头视图大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(SCREEN_WIDTH, Header_H);
}

//对头视图或者尾视图进行设置
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableView = nil;
    
    if (kind == UICollectionElementKindSectionHeader){
        
        StoreManageCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CollectionViewHeader" forIndexPath:indexPath];
        
        headerView.entity = self.entity;
        
        //点击头像
        headerView.LogoImageBlock = ^{
            
            StroeMessageViewController* controller = [[StroeMessageViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
            
        };
        
        headerView.btnChangeStatusBlock = ^(NSInteger tag) {
            if (tag == 10001) {
                
                [self changeStatusTouch];
                
            }else{
                
                //修改营业时间
                NSArray *time = @[@"0:00", @"0:30", @"1:00", @"1:30", @"2:00", @"2:30", @"3:00", @"3:30",@"4:00", @"4:30",@"5:00", @"5:30",@"6:00", @"6:30",@"7:00", @"7:30",@"8:00", @"8:30",@"9:00", @"9:30",@"10:00", @"10:30",@"11:00", @"11:30",@"12:00", @"12:30",@"13:00", @"13:30",@"14:00", @"14:30",@"15:00", @"15:30",@"16:00", @"16:30",@"17:00", @"17:30",@"18:00", @"18:30",@"19:00", @"19:30",@"20:00", @"20:30",@"21:00", @"21:30",@"22:00", @"22:30",@"23:00", @"23:30"];
                NSArray *dataSources = @[time, time];
                [CGXPickerView showStringPickerWithTitle:@"修改营业时间" DataSource:dataSources DefaultSelValue:@[@"0",@"0"] IsAutoSelect:NO Manager:nil ResultBlock:^(id selectValue, id selectRow) {
                    
                    NSArray *array = [NSArray arrayWithArray:selectValue];
                    NSString * open_time = [array[0] isEqualToString: @"0"]? @"0:00":array[0];
                    NSString * close_time = [array[1] isEqualToString: @"0"]? @"0:00":array[1];
                    [self showHUD];
                    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
                    NSDictionary* arg = @{
                                          @"ince":@"set_shop_open_time",
                                          @"sid":[UserModel sharedInstanced].ru_Id,
                                          @"open_time": open_time,
                                          @"close_time":close_time
                                          };
                    [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
                        [WMHelper outPutJsonString:responseObject];
                        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
                        if(flag == 200){
                            
                            NSString *text = [NSString stringWithFormat:@"%@ ~ %@", open_time, close_time];
                            headerView.labelStoreTime.text = [NSString stringWithFormat:@"营业时间：%@",text];
                            [self hidHUD];
                        }else{
                            
                            [self hidHUD:[responseObject objectForKey:@"msg"]];
                        }
                    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
                        [self hidHUD:@"网络异常"];
                    }];

                }];

                
            }
        };
        
        
        reusableView = headerView;
        
    }
    
    return reusableView;
    
}

//点击item
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger index = indexPath.row;
    switch (index) {
        case 0:
        {
            CommentList* controller = [[CommentList alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
            
        }
            break;
        case 1:
        {
            Account* controller = [[Account alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case 2:
        {
            GoodsList* controller =[[GoodsList alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case 3:
        {
            AddNotice* controller = [[AddNotice alloc]initWithNotice:self.entity.notice];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
            
        case 4:
        {
            ShopListViewController* controller = [[ShopListViewController alloc] init];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
            
        case 5:
        {
            PromotionSettingViewController* controller = [[PromotionSettingViewController alloc] init];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
            
            
        case 6:
        {
            NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"tel:%@",self.entity.tel];
            UIWebView *callWebview = [[UIWebView alloc] init];
            [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
            [self.view addSubview:callWebview];
        }
            break;
            
        case 7:
        {
            ReceivingOrderViewController* controller = [[ReceivingOrderViewController alloc] init];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
            
            
        default:
            break;
    }
    
    
}

#pragma mark =====================================================  DataSource
-(void)queryData{
  
    [self showHUD];
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{@"ince":@"get_shop_info",@"sid":[UserModel sharedInstanced].ru_Id};
    [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            NSDictionary* dict = [responseObject objectForKey:@"data"];
            if(![WMHelper isNULLOrnil:dict]){
                self.entity = [[MStore alloc]initWithStore:dict];
                MMember* item = self.Identity.userInfo;
                item.bankCard = [dict objectForKey: @"owner_bank"];
                item.cardPerson = [dict objectForKey: @"owner_name"];
                item.balance = self.entity.balance;
                item.top_note = [dict objectForKey: @"top_note"];
                item.bottom_note = [dict objectForKey: @"bottom_note"];
                item.balanc_desc = [dict objectForKey: @"balanc_desc"];
                item.frozen_money = [dict objectForKey: @"frozen_money"];
                item.frozen_money_desc = [dict objectForKey: @"frozen_money_desc"];
                item.print_num = [NSString stringWithFormat:@"%@",[dict objectForKey: @"print_num"]];
                item.confirm_order_way = [NSString stringWithFormat:@"%@",[dict objectForKey: @"confirm_order_way"]];


                Boolean flag = [NSKeyedArchiver archiveRootObject:item toFile:[WMHelper archiverPath]];
                if(flag){
                    
                }else
                    NSLog(@"%@",@"归档失败!");
                
                //                [self loadData:self.entity];
                
                [self.collectionView reloadData];
            }
            [self hidHUD];
        }else{
            
            [self hidHUD:[responseObject objectForKey:@"msg"]];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        [self hidHUD:@"网络异常"];
    }];
    
}
-(void)changeStatusTouch{
    
    [self showHUD];
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{@"ince":@"set_shop_open",@"sid":[UserModel sharedInstanced].ru_Id,@"close":[self.entity.status isEqualToString:@"1"]?@"0":@"1"};
    [mgr POST:kHost parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            self.entity.status =  [self.entity.status isEqualToString:@"1"]?@"0":@"1";
            [self.collectionView reloadData];
            //            [self loadData:self.entity];
            [self hidHUD];
        }else{
            
            [self hidHUD:[responseObject objectForKey:@"msg"]];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        [self hidHUD:@"网络异常"];
    }];
    
}

#pragma mark =====================================================  user interface layout
//-(void)layoutUI{
//    [self.view addSubview:self.topView];
//    [self.topView addSubview:self.iconLogo];
//    [self.topView addSubview:self.labelStoreName];
//    [self.topView addSubview:self.labelStoreSummary];
//    [self.topView addSubview:self.btnChangeStatus];
//    [self.view addSubview:self.bottomView];
//    [self.bottomView addSubview:self.labelTodaySale];
//    [self.bottomView addSubview:self.labelSumSale];
//
//
//
//    [self.view addSubview:self.contentView];
//    for (int row= 0; row<2; row++) {
//        for (int columns=0; columns<2; columns++) {
//            NSInteger index = 2*row+columns;
//            CGRect fram =CGRectMake(SCREEN_WIDTH*columns/2, 200/2*row, SCREEN_WIDTH/2, 200/2);
//            UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
//            btn.tag = index+55;
//            btn.frame =fram;
//            CALayer* border = [[CALayer alloc]init];
//            border.frame = CGRectMake(0,99.f, SCREEN_WIDTH/2, 1.f);
//            border.backgroundColor = [UIColor colorWithRed:236/255.f green:237/255.f blue:239/255.f alpha:1.0].CGColor;
//            [btn.layer addSublayer:border];
//            if(columns==0){
//                border = [[CALayer alloc]init];
//                border.frame = CGRectMake(SCREEN_WIDTH/2-1,0.f, 1.f, 100.f);
//                border.backgroundColor = [UIColor colorWithRed:236/255.f green:237/255.f blue:239/255.f alpha:1.0].CGColor;
//                [btn.layer addSublayer:border];
//            }
//            [btn addTarget:self action:@selector(categoryTouch:) forControlEvents:UIControlEventTouchUpInside];
//            [self.contentView addSubview:btn];
//            UIImageView* photo = [[UIImageView alloc]init];
//            photo.frame = CGRectMake((fram.size.width-60)/2, (fram.size.height-20-60)/2, 60, 60);
//            [photo setImage:[UIImage imageNamed:self.arrayImg[index]]];
//            [btn addSubview:photo];
//            UILabel* title = [[ UILabel alloc]init];
//            title.frame = CGRectMake(0, 70, fram.size.width, 20);
//            title.text = self.arrayTitle[index];
//            title.textAlignment = NSTextAlignmentCenter;
//            title.font = [UIFont systemFontOfSize:14.f];
//            [btn addSubview:title];
//        }
//    }
//}

//-(void)layoutConstraints{
//    self.topView.translatesAutoresizingMaskIntoConstraints = NO;
//    self.iconLogo.translatesAutoresizingMaskIntoConstraints = NO;
//    self.labelStoreName.translatesAutoresizingMaskIntoConstraints = NO;
//    self.labelStoreSummary.translatesAutoresizingMaskIntoConstraints = NO;
//    self.btnChangeStatus.translatesAutoresizingMaskIntoConstraints = NO;
//    self.bottomView.translatesAutoresizingMaskIntoConstraints = NO;
//    self.labelTodaySale.translatesAutoresizingMaskIntoConstraints = NO;
//    self.labelSumSale.translatesAutoresizingMaskIntoConstraints = NO;
//    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
//    
//    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.topView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:140.f]];
//    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.topView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.topView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.topView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
//    
//    [self.iconLogo addConstraint:[NSLayoutConstraint constraintWithItem:self.iconLogo attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:60.f]];
//    [self.iconLogo addConstraint:[NSLayoutConstraint constraintWithItem:self.iconLogo attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:60.f]];
//    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.iconLogo attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeTop multiplier:1.0 constant:44.f]];
//    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.iconLogo attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:15.f]];
//    
//    [self.labelStoreName addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStoreName attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:20.f]];
//    [self.labelStoreName addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStoreName attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/2]];
//    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStoreName attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.iconLogo attribute:NSLayoutAttributeTop multiplier:1.0 constant:10.f]];
//    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStoreName attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.iconLogo attribute:NSLayoutAttributeRight multiplier:1.0 constant:10.f]];
//    
//    [self.labelStoreSummary addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStoreSummary attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40.f]];
//    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStoreSummary attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelStoreName attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
//    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStoreSummary attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.iconLogo attribute:NSLayoutAttributeRight multiplier:1.0 constant:10.f]];
//    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStoreSummary attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
//    
//    [self.btnChangeStatus addConstraint:[NSLayoutConstraint constraintWithItem:self.btnChangeStatus attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:20.f]];
//    [self.btnChangeStatus addConstraint:[NSLayoutConstraint constraintWithItem:self.btnChangeStatus attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:60.f]];
//    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnChangeStatus attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelStoreName attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
//    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnChangeStatus attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
//    
//    [self.bottomView addConstraint:[NSLayoutConstraint constraintWithItem:self.bottomView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:80.f]];
//    [self.bottomView addConstraint:[NSLayoutConstraint constraintWithItem:self.bottomView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.bottomView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.bottomView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
//    
//    [self.labelTodaySale addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTodaySale attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:80.f]];
//    [self.labelTodaySale addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTodaySale attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/2]];
//    [self.bottomView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTodaySale attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.bottomView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
//    [self.bottomView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTodaySale attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.bottomView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
//    
//    [self.labelSumSale addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumSale attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:80.f]];
//    [self.labelSumSale addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumSale attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/2]];
//    [self.bottomView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumSale attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.bottomView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
//    [self.bottomView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumSale attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.labelTodaySale attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.f]];
//    
//    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:200.f]];
//    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.bottomView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
//    
//    
//}



#pragma mark =====================================================  SEL
//-(IBAction)categoryTouch:(UIButton*)sender{
//    NSInteger index = sender.tag;
//    switch (index) {
//        case 55:
//        {
//            CommentList* controller = [[CommentList alloc]init];
//            [self.navigationController pushViewController:controller animated:YES];
//
//        }
//            break;
//        case 56:
//        {
//            Account* controller = [[Account alloc]init];
//            [self.navigationController pushViewController:controller animated:YES];
//        }
//            break;
//        case 57:
//        {
//            GoodsList* controller =[[GoodsList alloc]init];
//            [self.navigationController pushViewController:controller animated:YES];
//        }
//            break;
//        case 58:
//        {
//            AddNotice* controller = [[AddNotice alloc]initWithNotice:self.entity.notice];
//            [self.navigationController pushViewController:controller animated:YES];
//        }
//            break;
//        default:
//            break;
//    }
//}



#pragma mark =====================================================  private method
//-(void)loadData:(MStore*)item{
//    [self.iconLogo sd_setImageWithURL:[NSURL URLWithString:item.logo] placeholderImage:[UIImage imageNamed:@"icon-store"]];
//    self.labelStoreName.text = item.storeName;
//    self.labelStoreSummary.text =item.notice;
//    self.labelTodaySale.text = item.todayMoney;
//    self.labelSumSale.text = item.balance;
//    [self.btnChangeStatus setTitle:[item.status isEqualToString:@"1"]?@"营业中":@"休息中" forState:UIControlStateNormal];
//
//    NSString* money = [NSString stringWithFormat:@"￥%@",item.todayMoney];
//    NSString* strMoney = [NSString stringWithFormat:@"%@ \n 今日收入",money];
//
//    NSMutableAttributedString* attributeStr  = [[NSMutableAttributedString alloc]initWithString:strMoney];
//    [attributeStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:22.f],NSForegroundColorAttributeName:[UIColor colorWithRed:251/255.f green:182/255.f blue:92/255.f alpha:1.0]} range:[strMoney rangeOfString:money]];
//    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    [paragraphStyle setLineSpacing:8];
//    [attributeStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [strMoney length])];
//    self.labelTodaySale.attributedText = attributeStr;
//    self.labelTodaySale.textAlignment = NSTextAlignmentCenter;
//
//    money = [NSString stringWithFormat:@"￥%@",item.balance];
//    strMoney = [NSString stringWithFormat:@"%@ \n 账户余额",money];
//    attributeStr  = [[NSMutableAttributedString alloc]initWithString:strMoney];
//    [attributeStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:22.f],NSForegroundColorAttributeName:[UIColor colorWithRed:64/255.f green:70/255.f blue:77/255.f alpha:1.0]} range:[strMoney rangeOfString:money]];
//    paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    [paragraphStyle setLineSpacing:8];
//    [attributeStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [strMoney length])];
//    self.labelSumSale.attributedText = attributeStr;
//    self.labelSumSale.textAlignment = NSTextAlignmentCenter;
//
//}

#pragma mark =====================================================  property package
//-(UIView *)topView{
//    if(!_topView){
//        _topView = [[UIView alloc]init];
//        _topView.backgroundColor = [UIColor colorWithRed:50/255.f green:58/255.f blue:69/255.f alpha:1.0];
//    }
//    return _topView;
//}
//
//-(UIImageView *)iconLogo{
//    if(!_iconLogo){
//        _iconLogo = [[UIImageView alloc]init];
//        _iconLogo.layer.masksToBounds = YES;
//        _iconLogo.layer.cornerRadius = 30.f;
//        _iconLogo.layer.borderColor =  [UIColor colorWithRed:190/255.f green:194/255.f blue:197/255.f alpha:1.0].CGColor;
//        _iconLogo.layer.borderWidth = 2.f;
//    }
//    return _iconLogo;
//}
//
//-(UILabel *)labelStoreName{
//    if(!_labelStoreName){
//        _labelStoreName = [[UILabel alloc]init];
//        _labelStoreName.textColor = [UIColor whiteColor];
//    }
//    return _labelStoreName;
//}
//
//-(UILabel *)labelStoreSummary{
//    if(!_labelStoreSummary){
//        _labelStoreSummary = [[UILabel alloc]init];
//        _labelStoreSummary.textColor = [UIColor colorWithRed:148/255.f green:152/255.f blue:157/255.f alpha:1.0];
//        _labelStoreSummary.font = [UIFont systemFontOfSize:14.f];
//        _labelStoreSummary.numberOfLines = 0;
//        _labelStoreSummary.lineBreakMode = NSLineBreakByWordWrapping;
//    }
//    return _labelStoreSummary;
//}
//
//-(UIButton *)btnChangeStatus{
//    if(!_btnChangeStatus){
//        _btnChangeStatus = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_btnChangeStatus setTitleColor: [UIColor colorWithRed:255/255.f green:190/255.f blue:33/255.f alpha:1.0] forState:UIControlStateNormal];
//        _btnChangeStatus.layer.masksToBounds = YES;
//        _btnChangeStatus.layer.cornerRadius = 5.f;
//        _btnChangeStatus.layer.borderColor =  [UIColor colorWithRed:255/255.f green:190/255.f blue:33/255.f alpha:1.0].CGColor;
//        _btnChangeStatus.layer.borderWidth = 1.f;
//        _btnChangeStatus.titleLabel.font = [UIFont systemFontOfSize:14.f];
//        [_btnChangeStatus addTarget:self action:@selector(changeStatusTouch:) forControlEvents:UIControlEventTouchUpInside];
//    }
//    return _btnChangeStatus;
//}
//
//-(UIView *)bottomView{
//    if(!_bottomView){
//        _bottomView = [[UIView alloc]init];
//        CALayer* border = [[CALayer alloc]init];
//        border.frame = CGRectMake(0, 79.f, SCREEN_WIDTH, 1.f);
//        border.backgroundColor = [UIColor colorWithRed:236/255.f green:237/255.f blue:239/255.f alpha:1.0].CGColor;
//        [_bottomView.layer addSublayer:border];
//    }
//    return _bottomView;
//}
//
//-(UILabel *)labelTodaySale{
//    if(!_labelTodaySale){
//        _labelTodaySale = [[UILabel alloc]init];
//        _labelTodaySale.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
//        _labelTodaySale.font = [UIFont systemFontOfSize:14.f];
//        _labelTodaySale.numberOfLines = 0;
//        _labelTodaySale.lineBreakMode = NSLineBreakByWordWrapping;
//        CALayer* border = [[CALayer alloc]init];
//        border.frame = CGRectMake(SCREEN_WIDTH/2-1, 20.f, 1.f, 40.f);
//        border.backgroundColor = [UIColor colorWithRed:236/255.f green:237/255.f blue:239/255.f alpha:1.0].CGColor;
//        [_labelTodaySale.layer addSublayer:border];
//
//    }
//    return _labelTodaySale;
//}
//
//-(UILabel *)labelSumSale{
//    if(!_labelSumSale){
//        _labelSumSale = [[UILabel alloc]init];
//        _labelSumSale.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
//        _labelSumSale.font = [UIFont systemFontOfSize:14.f];
//        _labelSumSale.numberOfLines = 0;
//        _labelSumSale.lineBreakMode = NSLineBreakByWordWrapping;
//    }
//    return _labelSumSale;
//}

//-(UIView *)contentView{
//    if(!_contentView){
//        _contentView = [[UIView alloc]init];
//    }
//    return _contentView;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
