//
//  AppDelegate.h
//  rrbi
//
//  Created by kyjun on 16/4/26.
//
//

#import <UIKit/UIKit.h>

#import <AudioToolbox/AudioToolbox.h>


static NSString *appKey = @"97bd9edade39b958fbded241";
static NSString *channel = @"http://www.xyrr.com";
static BOOL isProduction = FALSE;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, copy) NSString *fileName;


@end

