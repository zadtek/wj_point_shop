//
//  MainTabBarViewController.m
//  rrbi
//
//  Created by mac book on 2018/12/3.
//

#import "MainTabBarViewController.h"
#import "MainNavgationViewController.h"
#import "OrderManagementViewController.h"
#import "NewOrder.h"
#import "ProcessedOrder.h"
#import "StoreMange.h"
#import "Setting.h"

///新画页面
#import "NewOrderViewController.h"
//新门店运营
#import "StoreOperationMainViewController.h"

#import "MyViewShopController.h"



@interface MainTabBarViewController ()<UITabBarControllerDelegate>

@end

@implementation MainTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.delegate = self;
    [self.tabBar setTranslucent:NO];
//    self.tabBar.tintColor = theme_navigation_color;
    
    
//    [self addchildViewVc:[[NewOrder alloc]init] withImagName:@"tab-new-order-default" withSelectedImagName:@"tab-new-order-enter" andTitle:@"新订单"];
//    [self addchildViewVc:[[ProcessedOrder alloc]init] withImagName:@"tab-processed-order-default" withSelectedImagName:@"tab-processed-order-enter"  andTitle:@"已处理订单"];
//    [self addchildViewVc:[[StoreMange alloc]init] withImagName:@"tab-store-default" withSelectedImagName:@"tab-store-enter" andTitle:@"门店管理"];
//    [self addchildViewVc:[[Setting alloc]init] withImagName:@"tab-setting-default" withSelectedImagName:@"tab-setting-enter"  andTitle:@"设置"];
    
    [self addchildViewVc:[[NewOrderViewController alloc]init] withImagName:@"Tab_daichuli_noSelect" withSelectedImagName:@"Tab_daichuli_select" andTitle:@"待处理"];
    [self addchildViewVc:[[OrderManagementViewController alloc]init] withImagName:@"Tab_dingdnaguanli_noSelect" withSelectedImagName:@"Tab_dingdangunali_select"  andTitle:@"订单管理"];
    [self addchildViewVc:[[StoreOperationMainViewController alloc]init] withImagName:@"Tab_mendianyunying_noSelect" withSelectedImagName:@"Tab_mendianyunying_select" andTitle:@"门店运营"];
    [self addchildViewVc:[[MyViewShopController alloc]init] withImagName:@"Tab_wode_noSelect" withSelectedImagName:@"Tab_wode_select"  andTitle:@"我的"];
//    [self addchildViewVc:[[Setting alloc]init] withImagName:@"Tab_wode_noSelect" withSelectedImagName:@"Tab_wode_select"  andTitle:@"我的"];

}
//添加子控制器方法
- (void)addchildViewVc:(UIViewController *)vc withImagName:(NSString *)imgName withSelectedImagName:(NSString *)selectedName andTitle:(NSString *)titile{
    
    //设置标题
    vc.title = titile;
    //设置图片
//    vc.tabBarItem.image = [UIImage imageNamed:imgName];
    //设置选中图片
    //    vc.tabBarItem.selectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",imgName]];
    
    
    vc.tabBarItem.image  = [[UIImage imageNamed:imgName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    //设置选中状态item背景颜色//8fc31f
    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:getColor(mainColor),NSFontAttributeName:kFontNameSize(15)} forState:UIControlStateSelected];
    
    
    //添加子控制器
    MainNavgationViewController *navVc = [[MainNavgationViewController alloc] initWithRootViewController:vc];
    [self addChildViewController:navVc];
}


#pragma mark - ******* 处理远程推送过来的消息 *******
-(void)jumpHome{
    
    
    self.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:YES];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationRemoteAt object:nil];

    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
