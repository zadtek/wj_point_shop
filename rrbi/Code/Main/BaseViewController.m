//
//  BaseViewController.m
//  BaseFrame
//
//  Created by apple on 16/9/28.
//  Copyright © 2016年 com.mxh. All rights reserved.
//

#import "BaseViewController.h"


@interface BaseViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic,copy) void(^selectPhotoHandle)(UIImage *img);

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //布局 不延伸
//    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.automaticallyAdjustsScrollViewInsets = NO;
    

    //重新创建一个barButtonItem
//    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//    //设置backBarButtonItem即可
//    self.navigationItem.backBarButtonItem = backItem;
//    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];


    [self initNavigationBar];
    
    
}
-(void)initNavigationBar
{
    
    //返回按钮
    if (self.navigationController.viewControllers.count > 1) {
        
        UIBarButtonItem *backButton =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_return_b"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
        self.navigationItem.leftBarButtonItem = backButton;
    }
    
}

- (void)back {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (MBProgressHUD *)progressHUD {
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc]initWithView:self.view];
        //        _progressHUD.frame = [UIScreen mainScreen].bounds;
        //        _progressHUD.backgroundColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.75];
    }
    return _progressHUD;
}



/**
 *  显示大菊花
 */
-(void)showprogressHUD{
    self.tabBarController.view.userInteractionEnabled = NO;
    self.navigationController.view.userInteractionEnabled = NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.navigationController.view addSubview:self.progressHUD];
    [self.progressHUD showAnimated:YES];
}

/**
 *  隐藏大菊花
 */
-(void)hiddenProgressHUD{
    self.tabBarController.view.userInteractionEnabled = YES;
    self.navigationController.view.userInteractionEnabled = YES;
    [self.progressHUD hideAnimated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.progressHUD removeFromSuperview];
}

/**
 弹出提示框 选择相机或者相册
 
 @param selectPhotoHandle 选中或拍摄的图片
 */
- (void)selectPhotoAlbumWithSelectPhotoHandle:(void (^)(UIImage *))selectPhotoHandle{
    self.selectPhotoHandle = selectPhotoHandle;
    
    UIAlertController *av = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takePhoto];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"从手机相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self LocalPhoto];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [av addAction:action1];
    [av addAction:action2];
    [av addAction:cancelAction];
    [self presentViewController:av animated:YES completion:nil];
}

//开始拍照
-(void)takePhoto
{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        //设置拍照后的图片可被编辑
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:nil];
    }else{
        NSLog(@"模拟其中无法打开照相机,请在真机中使用");
    }
}

//打开本地相册
-(void)LocalPhoto{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    //设置选择后的图片可被编辑
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}

//当选择一张图片后进入这里
-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage* image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    if (self.selectPhotoHandle) {
        self.selectPhotoHandle(image);
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

/**
 *  拨打电话
 *
 *  @param phoneNumber 号码
 */
- (void)callPhone:(NSString *)phoneNumber{
    UIAlertController *av = [UIAlertController alertControllerWithTitle:@"拨打电话" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",phoneNumber]]];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [av addAction:action1];
    [av addAction:action2];
    [self presentViewController:av animated:YES completion:nil];
}
- (void)AlertMessage:(NSString *)msg complection:(AlertBolck)block {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        block();
    }];
    [alertVC addAction:action];
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (void)AlertMessage:(NSString *)msg {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertVC addAction:action];
    [self presentViewController:alertVC animated:YES completion:nil];
}
@end
