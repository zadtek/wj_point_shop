//
//  BaseViewController.h
//  BaseFrame
//
//  Created by apple on 16/9/28.
//  Copyright © 2016年 com.mxh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

typedef void(^AlertBolck)(void);
@interface BaseViewController : UIViewController


@property (nonatomic, strong) MBProgressHUD * progressHUD;



/**
 *  显示大菊花
 */
-(void)showprogressHUD;

/**
 *  隐藏大菊花
 */
-(void)hiddenProgressHUD;


/**
 弹出提示框 选择相机或者相册
 
 @param selectPhotoHandle 选中或拍摄的图片
 */
- (void)selectPhotoAlbumWithSelectPhotoHandle:(void (^)(UIImage *))selectPhotoHandle;

/**
 *  拨打电话
 *
 *  @param phoneNumber 号码
 */
- (void)callPhone:(NSString *)phoneNumber;
//弹出信息提示 单按钮
- (void)AlertMessage:(NSString *)msg
         complection:(AlertBolck)block;

- (void)back;
@end
