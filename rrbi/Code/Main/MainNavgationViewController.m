//
//  MainNavgationViewController.m
//  rrbi
//
//  Created by mac book on 2018/12/3.
//

#import "MainNavgationViewController.h"

@interface MainNavgationViewController ()

@end

@implementation MainNavgationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationBar.translucent = NO;
    
    //导航栏下蓝色线
    [self.navigationBar setShadowImage:[UIImage imageWithColor:getColor(bgColor)]];
    [self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    // 设置导航栏背景颜色
    self.navigationBar.barTintColor =  [UIColor whiteColor];
    [self.navigationBar setTintColor:[UIColor blackColor]];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;

    
}
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.viewControllers.count>0) {
        viewController.hidesBottomBarWhenPushed = YES;
        
        UIBarButtonItem *backButton =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_return_b"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
        self.navigationItem.leftBarButtonItem = backButton;
        
        
    }
    [super pushViewController:viewController animated:animated];
}
- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
