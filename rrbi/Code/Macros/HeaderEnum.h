//
//  HeaderEnum.h
//  RRQS
//
//  Created by kyjun on 16/3/31.
//
//

#import <Foundation/Foundation.h>


/**
 * 订单状态
 //4=>代表商家的新订单
 //5=>待抢单
 //6=>已抢单,取货中
 //7=>已取货,配送中
 //8=>已送达
 //9=>用户已确认
 //10=>已评价
 //11=>订单已经取消
 //12=>待确认
 //13=>商家已接单
 */
typedef enum : NSUInteger {
    /**
     *  已发货
     */
    OrderStatusSended =1,
    /**
     *  已付款（处理中）
     */
    OrderStatusPayed =2,
    /**
     *  退款中
     */
    OrderStatusRefunding=3,
    /**
     *  新订单
     */
    OrderStatusNewsOrder =4,
    /**
     *  待抢单
     */
    OrderStatusQiangOrder=5,
    /**
     *  取货中
     */
    OrderStatusGetingGoods=6,
    /**
     *  配送中
     */
    OrderStatusOnTheWay=7,
    /**
     *已送达
     */
    OrderStatusAlredayGetGoods=8,
    /**
     *用户已确认
     */
    OrderStatusUsersMakeSure=9,
    /**
     *已评价
     */
    OrderStatusAlredayPingja=10,
    /**
     *订单已经取消
     */
    OrderStatusChancleOrder=11,
    /**
     *待确认
     */
    OrderStatusIngMakeSure=12,
    /**
     *商家已接单
     */
    OrderStatusAlredayJieDan=13,
    /**
     *待自取
     */
    OrderStatusWaitInvite=14,
    /**
     *用户已自取
     */
    OrderStatusAlredayInvite=15
    
} EnumOrderStatus;
/**
 * 订单状态
 //4=>代表商家的新订单
 //5=>待抢单
 //6=>已抢单,取货中
 //7=>已取货,配送中
 //8=>已送达
 //9=>用户已确认
 //10=>已评价
 //11=>订单已经取消
 //12=>待确认
 //13=>商家已接单
 */
@interface HeaderEnum : NSObject

@end
