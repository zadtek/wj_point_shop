//
//  HeaderNotification.h
//  RRQS
//
//  Created by kyjun on 16/3/29.
//
//

#ifndef HeaderNotification_h
#define HeaderNotification_h


#define NotificationToFinishedTask @"NotificationToFinishedTask"

#define NotificationToSetting @"NotificationToSetting"

#define NotificationToAccount @"NotificationToAccount"

#define NotificationToLocationService @"NotificationToLocationService"

/**
 *  抢单  标示系统当前有新的订单 - 刷新任务列表
 *
 *  @return
 */
#define NotificationRemoteAt @"NotificationRemoteAt"
/**
 *  派单 表示系统派给的订单   - 刷新带取货列表
 *
 *  @return
 */
#define NotificationRemotePm @"NotificationRemotePm"

/**
 *  首页顶部数据刷新通知
 *
 *  @return
 */
#define NotificationHomeRefreshing @"NotificationHomeRefreshing"

/**
 *  抢单成功刷新待取货
 *
 *  @return
 */
#define NotificationGrabOrderRefreshWaitOrder @"NotificationGrabOrderRefreshWaitOrder"
/**
 *  刷新 - 新任务
 *
 *  @return
 */
#define NotificationRefreshNewTask @"NotificationRefreshNewTask"
/**
 *  刷新 - 待取货
 *
 *  @return
 */
#define NotificationRefreshWaitTask @"NotificationRefreshWaitTask"
/**
 *  刷新 - 配送中
 *
 *  @return
 */
#define NotificationRefreshShippedTask @"NotificationRefreshShippedTask"

/**
 *  当用户点击左侧菜单更新账户信息
 *
 *  @return 
 */
#define NotificationUpdateUserAccount @"NotificationUpdateUserAccount"


#define NotificationTestLocation @"NotificationTestLocation"

#define NotifciationReLoadUserInfo @"NotifciationReLoadUserInfo"


#endif /* HeaderNotification_h */
