//
//  AFAppDotNetAPIClient.h
//  BaseFrame
//
//  Created by 杜文杰 on 2018/6/11.
//  Copyright © 2018年 dwj. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>


//#define BASE_URL @"http://192.168.1.112:8080/ExoticFlowers/"
#define BASE_URL @"http://helpu.zadtek.com/"
@interface AFAppDotNetAPIClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

@end
