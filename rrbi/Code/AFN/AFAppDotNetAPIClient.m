//
//  AFAppDotNetAPIClient.m
//  BaseFrame
//
//  Created by 杜文杰 on 2018/6/11.
//  Copyright © 2018年 dwj. All rights reserved.
//

#import "AFAppDotNetAPIClient.h"

@implementation AFAppDotNetAPIClient

+ (instancetype)sharedClient {
    
    static AFAppDotNetAPIClient *_sharedClient = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        _sharedClient = [[AFAppDotNetAPIClient alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];

        //设置返回格式
        _sharedClient.responseSerializer = [AFJSONResponseSerializer serializer];
        
        _sharedClient.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/plain",@"text/html",@"text/xml",@"image/png", nil];
        
    });
    
    return _sharedClient;
}

@end
