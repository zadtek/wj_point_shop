//
//  HandleOrderHeader.m
//  rrbi
//
//  Created by mac book on 2018/12/19.
//

#import "HandleOrderHeader.h"

@interface HandleOrderHeader ()

@property(nonatomic,strong) UIView* statusView;
@property(nonatomic,strong) UILabel* labelNum;
@property(nonatomic,strong) UILabel* labelStatus;
@property(nonatomic,strong) UIImageView* iconArrow;
@property(nonatomic,strong) UILabel* labelCreateDate;
@property(nonatomic,strong) UILabel* labelLine;
@property(nonatomic,strong) UILabel* labelPayType;
@property(nonatomic,strong) UIView* userView;
@property(nonatomic,strong) UILabel* labelUserName;
@property(nonatomic,strong) UILabel* labelAddress;
@property(nonatomic,strong) UILabel* labelRemark;
@property(nonatomic,strong) UIButton *zhqsBtn;// 召唤骑士


@property(nonatomic,strong) MOrder* entity;

@end



@implementation HandleOrderHeader


-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
        //        self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self layoutUI];
        UITapGestureRecognizer *tapgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureTouch:)];
        [self addGestureRecognizer:tapgr];
    }
    return self;
}


#pragma mark =====================================================  user interface layout
-(void)layoutUI{
    
//    self.statusView.backgroundColor = [UIColor redColor];
    
    [self addSubview:self.statusView];
    [self.statusView addSubview:self.labelNum];
    [self.statusView addSubview:self.labelStatus];
    [self.statusView addSubview:self.iconArrow];
    [self.statusView addSubview:self.labelCreateDate];
    [self.statusView addSubview:self.labelLine];
    [self.statusView addSubview:self.labelUserName];
    [self.statusView addSubview:self.labelPayType];
    [self.statusView addSubview:self.zhqsBtn];
    
    [self addSubview:self.userView];
    [self.userView addSubview:self.labelAddress];
    [self.userView addSubview:self.labelRemark];
    
    //    self.userView.backgroundColor = [UIColor purpleColor];
    
    [self layoutConstraints];
    
    
    self.labelUserName.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callPhone)];
    [ self.labelUserName addGestureRecognizer:tapgr];
    
    
    
}
-(void)callPhone{
    
    if (self.callPersonPhoneBlock) {
        self.callPersonPhoneBlock();
    }
    
}


-(void)layoutConstraints{
    
    
    
    [self.statusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(90);
    }];
    
    
    
    
    [self.userView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.statusView.mas_bottom);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.bottom.equalTo(self);
    }];
    
    
    
    [self.labelNum mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(self.statusView);
        make.left.equalTo(self.statusView).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(45.f, 45.f));
    }];
    
    [self.labelStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.statusView).mas_offset(10);
        make.left.equalTo(self.labelNum.mas_right).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH/4, 30.f));
    }];
    
    [self.iconArrow mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(self.labelStatus);
        make.left.equalTo(self.labelStatus.mas_right).mas_offset(5);
        make.size.mas_equalTo(CGSizeMake(10.f, 10.f));
    }];
    
    [self.labelCreateDate mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.height.equalTo(self.labelStatus);
        make.left.equalTo(self.iconArrow.mas_right);
        make.right.equalTo(self.statusView).mas_offset(-10);
        
    }];
    
    [self.labelLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelStatus.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.labelStatus);
        make.right.equalTo(self.labelCreateDate);
        
    }];
    
    
    [self.zhqsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelLine.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(25);
        make.right.equalTo(self.labelLine);
        make.width.mas_equalTo(80);
        
    }];
    
    
    [self.labelUserName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.zhqsBtn);
        make.height.equalTo(self.zhqsBtn);
        make.left.equalTo(self.labelStatus);
        make.right.equalTo(self.zhqsBtn.mas_left);
        
    }];
    
    
    
    
    UILabel *labelLine1 = [[UILabel alloc]init];
    labelLine1.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    [self.userView addSubview:labelLine1];
    UILabel *labelLine2 = [[UILabel alloc]init];
    labelLine2.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    [self.userView addSubview:labelLine2];
    UILabel *labelLine3 = [[UILabel alloc]init];
    labelLine3.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    [self.userView addSubview:labelLine3];
    
    [labelLine1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userView);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.labelNum);
        make.right.equalTo(self.labelCreateDate);
        
    }];
    [self.labelAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelLine1.mas_bottom);
        make.height.mas_equalTo(40);
        make.left.right.equalTo(labelLine1);
    }];
    
    [labelLine2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelAddress.mas_bottom);
        make.height.left.right.equalTo(labelLine1);
        
    }];
    
    [self.labelRemark mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelLine2.mas_bottom);
        make.height.left.right.equalTo(self.labelAddress);
    }];
    
    [labelLine3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.userView.mas_bottom);
        make.height.left.right.equalTo(labelLine1);
        
    }];
    
    
    
    //    self.statusView.translatesAutoresizingMaskIntoConstraints = NO;
    //    self.labelNum.translatesAutoresizingMaskIntoConstraints = NO;
    //    self.labelStatus.translatesAutoresizingMaskIntoConstraints = NO;
    //    self.iconArrow.translatesAutoresizingMaskIntoConstraints = NO;
    //    self.labelCreateDate.translatesAutoresizingMaskIntoConstraints = NO;
    //    self.labelLine.translatesAutoresizingMaskIntoConstraints = NO;
    //    self.labelPayType.translatesAutoresizingMaskIntoConstraints = NO;
    //    self.labelUserName.translatesAutoresizingMaskIntoConstraints = NO;
    //    self.userView.translatesAutoresizingMaskIntoConstraints = NO;
    //    self.labelAddress.translatesAutoresizingMaskIntoConstraints = NO;
    //    self.labelRemark.translatesAutoresizingMaskIntoConstraints = NO;
    //    self.zhqsBtn.translatesAutoresizingMaskIntoConstraints = NO;
    //
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.statusView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:70.f]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.statusView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
    //    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.statusView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:10.f]];
    //    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.statusView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
    //
    //    [self.labelNum addConstraint:[NSLayoutConstraint constraintWithItem:self.labelNum attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45.f]];
    //    [self.labelNum addConstraint:[NSLayoutConstraint constraintWithItem:self.labelNum attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45.f]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelNum attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.statusView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.f]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelNum attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.statusView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    //
    //
    //    [self.labelStatus addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStatus attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:25.f]];
    //
    //    NSLayoutConstraint* constraint =[NSLayoutConstraint constraintWithItem:self.labelStatus attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/4];
    //    [self.labelStatus setContentHuggingPriority:501 forAxis:UILayoutConstraintAxisHorizontal];
    //    [self.labelStatus addConstraint:constraint];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStatus attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.statusView attribute:NSLayoutAttributeTop multiplier:1.0 constant:5.f]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStatus attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.labelNum attribute:NSLayoutAttributeRight multiplier:1.0 constant:10.f]];
    //
    //    [self.iconArrow addConstraint:[NSLayoutConstraint constraintWithItem:self.iconArrow attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:10.f]];
    //    [self.iconArrow addConstraint:[NSLayoutConstraint constraintWithItem:self.iconArrow attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:10.f]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.iconArrow attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.labelStatus attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.f]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.iconArrow attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.labelStatus attribute:NSLayoutAttributeRight multiplier:1.0 constant:5.f]];
    //
    //    [self.labelCreateDate addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCreateDate attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:25.f]];
    //    [self.labelCreateDate addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCreateDate attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH*2/5]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCreateDate attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.statusView attribute:NSLayoutAttributeTop multiplier:1.0 constant:5.f]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCreateDate attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.statusView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    //
    //    [self.labelLine addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:1.f]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.labelNum attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.f]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.labelNum attribute:NSLayoutAttributeRight multiplier:1.0 constant:5.f]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.statusView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    //
    //    [self.labelUserName addConstraint:[NSLayoutConstraint constraintWithItem:self.labelUserName attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:25.f]];
    //    [self.labelUserName addConstraint:[NSLayoutConstraint constraintWithItem:self.labelUserName attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/2]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelUserName attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelLine attribute:NSLayoutAttributeBottom multiplier:1.0 constant:5.f]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelUserName attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.labelNum attribute:NSLayoutAttributeRight multiplier:1.0 constant:10.f]];
    //
    //
    //    [self.labelPayType addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPayType attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:25.f]];
    //    [self.labelPayType addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPayType attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:80]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPayType attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelLine attribute:NSLayoutAttributeBottom multiplier:1.0 constant:5.f]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPayType attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.statusView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    //
    //    [self.userView addConstraint:[NSLayoutConstraint constraintWithItem:self.userView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:110.f]];
    //    [self.userView addConstraint:[NSLayoutConstraint constraintWithItem:self.userView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
    //    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.userView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.statusView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    //    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.userView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
    //
    //    [self.labelAddress addConstraint:[NSLayoutConstraint constraintWithItem:self.labelAddress attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:35.f]];
    //    [self.userView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelAddress attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.userView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    //    [self.userView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelAddress attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.userView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    //    [self.userView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelAddress attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.userView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    //
    //    [self.labelRemark addConstraint:[NSLayoutConstraint constraintWithItem:self.labelRemark attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:35.f]];
    //    [self.userView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelRemark attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelAddress attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    //    [self.userView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelRemark attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.userView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    //    [self.userView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelRemark attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.userView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    //
    //    [self.zhqsBtn addConstraint:[NSLayoutConstraint constraintWithItem:self.zhqsBtn attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:25.f]];
    //    [self.zhqsBtn addConstraint:[NSLayoutConstraint constraintWithItem:self.zhqsBtn attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:80]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.zhqsBtn attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelPayType attribute:NSLayoutAttributeBottom multiplier:1.0 constant:5.f]];
    //    [self.statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.zhqsBtn attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.statusView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
}

-(void)loadData:(MOrder *)entity index:(NSInteger)index{
    if(entity){
        _entity = entity;
        UIColor* color;
        NSString* imageName;
        NSString* btnName;

        _zhqsBtn.hidden = YES;

        switch (entity.status) {
            case OrderStatusNewsOrder:
            {
                self.labelStatus.text = @"新订单";
                color = [UIColor colorWithRed:255/255.f green:162/255.f blue:0/255.f alpha:1.0];
                imageName = @"icon-arrow-finished";
            }
                break;
            case OrderStatusQiangOrder:
            {
                self.labelStatus.text = @"待抢单";
                color = [UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0];
                imageName = @"icon-arrow-finished";
            }
                break;
            case OrderStatusGetingGoods:
            {
                self.labelStatus.text = @"取货中";
                color = [UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0];
                imageName = @"icon-arrow-finished";
                _zhqsBtn.hidden = NO;
                btnName = @"联系骑手";
            }
                break;
            case OrderStatusOnTheWay:
            {
                self.labelStatus.text = @"配送中";
                color = [UIColor colorWithRed:135/255.f green:135/255.f blue:135/255.f alpha:1.0];
                imageName = @"icon-arrow-cancel";
                _zhqsBtn.hidden = NO;
                btnName = @"联系骑手";
            }
                break;
                
            case OrderStatusAlredayGetGoods:
            {
                self.labelStatus.text = @"已送达";
                color = [UIColor colorWithRed:135/255.f green:135/255.f blue:135/255.f alpha:1.0];
                imageName = @"icon-arrow-cancel";
                _zhqsBtn.hidden = NO;
                btnName = @"联系骑手";
            }
                break;
                
            case OrderStatusUsersMakeSure:
            {
                self.labelStatus.text = @"已确认";
                color = [UIColor redColor];
                imageName = @"icon-arrow-ing";
                _zhqsBtn.hidden = NO;
                btnName = @"联系骑手";
            }
                break;
            case OrderStatusAlredayPingja:
            {
                self.labelStatus.text = @"已评价";
                color = [UIColor redColor];
                imageName = @"icon-arrow-ing";
                _zhqsBtn.hidden = NO;
                btnName = @"联系骑手";
            }
                break;
            case OrderStatusChancleOrder:
            {
                self.labelStatus.text = @"已取消";
                color = [UIColor redColor];
                imageName = @"icon-arrow-ing";
            }
                break;
            case OrderStatusIngMakeSure:
            {
                self.labelStatus.text = @"待确认";
                color = [UIColor redColor];
                imageName = @"icon-arrow-ing";
            }
                break;
            case OrderStatusAlredayJieDan:
            {
                self.labelStatus.text = @"已接单";
                color = [UIColor redColor];
                imageName = @"icon-arrow-ing";
                _zhqsBtn.hidden = NO;
                btnName = @"召唤骑士";

            }
                break;
                
            case OrderStatusWaitInvite://待自取
            {
                self.labelStatus.text = @"待自取";
                color = [UIColor grayColor];
                imageName = @"icon-arrow-cancel";
                _zhqsBtn.hidden = NO;
                btnName = @"完成自取";
            }
                break;
                
            case OrderStatusAlredayInvite://用户已自取
            {
                self.labelStatus.text = @"自取完成";
                color = [UIColor grayColor];
                imageName = @"icon-arrow-cancel";
            }
                break;
                
            default:
                break;
        }
        self.labelNum.textColor = color;
        self.labelNum.layer.borderColor = color.CGColor;
        self.labelStatus.textColor = color;
        self.labelCreateDate.textColor =color;
        self.labelLine.backgroundColor = color;
        self.labelUserName.textColor = color;
//        self.labelPayType.textColor = color;
//        self.labelPayType.layer.borderColor = color.CGColor;
        self.zhqsBtn.layer.borderColor = color.CGColor;
        [self.zhqsBtn setTitleColor:color forState:UIControlStateNormal];
        [_zhqsBtn setTitle:btnName forState:UIControlStateNormal];

        if(entity.status == OrderStatusPayed){
            self.iconArrow.hidden = YES;
        }else{
            self.iconArrow.hidden = NO;
            [self.iconArrow setImage:[UIImage imageNamed:imageName]];
        }
        if(entity.isExpansion){
            [UIView animateWithDuration:0.5 animations:^{
                self.iconArrow.transform = CGAffineTransformMakeRotation(M_PI/2);
            } completion:^(BOOL finished) {
                
            }];
            
        }else {
            [UIView animateWithDuration:0.5 animations:^{
                self.iconArrow.transform = CGAffineTransformMakeRotation(0);
            } completion:^(BOOL finished) {
                
            }];
            
        }
        self.labelNum.text = [NSString stringWithFormat:@"%02ld",(long)index];
        self.labelCreateDate.text = [NSString stringWithFormat:@"下单时间:%@",entity.createDate];
//        self.labelPayType.text = entity.payType;
        self.labelUserName.text = [NSString stringWithFormat:@"%@ / %@",entity.userName,entity.phone];
        self.labelAddress.text = [NSString stringWithFormat:@"地址 : %@",entity.address];
        NSString* mark =[WMHelper isEmptyOrNULLOrnil:entity.orderMark]?@"":entity.orderMark;
        NSString* str = [NSString stringWithFormat:@"备注 : %@",mark];
        NSMutableAttributedString* attributeStr = [[NSMutableAttributedString alloc]initWithString:str];
        [attributeStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:255/255.f green:77/255.f blue:77/255.f alpha:1.0]} range:[str rangeOfString:mark]];
        self.labelRemark.attributedText = attributeStr;
        
    }
}


#pragma mark =====================================================  SEL
-(void)gestureTouch:(UIGestureRecognizer*)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(handleOrderHeader:)]){
        [self.delegate handleOrderHeader:self.entity];
    }
}

#pragma mark =====================================================  property package
-(UIView *)statusView{
    if(!_statusView){
        _statusView = [[UIView alloc]init];
        _statusView.backgroundColor = [UIColor whiteColor];
    }
    return _statusView;
}

-(UILabel *)labelNum {
    if(!_labelNum){
        _labelNum = [[UILabel alloc]init];
        _labelNum.font = [UIFont systemFontOfSize:24.f];
        _labelNum.textAlignment = NSTextAlignmentCenter;
        _labelNum.layer.masksToBounds = YES;
        _labelNum.layer.cornerRadius = 5.f;
        _labelNum.layer.borderWidth = 1.f;
    }
    return _labelNum;
}

-(UILabel *)labelStatus{
    if(!_labelStatus){
        _labelStatus = [[UILabel alloc]init];
        _labelStatus.textAlignment = NSTextAlignmentLeft;
        _labelStatus.font = [UIFont systemFontOfSize:14.f];
    }
    return _labelStatus;
}

-(UIImageView *)iconArrow{
    if(!_iconArrow){
        _iconArrow = [[UIImageView alloc]init];
        _iconArrow.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _iconArrow;
}

-(UILabel *)labelCreateDate{
    if(!_labelCreateDate){
        _labelCreateDate = [[UILabel alloc]init];
        _labelCreateDate.font = [UIFont systemFontOfSize:12.f];
        _labelCreateDate.textAlignment = NSTextAlignmentRight;
    }
    return _labelCreateDate;
}

-(UILabel *)labelLine{
    if(!_labelLine){
        _labelLine = [[UILabel alloc]init];
        
    }
    return _labelLine;
}
-(UILabel *)labelUserName{
    if(!_labelUserName){
        _labelUserName = [[UILabel alloc]init];
        _labelUserName.textAlignment = NSTextAlignmentLeft;
        _labelUserName.font = [UIFont systemFontOfSize:14.f];
    }
    return _labelUserName;
}


-(UILabel *)labelPayType{
    if(!_labelPayType){
        _labelPayType = [[UILabel alloc]init];
        _labelPayType.textAlignment = NSTextAlignmentCenter;
        _labelPayType.font = [UIFont systemFontOfSize:14.f];
        _labelPayType.layer.masksToBounds = YES;
        _labelPayType.layer.cornerRadius = 5.f;
        _labelPayType.layer.borderWidth = 1.f;
    }
    return _labelPayType;
}

-(UIView *)userView{
    if(!_userView){
        _userView = [[UIView alloc]init];
        _userView.backgroundColor = [UIColor whiteColor];
        //        CALayer* border = [[CALayer alloc]init];
        //        border.frame = CGRectMake(10.f, 69.f, SCREEN_WIDTH-20, 1.0f);
        //        border.borderWidth = 1.f;
        //        border.borderColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0].CGColor;
        //        [_userView.layer addSublayer:border];
    }
    return _userView;
}

-(UILabel *)labelAddress{
    if(!_labelAddress){
        _labelAddress = [[UILabel alloc]init];
        _labelAddress.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelAddress.font = [UIFont systemFontOfSize:14.f];
        _labelAddress.numberOfLines = 0;
        _labelAddress.lineBreakMode = NSLineBreakByCharWrapping;
    }
    return _labelAddress;
}

-(UILabel *)labelRemark{
    if(!_labelRemark){
        _labelRemark = [[UILabel alloc]init];
        _labelRemark.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelRemark.font = [UIFont systemFontOfSize:13.f];
        _labelRemark.numberOfLines = 0;
        _labelRemark.lineBreakMode = NSLineBreakByCharWrapping;
    }
    return _labelRemark;
}

- (UIButton *)zhqsBtn
{
    if (!_zhqsBtn) {
        _zhqsBtn = [[UIButton alloc]init];
        _zhqsBtn.backgroundColor = [UIColor clearColor];
        _zhqsBtn.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_zhqsBtn setTitle:@"召唤骑士" forState:UIControlStateNormal];
        _zhqsBtn.layer.cornerRadius = 5;
        _zhqsBtn.layer.masksToBounds = YES;
        _zhqsBtn.layer.borderWidth = 1.f;
        _zhqsBtn.hidden = YES;
        [_zhqsBtn addTarget:self action:@selector(SummonKnightButton) forControlEvents:UIControlEventTouchUpInside];

    }
    return _zhqsBtn;
}

-(void)SummonKnightButton{
    
    if (self.SummonKnightBlock) {
        self.SummonKnightBlock();
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
