//
//  FinishedOrderFooter.m
//  RRBI
//
//  Created by kyjun on 16/4/28.
//
//

#import "FinishedOrderFooter.h"

@interface FinishedOrderFooter ()

@property(nonatomic,strong) UILabel* labelOtherTitle;
@property(nonatomic,strong) UILabel* labelOtherValue;
@property(nonatomic,strong) UILabel* labelPackageFeeTitle;
@property(nonatomic,strong) UILabel* labelPackageFeeValue;
/**
 *  满减优惠
 */
@property(nonatomic,strong) UIView* fullCutView;
@property(nonatomic,strong) UILabel* labelCutIcon;
@property(nonatomic,strong) UILabel* labelCutTitle;
@property(nonatomic,strong) UILabel* labelFullCutPrice;

@property(nonatomic,strong) UILabel* labelLine;
@property(nonatomic,strong) UILabel* labelLine1;
@property(nonatomic,strong) UILabel* labelLine2;
@property(nonatomic,strong) UILabel* labelLine3;

@property(nonatomic,strong) UILabel* labelSumPriceTitle;
@property(nonatomic,strong) UILabel* labelSumPriceValue;


@property(nonatomic,strong) UILabel* labelTypeTitle;
@property(nonatomic,strong) UILabel* labelTypeValue;

@property(nonatomic,strong) UILabel* labelOrderTitle;
@property(nonatomic,strong) UILabel* labelOrderValue;


@property(nonatomic,strong) UILabel* labelEarningsTitle;
@property(nonatomic,strong) UILabel* labelEarningsValue;


@property(nonatomic,strong) UIView* bottomView;

/**
 *  快递员信息
 */
@property(nonatomic,strong) UIView* deliveryView;
@property(nonatomic,strong) UIView* deliveryLineView;
@property(nonatomic,strong) UIButton* deliveryBtn;
@property(nonatomic,strong) UILabel* deliveryCountLabel;




@end

@implementation FinishedOrderFooter


-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
        self.contentView.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha:1.0];
        [self layoutUI];

    }
    return self;
}

#pragma mark =====================================================  user interface layout
-(void)layoutUI{
    [self addSubview:self.labelOtherTitle];
    [self addSubview:self.labelOtherValue];
    [self addSubview:self.labelPackageFeeTitle];
    [self addSubview:self.labelPackageFeeValue];
    [self addSubview:self.fullCutView];
    [self.fullCutView addSubview:self.labelCutIcon];
    [self.fullCutView addSubview:self.labelCutTitle];
    [self.fullCutView addSubview:self.labelFullCutPrice];
    [self addSubview:self.labelLine];
    [self addSubview:self.labelLine1];
    [self addSubview:self.labelLine2];
    [self addSubview:self.labelLine3];
    
    [self addSubview:self.labelTypeTitle];
    [self addSubview:self.labelTypeValue];
    
    [self addSubview:self.labelOrderTitle];
    [self addSubview:self.labelOrderValue];
    
    [self addSubview:self.labelEarningsTitle];
    [self addSubview:self.labelEarningsValue];
    
    
    [self addSubview:self.labelSumPriceTitle];
    [self addSubview:self.labelSumPriceValue];
    
    
    [self addSubview:self.deliveryView];
    [self.deliveryView addSubview:self.deliveryLineView];
    [self.deliveryView addSubview:self.deliveryBtn];
    [self.deliveryView addSubview:self.deliveryCountLabel];
    
    
    
    [self addSubview:self.bottomView];
    
    [self layoutConstraints];

}

-(void)layoutConstraints{

    [self.labelOtherTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self).offset(10);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(35);
    }];
    
    [self.labelOtherValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.labelOtherTitle);
        make.right.equalTo(self).offset(-10);
        make.left.equalTo(self.labelOtherTitle.mas_right);
    }];
    
    [self.labelPackageFeeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.labelOtherTitle);
        make.top.equalTo(self.labelOtherTitle.mas_bottom);
        make.height.mas_equalTo(20);
    }];
    
    [self.labelPackageFeeValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.labelPackageFeeTitle);
        make.right.equalTo(self.labelOtherValue);
        make.left.equalTo(self.labelPackageFeeTitle.mas_right);
    }];
    
    
    
    
    
    [self.fullCutView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelPackageFeeTitle.mas_bottom);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(20);
    }];
    
    
    [self.labelCutIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fullCutView);
        make.left.equalTo(self.labelPackageFeeTitle);
        make.height.width.equalTo(self.fullCutView.mas_height).multipliedBy(0.8);
        
    }];
    
    [self.labelCutTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fullCutView);
        make.left.equalTo(self.labelCutIcon.mas_right);
        make.width.equalTo(self.labelPackageFeeTitle);
        make.height.equalTo(self.fullCutView);
    }];
    
    [self.labelFullCutPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fullCutView);
        make.left.equalTo(self.labelCutTitle.mas_right);
        make.right.equalTo(self.labelPackageFeeValue);
        make.height.equalTo(self.fullCutView);
    }];
    
    [self.labelLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.fullCutView.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.labelOtherTitle);
        make.right.equalTo(self.labelOtherValue);
        
    }];
    
    [self.labelTypeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelLine.mas_bottom);
        make.left.right.equalTo(self.labelOtherTitle);
        make.height.mas_equalTo(40);
    }];
    
    [self.labelTypeValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.labelTypeTitle);
        make.right.equalTo(self.labelOtherValue);
        make.left.equalTo(self.labelTypeTitle.mas_right);
    }];
    
    [self.labelLine1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelTypeTitle.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.labelTypeTitle);
        make.right.equalTo(self.labelTypeValue);
        
    }];
    
    
    [self.labelOrderTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelLine1.mas_bottom);
        make.left.right.height.equalTo(self.labelTypeTitle);
    }];
    
    [self.labelOrderValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.labelOrderTitle);
        make.right.equalTo(self.labelTypeValue);
        make.left.equalTo(self.labelTypeTitle.mas_right);
    }];
    
    [self.labelLine2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelOrderTitle.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.labelOrderTitle);
        make.right.equalTo(self.labelOrderValue);
        
    }];
    
    
    [self.labelEarningsTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelLine2.mas_bottom);
        make.left.right.height.equalTo(self.labelTypeTitle);
    }];
    
    [self.labelEarningsValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.labelEarningsTitle);
        make.right.equalTo(self.labelOrderValue);
        make.left.equalTo(self.labelEarningsTitle.mas_right);
    }];
    
    [self.labelLine3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelEarningsTitle.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.labelEarningsTitle);
        make.right.equalTo(self.labelEarningsValue);
        
    }];
    
    [self.labelSumPriceTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelLine3.mas_bottom);
        make.left.right.height.equalTo(self.labelTypeTitle);
    }];
    
    [self.labelSumPriceValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.labelSumPriceTitle);
        make.right.equalTo(self.labelOrderValue);
        make.left.equalTo(self.labelSumPriceTitle.mas_right);
    }];
    
    
    [self.deliveryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelSumPriceTitle.mas_bottom);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(40);
    }];
    
    
    [self.deliveryLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.deliveryView);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.labelLine2);
        make.right.equalTo(self.labelLine2);
        
    }];
    
    
    [self.deliveryBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.deliveryLineView.mas_bottom);
        make.width.equalTo(self.deliveryView).multipliedBy(0.6);
        make.left.equalTo(self.deliveryView).offset(10);
        make.bottom.equalTo(self.deliveryView);
    }];
    
    
    [self.deliveryCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.deliveryLineView.mas_bottom);
        make.right.equalTo(self.deliveryView).offset(-10);
        make.left.equalTo(self.deliveryBtn.mas_right);
        make.bottom.equalTo(self.deliveryView);
    }];
    
    
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(10);
    }];
    
    
}

#pragma mark - 点击事件
-(void)callEmpty:(UIButton *)sender{
    
    MOrder *deliveryModel = self.entity.delivery.firstObject;
    if (self.callEmptyBlock) {
        self.callEmptyBlock(deliveryModel);
    }
    
}



#pragma mark =====================================================  property package

-(void)setEntity:(MOrder *)entity{
    if(entity){
        _entity = entity;
        double packageFee = [entity.packageFee doubleValue];
        double totalPrice =[entity.sumPrice doubleValue];
        double storeDiscount = [entity.storeDiscount doubleValue];
        
        self.labelPackageFeeValue.text = [NSString stringWithFormat:@"￥%.2f",packageFee];
        self.labelSumPriceValue.text = [NSString stringWithFormat:@"￥%.2f",totalPrice];
        self.labelFullCutPrice.text = [NSString stringWithFormat:@"-￥%.2f",storeDiscount];
        if(storeDiscount>0.00){
            self.fullCutView.hidden = NO;
        }else{
            self.fullCutView.hidden = YES;
        }
        
        
        self.labelOrderValue.text = [NSString stringWithFormat:@"%@",entity.order_sn];
        
        self.labelEarningsValue.text = [NSString stringWithFormat:@"%@",entity.benifit];

//        NSString *type;
//        if([entity.payType isEqualToString:@"0"]){
//            type = @"现金支付";
//        }else if ([entity.payType isEqualToString:@"1"]){
//            type = @"支付宝支付";
//        }
//        else if ([entity.payType isEqualToString:@"2"]){
//            type = @"微信支付";
//        }
//        else if ([entity.payType isEqualToString:@"3"]){
//            type = @"手机支付宝";
//        }else{
//            type =  @"其它支付";
//        }
     
        self.labelTypeValue.text = [NSString stringWithFormat:@"%@",entity.pay_name];

        
    }
    NSArray *deliveryArr = entity.delivery;
    MOrder *deliveryModel = deliveryArr.firstObject;
    if(deliveryArr.count != 0){
        self.deliveryView.hidden = NO;
        NSString* str = [NSString stringWithFormat: @"配送员: %@ / %@",deliveryModel.emptyName,deliveryModel.emptyPhone];
        NSMutableAttributedString* attributeStr = [[NSMutableAttributedString alloc]initWithString: str];
        [attributeStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0]} range:NSMakeRange(0, str.length)];
        [attributeStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:30/255.f green:147/255.f blue:238/255.f alpha:1.0],NSFontAttributeName:[UIFont systemFontOfSize:15.f]} range:[str rangeOfString:deliveryModel.phone]];
        [self.deliveryBtn setAttributedTitle:attributeStr forState:UIControlStateNormal];
        str = [NSString stringWithFormat: @"当前已接:%@单",deliveryModel.emptyOrders];
        attributeStr = [[NSMutableAttributedString alloc]initWithString: str];
        [attributeStr addAttributes:@{NSForegroundColorAttributeName:[UIColor redColor],NSFontAttributeName:[UIFont systemFontOfSize:15.f]} range:[str rangeOfString:deliveryModel.emptyOrders]];
        self.deliveryCountLabel.attributedText = attributeStr;
    }else{
        self.deliveryView.hidden = YES;
    }
    
}
    
-(UIView *)deliveryView{
    if(!_deliveryView){
        _deliveryView = [[UIView alloc]init];
    }
    return _deliveryView;
}

-(UIView *)deliveryLineView{
    if(!_deliveryLineView){
        _deliveryLineView = [[UIView alloc]init];
        _deliveryLineView.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _deliveryLineView;
}


-(UIButton *)deliveryBtn{
    if(!_deliveryBtn){
        _deliveryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deliveryBtn setTitleColor:[UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0] forState:UIControlStateNormal] ;
        _deliveryBtn.titleLabel.font = kFontNameSize(14);
        [_deliveryBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_deliveryBtn addTarget:self action:@selector(callEmpty:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deliveryBtn;
}

-(UILabel *)deliveryCountLabel{
    if(!_deliveryCountLabel){
        _deliveryCountLabel = [[UILabel alloc]init];
        _deliveryCountLabel.textColor = [UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0];
        _deliveryCountLabel.textAlignment = NSTextAlignmentRight;
        _deliveryCountLabel.font = kFontNameSize(14);
    }
    return _deliveryCountLabel;
}


-(UILabel *)labelOtherTitle{
    if(!_labelOtherTitle){
        _labelOtherTitle = [[UILabel alloc]init];
        _labelOtherTitle.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelOtherTitle.text = @"其他费用";
    }
    return _labelOtherTitle;
}

-(UILabel *)labelOtherValue{
    if(!_labelOtherValue){
        _labelOtherValue = [[UILabel alloc]init];
        _labelOtherValue.textAlignment = NSTextAlignmentRight;
    }
    return _labelOtherValue;
}

-(UILabel *)labelPackageFeeTitle{
    if(!_labelPackageFeeTitle){
        _labelPackageFeeTitle = [[UILabel alloc]init];
        _labelPackageFeeTitle.font = [UIFont systemFontOfSize:14.f];
        _labelPackageFeeTitle.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _labelPackageFeeTitle.text = @"打包费";
    }
    return _labelPackageFeeTitle;
}

-(UILabel *)labelPackageFeeValue{
    if(!_labelPackageFeeValue){
        _labelPackageFeeValue = [[UILabel alloc]init];
        _labelPackageFeeValue.textColor= [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _labelPackageFeeValue.textAlignment = NSTextAlignmentRight;
    }
    return _labelPackageFeeValue;
}


-(UIView *)fullCutView{
    if(!_fullCutView){
        _fullCutView = [[UIView alloc]init];
        _fullCutView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _fullCutView;
}

-(UILabel *)labelCutIcon{
    if(!_labelCutIcon){
        _labelCutIcon = [[UILabel alloc]init];
        _labelCutIcon.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"icon-full-cut"]];;
        _labelCutIcon.font = [UIFont systemFontOfSize:14.f];
        _labelCutIcon.textColor = [UIColor whiteColor];
        _labelCutIcon.layer.masksToBounds = YES;
        _labelCutIcon.layer.cornerRadius = 3.f;
        _labelCutIcon.textAlignment = NSTextAlignmentCenter;
        _labelCutIcon.text =  @"减";
        _labelCutIcon.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _labelCutIcon;
}

-(UILabel *)labelCutTitle{
    if(!_labelCutTitle){
        _labelCutTitle = [[UILabel alloc]init];
        _labelCutTitle.font = [UIFont systemFontOfSize:14.f];
        _labelCutTitle.text =  @"满减优惠(商家承担):";
        _labelCutTitle.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _labelCutTitle.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _labelCutTitle;
}

-(UILabel *)labelFullCutPrice{
    if(!_labelFullCutPrice){
        _labelFullCutPrice = [[UILabel alloc]init];
        _labelFullCutPrice.text = @"￥0.00";
        _labelFullCutPrice.textAlignment = NSTextAlignmentRight;
        _labelFullCutPrice.textColor = [UIColor redColor];
        _labelFullCutPrice.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _labelFullCutPrice;
}


-(UILabel *)labelLine{
    if(!_labelLine){
        _labelLine = [[UILabel alloc]init];
        _labelLine.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _labelLine;
}
-(UILabel *)labelLine1{
    if(!_labelLine1){
        _labelLine1 = [[UILabel alloc]init];
        _labelLine1.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _labelLine1;
}

-(UILabel *)labelLine2{
    if(!_labelLine2){
        _labelLine2 = [[UILabel alloc]init];
        _labelLine2.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _labelLine2;
}

-(UILabel *)labelLine3{
    if(!_labelLine3){
        _labelLine3 = [[UILabel alloc]init];
        _labelLine3.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _labelLine3;
}


-(UILabel *)labelSumPriceTitle{
    if(!_labelSumPriceTitle){
        _labelSumPriceTitle = [[UILabel alloc]init];
        _labelSumPriceTitle.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelSumPriceTitle.text = @"总计";
    }
    return _labelSumPriceTitle;
}

-(UILabel *)labelSumPriceValue{
    if(!_labelSumPriceValue){
        _labelSumPriceValue = [[UILabel alloc]init];
        _labelSumPriceValue.textColor = [UIColor colorWithRed:255/255.f green:77/255.f blue:77/255.f alpha:1.0];
        _labelSumPriceValue.textAlignment = NSTextAlignmentRight;
    }
    return _labelSumPriceValue;
}
-(UIView *)bottomView{
    if(!_bottomView){
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    return _bottomView;
}


-(UILabel *)labelTypeTitle{
    if(!_labelTypeTitle){
        _labelTypeTitle = [[UILabel alloc]init];
        _labelTypeTitle.font = [UIFont systemFontOfSize:15.f];
        _labelTypeTitle.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelTypeTitle.text = @"支付方式";
    }
    return _labelTypeTitle;
}

-(UILabel *)labelTypeValue{
    if(!_labelTypeValue){
        _labelTypeValue = [[UILabel alloc]init];
        _labelTypeValue.textColor= [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelTypeValue.font = [UIFont systemFontOfSize:15.f];
        _labelTypeValue.textAlignment = NSTextAlignmentRight;
    }
    return _labelTypeValue;
}


-(UILabel *)labelOrderTitle{
    if(!_labelOrderTitle){
        _labelOrderTitle = [[UILabel alloc]init];
        _labelOrderTitle.font = [UIFont systemFontOfSize:15.f];
        _labelOrderTitle.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelOrderTitle.text = @"订单号";
    }
    return _labelOrderTitle;
}

-(UILabel *)labelOrderValue{
    if(!_labelOrderValue){
        _labelOrderValue = [[UILabel alloc]init];
        _labelOrderValue.textColor= [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelOrderValue.font = [UIFont systemFontOfSize:15.f];
        _labelOrderValue.textAlignment = NSTextAlignmentRight;
    }
    return _labelOrderValue;
}


-(UILabel *)labelEarningsTitle{
    if(!_labelEarningsTitle){
        _labelEarningsTitle = [[UILabel alloc]init];
        _labelEarningsTitle.font = [UIFont systemFontOfSize:15.f];
        _labelEarningsTitle.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelEarningsTitle.text = @"本单收益";
    }
    return _labelEarningsTitle;
}

-(UILabel *)labelEarningsValue{
    if(!_labelEarningsValue){
        _labelEarningsValue = [[UILabel alloc]init];
        _labelEarningsValue.textColor= [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelEarningsValue.font = [UIFont systemFontOfSize:15.f];
        _labelEarningsValue.textAlignment = NSTextAlignmentRight;
    }
    return _labelEarningsValue;
}




@end
