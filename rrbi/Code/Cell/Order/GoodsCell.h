//
//  GoodsCell.h
//  RRBI
//
//  Created by kyjun on 16/4/27.
//
//

#import <UIKit/UIKit.h>
#import "MGoods.h"

@interface GoodsCell : UITableViewCell

@property(nonatomic,strong) MGoods* entity;

@end
