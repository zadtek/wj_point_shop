//
//  HandleOrderHeader.h
//  rrbi
//
//  Created by mac book on 2018/12/19.
//

#import <UIKit/UIKit.h>
#import "MOrder.h"

NS_ASSUME_NONNULL_BEGIN


@protocol HandleOrderHeaderDelegate <NSObject>

@optional
-(void)handleOrderHeader:(MOrder*)entity;

@end


@interface HandleOrderHeader : UITableViewHeaderFooterView


-(void)loadData:(MOrder*)entity index:(NSInteger)index;

@property(nonatomic,weak) id<HandleOrderHeaderDelegate> delegate;

@property (copy, nonatomic) void(^SummonKnightBlock)(void);

@property (copy, nonatomic) void(^callPersonPhoneBlock)(void);

@end

NS_ASSUME_NONNULL_END
