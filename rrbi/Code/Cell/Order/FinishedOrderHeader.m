//
//  FinishedOrderHeader.m
//  RRBI
//
//  Created by kyjun on 16/5/4.
//
//

#import "FinishedOrderHeader.h"

@interface FinishedOrderHeader ()
@property(nonatomic,strong) UIView* statusView;
@property(nonatomic,strong) UILabel* labelNum;
@property(nonatomic,strong) NSLayoutConstraint* statusConstraint;
@property(nonatomic,strong) UILabel* labelStatus;
@property(nonatomic,strong) UIImageView* iconArrow;
@property(nonatomic,strong) UILabel* labelCreateDate;
@property(nonatomic,strong) UILabel* labelLine;
@property(nonatomic,strong) UILabel* labelPayType;
@property(nonatomic,strong) UILabel* labelUserName;
@property(nonatomic,strong) UIButton *zhqsBtn;// 召唤骑士

@property(nonatomic,strong) MOrder* entity;
@end

@implementation FinishedOrderHeader

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
        self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self layoutUI];
        UITapGestureRecognizer *tapgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureTouch:)];
        [self addGestureRecognizer:tapgr];
    }
    return self;
}

#pragma mark =====================================================  user interface layout
-(void)layoutUI{
    [self addSubview:self.statusView];
    [self.statusView addSubview:self.labelNum];
    [self.statusView addSubview:self.labelStatus];
    [self.statusView addSubview:self.iconArrow];
    [self.statusView addSubview:self.labelCreateDate];
    [self.statusView addSubview:self.labelLine];
    [self.statusView addSubview:self.labelUserName];
//    [self.statusView addSubview:self.labelPayType];
    [self.statusView addSubview:self.zhqsBtn];
    
    self.statusView.backgroundColor = [UIColor whiteColor];

    
    [self.statusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.bottom.equalTo(self).offset(-10);

    }];
    
    
    [self.labelNum mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(self.statusView);
        make.left.equalTo(self.statusView).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(45.f, 45.f));
    }];
    
    
    
    [self.labelStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.statusView).mas_offset(10);
        make.left.equalTo(self.labelNum.mas_right).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH/4, 30.f));
    }];
    
    [self.iconArrow mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(self.labelStatus);
        make.left.equalTo(self.labelStatus.mas_right).mas_offset(5);
        make.size.mas_equalTo(CGSizeMake(10.f, 10.f));
    }];
    
    [self.labelCreateDate mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.height.equalTo(self.labelStatus);
        make.left.equalTo(self.iconArrow.mas_right);
        make.right.equalTo(self.statusView).mas_offset(-10);
        
    }];
    
    [self.labelLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelStatus.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.labelStatus);
        make.right.equalTo(self.labelCreateDate);
        
    }];
    
    
    [self.zhqsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelLine.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(25);
        make.right.equalTo(self.labelLine);
        make.width.mas_equalTo(80);
        
    }];
    
    
    [self.labelUserName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.zhqsBtn);
        make.height.equalTo(self.zhqsBtn);
        make.left.equalTo(self.labelStatus);
        make.right.equalTo(self.zhqsBtn.mas_left);
        
    }];
    
    
    self.labelUserName.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callPhone)];
    [ self.labelUserName addGestureRecognizer:tapgr];
    
    
    
}
-(void)callPhone{
    
    if (self.callPersonPhoneBlock) {
        self.callPersonPhoneBlock();
    }
    
}



-(void)loadData:(MOrder *)entity index:(NSInteger)index{
    if(entity){
        _entity = entity;
        UIColor* color;
        NSString* statusName;
        NSString* imageName;
        NSString* btnName;

        _zhqsBtn.hidden = YES;

        switch (entity.status) {
            case OrderStatusNewsOrder:
            {
                self.labelStatus.text = @"新订单";
                color = [UIColor colorWithRed:255/255.f green:162/255.f blue:0/255.f alpha:1.0];
                imageName = @"icon-arrow-finished";
            }
                break;
            case OrderStatusQiangOrder:
            {
                self.labelStatus.text = @"待抢单";
                color = [UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0];
                imageName = @"icon-arrow-finished";
            }
                break;
            case OrderStatusGetingGoods:
            {
                self.labelStatus.text = @"取货中";
                color = [UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0];
                imageName = @"icon-arrow-finished";
                _zhqsBtn.hidden = NO;
                btnName = @"联系骑手";

            }
                break;
            case OrderStatusOnTheWay:
            {
                self.labelStatus.text = @"配送中";
                color = [UIColor colorWithRed:135/255.f green:135/255.f blue:135/255.f alpha:1.0];
                imageName = @"icon-arrow-cancel";
                _zhqsBtn.hidden = NO;
                btnName = @"联系骑手";

            }
                break;
                
            case OrderStatusAlredayGetGoods:
            {
                self.labelStatus.text = @"已送达";
                color = [UIColor colorWithRed:135/255.f green:135/255.f blue:135/255.f alpha:1.0];
                imageName = @"icon-arrow-cancel";
                _zhqsBtn.hidden = NO;
                btnName = @"联系骑手";

            }
                break;
                
            case OrderStatusUsersMakeSure:
            {
                self.labelStatus.text = @"已确认";
                color = [UIColor redColor];
                imageName = @"icon-arrow-ing";
                _zhqsBtn.hidden = NO;
                btnName = @"联系骑手";

            }
                break;
            case OrderStatusAlredayPingja:
            {
                self.labelStatus.text = @"已评价";
                color = [UIColor redColor];
                imageName = @"icon-arrow-ing";
                _zhqsBtn.hidden = NO;
                btnName = @"联系骑手";

            }
                break;
            case OrderStatusChancleOrder:
            {
                self.labelStatus.text = @"已取消";
                color = [UIColor redColor];
                imageName = @"icon-arrow-ing";
            }
                break;
            case OrderStatusIngMakeSure:
            {
                self.labelStatus.text = @"待确认";
                color = [UIColor redColor];
                imageName = @"icon-arrow-ing";
            }
                break;
            case OrderStatusAlredayJieDan:
            {
                self.labelStatus.text = @"已接单";
                color = [UIColor redColor];
                imageName = @"icon-arrow-ing";
                btnName = @"召唤骑士";
                _zhqsBtn.hidden = NO;

            }
                break;
            case OrderStatusWaitInvite://待自取
            {
                self.labelStatus.text = @"待自取";
                color = [UIColor grayColor];
                imageName = @"icon-arrow-cancel";
                _zhqsBtn.hidden = NO;
                btnName = @"完成自取";
            }
                break;
                
            case OrderStatusAlredayInvite://用户已自取
            {
                self.labelStatus.text = @"自取完成";
                color = [UIColor grayColor];
                imageName = @"icon-arrow-cancel";
            }
                break;
                
                
            default:
                break;
        }
        self.labelNum.textColor = color;
        self.labelNum.layer.borderColor = color.CGColor;
        self.labelStatus.textColor = color;
        self.labelCreateDate.textColor =color;
        self.labelLine.backgroundColor = color;
        self.labelUserName.textColor = color;
//        self.labelPayType.textColor = color;
//        self.labelPayType.layer.borderColor = color.CGColor;
        
        self.zhqsBtn.layer.borderColor = color.CGColor;
        [self.zhqsBtn setTitleColor:color forState:UIControlStateNormal];
        [_zhqsBtn setTitle:btnName forState:UIControlStateNormal];

        if(entity.status == OrderStatusPayed){
            self.iconArrow.hidden = YES;
        }else{
            self.iconArrow.hidden = NO;
            [self.iconArrow setImage:[UIImage imageNamed:imageName]];
        }
//        CGFloat width = [WMHelper calculateTextWidth:statusName font:[UIFont systemFontOfSize:14.f] height:25.f];
//        if(self.statusConstraint){
//            [self.labelStatus removeConstraint:self.statusConstraint];
//            self.statusConstraint =[NSLayoutConstraint constraintWithItem:self.labelStatus attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:width];
//            self.statusConstraint.priority = 501;
//            [self.labelStatus addConstraint:self.statusConstraint];
//        }
//        self.labelStatus.text = statusName;
        if(entity.isExpansion){
            [UIView animateWithDuration:0.5 animations:^{
                self.iconArrow.transform = CGAffineTransformMakeRotation(M_PI/2);
            } completion:^(BOOL finished) {
                
            }];
            
        }else {
            [UIView animateWithDuration:0.5 animations:^{
                self.iconArrow.transform = CGAffineTransformMakeRotation(0);
            } completion:^(BOOL finished) {
                
            }];
            
        }
        self.labelNum.text = [NSString stringWithFormat:@"%02ld",index];
        self.labelCreateDate.text = [NSString stringWithFormat:@"下单时间:%@",entity.createDate];
//        self.labelPayType.text = entity.payType;
        self.labelUserName.text = [NSString stringWithFormat:@"%@ / %@",entity.userName,entity.phone];
        
    }
}


#pragma mark =====================================================  SEL
-(IBAction)gestureTouch:(UIGestureRecognizer*)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(handleOrderHeader:)]){
        [self.delegate handleOrderHeader:self.entity];
    }
}

#pragma mark =====================================================  property package
-(UIView *)statusView{
    if(!_statusView){
        _statusView = [[UIView alloc]init];
        _statusView.backgroundColor = [UIColor whiteColor];
    }
    return _statusView;
}

-(UILabel *)labelNum {
    if(!_labelNum){
        _labelNum = [[UILabel alloc]init];
        _labelNum.font = [UIFont systemFontOfSize:24.f];
        _labelNum.textAlignment = NSTextAlignmentCenter;
        _labelNum.layer.masksToBounds = YES;
        _labelNum.layer.cornerRadius = 5.f;
        _labelNum.layer.borderWidth = 1.f;
    }
    return _labelNum;
}

-(UILabel *)labelStatus{
    if(!_labelStatus){
        _labelStatus = [[UILabel alloc]init];
        _labelStatus.textAlignment = NSTextAlignmentLeft;
        _labelStatus.font = [UIFont systemFontOfSize:14.f];
    }
    return _labelStatus;
}

-(UIImageView *)iconArrow{
    if(!_iconArrow){
        _iconArrow = [[UIImageView alloc]init];
        _iconArrow.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _iconArrow;
}

-(UILabel *)labelCreateDate{
    if(!_labelCreateDate){
        _labelCreateDate = [[UILabel alloc]init];
        _labelCreateDate.font = [UIFont systemFontOfSize:12.f];
        _labelCreateDate.textAlignment = NSTextAlignmentRight;
    }
    return _labelCreateDate;
}

-(UILabel *)labelLine{
    if(!_labelLine){
        _labelLine = [[UILabel alloc]init];
        
    }
    return _labelLine;
}
-(UILabel *)labelUserName{
    if(!_labelUserName){
        _labelUserName = [[UILabel alloc]init];
        
        _labelUserName.textAlignment = NSTextAlignmentLeft;
        _labelUserName.font = [UIFont systemFontOfSize:14.f];
    }
    return _labelUserName;
}
-(UILabel *)labelPayType{
    if(!_labelPayType){
        _labelPayType = [[UILabel alloc]init];
        
        _labelPayType.textAlignment = NSTextAlignmentCenter;
        _labelPayType.font = [UIFont systemFontOfSize:14.f];
        _labelPayType.layer.masksToBounds = YES;
        _labelPayType.layer.cornerRadius = 5.f;
        
        _labelPayType.layer.borderWidth = 1.f;
    }
    return _labelPayType;
}
- (UIButton *)zhqsBtn
{
    if (!_zhqsBtn) {
        _zhqsBtn = [[UIButton alloc]init];
        _zhqsBtn.backgroundColor = [UIColor clearColor];
        _zhqsBtn.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_zhqsBtn setTitle:@"召唤骑士" forState:UIControlStateNormal];
        _zhqsBtn.layer.cornerRadius = 5;
        _zhqsBtn.layer.borderWidth = 1.f;
        _zhqsBtn.layer.masksToBounds = YES;
        _zhqsBtn.hidden = YES;
        [_zhqsBtn addTarget:self action:@selector(SummonKnightButton) forControlEvents:UIControlEventTouchUpInside];
    }
    return _zhqsBtn;
}

-(void)SummonKnightButton{
    
    if (self.SummonKnightBlock) {
        self.SummonKnightBlock();
    }
}
@end
