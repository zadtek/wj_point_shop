//
//  OrderFooter.h
//  RRBI
//
//  Created by kyjun on 16/4/27.
//
//

#import <UIKit/UIKit.h>
#import "MOrder.h"

@protocol OrderFooterDeleagte <NSObject>

@optional
/**
 *  取消订单
 *
 *  @param item MOrder
 */
-(void)orderFooterCancel:(MOrder*)item;
/**
 *  确认发货
 *
 *  @param item MOrder
 */
-(void)orderFooterSend:(MOrder*)item;

-(void)orderSended:(MOrder*)item;

-(void)callDelivery:(NSString*)phone;

@end

@interface OrderFooter : UITableViewHeaderFooterView

@property(nonatomic,strong) MOrder* entity;

@property(nonatomic,weak) id<OrderFooterDeleagte> delegate;

@end
