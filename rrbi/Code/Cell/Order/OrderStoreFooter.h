//
//  OrderStoreFooter.h
//  RRBI
//
//  Created by kyjun on 16/5/5.
//
//

#import <UIKit/UIKit.h>
#import "OrderFooter.h"
#import "MOrder.h"

@interface OrderStoreFooter : UITableViewHeaderFooterView
@property(nonatomic,strong) MOrder* entity;

@property(nonatomic,weak) id<OrderFooterDeleagte> delegate;
@end
