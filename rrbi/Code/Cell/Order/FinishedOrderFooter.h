//
//  FinishedOrderFooter.h
//  RRBI
//
//  Created by kyjun on 16/4/28.
//
//

#import <UIKit/UIKit.h>
#import "MOrder.h"

@interface FinishedOrderFooter : UITableViewHeaderFooterView

@property(nonatomic,strong) MOrder* entity;

@property (copy, nonatomic) void(^callEmptyBlock)(MOrder *entity);

@end
