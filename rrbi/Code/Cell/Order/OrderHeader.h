//
//  OrderHeader.h
//  RRBI
//
//  Created by kyjun on 16/4/27.
//
//

#import <UIKit/UIKit.h>
#import "MOrder.h"

@protocol OrderHeaderDelegate <NSObject>

@optional
-(void)orderHeader:(MOrder*)entity;

@end

@interface OrderHeader : UITableViewHeaderFooterView

-(void)loadData:(MOrder*)entity index:(NSInteger)index;

@property(nonatomic,weak) id<OrderHeaderDelegate> delegate;

@end
