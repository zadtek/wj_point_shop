//
//  OrderFooter.m
//  RRBI
//
//  Created by kyjun on 16/4/27.
//
//

#import "OrderFooter.h"

@interface OrderFooter ()

@property(nonatomic,strong) UILabel* labelOtherTitle;
@property(nonatomic,strong) UILabel* labelOtherValue;
@property(nonatomic,strong) UILabel* labelPackageFeeTitle;
@property(nonatomic,strong) UILabel* labelPackageFeeValue;
/**
 *  满减优惠
 */
@property(nonatomic,strong) UIView* fullCutView;
@property(nonatomic,strong) NSLayoutConstraint* fullCutConstraint;
@property(nonatomic,strong) UILabel* labelCutIcon;
@property(nonatomic,strong) UILabel* labelCutTitle;
@property(nonatomic,strong) UILabel* labelFullCutPrice;
@property(nonatomic,strong) UILabel* labelLine;
@property(nonatomic,strong) UILabel* labelSumPriceTitle;
@property(nonatomic,strong) UILabel* labelSumPriceValue;
@property(nonatomic,strong) UILabel* labelLine2;
@property(nonatomic,strong) UIView* emptyView;
/**
 *  快递员信息
 */
@property(nonatomic,strong) UIButton* btnEmpty;
/**
 *  接单数
 */
@property(nonatomic,strong) UILabel* labelOrders;
@property(nonatomic,strong) UIButton* btnCancel;


@end

@implementation OrderFooter

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
        self.contentView.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha:1.0];
        [self layoutUI];
        [self layoutConstraints];
    }
    return self;
}

#pragma mark =====================================================  user interface layout
-(void)layoutUI{
    [self addSubview:self.labelOtherTitle];
    [self addSubview:self.labelOtherValue];
    [self addSubview:self.labelPackageFeeTitle];
    [self addSubview:self.labelPackageFeeValue];
    [self addSubview:self.fullCutView];
    [self.fullCutView addSubview:self.labelCutIcon];
    [self.fullCutView addSubview:self.labelCutTitle];
    [self.fullCutView addSubview:self.labelFullCutPrice];
    [self addSubview:self.labelLine];
    [self addSubview:self.labelSumPriceTitle];
    [self addSubview:self.labelSumPriceValue];
    [self addSubview:self.labelLine2];
    [self addSubview:self.emptyView];
    [self.emptyView addSubview:self.btnEmpty];
    [self.emptyView addSubview:self.labelOrders];
    [self addSubview:self.btnCancel];
}

-(void)layoutConstraints{
    self.labelOtherTitle.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelOtherValue.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelPackageFeeTitle.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelPackageFeeValue.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelLine.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelSumPriceTitle.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelSumPriceValue.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelLine2.translatesAutoresizingMaskIntoConstraints = NO;
    self.emptyView.translatesAutoresizingMaskIntoConstraints = NO;
    self.btnEmpty.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelOrders.translatesAutoresizingMaskIntoConstraints = NO;
    self.btnCancel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.labelOtherTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherTitle attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40.f]];
    [self.labelOtherTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherTitle attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherTitle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherTitle attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.labelOtherValue addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherValue attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40.f]];
    [self.labelOtherValue addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherValue attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherValue attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherValue attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    
    [self.labelPackageFeeTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeTitle attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30.f]];
    [self.labelPackageFeeTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeTitle attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeTitle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelOtherTitle attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeTitle attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.labelPackageFeeValue addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeValue attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30.f]];
    [self.labelPackageFeeValue addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeValue attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeValue attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelOtherValue attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeValue attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    
    
    self.fullCutConstraint = [NSLayoutConstraint constraintWithItem:self.fullCutView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:20.f];
    self.fullCutConstraint.priority = 701;
    [self.fullCutView addConstraint:self.fullCutConstraint];
    [self.self addConstraint:[NSLayoutConstraint constraintWithItem:self.fullCutView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.f]];
    [self.self addConstraint:[NSLayoutConstraint constraintWithItem:self.fullCutView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelPackageFeeValue attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self.self addConstraint:[NSLayoutConstraint constraintWithItem:self.fullCutView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
    
    
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutIcon attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeHeight multiplier:0.8 constant:0.f]];
    [self.labelCutIcon addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutIcon attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.labelCutIcon attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutIcon attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.f]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutIcon attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelFullCutPrice attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.labelFullCutPrice addConstraint:[NSLayoutConstraint constraintWithItem:self.labelFullCutPrice attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelFullCutPrice attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelFullCutPrice attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutTitle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutTitle attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.labelCutIcon attribute:NSLayoutAttributeRight multiplier:1.0 constant:5.f]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutTitle attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutTitle attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.labelFullCutPrice attribute:NSLayoutAttributeLeft multiplier:1.0 constant:-10.f]];
    
    
    [self.labelLine addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:1.f]];
    [self.labelLine addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH-20]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.labelSumPriceTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceTitle attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40.f]];
    [self.labelSumPriceTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceTitle attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceTitle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelLine attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceTitle attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.labelSumPriceValue addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceValue attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40.f]];
    [self.labelSumPriceValue addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceValue attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceValue attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelLine attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceValue attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    
    [self.labelLine2 addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine2 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:1.f]];
    [self.labelLine2 addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine2 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH-20]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine2 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelSumPriceTitle attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine2 attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.emptyView addConstraint:[NSLayoutConstraint constraintWithItem:self.emptyView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30.f]];
    [self.emptyView addConstraint:[NSLayoutConstraint constraintWithItem:self.emptyView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.emptyView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelLine2 attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.emptyView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
    
    
    [self.btnEmpty addConstraint:[NSLayoutConstraint constraintWithItem:self.btnEmpty attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30.f]];
    [self.btnEmpty addConstraint:[NSLayoutConstraint constraintWithItem:self.btnEmpty attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH*2/3]];
    [self.emptyView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnEmpty attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.emptyView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.emptyView addConstraint:[NSLayoutConstraint constraintWithItem:self.btnEmpty attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.emptyView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.labelOrders addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOrders attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30.f]];
    [self.labelOrders addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOrders attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self.emptyView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOrders attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.emptyView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.emptyView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOrders attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.emptyView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    
    [self.btnCancel addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCancel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30.f]];
    [self.btnCancel addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCancel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:80.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCancel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-10.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCancel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
}

#pragma mark =====================================================  SEL
-(IBAction)cancelTouch:(UIButton*)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(orderFooterCancel:)]){
        [self.delegate orderFooterCancel:self.entity];
    }
}

-(IBAction)sendTouch:(UIButton*)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(orderFooterSend:)]){
        [self.delegate orderFooterSend:self.entity];
    }
}

-(IBAction)callEmpty:(id)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(callDelivery:)]){
        [self.delegate callDelivery:self.entity.emptyPhone];
    }
}

#pragma mark =====================================================  property package

-(void)setEntity:(MOrder *)entity{
    if(entity){
        _entity = entity;
        double packageFee = [entity.packageFee doubleValue];
        double totalPrice =[entity.sumPrice doubleValue];
        double storeDiscount = [entity.storeDiscount doubleValue];
        self.labelPackageFeeValue.text = [NSString stringWithFormat:@"￥%.2f",packageFee];
        self.labelSumPriceValue.text = [NSString stringWithFormat:@"￥%.2f",totalPrice];
        self.labelFullCutPrice.text = [NSString stringWithFormat:@"-￥%.2f",storeDiscount];
        
        if(storeDiscount>0.00){
            self.fullCutView.hidden = NO;
        }else{
            self.fullCutView.hidden = YES;
        }
        
        if(entity.isDelivery){
            self.emptyView.hidden = NO;
            self.labelLine2.hidden = YES;
            NSString* str = [NSString stringWithFormat: @"配送员: %@ / %@",entity.emptyName,entity.emptyPhone];
            NSMutableAttributedString* attributeStr = [[NSMutableAttributedString alloc]initWithString: str];
            [attributeStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0]} range:NSMakeRange(0, str.length)];
            [attributeStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:30/255.f green:147/255.f blue:238/255.f alpha:1.0],NSFontAttributeName:[UIFont systemFontOfSize:15.f]} range:[str rangeOfString:entity.emptyPhone]];
            [self.btnEmpty setAttributedTitle:attributeStr forState:UIControlStateNormal];
            str = [NSString stringWithFormat: @"当前已接:%@单",entity.emptyOrders];
            attributeStr = [[NSMutableAttributedString alloc]initWithString: str];
            [attributeStr addAttributes:@{NSForegroundColorAttributeName:[UIColor redColor],NSFontAttributeName:[UIFont systemFontOfSize:15.f]} range:[str rangeOfString:entity.emptyOrders]];
            self.labelOrders.attributedText = attributeStr;
        }else{
            self.emptyView.hidden = YES;
            self.labelLine2.hidden = NO;
        }
    }
}

-(UILabel *)labelOtherTitle{
    if(!_labelOtherTitle){
        _labelOtherTitle = [[UILabel alloc]init];
        _labelOtherTitle.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelOtherTitle.text = @"其他费用";
    }
    return _labelOtherTitle;
}

-(UILabel *)labelOtherValue{
    if(!_labelOtherValue){
        _labelOtherValue = [[UILabel alloc]init];
        _labelOtherValue.textAlignment = NSTextAlignmentRight;
    }
    return _labelOtherValue;
}

-(UILabel *)labelPackageFeeTitle{
    if(!_labelPackageFeeTitle){
        _labelPackageFeeTitle = [[UILabel alloc]init];
        _labelPackageFeeTitle.font = [UIFont systemFontOfSize:14.f];
        _labelPackageFeeTitle.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _labelPackageFeeTitle.text = @"打包费";
    }
    return _labelPackageFeeTitle;
}

-(UILabel *)labelPackageFeeValue{
    if(!_labelPackageFeeValue){
        _labelPackageFeeValue = [[UILabel alloc]init];
        _labelPackageFeeValue.textColor= [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _labelPackageFeeValue.textAlignment = NSTextAlignmentRight;
    }
    return _labelPackageFeeValue;
}

-(UIView *)fullCutView{
    if(!_fullCutView){
        _fullCutView = [[UIView alloc]init];
        _fullCutView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _fullCutView;
}

-(UILabel *)labelCutIcon{
    if(!_labelCutIcon){
        _labelCutIcon = [[UILabel alloc]init];
        _labelCutIcon.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"icon-full-cut"]];;
        _labelCutIcon.font = [UIFont systemFontOfSize:14.f];
        _labelCutIcon.textColor = [UIColor whiteColor];
        _labelCutIcon.layer.masksToBounds = YES;
        _labelCutIcon.layer.cornerRadius = 3.f;
        _labelCutIcon.textAlignment = NSTextAlignmentCenter;
        _labelCutIcon.text =  @"减";
        _labelCutIcon.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _labelCutIcon;
}

-(UILabel *)labelCutTitle{
    if(!_labelCutTitle){
        _labelCutTitle = [[UILabel alloc]init];
        _labelCutTitle.font = [UIFont systemFontOfSize:14.f];
        _labelCutTitle.text =  @"满减优惠(商家承担):";
        _labelCutTitle.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _labelCutTitle.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _labelCutTitle;
}

-(UILabel *)labelFullCutPrice{
    if(!_labelFullCutPrice){
        _labelFullCutPrice = [[UILabel alloc]init];
        _labelFullCutPrice.text = @"￥0.00";
        _labelFullCutPrice.textAlignment = NSTextAlignmentRight;
        _labelFullCutPrice.textColor = [UIColor redColor];
        _labelFullCutPrice.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _labelFullCutPrice;
}

-(UILabel *)labelLine{
    if(!_labelLine){
        _labelLine = [[UILabel alloc]init];
        _labelLine.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _labelLine;
}

-(UILabel *)labelSumPriceTitle{
    if(!_labelSumPriceTitle){
        _labelSumPriceTitle = [[UILabel alloc]init];
        _labelSumPriceTitle.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelSumPriceTitle.text = @"总计";
    }
    return _labelSumPriceTitle;
}

-(UILabel *)labelSumPriceValue{
    if(!_labelSumPriceValue){
        _labelSumPriceValue = [[UILabel alloc]init];
        _labelSumPriceValue.textColor = [UIColor colorWithRed:255/255.f green:77/255.f blue:77/255.f alpha:1.0];
        _labelSumPriceValue.textAlignment = NSTextAlignmentRight;
    }
    return _labelSumPriceValue;
}

-(UILabel *)labelLine2{
    if(!_labelLine2){
        _labelLine2 = [[UILabel alloc]init];
        _labelLine2.hidden = YES;
        _labelLine2.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _labelLine2;
}

-(UIView *)emptyView{
    if(!_emptyView){
        _emptyView = [[UIView alloc]init];
        _emptyView.backgroundColor = [UIColor colorWithRed:239/255.f green:232/255.f blue:38/255.f alpha:1.0];
    }
    return _emptyView;
}

-(UIButton *)btnEmpty{
    if(!_btnEmpty){
        _btnEmpty = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnEmpty setTitleColor:[UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0] forState:UIControlStateNormal] ;
        _btnEmpty.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_btnEmpty setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_btnEmpty addTarget:self action:@selector(callEmpty:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnEmpty;
}

-(UILabel *)labelOrders{
    if(!_labelOrders){
        _labelOrders = [[UILabel alloc]init];
        _labelOrders.textColor = [UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0];
        _labelOrders.textAlignment = NSTextAlignmentRight;
        _labelOrders.font = [UIFont systemFontOfSize:14.f];
    }
    return _labelOrders;
}

-(UIButton *)btnCancel{
    if(!_btnCancel){
        _btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnCancel setTitleColor:[UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0] forState:UIControlStateNormal];
        [_btnCancel setTitle:@"取消订单" forState:UIControlStateNormal];
        _btnCancel.layer.masksToBounds = YES;
        _btnCancel.layer.cornerRadius = 5.f;
        _btnCancel.layer.borderColor = [UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0].CGColor;
        _btnCancel.layer.borderWidth = 1.f;
        _btnCancel.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_btnCancel addTarget:self action:@selector(cancelTouch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnCancel;
}


@end
