//
//  GoodsCell.m
//  RRBI
//
//  Created by kyjun on 16/4/27.
//
//

#import "GoodsCell.h"

@interface GoodsCell ()

@property(nonatomic,strong)UILabel* labelFoodName;
@property(nonatomic,strong) UILabel* labelQuantity;
@property(nonatomic,strong) UILabel* labelPrice;
@property(nonatomic,strong) UILabel* labelLine;

@end

@implementation GoodsCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self layoutUI];
        [self layoutConstraints];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark =====================================================  user interface layout
-(void)layoutUI{
    [self.contentView addSubview:self.labelFoodName];
    [self.contentView addSubview:self.labelQuantity];
    [self.contentView addSubview:self.labelPrice];
    [self.contentView addSubview:self.labelLine];
}

-(void)layoutConstraints{
    self.labelFoodName.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelQuantity.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelPrice.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelLine.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.labelFoodName addConstraint:[NSLayoutConstraint constraintWithItem:self.labelFoodName attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH*3/5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelFoodName attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelFoodName attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelFoodName attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
  
    [self.labelQuantity addConstraint:[NSLayoutConstraint constraintWithItem:self.labelQuantity attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/10]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelQuantity attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelQuantity attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelQuantity attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.labelFoodName attribute:NSLayoutAttributeRight multiplier:1.0 constant:10.f]];
    
    [self.labelPrice addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPrice attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPrice attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPrice attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPrice attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    
    [self.labelLine addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH-20]];
    [self.labelLine addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:1.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
}

#pragma mark =====================================================  property package
-(void)setEntity:(MGoods *)entity{
    if(entity){
        _entity = entity;
        self.labelFoodName.text = entity.goodsName;
        self.labelQuantity.text = [NSString stringWithFormat:@"x%@",entity.quantity];
        self.labelPrice.text = [NSString stringWithFormat:@"￥%.2f",[entity.sumPrice floatValue]];
    }
}

-(UILabel *)labelFoodName{
    if(!_labelFoodName){
        _labelFoodName = [[UILabel alloc]init];
        _labelFoodName.font = [UIFont systemFontOfSize:14.f];
        _labelFoodName.textAlignment = NSTextAlignmentLeft;
        _labelFoodName.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _labelFoodName.numberOfLines=0;
        _labelFoodName.lineBreakMode = NSLineBreakByCharWrapping;
    }
    return _labelFoodName;
}

-(UILabel *)labelQuantity{
    if (!_labelQuantity) {
        _labelQuantity = [[UILabel alloc]init];
        _labelQuantity.font = [UIFont systemFontOfSize:14.f];
        _labelQuantity.textAlignment = NSTextAlignmentLeft;
        _labelQuantity.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
    }
    return _labelQuantity;
}

-(UILabel *)labelPrice{
    if (!_labelPrice) {
        _labelPrice = [[UILabel alloc]init];
        _labelPrice.font = [UIFont systemFontOfSize:14.f];
        _labelPrice.textAlignment = NSTextAlignmentRight;
        _labelPrice.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
    }
    return _labelPrice;
}

-(UILabel *)labelLine{
    if(!_labelLine){
        _labelLine = [[UILabel alloc]init];
        _labelLine.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _labelLine;
}

@end
