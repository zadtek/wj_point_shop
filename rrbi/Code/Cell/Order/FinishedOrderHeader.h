//
//  FinishedOrderHeader.h
//  RRBI
//
//  Created by kyjun on 16/5/4.
//
//

#import <UIKit/UIKit.h>
#import "HandleOrderHeader.h"
#import "MOrder.h"

@interface FinishedOrderHeader : UITableViewHeaderFooterView

-(void)loadData:(MOrder*)entity index:(NSInteger)index;

@property(nonatomic,weak) id<HandleOrderHeaderDelegate> delegate;

@property (copy, nonatomic) void(^callPersonPhoneBlock)(void);


@property (copy, nonatomic) void(^SummonKnightBlock)(void);
@end
