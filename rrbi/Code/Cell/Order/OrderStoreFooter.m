//
//  OrderStoreFooter.m
//  RRBI
//
//  Created by kyjun on 16/5/5.
//
//

#import "OrderStoreFooter.h"

@interface OrderStoreFooter ()

@property(nonatomic,strong) UILabel* labelOtherTitle;
@property(nonatomic,strong) UILabel* labelOtherValue;
@property(nonatomic,strong) UILabel* labelPackageFeeTitle;
@property(nonatomic,strong) UILabel* labelPackageFeeValue;

/**
 *  满减优惠
 */
@property(nonatomic,strong) UIView* fullCutView;
@property(nonatomic,strong) NSLayoutConstraint* fullCutConstraint;
@property(nonatomic,strong) UILabel* labelCutIcon;
@property(nonatomic,strong) UILabel* labelCutTitle;
@property(nonatomic,strong) UILabel* labelFullCutPrice;

@property(nonatomic,strong) UILabel* labelLine;
@property(nonatomic,strong) UILabel* labelSumPriceTitle;
@property(nonatomic,strong) UILabel* labelSumPriceValue;
@property(nonatomic,strong) UILabel* labelLine2;
@property(nonatomic,strong) UIButton* btnCancel;
@property(nonatomic,strong) UIButton* btnSend;

@property(nonatomic,strong) UIView* bottomView;

@end

@implementation OrderStoreFooter


-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self layoutUI];
        [self layoutConstraints];
    }
    return self;
}

#pragma mark =====================================================  user interface layout
-(void)layoutUI{
    [self addSubview:self.labelOtherTitle];
    [self addSubview:self.labelOtherValue];
    [self addSubview:self.labelPackageFeeTitle];
    [self addSubview:self.labelPackageFeeValue];
    [self addSubview:self.fullCutView];
    [self.fullCutView addSubview:self.labelCutIcon];
    [self.fullCutView addSubview:self.labelCutTitle];
    [self.fullCutView addSubview:self.labelFullCutPrice];
    [self addSubview:self.labelLine];
    [self addSubview:self.labelSumPriceTitle];
    [self addSubview:self.labelSumPriceValue];
    [self addSubview:self.labelLine2];
    [self addSubview:self.btnCancel];
    [self addSubview:self.btnSend];
    [self addSubview:self.bottomView];

}

-(void)layoutConstraints{
    self.labelOtherTitle.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelOtherValue.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelPackageFeeTitle.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelPackageFeeValue.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelLine.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelSumPriceTitle.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelSumPriceValue.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelLine2.translatesAutoresizingMaskIntoConstraints = NO;
    self.btnCancel.translatesAutoresizingMaskIntoConstraints = NO;
    self.btnSend.translatesAutoresizingMaskIntoConstraints = NO;
    self.bottomView.translatesAutoresizingMaskIntoConstraints = NO;

    [self.labelOtherTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherTitle attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40.f]];
    [self.labelOtherTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherTitle attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherTitle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherTitle attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.labelOtherValue addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherValue attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40.f]];
    [self.labelOtherValue addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherValue attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherValue attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelOtherValue attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    
    [self.labelPackageFeeTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeTitle attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30.f]];
    [self.labelPackageFeeTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeTitle attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeTitle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelOtherTitle attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeTitle attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.labelPackageFeeValue addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeValue attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30.f]];
    [self.labelPackageFeeValue addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeValue attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeValue attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelOtherValue attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelPackageFeeValue attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    
    
    self.fullCutConstraint = [NSLayoutConstraint constraintWithItem:self.fullCutView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:20.f];
    self.fullCutConstraint.priority = 701;
    [self.fullCutView addConstraint:self.fullCutConstraint];
    [self.self addConstraint:[NSLayoutConstraint constraintWithItem:self.fullCutView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.f]];
    [self.self addConstraint:[NSLayoutConstraint constraintWithItem:self.fullCutView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelPackageFeeValue attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self.self addConstraint:[NSLayoutConstraint constraintWithItem:self.fullCutView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
    
    
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutIcon attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeHeight multiplier:0.8 constant:0.f]];
    [self.labelCutIcon addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutIcon attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.labelCutIcon attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutIcon attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.f]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutIcon attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelFullCutPrice attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.labelFullCutPrice addConstraint:[NSLayoutConstraint constraintWithItem:self.labelFullCutPrice attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelFullCutPrice attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelFullCutPrice attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutTitle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutTitle attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.labelCutIcon attribute:NSLayoutAttributeRight multiplier:1.0 constant:5.f]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutTitle attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self.fullCutView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCutTitle attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.labelFullCutPrice attribute:NSLayoutAttributeLeft multiplier:1.0 constant:-10.f]];
    
    
    
    [self.labelLine addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:1.f]];
    [self.labelLine addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH-20]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.fullCutView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.labelSumPriceTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceTitle attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40.f]];
    [self.labelSumPriceTitle addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceTitle attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceTitle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelLine attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceTitle attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.labelSumPriceValue addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceValue attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40.f]];
    [self.labelSumPriceValue addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceValue attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceValue attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelLine attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelSumPriceValue attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    
    [self.labelLine2 addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine2 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:1.f]];
    [self.labelLine2 addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine2 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH-20]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine2 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelSumPriceTitle attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.labelLine2 attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.btnSend addConstraint:[NSLayoutConstraint constraintWithItem:self.btnSend attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30.f]];
    [self.btnSend addConstraint:[NSLayoutConstraint constraintWithItem:self.btnSend attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:80.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.btnSend attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelLine2 attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.btnSend attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    
    [self.btnCancel addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCancel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30.f]];
    [self.btnCancel addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCancel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:80.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCancel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.labelLine2 attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10.f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCancel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.btnSend attribute:NSLayoutAttributeLeft multiplier:1.0 constant:-10.f]];
    
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(10);
    }];
    
}

#pragma mark =====================================================  SEL
-(IBAction)cancelTouch:(UIButton*)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(orderFooterCancel:)]){
        [self.delegate orderFooterCancel:self.entity];
    }
}

-(IBAction)sendTouch:(UIButton*)sender{
    if(self.entity.status == OrderStatusNewsOrder){
        if(self.delegate && [self.delegate respondsToSelector:@selector(orderFooterSend:)]){
            [self.delegate orderFooterSend:self.entity];
        }
    }else{
        if(self.delegate && [self.delegate respondsToSelector:@selector(orderSended:)]){
            [self.delegate orderSended:self.entity];
        }
    }
}
#pragma mark =====================================================  property package

-(void)setEntity:(MOrder *)entity{
    if(entity){
        _entity = entity;
        double packageFee = [entity.packageFee doubleValue];
        double totalPrice =[entity.sumPrice doubleValue];
        double storeDiscount = [entity.storeDiscount doubleValue];
        self.labelPackageFeeValue.text = [NSString stringWithFormat:@"￥%.2f",packageFee];
        self.labelSumPriceValue.text = [NSString stringWithFormat:@"￥%.2f",totalPrice];
        self.labelFullCutPrice.text = [NSString stringWithFormat:@"-￥%.2f",storeDiscount];
        if(storeDiscount>0.00){
            self.fullCutView.hidden = NO;
        }else{
            self.fullCutView.hidden = YES;
        }
        if(entity.status == OrderStatusNewsOrder){
            self.btnCancel.userInteractionEnabled = YES;
            [self.btnSend setTitle: @"确认接单" forState:UIControlStateNormal];
        }else{
            self.btnCancel.userInteractionEnabled = NO;
            [self.btnSend setTitle: @"确认送达" forState:UIControlStateNormal];
        }
    }
}

-(UILabel *)labelOtherTitle{
    if(!_labelOtherTitle){
        _labelOtherTitle = [[UILabel alloc]init];
        _labelOtherTitle.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelOtherTitle.text = @"其他费用";
    }
    return _labelOtherTitle;
}

-(UILabel *)labelOtherValue{
    if(!_labelOtherValue){
        _labelOtherValue = [[UILabel alloc]init];
        _labelOtherValue.textAlignment = NSTextAlignmentRight;
    }
    return _labelOtherValue;
}

-(UILabel *)labelPackageFeeTitle{
    if(!_labelPackageFeeTitle){
        _labelPackageFeeTitle = [[UILabel alloc]init];
        _labelPackageFeeTitle.font = [UIFont systemFontOfSize:14.f];
        _labelPackageFeeTitle.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _labelPackageFeeTitle.text = @"打包费";
    }
    return _labelPackageFeeTitle;
}

-(UILabel *)labelPackageFeeValue{
    if(!_labelPackageFeeValue){
        _labelPackageFeeValue = [[UILabel alloc]init];
        _labelPackageFeeValue.textColor= [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _labelPackageFeeValue.textAlignment = NSTextAlignmentRight;
    }
    return _labelPackageFeeValue;
}

-(UIView *)fullCutView{
    if(!_fullCutView){
        _fullCutView = [[UIView alloc]init];
        _fullCutView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _fullCutView;
}

-(UILabel *)labelCutIcon{
    if(!_labelCutIcon){
        _labelCutIcon = [[UILabel alloc]init];
        _labelCutIcon.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"icon-full-cut"]];;
        _labelCutIcon.font = [UIFont systemFontOfSize:14.f];
        _labelCutIcon.textColor = [UIColor whiteColor];
        _labelCutIcon.layer.masksToBounds = YES;
        _labelCutIcon.layer.cornerRadius = 3.f;
        _labelCutIcon.textAlignment = NSTextAlignmentCenter;
        _labelCutIcon.text =  @"减";
        _labelCutIcon.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _labelCutIcon;
}

-(UILabel *)labelCutTitle{
    if(!_labelCutTitle){
        _labelCutTitle = [[UILabel alloc]init];
        _labelCutTitle.font = [UIFont systemFontOfSize:14.f];
        _labelCutTitle.text =  @"满减优惠(商家承担):";
        _labelCutTitle.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _labelCutTitle.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _labelCutTitle;
}

-(UILabel *)labelFullCutPrice{
    if(!_labelFullCutPrice){
        _labelFullCutPrice = [[UILabel alloc]init];
        _labelFullCutPrice.text = @"￥0.00";
        _labelFullCutPrice.textAlignment = NSTextAlignmentRight;
        _labelFullCutPrice.textColor = [UIColor redColor];
        _labelFullCutPrice.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _labelFullCutPrice;
}
-(UILabel *)labelLine{
    if(!_labelLine){
        _labelLine = [[UILabel alloc]init];
        _labelLine.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _labelLine;
}

-(UILabel *)labelSumPriceTitle{
    if(!_labelSumPriceTitle){
        _labelSumPriceTitle = [[UILabel alloc]init];
        _labelSumPriceTitle.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelSumPriceTitle.text = @"总计";
    }
    return _labelSumPriceTitle;
}

-(UILabel *)labelSumPriceValue{
    if(!_labelSumPriceValue){
        _labelSumPriceValue = [[UILabel alloc]init];
        _labelSumPriceValue.textColor = [UIColor colorWithRed:255/255.f green:77/255.f blue:77/255.f alpha:1.0];
        _labelSumPriceValue.textAlignment = NSTextAlignmentRight;
    }
    return _labelSumPriceValue;
}

-(UILabel *)labelLine2{
    if(!_labelLine2){
        _labelLine2 = [[UILabel alloc]init];
        _labelLine2.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _labelLine2;
}

-(UIButton *)btnCancel{
    if(!_btnCancel){
        _btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnCancel setTitleColor:[UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0] forState:UIControlStateNormal];
        [_btnCancel setTitle:@"拒绝接单" forState:UIControlStateNormal];
        _btnCancel.layer.masksToBounds = YES;
        _btnCancel.layer.cornerRadius = 5.f;
        _btnCancel.layer.borderColor = [UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0].CGColor;
        _btnCancel.layer.borderWidth = 1.f;
        _btnCancel.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_btnCancel addTarget:self action:@selector(cancelTouch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnCancel;
}

-(UIButton *)btnSend{
    if(!_btnSend){
        _btnSend = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnSend setTitleColor: [UIColor colorWithRed:255/255.f green:162/255.f blue:0/255.f alpha:1.0] forState:UIControlStateNormal];
        [_btnSend setTitle:@"确认发货" forState:UIControlStateNormal];
        _btnSend.layer.masksToBounds = YES;
        _btnSend.layer.cornerRadius = 5.f;
        _btnSend.layer.borderColor =  [UIColor colorWithRed:255/255.f green:162/255.f blue:0/255.f alpha:1.0].CGColor;
        _btnSend.layer.borderWidth = 1.f;
        _btnSend.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_btnSend addTarget:self action:@selector(sendTouch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnSend;
    
}
-(UIView *)bottomView{
    if(!_bottomView){
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    return _bottomView;
}
@end
