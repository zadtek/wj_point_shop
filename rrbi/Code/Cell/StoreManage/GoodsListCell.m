//
//  GoodsListCell.m
//  RRBI
//
//  Created by kuyuZJ on 16/7/5.
//
//

#import "GoodsListCell.h"
#import "TopLeftLabe.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface GoodsListCell ()

@property(nonatomic,strong) UIImageView* photo;
@property(nonatomic,strong) UIView* middleView;
@property(nonatomic,strong) UIView* backView;
@property(nonatomic,strong) TopLeftLabe* labelTitle;
@property(nonatomic,strong) UILabel* labelPrice;
@property(nonatomic,strong) UILabel* labelType;
@property(nonatomic,strong) UILabel* labelCag;//分类
@property(nonatomic,strong) UIView* rightView;
@property(nonatomic,strong) UIButton* btnpPrice;
@property(nonatomic,strong) UIButton* btnStatus;
@property(nonatomic,strong) UIImageView* line;

@end

@implementation GoodsListCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self layoutUI];
        [self layoutConstraints];
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark =====================================================  user interface layout
-(void)layoutUI{
    
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.photo];
    [self.backView addSubview:self.middleView];
    [self.middleView addSubview:self.labelTitle];
    [self.middleView addSubview:self.labelPrice];
    [self.middleView addSubview:self.labelType];
    [self.middleView addSubview:self.labelCag];
    [self.backView addSubview:self.rightView];
    [self.rightView addSubview:self.btnpPrice];
    [self.rightView addSubview:self.btnStatus];
    [self.backView addSubview:self.line];


}

-(void)layoutConstraints{
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView);
        make.height.equalTo(self.contentView);
        make.left.right.equalTo(self.contentView);
    }];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView);
        make.height.mas_equalTo(1);
        make.left.right.equalTo(self.backView);
    }];
    
    [self.photo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.backView).offset(10);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    [self.middleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.rightView);
        make.right.equalTo(self.rightView.mas_left).offset(-5);
        make.left.equalTo(self.photo.mas_right).offset(5);
    }];
    
    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.photo);
        make.right.equalTo(self.backView).offset(-10);
        make.width.mas_equalTo(60);
    }];
    
    
    [self.btnpPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.rightView);
        make.width.equalTo(self.rightView);
        make.height.mas_equalTo(20);
        make.left.equalTo(self.rightView);
    }];
    
    [self.btnStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self.rightView);
        make.width.equalTo(self.rightView);
        make.height.equalTo(self.btnpPrice);
        make.left.equalTo(self.rightView);
    }];
    
    
    
    [self.labelType mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self.middleView);
        make.width.equalTo(self.middleView);
        make.height.mas_equalTo(15);
        make.left.equalTo(self.middleView);
    }];
    
    
    [self.labelPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self.labelType.mas_top);
        make.width.equalTo(self.middleView);
        make.height.equalTo(self.labelType);
        make.left.equalTo(self.middleView);
    }];
    
    [self.labelTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.middleView);
        make.bottom.equalTo(self.labelPrice.mas_top);
        make.width.equalTo(self.middleView.mas_width).offset(-60);
        make.left.equalTo(self.middleView);
    }];
    
    [self.labelCag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.middleView);
        make.right.equalTo(self.middleView);
        make.bottom.equalTo(self.labelPrice.mas_top);
        make.width.mas_equalTo(60);
    }];
    
}

#pragma mark =====================================================  SEL
-(IBAction)updatePriceTouch:(id)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(updateGoodsPrice:)]){
        [self.delegate updateGoodsPrice:self.entity];
    }
}

-(IBAction)pushlishTouch:(id)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(publishGoods:)]){
        [self.delegate publishGoods:self.entity];
    }
}

-(void) labelTouchUpInside:(UITapGestureRecognizer *)recognizer{
    
//    UILabel *label=(UILabel*)recognizer.view;
//    NSLog(@"%@被点击了",label.text);
    if(self.delegate && [self.delegate respondsToSelector:@selector(updateGoodsTitle:)]){
        [self.delegate updateGoodsTitle:self.entity];
    }
}

-(void) photoTouchUpInside:(UITapGestureRecognizer *)recognizer{
    
    //    UILabel *label=(UILabel*)recognizer.view;
    //    NSLog(@"%@被点击了",label.text);
    if(self.delegate && [self.delegate respondsToSelector:@selector(updateGoodsImage:)]){
        [self.delegate updateGoodsImage:self.entity];
    }
}

#pragma mark =====================================================  property package
-(void)setEntity:(MGoods *)entity{
    if(entity){
        _entity = entity;
        [self.photo sd_setImageWithURL:[NSURL URLWithString:entity.default_image] placeholderImage:[UIImage imageNamed: @"placeholder"]];
//        self.labelTitle.frame = CGRectMake(0, 0, SCREEN_WIDTH-140, CGRectGetHeight(self.frame)-35);
        self.labelTitle.text = entity.name;
        self.labelPrice.text = [NSString stringWithFormat: @"进价:¥%@   售价:¥%@",entity.jinhuo_price,entity.price];
        if([entity.status integerValue] == 0){
            self.btnStatus.backgroundColor = [UIColor colorWithRed:181/255.f green:181/255.f blue:182/255.f alpha:1.0];
            [self.btnStatus setTitle: @"已下架" forState:UIControlStateNormal];
        }else{
            self.btnStatus.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"bg-store-btn-publish"]];
            [self.btnStatus setTitle: @"已上架" forState:UIControlStateNormal];

        }
        
        self.labelType.text = [NSString stringWithFormat: @"会员价:¥%@  销量%@   库存%@",entity.members_price,entity.sales_num,entity.stock];
        self.labelCag.text = [NSString stringWithFormat:@"%@",entity.cate_name];
    }
}
-(UIImageView *)photo{
    if(!_photo){
        _photo = [[UIImageView alloc]init];
//        _photo.layer.borderColor = theme_line_color.CGColor;
//        _photo.layer.borderWidth = 1.f;
        _photo.userInteractionEnabled=YES;
        UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(photoTouchUpInside:)];
        [_photo addGestureRecognizer:labelTapGestureRecognizer];
    }
    return _photo;
}
-(UIView *)middleView{
    if(!_middleView){
        _middleView = [[UIView alloc]init];
    }
    return _middleView;
}

-(TopLeftLabe *)labelTitle{
    if(!_labelTitle){
        _labelTitle = [[TopLeftLabe alloc]init];
        _labelTitle.numberOfLines = 0;
        _labelTitle.textColor = [UIColor colorWithRed:60/255.f green:60/255.f blue:60/255.f alpha:1.0];
        _labelTitle.font = [UIFont systemFontOfSize:14.f];
        [_labelTitle setContentMode:UIViewContentModeTop];
        _labelTitle.userInteractionEnabled=YES;
        UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelTouchUpInside:)];
        [_labelTitle addGestureRecognizer:labelTapGestureRecognizer];
        
    }
    return _labelTitle;
}

-(UILabel *)labelCag{
    if(!_labelCag){
        _labelCag = [[UILabel alloc]init];
        _labelCag.textColor = [UIColor colorWithRed:125.0/255.f green:125.0/255.f blue:125.0/255.f alpha:1.0];
        _labelCag.font = [UIFont systemFontOfSize:12.f];
        [_labelCag setContentMode:UIViewContentModeTop];
        
    }
    return _labelCag;
}


-(UILabel *)labelType{
    if(!_labelType){
        _labelType = [[UILabel alloc]init];
        _labelType.textColor = [UIColor colorWithRed:125/255.f green:125/255.f blue:125/255.f alpha:1.f];
        _labelType.font = [UIFont systemFontOfSize:12.f];
    }
    return _labelType;
}
-(UILabel *)labelPrice{
    if(!_labelPrice){
        _labelPrice = [[UILabel alloc]init];
        _labelPrice.textColor = [UIColor colorWithRed:125/255.f green:125/255.f blue:125/255.f alpha:1.f];
        _labelPrice.font = [UIFont systemFontOfSize:12.f];

    }
    return _labelPrice;
}

-(UIView *)rightView{
    if(!_rightView){
        _rightView = [[UIView alloc]init];
    }
    return _rightView;
}

-(UIButton *)btnpPrice{
    if(!_btnpPrice){
        _btnpPrice = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnpPrice setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnpPrice setTitle: @"修改" forState:UIControlStateNormal];
        _btnpPrice.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"bg-store-btn-update"]];
        _btnpPrice.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_btnpPrice addTarget:self action:@selector(updatePriceTouch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnpPrice;
}

-(UIButton *)btnStatus{
    if(!_btnStatus){
        _btnStatus = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnStatus setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnStatus setTitle: @"已上架" forState:UIControlStateNormal];
        _btnStatus.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"bg-store-btn-publish"]];
        _btnStatus.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_btnStatus addTarget:self action:@selector(pushlishTouch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnStatus;
}

-(UIImageView *)line{
    if(!_line){
        _line = [[UIImageView alloc]init];
        _line.backgroundColor = theme_line_color;
    }
    return _line;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}



@end
