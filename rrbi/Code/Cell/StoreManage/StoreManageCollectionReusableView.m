//
//  StoreManageCollectionReusableView.m
//  rrbi
//
//  Created by mac book on 2018/12/3.
//

#import "StoreManageCollectionReusableView.h"

@interface StoreManageCollectionReusableView ()

@property(nonatomic,strong) UIView* topView;
@property(nonatomic,strong) UIView* bottomView;

@property(nonatomic,strong) UIImageView* iconLogo;
@property(nonatomic,strong) UILabel* labelStoreName;
@property(nonatomic,strong) UILabel* labelStoreSummary;

@property(nonatomic,strong) UIButton* btnChangeStatus;
@property(nonatomic,strong) UIButton* timeChangeStatus;


@property(nonatomic,strong) UILabel* labelTodaySale;
@property(nonatomic,strong) UILabel* labelSumSale;
@property(nonatomic,strong) UILabel* labelTodayNum;

@end


@implementation StoreManageCollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
       
        [self setUpView];
    }
    return self;
}

-(void)setEntity:(MStore *)entity{
    
    
    _entity = entity;
    [self.iconLogo sd_setImageWithURL:[NSURL URLWithString:entity.logo] placeholderImage:[UIImage imageNamed:@"icon-store"]];
    self.labelStoreName.text = entity.storeName;
    self.labelStoreSummary.text =entity.notice;
    self.labelTodaySale.text = entity.todayMoney;
    self.labelSumSale.text = entity.balance;
    [self.btnChangeStatus setTitle:[entity.status isEqualToString:@"1"]?@"营业中":@"休息中" forState:UIControlStateNormal];
    [self.timeChangeStatus setTitle:@"修改" forState:UIControlStateNormal];
    self.labelStoreTime.text = [NSString stringWithFormat:@"营业时间：%@ ~ %@",entity.open_time,entity.close_time];
    
    
    
    
    NSString* money = [NSString stringWithFormat:@"￥%@",entity.todayMoney];
    NSString* strMoney = [NSString stringWithFormat:@"%@ \n 今日收入",money];
    
    NSMutableAttributedString* attributeStr  = [[NSMutableAttributedString alloc]initWithString:strMoney];
    [attributeStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:22.f],NSForegroundColorAttributeName:[UIColor colorWithRed:251/255.f green:182/255.f blue:92/255.f alpha:1.0]} range:[strMoney rangeOfString:money]];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:8];
    [attributeStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [strMoney length])];
    self.labelTodaySale.attributedText = attributeStr;
    self.labelTodaySale.textAlignment = NSTextAlignmentCenter;
    



    money = [NSString stringWithFormat:@"￥%@",entity.balance];
    strMoney = [NSString stringWithFormat:@"%@ \n 账户余额",money];
    attributeStr  = [[NSMutableAttributedString alloc]initWithString:strMoney];
    [attributeStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:22.f],NSForegroundColorAttributeName:[UIColor colorWithRed:251/255.f green:182/255.f blue:92/255.f alpha:1.0]} range:[strMoney rangeOfString:money]];
    paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:8];
    [attributeStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [strMoney length])];
    self.labelSumSale.attributedText = attributeStr;
    self.labelSumSale.textAlignment = NSTextAlignmentCenter;
    
  
    
    
   NSString *num = [NSString stringWithFormat:@"%@",entity.today_order_num];
   NSString *numStr = [NSString stringWithFormat:@"%@ \n 今日单量",num];
    attributeStr  = [[NSMutableAttributedString alloc]initWithString:numStr];
    [attributeStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:22.f],NSForegroundColorAttributeName:[UIColor colorWithRed:251/255.f green:182/255.f blue:92/255.f alpha:1.0]} range:[numStr rangeOfString:num]];
    paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:8];
    [attributeStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [numStr length])];
    self.labelTodayNum.attributedText = attributeStr;
    self.labelTodayNum.textAlignment = NSTextAlignmentCenter;
    
    
}

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
    
    //do something....
    if (self.LogoImageBlock) {
        self.LogoImageBlock();
    }
}

- (void)setUpView{
    
    
     [self setBackgroundColor:[UIColor whiteColor]];
    
    [self addSubview:self.topView];
    [self addSubview:self.bottomView];
    
    self.iconLogo.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.iconLogo addGestureRecognizer:singleTap];
    
    
    [self.topView addSubview:self.iconLogo];
    [self.topView addSubview:self.labelStoreName];
    [self.topView addSubview:self.labelStoreTime];
    [self.topView addSubview:self.labelStoreSummary];
    [self.topView addSubview:self.btnChangeStatus];
    [self.topView addSubview:self.timeChangeStatus];
    [self.bottomView addSubview:self.labelTodaySale];
    [self.bottomView addSubview:self.labelTodayNum];
    [self.bottomView addSubview:self.labelSumSale];
    
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.equalTo(self).multipliedBy(0.7);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topView.mas_bottom);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.bottom.equalTo(self);
    }];
    
    
    [self.iconLogo mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(self.topView);
        make.left.equalTo(self.topView).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    
    
    [self.btnChangeStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.iconLogo);
        make.right.equalTo(self.topView).mas_offset(-10);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(60);

    }];
    
    
    
    
    [self.labelStoreName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconLogo);
        make.right.equalTo(self.btnChangeStatus.mas_left);
        make.left.equalTo(self.iconLogo.mas_right).mas_offset(10);
        make.height.equalTo(self.iconLogo).multipliedBy(0.5);
    }];
    
    
    [self.labelStoreSummary mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelStoreName.mas_bottom);
        make.left.right.height.equalTo(self.labelStoreName);
    }];




    [self.labelStoreTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.iconLogo.mas_bottom);
        make.right.equalTo(self.labelStoreSummary);
        make.left.equalTo(self.labelStoreSummary);
        make.bottom.equalTo(self.topView);
    }];

    
    [self.timeChangeStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.labelStoreTime);
        make.left.right.height.equalTo(self.btnChangeStatus);
        
    }];
    
    
    float common_w = SCREEN_WIDTH/3;
    [self.labelTodaySale mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.equalTo(self.bottomView);
        make.width.mas_equalTo(common_w);
    }];
    
    [self.labelTodayNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.bottomView);
        make.width.equalTo(self.labelTodaySale);
        make.left.equalTo(self.labelTodaySale.mas_right);
    }];
    
    [self.labelSumSale mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.bottomView);
        make.width.equalTo(self.labelTodaySale);
        make.left.equalTo(self.labelTodayNum.mas_right);

    }];
    
    
    
    
    
    
    self.btnChangeStatus.tag = 10001;
    self.timeChangeStatus.tag = 10002;
}


-(UIView *)topView{
    if(!_topView){
        _topView = [[UIView alloc]init];
        _topView.backgroundColor = [UIColor colorWithRed:50/255.f green:58/255.f blue:69/255.f alpha:1.0];
    }
    return _topView;
    
}


-(UIImageView *)iconLogo{
    if(!_iconLogo){
        _iconLogo = [[UIImageView alloc]init];
        _iconLogo.layer.masksToBounds = YES;
        _iconLogo.layer.cornerRadius = 30.f;
        _iconLogo.layer.borderColor =  [UIColor colorWithRed:190/255.f green:194/255.f blue:197/255.f alpha:1.0].CGColor;
        _iconLogo.layer.borderWidth = 2.f;
    }
    return _iconLogo;
}

-(UILabel *)labelStoreName{
    if(!_labelStoreName){
        _labelStoreName = [[UILabel alloc]init];
        _labelStoreName.numberOfLines = 0;
        _labelStoreName.lineBreakMode = NSLineBreakByWordWrapping;
        _labelStoreName.textColor = [UIColor whiteColor];
    }
    return _labelStoreName;
}

-(UILabel *)labelStoreSummary{
    if(!_labelStoreSummary){
        _labelStoreSummary = [[UILabel alloc]init];
        _labelStoreSummary.textColor = [UIColor colorWithRed:148/255.f green:152/255.f blue:157/255.f alpha:1.0];
        _labelStoreSummary.font = [UIFont systemFontOfSize:14.f];
        _labelStoreSummary.numberOfLines = 0;
        _labelStoreSummary.lineBreakMode = NSLineBreakByWordWrapping;
    }
    return _labelStoreSummary;
}

-(UILabel *)labelStoreTime{
    
    if (!_labelStoreTime) {
        _labelStoreTime = [[UILabel alloc]init];
        _labelStoreTime.textColor = [UIColor colorWithRed:148/255.f green:152/255.f blue:157/255.f alpha:1.0];
        _labelStoreTime.font = [UIFont systemFontOfSize:14.f];
    }
    return _labelStoreTime;
}
-(UIButton *)btnChangeStatus{
    if(!_btnChangeStatus){
        _btnChangeStatus = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnChangeStatus setTitleColor: [UIColor colorWithRed:255/255.f green:190/255.f blue:33/255.f alpha:1.0] forState:UIControlStateNormal];
        _btnChangeStatus.layer.masksToBounds = YES;
        _btnChangeStatus.layer.cornerRadius = 5.f;
        _btnChangeStatus.layer.borderColor =  [UIColor colorWithRed:255/255.f green:190/255.f blue:33/255.f alpha:1.0].CGColor;
        _btnChangeStatus.layer.borderWidth = 1.f;
        _btnChangeStatus.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_btnChangeStatus addTarget:self action:@selector(changeStatusTouch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnChangeStatus;
}

-(UIButton *)timeChangeStatus{
    if(!_timeChangeStatus){
        _timeChangeStatus = [UIButton buttonWithType:UIButtonTypeCustom];
        [_timeChangeStatus setTitleColor: [UIColor colorWithRed:255/255.f green:190/255.f blue:33/255.f alpha:1.0] forState:UIControlStateNormal];
        _timeChangeStatus.layer.masksToBounds = YES;
        _timeChangeStatus.layer.cornerRadius = 5.f;
        _timeChangeStatus.layer.borderColor =  [UIColor colorWithRed:255/255.f green:190/255.f blue:33/255.f alpha:1.0].CGColor;
        _timeChangeStatus.layer.borderWidth = 1.f;
        _timeChangeStatus.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_timeChangeStatus addTarget:self action:@selector(changeStatusTouch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _timeChangeStatus;
    
}



-(UIView *)bottomView{
    if(!_bottomView){
        _bottomView = [[UIView alloc]init];
        CALayer* border = [[CALayer alloc]init];
        border.frame = CGRectMake(0, 79.f, SCREEN_WIDTH, 1.f);
        border.backgroundColor = [UIColor colorWithRed:236/255.f green:237/255.f blue:239/255.f alpha:1.0].CGColor;
        [_bottomView.layer addSublayer:border];
    }
    return _bottomView;
}

-(UILabel *)labelTodaySale{
    if(!_labelTodaySale){
        _labelTodaySale = [[UILabel alloc]init];
        _labelTodaySale.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelTodaySale.font = [UIFont systemFontOfSize:14.f];
        _labelTodaySale.numberOfLines = 0;
        _labelTodaySale.lineBreakMode = NSLineBreakByWordWrapping;
        CALayer* border = [[CALayer alloc]init];
        border.frame = CGRectMake(SCREEN_WIDTH/3-1, 20.f, 1.f, 40.f);
        border.backgroundColor = [UIColor colorWithRed:236/255.f green:237/255.f blue:239/255.f alpha:1.0].CGColor;
        [_labelTodaySale.layer addSublayer:border];
        
    }
    return _labelTodaySale;
}
-(UILabel *)labelTodayNum{
    if(!_labelTodayNum){
        _labelTodayNum = [[UILabel alloc]init];
        _labelTodayNum.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelTodayNum.font = [UIFont systemFontOfSize:14.f];
        _labelTodayNum.numberOfLines = 0;
        _labelTodayNum.lineBreakMode = NSLineBreakByWordWrapping;
        CALayer* border = [[CALayer alloc]init];
        border.frame = CGRectMake(SCREEN_WIDTH/3-1, 20.f, 1.f, 40.f);
        border.backgroundColor = [UIColor colorWithRed:236/255.f green:237/255.f blue:239/255.f alpha:1.0].CGColor;
        [_labelTodayNum.layer addSublayer:border];
        
    }
    return _labelTodayNum;
}

-(UILabel *)labelSumSale{
    if(!_labelSumSale){
        _labelSumSale = [[UILabel alloc]init];
        _labelSumSale.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _labelSumSale.font = [UIFont systemFontOfSize:14.f];
        _labelSumSale.numberOfLines = 0;
        _labelSumSale.lineBreakMode = NSLineBreakByWordWrapping;
    }
    return _labelSumSale;
}

-(void)changeStatusTouch:(UIButton *)sender{
    
    if (self.btnChangeStatusBlock) {
        self.btnChangeStatusBlock(sender.tag);
    }
    
}


@end
