//
//  StoreManageCollectionViewCell.m
//  rrbi
//
//  Created by mac book on 2018/12/3.
//

#import "StoreManageCollectionViewCell.h"


@interface StoreManageCollectionViewCell ()

@property(nonatomic,strong) UIImageView* photoImageView;
@property(nonatomic,strong) UILabel* titleLabel;


@end


@implementation StoreManageCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        [self setUpView];
    }
    return self;
}

- (void)setUpView{
    
    
    [self setBackgroundColor:[UIColor whiteColor]];
    
    self.photoImageView = [[UIImageView alloc]init];
    self.photoImageView.frame = CGRectMake((self.frame.size.width-60)/2, (self.frame.size.height-20-60)/2, 60, 60);
    [self addSubview:self.photoImageView];
    
    
    
    self.titleLabel = [[ UILabel alloc]init];
    self.titleLabel.frame = CGRectMake(0, 70, self.frame.size.width, 20);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.font = [UIFont systemFontOfSize:14.f];
    [self addSubview:self.titleLabel];
    
    
    
    //    [self.photoImageView setImage:[UIImage imageNamed:self.arrayImg[index]]];
    //    self.titleLabel.text = self.arrayTitle[index];

}


-(void)setDataWithImage:(NSString *)image title:(NSString *)title{
    
    [self.photoImageView setImage:[UIImage imageNamed:image]];
     self.titleLabel.text = title;
}


@end
