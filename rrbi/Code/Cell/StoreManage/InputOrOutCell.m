//
//  InputOrOutCell.m
//  RRQS
//
//  Created by kyjun on 16/4/22.
//
//

#import "InputOrOutCell.h"

@interface InputOrOutCell ()

@property(nonatomic,strong) UILabel* labelDate;
@property(nonatomic,strong) UILabel* labelCash;
@property(nonatomic,strong) UILabel* labelStatus;
@property(nonatomic,strong) UIImageView* line;

@end

@implementation InputOrOutCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self layoutUI];
//        [self layoutConstraints];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark =====================================================  user interface layout
-(void)layoutUI{
    [self.contentView addSubview:self.labelDate];
    [self.contentView addSubview:self.labelCash];
    [self.contentView addSubview:self.labelStatus];
    
    
    [self.labelDate mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.width.equalTo(self.contentView).multipliedBy(0.35);
        make.left.equalTo(self.contentView);
    }];
    
    [self.labelCash mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.width.equalTo(self.contentView).multipliedBy(0.25);
        make.left.equalTo(self.labelDate.mas_right);
    }];
    
    [self.labelStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.left.equalTo(self.labelCash.mas_right);
        make.right.equalTo(self.contentView).offset(-10);

    }];
        
    
//    [self.contentView addSubview:self.line];
}

-(void)layoutConstraints{
    self.labelDate.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelCash.translatesAutoresizingMaskIntoConstraints = NO;
    self.labelStatus.translatesAutoresizingMaskIntoConstraints = NO;
    self.line.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.labelDate addConstraint:[NSLayoutConstraint constraintWithItem:self.labelDate attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/3]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelDate attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelDate attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelDate attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.f]];
    
    [self.labelCash addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCash attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/4]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCash attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCash attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelCash attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.f]];
    
    [self.labelStatus addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStatus attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SCREEN_WIDTH/5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStatus attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStatus attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.labelStatus attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.f]];
    
//    [self.line addConstraint:[NSLayoutConstraint constraintWithItem:self.line attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.5f]];
//    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.line attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.f]];
//    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.line attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.f]];
//    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.line attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.f]];
}

#pragma mark =====================================================  property package
-(void)setEntity:(MIncome *)entity{
    if(entity){
        _entity = entity;
        self.labelDate.text =entity.createDate;
        self.labelCash.text = [NSString stringWithFormat:@"%.2f",[entity.money floatValue]];
        self.labelStatus.text = entity.mark;
    }
}

-(UILabel *)labelDate{
    if(!_labelDate){
        _labelDate = [[UILabel alloc]init];
        _labelDate.textColor = [UIColor colorWithRed:130/255.f green:130/255.f blue:130/255.f alpha:1.0];
        _labelDate.font = [UIFont systemFontOfSize:14.f];
        _labelDate.textAlignment = NSTextAlignmentCenter;
    }
    return _labelDate;
}

-(UILabel *)labelCash{
    if(!_labelCash){
        _labelCash = [[UILabel alloc]init];
        _labelCash.textColor = [UIColor redColor];
        _labelCash.font = [UIFont systemFontOfSize:14.f];
        _labelCash.textAlignment = NSTextAlignmentCenter;
    }
    return _labelCash;
}

-(UILabel *)labelStatus{
    if(!_labelStatus){
        _labelStatus = [[UILabel alloc]init];
        _labelStatus.textColor = [UIColor colorWithRed:130/255.f green:130/255.f blue:130/255.f alpha:1.0];
        _labelStatus.font = [UIFont systemFontOfSize:14.f];
        _labelStatus.textAlignment = NSTextAlignmentRight;
    }
    return _labelStatus;
}
-(UIImageView *)line{
    if(!_line){
        _line = [[UIImageView alloc]init];
        _line.backgroundColor = [UIColor colorWithRed:219/255.f green:219/255.f blue:219/255.f alpha:1.0];
    }
    return _line;
}


@end
