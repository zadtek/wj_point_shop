//
//  CommentListCell.h
//  RRBI
//
//  Created by kuyuZJ on 16/7/6.
//
//

#import <UIKit/UIKit.h>

@interface CommentListCell : UITableViewCell

@property(nonatomic,strong) MComment* entity;

@end
