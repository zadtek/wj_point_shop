//
//  ShopListTableViewCell.m
//  rrbi
//
//  Created by mac book on 2018/12/4.
//

#import "ShopListTableViewCell.h"


@interface ShopListTableViewCell ()


@property(nonatomic,strong) UILabel * titleLabel;
@property(nonatomic,strong) UIButton * deleteBtn;

@end



@implementation ShopListTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        [self createSubviews];
    }
    
    return self;
}

#pragma mark -
-(void)createSubviews{
    
    
    self.titleLabel = [[UILabel alloc]init];
//    self.titleLabel.textColor = [UIColor colorWithRed:148/255.f green:152/255.f blue:157/255.f alpha:1.0];
    self.titleLabel.font = kFontNameSize(15);
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.contentView addSubview:self.titleLabel];
    
    
    self.deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.deleteBtn.backgroundColor = getColor(greenBackColor);
    [self.deleteBtn setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    [self.deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    self.deleteBtn.layer.masksToBounds = YES;
    self.deleteBtn.layer.cornerRadius = 5.f;
    self.deleteBtn.titleLabel.font = kFontNameSize(14);
    [self.deleteBtn addTarget:self action:@selector(deleteBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.deleteBtn];

    
    
    
    [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).mas_offset(-10);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(60);
        
    }];
    
    
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.right.equalTo(self.deleteBtn.mas_left);
        make.left.equalTo(self.contentView).mas_offset(20);
    }];

    
    
    
    
}

-(void)setEntity:(MStore *)entity{
    
    _entity = entity;
    
    self.titleLabel.text = entity.cate_name;
    
    
}

-(void)deleteBtnDidClick{
    
    if (self.deleteBtnBlock) {
        self.deleteBtnBlock();
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
