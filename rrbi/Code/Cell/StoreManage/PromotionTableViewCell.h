//
//  PromotionTableViewCell.h
//  rrbi
//
//  Created by mac book on 2018/12/4.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PromotionTableViewCell : UITableViewCell

@property(nonatomic,strong) MStore* entity;

@property (copy, nonatomic) void(^promotionDeleteBtnBlock)(void);


@end

NS_ASSUME_NONNULL_END
