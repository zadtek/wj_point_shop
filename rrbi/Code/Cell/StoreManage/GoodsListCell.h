//
//  GoodsListCell.h
//  RRBI
//
//  Created by kuyuZJ on 16/7/5.
//
//

#import <UIKit/UIKit.h>

@protocol GoodsListCellDelegate <NSObject>

@optional

-(void)updateGoodsPrice:(MGoods*)sender;
-(void)publishGoods:(MGoods*)sender;
-(void)updateGoodsTitle:(MGoods*)sender;
-(void)updateGoodsImage:(MGoods*)sender;

@end


@interface GoodsListCell : UITableViewCell

@property(nonatomic,weak) id<GoodsListCellDelegate> delegate;

@property(nonatomic,strong) MGoods* entity;

@end
