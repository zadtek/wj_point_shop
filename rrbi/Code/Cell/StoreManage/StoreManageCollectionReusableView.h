//
//  StoreManageCollectionReusableView.h
//  rrbi
//
//  Created by mac book on 2018/12/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StoreManageCollectionReusableView : UICollectionReusableView

@property(nonatomic,strong) UILabel* labelStoreTime;

@property(nonatomic,strong) MStore* entity;
@property (copy, nonatomic) void(^btnChangeStatusBlock)(NSInteger tag);

@property (copy, nonatomic) void(^LogoImageBlock)(void);

@end

NS_ASSUME_NONNULL_END
