//
//  CommentNewListModel.m
//  rrbi
//
//  Created by MyMac on 2019/5/7.
//

#import "CommentNewListModel.h"

@implementation CommentNewListModel




- (CGFloat)cell_H {
    CGSize size = [self.shop_msg boundingRectWithSize:CGSizeMake(SCREEN_WIDTH-30, MAXFLOAT)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                                          context:nil].size;
    return 100+size.height;
}
@end
