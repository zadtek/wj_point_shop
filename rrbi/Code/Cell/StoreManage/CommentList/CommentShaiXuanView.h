//
//  CommentShaiXuanView.h
//  rrbi
//
//  Created by MyMac on 2019/5/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommentShaiXuanView : UIView
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) NSInteger subIndex;
@property (nonatomic, copy) void(^weiHuiFuButtonBlock)();
@property (nonatomic, copy) void(^TopShuaiXuanButtonBlock)(NSInteger index);
@property (nonatomic, copy) void(^BottomShuaiXuanButtonBlock)(NSInteger index);

@property (nonatomic, strong) NSDictionary *mainDict;

@end

NS_ASSUME_NONNULL_END
