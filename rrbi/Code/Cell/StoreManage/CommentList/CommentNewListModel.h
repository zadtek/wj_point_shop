//
//  CommentNewListModel.h
//  rrbi
//
//  Created by MyMac on 2019/5/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommentNewListModel : NSObject

@property (nonatomic, copy) NSString *ru_id;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *shop;
@property (nonatomic, copy) NSString *shop_msg;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *shop_comment;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *name;



- (CGFloat)cell_H;
@end

NS_ASSUME_NONNULL_END
