//
//  CommentNewListCell.m
//  rrbi
//
//  Created by MyMac on 2019/5/7.
//

#import "CommentNewListCell.h"

@implementation CommentNewListCell


- (void)updataDataWithModel:(CommentNewListModel *)model {
    [self.AimageView sd_setImageWithURL:[NSURL URLWithString:model.image]];
    self.titleLabel.text = model.name;
    self.timeLabel.text = model.time;
    
    self.commentLabel.text = model.shop_msg;
    
    if ([model.shop isEqualToString:@"0"]) {
        
    } else if ([model.shop isEqualToString:@"1"]) {
        self.yixingImage.image = [UIImage imageNamed:@"icon-star-enter"];
    } else if ([model.shop isEqualToString:@"2"]) {
        self.yixingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        self.erXingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        
    } else if ([model.shop isEqualToString:@"3"]) {
        self.yixingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        self.erXingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        self.sanXingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        
    }else if ([model.shop isEqualToString:@"4"]) {
        self.yixingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        self.erXingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        self.sanXingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        self.siXingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        
    }else if ([model.shop isEqualToString:@"5"]) {
        self.yixingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        self.erXingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        self.sanXingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        self.siXingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        self.wuXingImage.image = [UIImage imageNamed:@"icon-star-enter"];
        
    }
    
}

- (IBAction)faQuanButtonActon:(id)sender {
    if (self.FaQuanButtonBlock) {
        self.FaQuanButtonBlock();
    }
    
}
- (IBAction)fuifuButtonAction:(id)sender {
    
    if (self.HuiFuButtonBlock) {
        self.HuiFuButtonBlock();
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
