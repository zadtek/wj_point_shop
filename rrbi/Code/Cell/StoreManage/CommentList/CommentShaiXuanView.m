//
//  CommentShaiXuanView.m
//  rrbi
//
//  Created by MyMac on 2019/5/8.
//

#import "CommentShaiXuanView.h"

@implementation CommentShaiXuanView


- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        self.index = 0;
        self.subIndex = 0;
        [self createUI];
    }
    return self;
}


- (void)setMainDict:(NSDictionary *)mainDict {
    _mainDict = mainDict;
    [self createUI];
}
- (void)createUI {
    
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    CGFloat button_W = (self.bounds.size.width - 15*5)/4;
    CGFloat button_H = (button_W*56/138);//138x56
    
    for (NSInteger i=0; i<4; i++) {
        
        UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(15+i*(button_W+15), 10, button_W, button_H)];
        button.tag = i;
         button.titleLabel.font = [UIFont systemFontOfSize:13];
        [button addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
        if (i==self.index) {
            [button setBackgroundImage:[UIImage imageNamed:@"icon-comment-enter"] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        } else {
            [button setBackgroundImage:[UIImage imageNamed:@"icon-comment-default"] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        
        
        if (i==0) {
            [button setTitle:[NSString stringWithFormat:@"全部(%@)",[self.mainDict objectForKey:@"all_count"]] forState:UIControlStateNormal];
        } else if (i==1) {
            [button setTitle:[NSString stringWithFormat:@"好评(%@)",[self.mainDict objectForKey:@"good_count"]] forState:UIControlStateNormal];
        } else if (i==2) {
            [button setTitle:[NSString stringWithFormat:@"中评(%@)",[self.mainDict objectForKey:@"zhong_count"]] forState:UIControlStateNormal];
        } else {
            [button setTitle:[NSString stringWithFormat:@"差评(%@)",[self.mainDict objectForKey:@"bad_count"]] forState:UIControlStateNormal];
        }
        
    }
    
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 10+button_H+10, SCREEN_WIDTH, 0.5)];
    lineView.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:lineView];
    
    
    
    CGFloat subButton_W = (self.bounds.size.width - 15*2-30*3)/4;
    CGFloat subButton_H = (100-20-button_H-10-2);
    for (NSInteger i=0; i<4; i++) {
        
        UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(15+i*(subButton_W+30), 20+button_H+10, subButton_W, subButton_H)];
        button.tag = i;
        button.layer.cornerRadius = 3;
        button.layer.masksToBounds = YES;
        button.titleLabel.font = [UIFont systemFontOfSize:13];
        [button addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
        if (i == 3) {
            button.backgroundColor = [UIColor whiteColor];
            [button setImage:[UIImage imageNamed:@"downarr"] forState:UIControlStateNormal];
        } else {
            if (i==self.subIndex) {
                button.backgroundColor = [UIColor colorWithRed:249/255.0 green:232/255.0 blue:182/255.0 alpha:1];
                [button setTitleColor:[UIColor colorWithRed:223/255.0 green:190/255.0 blue:106/255.0 alpha:1] forState:UIControlStateNormal];
            } else {
                button.backgroundColor = [UIColor colorWithRed:234/255.0 green:234/255.0 blue:234/255.0 alpha:1];
                [button setTitleColor:[UIColor colorWithRed:106/255.0 green:106/255.0 blue:106/255.0 alpha:1] forState:UIControlStateNormal];
            }
        }
        if (i==0) {
            [button setTitle:[NSString stringWithFormat:@"全部(%@)",[self.mainDict objectForKey:@"all_comment_count"]] forState:UIControlStateNormal];
        } else if (i==1) {
            [button setTitle:[NSString stringWithFormat:@"有内容(%@)",[self.mainDict objectForKey:@"you_comment_count"]] forState:UIControlStateNormal];
        } else if (i==2) {
            [button setTitle:[NSString stringWithFormat:@"无内容(%@)",[self.mainDict objectForKey:@"wu_comment_count"]] forState:UIControlStateNormal];
        } else {
        
            [button setImage:[UIImage imageNamed:@"更多"] forState:UIControlStateNormal];
        }
    }
}

- (void)button1Action:(UIButton *)button {
    if (self.TopShuaiXuanButtonBlock) {
        self.TopShuaiXuanButtonBlock(button.tag+1);
    }
    self.index = button.tag;
    [self createUI];
    
}
- (void)button2Action:(UIButton *)button {
    
    if (button.tag == 3) {//图片点击
        if (self.weiHuiFuButtonBlock) {
            self.weiHuiFuButtonBlock();
        }
    } else {
        if (self.BottomShuaiXuanButtonBlock) {
            self.BottomShuaiXuanButtonBlock(button.tag+1);
        }
        self.subIndex = button.tag;
        [self createUI];
    }
   
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
