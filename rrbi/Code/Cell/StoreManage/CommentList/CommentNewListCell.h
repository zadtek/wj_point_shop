//
//  CommentNewListCell.h
//  rrbi
//
//  Created by MyMac on 2019/5/7.
//

#import <UIKit/UIKit.h>

#import "CommentNewListModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface CommentNewListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *AimageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UIButton *faquanButton;
@property (weak, nonatomic) IBOutlet UIButton *huifuButtton;
@property (weak, nonatomic) IBOutlet UIImageView *yixingImage;
@property (weak, nonatomic) IBOutlet UIImageView *erXingImage;
@property (weak, nonatomic) IBOutlet UIImageView *sanXingImage;
@property (weak, nonatomic) IBOutlet UIImageView *siXingImage;
@property (weak, nonatomic) IBOutlet UIImageView *wuXingImage;

- (void)updataDataWithModel:(CommentNewListModel *)model;

@property (nonatomic, copy) void(^FaQuanButtonBlock)();
@property (nonatomic, copy) void(^HuiFuButtonBlock)();

@end

NS_ASSUME_NONNULL_END
