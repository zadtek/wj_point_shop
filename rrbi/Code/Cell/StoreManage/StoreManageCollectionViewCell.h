//
//  StoreManageCollectionViewCell.h
//  rrbi
//
//  Created by mac book on 2018/12/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StoreManageCollectionViewCell : UICollectionViewCell

-(void)setDataWithImage:(NSString *)image title:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
