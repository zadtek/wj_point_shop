//
//  MMember.h
//  RRQS
//
//  Created by kyjun on 16/3/28.
//
//

#import <Foundation/Foundation.h>

@interface MMember : NSObject<NSCoding>


-(instancetype)initWithItem:(NSDictionary*)item;

@property(nonatomic,assign) BOOL isLogin;

/**
 *  店铺编号
 */
@property(nonatomic,strong) NSString* storeID;
/**
 *  商家编号
 */
@property(nonatomic,strong) NSString* userID;
/**
 *  店长姓名
 */
@property(nonatomic,strong) NSString* ownerName;
/**
 *  商家电话
 */
@property(nonatomic,strong) NSString* phone;
/**
 *  商家手机号
 */
@property(nonatomic,strong) NSString* mobile;
/**
 *  免费配送额度
 */
@property(nonatomic,strong) NSString* freeShip;
/**
 *  店铺名称
 */
@property(nonatomic,strong) NSString* storeName;
/**
 *  店铺公告店铺状态会用到
 */
@property(nonatomic,strong) NSString* statusRemark;
/**
 *  店铺地址
 */
@property(nonatomic,strong) NSString* storeAddress;
/**
 *  营业额度
 */
@property(nonatomic,strong) NSString* balance;
/**
 *  图标
 */
@property(nonatomic,strong) NSString* logo;
/**
 *  商家配送 0 / 1 人人配送
 */
@property(nonatomic,strong) NSString* send;
/**
 *  店铺销量
 */
@property(nonatomic,strong) NSString* shopSale;

/**
 *  店铺状态 (1 营业 /休息)
 */
@property(nonatomic,strong) NSString* status;

@property(nonatomic,strong) NSString* bankName;
/**
 *  银行卡号
 */
@property(nonatomic,strong) NSString* bankCard;
/**
 *  银行卡持有人
 */
@property(nonatomic,strong) NSString* cardPerson;


@property(nonatomic,strong) NSString* top_note;


@property(nonatomic,strong) NSString* bottom_note;

@property(nonatomic,strong) NSString* balanc_desc;

@property(nonatomic,strong) NSString* frozen_money_desc;

@property(nonatomic,strong) NSString* frozen_money;

@property(nonatomic,strong) NSString* confirm_order_way;//0手动接单；1自动接单

@property(nonatomic,strong) NSString* print_num;//打印联数

@end
