//
//  MStore.m
//  RRBI
//
//  Created by kyjun on 16/4/27.
//
//

#import "MStore.h"

@implementation MStore

-(instancetype)initWithStore:(NSDictionary *)item{
    self = [super init];
    if(self){
        self.storeID = [item objectForKey:@"site_id"];
        self.userID = [item objectForKey:@"uid"];
        self.ownerName = [item objectForKey:@"owner_name"];
        self.phone = [item objectForKey:@"phone"];
        self.mobile = [item objectForKey:@"mobile"];
        self.status = [item objectForKey:@"status"];
        self.freeShip = [item objectForKey:@"freeship_amount"];
        self.storeName = [item objectForKey:@"site_name"];
        self.statusRemark = [item objectForKey:@"status_remark"];
        self.notice = [item objectForKey:@"notice"];
        self.storeAddress = [item objectForKey:@"address"];
        self.balance = [item objectForKey:@"balance"];
        self.logo = [item objectForKey:@"logo"];
        self.send = [item objectForKey:@"send"];
        self.shopSale = [item objectForKey:@"shop_sale"];
        self.todayMoney = [item objectForKey:@"today"];
        
        self.open_time = [item objectForKey:@"open_time"];
        self.close_time = [item objectForKey:@"close_time"];
        self.tel = [item objectForKey:@"tel"];
        self.today_order_num = [NSString stringWithFormat:@"%@", [item objectForKey:@"today_order_num"]];


        
    }
    return self;
}
+(MStore *) InitStoreWithJsonData:(NSDictionary *) jsonData{
    MStore *model = [[MStore alloc]init];
    model.shop_range = [jsonData objectForKey:@"shop_range"];
    model.shop_dh = [jsonData objectForKey:@"shop_dh"];
    model.shop_remark = [jsonData objectForKey:@"shop_remark"];
    model.pf = [jsonData objectForKey:@"pf"];
    
    
    model.stroe_id = [jsonData objectForKey:@"id"];
    model.cate_name = [jsonData objectForKey:@"cate_name"];
    model.site_id = [jsonData objectForKey:@"site_id"];
    model.cat_id = [jsonData objectForKey:@"cat_id"];
    model.cat_name = [jsonData objectForKey:@"cat_name"];
    model.catestatus = [jsonData objectForKey:@"catestatus"];

    model.man = [jsonData objectForKey:@"man"];
    model.jian = [jsonData objectForKey:@"jian"];

    model.shop_address = [jsonData objectForKey:@"shop_address"];
    model.shop_name = [jsonData objectForKey:@"shop_name"];
    model.agent_code = [NSString stringWithFormat:@"%@", [jsonData objectForKey:@"agent_code"]];
    model.shop_tel = [NSString stringWithFormat:@"%@", [jsonData objectForKey:@"shop_tel"]];


    model.shop_owner = [jsonData objectForKey:@"shop_owner"];
    model.bank_name = [jsonData objectForKey:@"bank_name"];
    model.bank_host = [jsonData objectForKey:@"bank_host"];
    model.bank_num = [NSString stringWithFormat:@"%@", [jsonData objectForKey:@"bank_num"]];

    
    model.longitude = [NSString stringWithFormat:@"%@", [jsonData objectForKey:@"longitude"]];
    model.latitude = [NSString stringWithFormat:@"%@", [jsonData objectForKey:@"latitude"]];
    
    
    model.license = [jsonData objectForKey:@"license"];
    model.other_cert = [jsonData objectForKey:@"other_cert"];
    model.shop_back = [jsonData objectForKey:@"shop_back"];
    model.shop_logo = [jsonData objectForKey:@"shop_logo"];
    
    
    model.today_order_num = [NSString stringWithFormat:@"%@", [jsonData objectForKey:@"today_order_num"]];

    return model;
    
}

-(NSString *)todayMoney{
    if([WMHelper isNULLOrnil:_todayMoney]){
        return @"0.00";
    }
    return [NSString stringWithFormat:@"%.2f",[_todayMoney floatValue]];
}
@end
