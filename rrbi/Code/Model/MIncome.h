//
//  MIncome.h
//  RRBI
//
//  Created by kuyuZJ on 16/7/7.
//
//

#import <Foundation/Foundation.h>

/**
 *  收入明细
 */
@interface MIncome : NSObject

/**
 *  编号
 */
@property(nonatomic,copy) NSString* rowID;
/**
 *  店铺编号
 */
@property(nonatomic,copy) NSString* storeID;
/**
 *  余额
 */
@property(nonatomic,copy) NSString* balances;
/**
 *  交易金额
 */
@property(nonatomic,copy) NSString* money;
/**
 *  订单号
 */
@property(nonatomic,copy) NSString* orderID;
/**
 *  日期
 */
@property(nonatomic,copy) NSString* createDate;
/**
 *  备注
 */
@property(nonatomic,copy) NSString* mark;

@property(nonatomic,copy) NSString* h5_url;

@property(nonatomic,copy) NSString* page_title;



@end
