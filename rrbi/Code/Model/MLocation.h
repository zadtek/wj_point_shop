//
//  MLocation.h
//  RRQS
//
//  Created by kyjun on 16/3/29.
//
//

#import <Foundation/Foundation.h>

@interface MLocation : NSObject<NSCoding>
/**
 *  当前定位服务是否打开了
 */
@property(nonatomic,assign) BOOL enabled;
@property(nonatomic,strong) NSString* lat;
@property(nonatomic,strong) NSString* lng;

@end
