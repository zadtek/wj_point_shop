//
//  MCash.m
//  RRBI
//
//  Created by kuyuZJ on 16/7/7.
//
//

#import "MCash.h"

@implementation MCash

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if(key){
        if([key isEqualToString: @"item_id"]){
            _rowID = value;
        }else if([key isEqualToString: @"site_id"]){
            _storeID = value;
        }else if([key isEqualToString: @"uid"]){
            _userID = value;
        }else if([key isEqualToString: @"addtime"]){
            _createDate = value;
        }else if([key isEqualToString: @"add_year_month"]){
            _month = value;
        }else if([key isEqualToString: @"owner_name"]){
            _accountPeron = value;
        }else if([key isEqualToString: @"owner_bank"]){
            _account = value;
        }else if([key isEqualToString: @"bank_name"]){
            _bankName = value;
        }else if([key isEqualToString: @"notice"]){
            _mark = value;
        }
    }
}

@end
