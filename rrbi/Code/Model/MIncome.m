//
//  MIncome.m
//  RRBI
//
//  Created by kuyuZJ on 16/7/7.
//
//

#import "MIncome.h"

@implementation MIncome

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if(key){
        if([key isEqualToString: @"item_id"]){
            _rowID =value;
        }else if ([key isEqualToString: @"site_id"]){
            _storeID = value;
        }else if ([key isEqualToString: @"money_before"]){
            _balances = value;
        }else if ([key isEqualToString: @"amount"]){
            _money = value;
        }else if ([key isEqualToString: @"order_id"]){
            _orderID = value;
        }else if ([key isEqualToString: @"addtime"]){
            _createDate = value;
        }
    }
}

@end
