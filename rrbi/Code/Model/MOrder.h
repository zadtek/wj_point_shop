//
//  MOrder.h
//  RRBI
//
//  Created by kyjun on 16/4/27.
//
//

#import <Foundation/Foundation.h>

@interface MOrder : NSObject

-(instancetype)initWithItem:(NSDictionary*)item;
/**
 *  订单编号
 */
@property(nonatomic,strong) NSString* orderID;
/**
 *  收货人
 */
@property(nonatomic,strong) NSString* userName;
/**
 *  收货人电话
 */
@property(nonatomic,strong) NSString* phone;
/**
 *  收货人备注
 */
@property(nonatomic,strong) NSString* orderMark;
/**
 *  下单时间
 */
@property(nonatomic,strong) NSString* createDate;
/**
 *  支付方式
 */
@property(nonatomic,strong) NSString* payType;
/**
 *  客服备注
 */
@property(nonatomic,strong) NSString* remark;
/**
 *  总金额（目前包含打包费）
 */
@property(nonatomic,strong) NSString* sumPrice;
/**
 *  配送费
 */
@property(nonatomic,strong) NSString* shipFee;
/**
 *  打包费
 */
@property(nonatomic,copy) NSString* packageFee;
/**
 *  满减商户承担金额
 */
@property(nonatomic,copy) NSString* storeDiscount;
/**
 *  状态
 */
@property(nonatomic,assign) EnumOrderStatus status;
/**
 *  配送员编号
 */
@property(nonatomic,strong) NSString* emptyID;
/**
 *  收货地址
 */
@property(nonatomic,strong) NSString* address;
/**
 *  是否展开默认为 NO (收缩)
 */
@property(nonatomic,assign) BOOL isExpansion;

@property(nonatomic,strong) NSMutableArray* arrayGoods;
/**
 *  是否有快递员配送
 */
@property(nonatomic,assign) BOOL isDelivery;
/**
 *  快递员姓名
 */
@property(nonatomic,copy) NSString* emptyName;
/**
 *  快递员电话
 */
@property(nonatomic,copy) NSString* emptyPhone;
/**
 *  快递员接单数
 */
@property(nonatomic,copy) NSString* emptyOrders;


@property(nonatomic,copy) NSString* order_sn;

@property(nonatomic,copy) NSString* benifit;


@property(nonatomic,strong) NSMutableArray* delivery;

@property(nonatomic,copy) NSString* pay_name;

@end
