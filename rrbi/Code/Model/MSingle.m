//
//  MSingle.m
//  RRQS
//
//  Created by kyjun on 16/3/23.
//
//

#import "MSingle.h"

@implementation MSingle
+(instancetype)shareAuhtorization{
    static dispatch_once_t onceToken;
    static MSingle* shareAuhtor;
    dispatch_once(&onceToken, ^{
        shareAuhtor = [[self alloc] init];
    });
    return shareAuhtor;
}
-(instancetype)init{
    
    if (self = [super init])
    {
    }
    return self;
}

-(MLocation*)locationInfo{
    return [NSKeyedUnarchiver unarchiveObjectWithFile:[WMHelper archiverLocationCirclePath]];
}

-(MMember*)userInfo{
    return [NSKeyedUnarchiver unarchiveObjectWithFile:[WMHelper archiverPath]];
}

@end
