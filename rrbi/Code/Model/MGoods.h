//
//  MGoods.h
//  RRBI
//
//  Created by kyjun on 16/4/27.
//
//

#import <Foundation/Foundation.h>

@interface MGoods : NSObject

-(instancetype)initWithItem:(NSDictionary*)item;
/**
 *  商品编号
 */
@property(nonatomic,copy) NSString* rowID;
/**
 *  商品名称
 */
@property(nonatomic,strong) NSString* goodsName;
/**
 *  商品数量
 */
@property(nonatomic,strong) NSString* quantity;
/**
 *  商品单价(销售价)
 */
@property(nonatomic,strong) NSString* price;
/**
 *  商品总价
 */
@property(nonatomic,strong) NSString* sumPrice;

/**
 *  商品状态 0：上架 1：下架
 */
@property(nonatomic,copy) NSString* status;
/**
 *  店铺编号
 */
@property(nonatomic,copy) NSString* storeID;
/**
 *  库存
 */
@property(nonatomic,copy) NSString* stock;
/**
 *  市场价
 */
@property(nonatomic,copy) NSString* marketPrice;
/**
 *  结算价
 */
@property(nonatomic,copy) NSString* checkPrice;
/**
 *  创建日期
 */
@property(nonatomic,copy) NSString* createDate;
/**
 *  商品图片
 */
@property(nonatomic,copy) NSString* thumbnail;

@property(nonatomic,copy) NSString* cate_name;


@property(nonatomic,copy) NSString* jinhuo_price;
@property(nonatomic,copy) NSString* sales_num;
@property(nonatomic,copy) NSString* members_price;
@property(nonatomic,copy) NSString* fid;
@property(nonatomic,copy) NSString* goods_number;
@property(nonatomic,copy) NSString* goods_sn;

@property(nonatomic,copy) NSString* user_cat;
@property(nonatomic,copy) NSString* market_price;
@property(nonatomic,copy) NSString* default_image;
@property(nonatomic,copy) NSString* check_price;
@property(nonatomic,copy) NSString* shopcate;
@property(nonatomic,copy) NSString* sid;
@property(nonatomic,copy) NSString* name;


@end
