//
//  MOrder.m
//  RRBI
//
//  Created by kyjun on 16/4/27.
//
//

#import "MOrder.h"
#import "MGoods.h"

@implementation MOrder

-(instancetype)initWithItem:(NSDictionary *)item{
    self = [super init];
    if(self){
        self.orderID = [item objectForKey:@"order_id"];
        self.userName = [item objectForKey:@"uname"];
        self.phone = [item objectForKey:@"phone"];
        self.orderMark = [item objectForKey:@"order_mark"];
        self.createDate = [item objectForKey:@"add_time"];
        self.payType = [item objectForKey:@"paytype"];
        self.remark = [item objectForKey:@"remark"];
        self.shipFee = [item objectForKey:@"ship_fee"];
        self.packageFee = [item objectForKey:@"packing_fee"];
        self.sumPrice =[item objectForKey:@"total_amount"];
        self.status = [[item objectForKey:@"status"]integerValue];
        self.address = [item objectForKey:@"address1"];
        self.emptyID = [item objectForKey:@"emp_id"];
        self.storeDiscount = [item objectForKey: @"discount"];
        self.order_sn = [item objectForKey: @"order_sn"];
        self.benifit = [NSString stringWithFormat:@"%.2f",[[item objectForKey: @"benifit"] doubleValue]];
        self.pay_name = [item objectForKey: @"pay_name"];

        
        self.emptyName = [item objectForKey: @"realname"];
        self.emptyPhone = [item objectForKey: @"phone"];
        self.emptyOrders = [item objectForKey: @"count"];
        
        
        NSArray* array = [item objectForKey:@"items"];
        if(![WMHelper isNULLOrnil:array]){
            [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                MGoods* sub = [[MGoods alloc]initWithItem:obj];
                [self.arrayGoods addObject:sub];
            }];
        }

        self.delivery = [NSMutableArray array];
        NSArray* deliveryArray = [item objectForKey:@"delivery"];
        if(![WMHelper isNULLOrnil:deliveryArray]){
            [deliveryArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                MOrder* sub = [[MOrder alloc]initWithItem:obj];
                [self.delivery addObject:sub];
            }];
        }
        self.isDelivery = NO;
        NSDictionary* delivery = [item objectForKey: @"delivery"];
        if(![WMHelper isNULLOrnil:delivery] && [delivery isKindOfClass:[NSDictionary class]]){
            self.emptyName = [delivery objectForKey: @"realname"];
            self.emptyPhone = [delivery objectForKey: @"phone"];
            self.emptyOrders = [delivery objectForKey: @"count"];
            if(![WMHelper isEmptyOrNULLOrnil:self.emptyName]){
                self.isDelivery = YES;
            }
        }
    }
    
    
    
    return self;
}

-(NSMutableArray *)arrayGoods{
    if(!_arrayGoods){
        _arrayGoods = [[NSMutableArray alloc]init];
    }
    return _arrayGoods;
}

//-(NSString *)payType{
//    if([_payType isEqualToString:@"0"]){
//        return @"现金支付";
//    }else if ([_payType isEqualToString:@"1"]){
//        return @"支付宝支付";
//    }
//    else if ([_payType isEqualToString:@"2"]){
//        return @"微信支付";
//    }
//    else if ([_payType isEqualToString:@"3"]){
//        return @"手机支付宝";
//    }
//    return @"其它支付";
//}
@end
