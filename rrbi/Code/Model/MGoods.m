//
//  MGoods.m
//  RRBI
//
//  Created by kyjun on 16/4/27.
//
//

#import "MGoods.h"

@implementation MGoods


-(instancetype)initWithItem:(NSDictionary *)item{
    self = [super init];
    if(self){
        self.goodsName = [item objectForKey:@"fname"];
        self.quantity = [item objectForKey:@"quantity"];
        self.price = [item objectForKey:@"price"];
        self.sumPrice = [item objectForKey:@"check_price"];
    }
    return self;
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if(key){
        if([key isEqualToString:@"fid"]){
            _rowID = value;
        }else if ([key isEqualToString:@"sid"]){
            _storeID = value;
        }else if ([key isEqualToString:@"stock"]){
            _stock = value;
        }else if ([key isEqualToString:@"market_price"]){
            _marketPrice = value;
        }else if ([key isEqualToString:@"check_price"]){
            _checkPrice = value;
        }else if ([key isEqualToString:@"add_time"]){
            _createDate = value;
        }else if ([key isEqualToString:@"name"]){
            _goodsName = value;
        }else if ([key isEqualToString: @"default_image"]){
            _thumbnail = value;
        }
    }
}

@end
