//
//  MMember.m
//  RRQS
//
//  Created by kyjun on 16/3/28.
//
//

#import "MMember.h"

@implementation MMember


-(instancetype)initWithItem:(NSDictionary *)item{
    self = [super init];
    if(self){
        self.storeID = [item objectForKey:@"site_id"];
        self.userID = [item objectForKey:@"uid"];
        self.ownerName = [item objectForKey:@"owner_name"];
        self.phone = [item objectForKey:@"phone"];
        self.mobile = [item objectForKey:@"mobile"];
        self.status = [item objectForKey:@"status"];
        self.freeShip = [item objectForKey:@"freeship_amount"];
        self.storeName = [item objectForKey:@"site_name"];
        self.statusRemark = [item objectForKey:@"status_remark"];
        self.storeAddress = [item objectForKey:@"address"];
        self.balance = [item objectForKey:@"balance"];        
        self.logo = [item objectForKey:@"logo"];
        self.send = [item objectForKey:@"send"];
        self.shopSale = [item objectForKey:@"shop_sale"];
        self.top_note = [item objectForKey:@"top_note"];
        self.bottom_note = [item objectForKey:@"bottom_note"];

        self.balanc_desc = [item objectForKey:@"balanc_desc"];
        self.frozen_money_desc = [item objectForKey:@"frozen_money_desc"];
        self.frozen_money = [item objectForKey:@"frozen_money"];
        
        self.print_num = [item objectForKey:@"print_num"];
        self.confirm_order_way = [item objectForKey:@"confirm_order_way"];

    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.storeID = [aDecoder decodeObjectForKey:@"storeID"];
        self.userID = [aDecoder decodeObjectForKey:@"userID"];
        self.ownerName = [aDecoder decodeObjectForKey:@"ownerName"];
        self.phone = [aDecoder decodeObjectForKey:@"phone"];
        self.mobile = [aDecoder decodeObjectForKey:@"mobile"];
        self.status = [aDecoder decodeObjectForKey:@"status"];
        self.freeShip = [aDecoder decodeObjectForKey:@"freeShip"];
        self.storeName = [aDecoder decodeObjectForKey:@"storeName"];
        self.statusRemark = [aDecoder decodeObjectForKey:@"statusRemark"];
        self.storeAddress = [aDecoder decodeObjectForKey:@"storeAddress"];
        self.balance = [aDecoder decodeObjectForKey:@"balance"];
        self.bankCard = [aDecoder decodeObjectForKey:@"bankCard"];
        self.cardPerson = [aDecoder decodeObjectForKey:@"cardPerson"];
        self.logo = [aDecoder decodeObjectForKey:@"logo"];
        self.send = [aDecoder decodeObjectForKey:@"send"];
        self.shopSale = [aDecoder decodeObjectForKey:@"shopSale"];
        self.isLogin = [[aDecoder decodeObjectForKey:@"isLogin"] boolValue];
        
        self.top_note = [aDecoder decodeObjectForKey:@"top_note"];
        self.bottom_note = [aDecoder decodeObjectForKey:@"bottom_note"];
        
        self.balanc_desc = [aDecoder decodeObjectForKey:@"balanc_desc"];
        self.frozen_money_desc = [aDecoder decodeObjectForKey:@"frozen_money_desc"];
        self.frozen_money = [aDecoder decodeObjectForKey:@"frozen_money"];
        
        self.print_num = [aDecoder decodeObjectForKey:@"print_num"];
        self.confirm_order_way = [aDecoder decodeObjectForKey:@"confirm_order_way"];

        return self;
    }else{
        return nil;
    }
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.storeID forKey:@"storeID"];
    [aCoder encodeObject:self.userID forKey:@"userID"];
    [aCoder encodeObject:self.ownerName forKey:@"ownerName"];
    [aCoder encodeObject:self.phone forKey:@"phone"];
    [aCoder encodeObject:self.mobile forKey:@"mobile"];
    [aCoder encodeObject:self.status forKey:@"status"];
    [aCoder encodeObject:self.freeShip forKey:@"freeShip"];
    [aCoder encodeObject:self.storeName forKey:@"storeName"];
    [aCoder encodeObject:self.statusRemark forKey:@"statusRemark"];
    [aCoder encodeObject:self.storeAddress forKey:@"storeAddress"];
    [aCoder encodeObject:self.balance forKey:@"balance"];
    [aCoder encodeObject:self.bankCard forKey:@"bankCard"];
    [aCoder encodeObject:self.cardPerson forKey:@"cardPerson"];
    [aCoder encodeObject:self.logo forKey:@"logo"];
    [aCoder encodeObject:self.send forKey:@"send"];
    [aCoder encodeObject:self.shopSale forKey:@"shopSale"];
    [aCoder encodeObject:[NSNumber numberWithBool:self.isLogin] forKey:@"isLogin"];
    
    [aCoder encodeObject:self.top_note forKey:@"top_note"];
    [aCoder encodeObject:self.bottom_note forKey:@"bottom_note"];
    
    [aCoder encodeObject:self.balanc_desc forKey:@"balanc_desc"];
    [aCoder encodeObject:self.frozen_money_desc forKey:@"frozen_money_desc"];
    [aCoder encodeObject:self.frozen_money forKey:@"frozen_money"];
    
    [aCoder encodeObject:self.confirm_order_way forKey:@"confirm_order_way"];
    [aCoder encodeObject:self.print_num forKey:@"print_num"];


}

-(NSString *)logo{
    if([WMHelper isNULLOrnil:_logo]){
        _logo = @"";
    }
    return _logo;
}

@end
