//
//  MComment.m
//  RRBI
//
//  Created by kuyuZJ on 16/7/6.
//
//

#import "MComment.h"

@implementation MComment

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if(key){
        if([key isEqualToString:@"itemid"]){
            _rowID = value;
        }else if ([key isEqualToString:@"order_id"]){
            _orderID = value;
        }else if ([key isEqualToString:@"sid"]){
            _storeID = value;
        }else if ([key isEqualToString:@"addtime"]){
            _createDate = value;
        }else if ([key isEqualToString:@"comment"]){
            _comment = value;
        }else if ([key isEqualToString:@"name"]){
            _userName = value;
        }else if ([key isEqualToString:@"score"]){
            _scoreToal = value;
        }else if ([key isEqualToString:@"score1"]){
            _scoreFood = value;
        }else if ([key isEqualToString:@"score2"]){
            _scoreExpress = value;
        }else if ([key isEqualToString: @"img_url"]){
            _avatar = value;
        }
    }
}

-(NSString *)avatar{
    if([WMHelper isNULLOrnil:_avatar]){
        return  @"";
    }
    return _avatar;
}

@end
