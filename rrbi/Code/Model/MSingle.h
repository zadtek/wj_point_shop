//
//  MSingle.h
//  RRQS
//
//  Created by kyjun on 16/3/23.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "MMember.h"
#import "MLocation.h"

@interface MSingle : NSObject

+(instancetype)shareAuhtorization;

@property(nonatomic,strong,readonly) MLocation* locationInfo;

@property(nonatomic,strong,readonly) MMember* userInfo;



@end
