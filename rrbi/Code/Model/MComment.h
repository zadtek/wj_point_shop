//
//  MComment.h
//  RRBI
//
//  Created by kuyuZJ on 16/7/6.
//
//

#import <Foundation/Foundation.h>

@interface MComment : NSObject


/**
 *  唯一标示
 */
@property(nonatomic,strong) NSString* rowID;
/**
 *  订单编号
 */
@property(nonatomic,strong) NSString* orderID;
/**
 *  评论时间
 */
@property(nonatomic,strong) NSString* createDate;
/**
 *  店铺编号
 */
@property(nonatomic,strong) NSString* storeID;
/**
 *  评论内容
 */
@property(nonatomic,strong) NSString* comment;
/**
 *  用户名称
 */
@property(nonatomic,strong) NSString* userName;
@property(nonatomic,copy) NSString* avatar;


/**
 *  综合评价
 */
@property(nonatomic,strong) NSString* scoreToal;
/**
 *  口味评论
 */
@property(nonatomic,strong) NSString* scoreFood;
/**
 *  快递评论
 */
@property(nonatomic,strong) NSString* scoreExpress;


@end
