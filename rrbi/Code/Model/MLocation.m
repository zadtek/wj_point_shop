//
//  MLocation.m
//  RRQS
//
//  Created by kyjun on 16/3/29.
//
//

#import "MLocation.h"

@implementation MLocation

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.lat = [aDecoder decodeObjectForKey:@"lat"];
        self.lng = [aDecoder decodeObjectForKey:@"lng"];
        self.enabled = [[aDecoder decodeObjectForKey:@"enabled"] boolValue];
        return self;
    }else{
        return nil;
    }
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.lat forKey:@"lat"];
    [aCoder encodeObject:self.lng forKey:@"lng"];
    [aCoder encodeObject:[NSNumber numberWithBool:self.enabled] forKey:@"enabled"];
}

-(NSString *)lat{
    if([WMHelper isNULLOrnil:_lat]){
        return @"";
    }
    return _lat;
}

-(NSString *)lng{
    if ([WMHelper isNULLOrnil:_lng]) {
        return @"";
    }
    return _lng;
}

@end
