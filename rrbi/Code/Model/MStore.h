//
//  MStore.h
//  RRBI
//
//  Created by kyjun on 16/4/27.
//
//

#import <Foundation/Foundation.h>
#import "MMember.h"

@interface MStore : MMember

-(instancetype)initWithStore:(NSDictionary*)item;
@property(nonatomic,strong) NSString* todayMoney;
@property(nonatomic,strong) NSString* notice;


@property(nonatomic,strong) NSString* close_time;
@property(nonatomic,strong) NSString* open_time;
@property(nonatomic,strong) NSString* tel;

@property(nonatomic,strong) NSString* stroe_id;
@property(nonatomic,strong) NSString* cate_name;
@property(nonatomic,strong) NSString* site_id;
@property(nonatomic,strong) NSString* catestatus;
@property(nonatomic,strong) NSString* cat_id;
@property(nonatomic,strong) NSString* cat_name;

@property(nonatomic,strong) NSString* man;
@property(nonatomic,strong) NSString* jian;



@property(nonatomic,strong) NSString* shop_address;
@property(nonatomic,strong) NSString* shop_name;
@property(nonatomic,strong) NSString* agent_code;
@property(nonatomic,strong) NSString* shop_tel;


@property(nonatomic,strong) NSString* shop_owner;
@property(nonatomic,strong) NSString* bank_name;
@property(nonatomic,strong) NSString* bank_host;
@property(nonatomic,strong) NSString* bank_num;


@property(nonatomic,strong) NSString* longitude;
@property(nonatomic,strong) NSString* latitude;


@property(nonatomic,strong) NSString* license;
@property(nonatomic,strong) NSString* other_cert;
@property(nonatomic,strong) NSString* shop_back;
@property(nonatomic,strong) NSString* shop_logo;
@property(nonatomic,strong) NSString* today_order_num;
@property (nonatomic, copy) NSString *shop_range;
@property (nonatomic, copy) NSString *shop_dh;
@property (nonatomic, copy) NSString *shop_remark;
@property (nonatomic, copy) NSString *pf;


+(MStore *) InitStoreWithJsonData:(NSDictionary *) jsonData;

@end
