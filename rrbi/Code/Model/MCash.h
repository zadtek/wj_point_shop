//
//  MCash.h
//  RRBI
//
//  Created by kuyuZJ on 16/7/7.
//
//

#import <Foundation/Foundation.h>


/**
 *  提现明细
 */
@interface MCash : NSObject
/**
 *  编号
 */
@property(nonatomic,copy) NSString* rowID;
/**
 *  提款店铺编号
 */
@property(nonatomic,copy) NSString* storeID;
/**
 *  提款用户编号
 */
@property(nonatomic,copy) NSString* userID;
/**
 *  提款日期
 */
@property(nonatomic,copy) NSString* createDate;
/**
 *  提款月份
 */
@property(nonatomic,copy) NSString* month;
/**
 *  提款金额
 */
@property(nonatomic,copy) NSString* money;
/**
 *  提款状态 "0申请中1已打款"
 */
@property(nonatomic,copy) NSString* status;
/**
 *  账号-开通人
 */
@property(nonatomic,copy) NSString* accountPeron;
/**
 *  账号
 */
@property(nonatomic,copy) NSString* account;
/**
 *  所属银行
 */
@property(nonatomic,copy) NSString* bankName;
/**
 *  备注
 */
@property(nonatomic,copy) NSString* mark;


@property(nonatomic,copy) NSString* h5_url;

@property(nonatomic,copy) NSString* page_title;




@end
