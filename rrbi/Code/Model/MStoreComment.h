//
//  MStoreComment.h
//  RRBI
//
//  Created by kuyuZJ on 16/7/6.
//
//

#import <Foundation/Foundation.h>

@interface MStoreComment : NSObject

#pragma mark =====================================================  总体评分
@property(nonatomic,copy) NSString* totalComment;
@property(nonatomic,copy) NSString* foodComment;
@property(nonatomic,copy) NSString* shipComment;

#pragma mark =====================================================  评分等级
@property(nonatomic,copy) NSString* totalNum;
@property(nonatomic,copy) NSString* badNum;
@property(nonatomic,copy) NSString* goodNum;
@property(nonatomic,copy) NSString* bestNum;

@end
