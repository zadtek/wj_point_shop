//
//  RepurchaseDataTableViewCell.m
//  rrbi
//
//  Created by 徐浩 on 2019/5/5.
//

#import "RepurchaseDataTableViewCell.h"

@interface RepurchaseDataTableViewCell()
//复购人数
@property (strong, nonatomic) UILabel *namelabel;
//复购人数
@property (strong, nonatomic) UILabel *peopleNumLabel;
//复购人数
@property (strong, nonatomic) UILabel *peopleNumLabel1;
//复购新增人数
@property (strong, nonatomic) UILabel *peopleNumNewLabel;
//复购新增人数
@property (strong, nonatomic) UILabel *peopleNumNewLabel1;
//复购新增人数
@property (strong, nonatomic) UILabel *peopleNumOldLabel;
//复购新增人数
@property (strong, nonatomic) UILabel *peopleNumOldLabel1;
@end

@implementation RepurchaseDataTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.contentView.backgroundColor = COLORRGB(0xececec);
        
        UIView *topView = [[UIView alloc]init];
        [self.contentView addSubview:topView];
        [topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(self.contentView);
            make.height.mas_equalTo(36);
        }];
        
        UILabel *shuLabel = [[UILabel alloc]init];
        [topView addSubview:shuLabel];
        [shuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(topView);
            make.left.equalTo(topView.mas_left).offset(12);
            make.size.mas_equalTo(CGSizeMake(4, 14));
        }];
        shuLabel.backgroundColor = COLORRGB(0x666666);
        
        self.namelabel = [[UILabel alloc]init];
        [topView addSubview:self.namelabel];
        [self.namelabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(shuLabel.mas_right).offset(6);
            make.centerY.equalTo(topView);
        }];
        self.namelabel.textAlignment = 0;
        self.namelabel.textColor = COLORRGB(0x666666);
        self.namelabel.font= kFontNameSize(14);
        
        UIView *bottomView = [[UIView alloc]init];
        [self.contentView addSubview:bottomView];
        [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.contentView);
            make.top.equalTo(topView.mas_bottom);
            make.height.mas_equalTo(73);
        }];
        bottomView.backgroundColor = COLORRGB(0xffffff);
        
        self.peopleNumLabel = [[UILabel alloc]init];
        [bottomView addSubview:self.peopleNumLabel];
        [self.peopleNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(bottomView);
            make.top.equalTo(bottomView.mas_top).offset(22);
            make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
        }];
        self.peopleNumLabel.textColor = [UIColor blackColor];
        self.peopleNumLabel.font = kFontNameSize(14);
        self.peopleNumLabel.textAlignment = 1;
        
        self.peopleNumLabel1 = [[UILabel alloc]init];
        [bottomView addSubview:self.peopleNumLabel1];
        [self.peopleNumLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(bottomView);
            make.top.equalTo(self.peopleNumLabel.mas_bottom).offset(6);
            make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
        }];
        self.peopleNumLabel1.textColor = COLORRGB(0x999999);
        self.peopleNumLabel1.font = kFontNameSize(12);
        self.peopleNumLabel1.textAlignment = 1;
        
        UILabel *shuLabel1 = [[UILabel alloc]init];
        [bottomView addSubview:shuLabel1];
        [shuLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.peopleNumLabel.mas_top);
            make.bottom.equalTo(self.peopleNumLabel1.mas_bottom);
            make.left.equalTo(self.peopleNumLabel.mas_right);
            make.width.mas_equalTo(1);
        }];
        shuLabel1.backgroundColor = COLORRGB(0x999999);
        
        self.peopleNumNewLabel = [[UILabel alloc]init];
        [bottomView addSubview:self.peopleNumNewLabel];
        [self.peopleNumNewLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(shuLabel1.mas_right);
            make.top.equalTo(bottomView.mas_top).offset(22);
            make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
        }];
        self.peopleNumNewLabel.textColor = [UIColor blackColor];
        self.peopleNumNewLabel.font = kFontNameSize(14);
        self.peopleNumNewLabel.textAlignment = 1;
        
        self.peopleNumNewLabel1 = [[UILabel alloc]init];
        [bottomView addSubview:self.peopleNumNewLabel1];
        [self.peopleNumNewLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(shuLabel1.mas_right);
            make.top.equalTo(self.peopleNumNewLabel.mas_bottom).offset(6);
            make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
        }];
        self.peopleNumNewLabel1.textColor = COLORRGB(0x999999);
        self.peopleNumNewLabel1.font = kFontNameSize(12);
        self.peopleNumNewLabel1.textAlignment = 1;
        
        UILabel *shuLabel2 = [[UILabel alloc]init];
        [bottomView addSubview:shuLabel2];
        [shuLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.peopleNumLabel.mas_top);
            make.bottom.equalTo(self.peopleNumLabel1.mas_bottom);
            make.left.equalTo(self.peopleNumNewLabel1.mas_right);
            make.width.mas_equalTo(1);
        }];
        shuLabel2.backgroundColor = COLORRGB(0x999999);
        
        self.peopleNumOldLabel = [[UILabel alloc]init];
        [bottomView addSubview:self.peopleNumOldLabel];
        [self.peopleNumOldLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(shuLabel2.mas_right);
            make.top.equalTo(bottomView.mas_top).offset(22);
            make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
        }];
        self.peopleNumOldLabel.textColor = [UIColor blackColor];
        self.peopleNumOldLabel.font = kFontNameSize(14);
        self.peopleNumOldLabel.textAlignment = 1;
        
        self.peopleNumOldLabel1= [[UILabel alloc]init];
        [bottomView addSubview:self.peopleNumOldLabel1];
        [self.peopleNumOldLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(shuLabel2.mas_right);
            make.top.equalTo(self.peopleNumOldLabel.mas_bottom).offset(6);
            make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
        }];
        self.peopleNumOldLabel1.textColor = COLORRGB(0x999999);
        self.peopleNumOldLabel1.font = kFontNameSize(12);
        self.peopleNumOldLabel1.textAlignment = 1;
        
    }
    return self;
}

-(void) reloadUIWithName:(NSString *)name peopleNum:(NSString *)peopleNum peopleNum1:(NSString *)peopleNum1 peopleNumNew:(NSString *)peopleNumNew peopleNumNew1:(NSString *)peopleNumNew1 peopleNumOld:(NSString *)peopleNumOld peopleNumOld1:(NSString *)peopleNumOld1
{
    self.namelabel.text = name;
    self.peopleNumLabel.text = peopleNum;
    self.peopleNumLabel1.text = peopleNum1;
    self.peopleNumNewLabel.text = peopleNumNew;
    self.peopleNumNewLabel1.text = peopleNumNew1;
    self.peopleNumOldLabel.text = peopleNumOld;
    self.peopleNumOldLabel1.text = peopleNumOld1;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
