//
//  StoreOperationMainTableViewCell.h
//  rrbi
//
//  Created by 徐浩 on 2019/5/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StoreOperationMainTableViewCell : UITableViewCell

-(void) reloadUIWithImageName:(NSString *)imageName name:(NSString *)name isShowHeng:(BOOL)isShow;

@end

NS_ASSUME_NONNULL_END
