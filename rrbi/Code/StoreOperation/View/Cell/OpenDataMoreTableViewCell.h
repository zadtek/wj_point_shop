//
//  OpenDataMoreTableViewCell.h
//  rrbi
//
//  Created by MyMac on 2019/5/10.
//

#import <UIKit/UIKit.h>
#import "FinancaialOrderModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface OpenDataMoreTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *shijianLabel;
@property (weak, nonatomic) IBOutlet UIButton *yiwanChengButton;
@property (weak, nonatomic) IBOutlet UILabel *diandanBianHaoLabel;
@property (weak, nonatomic) IBOutlet UILabel *liushuiLabel;
@property (weak, nonatomic) IBOutlet UILabel *shangpinPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *baoZhuangFeiLabel;
@property (weak, nonatomic) IBOutlet UILabel *peisongFeiLabel;
@property (weak, nonatomic) IBOutlet UILabel *yujiShouRuLabel;
@property (weak, nonatomic) IBOutlet UILabel *pingtaiBuTieLabel;
@property (weak, nonatomic) IBOutlet UILabel *shangjiaBULabel;
@property (weak, nonatomic) IBOutlet UILabel *dingdanShiChangLabel;
@property (weak, nonatomic) IBOutlet UILabel *wanChengTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *huodongTypeLabel;

- (void)updataWithModel:(FinancaialOrderModel *)model;

@end

NS_ASSUME_NONNULL_END
