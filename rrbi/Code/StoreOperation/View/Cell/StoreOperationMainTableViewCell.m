//
//  StoreOperationMainTableViewCell.m
//  rrbi
//
//  Created by 徐浩 on 2019/5/5.
//

#import "StoreOperationMainTableViewCell.h"

@interface StoreOperationMainTableViewCell()
//图标
@property (strong, nonatomic) UIImageView *iconImageView;
//名称
@property (strong, nonatomic) UILabel *namelabel;
//横线
@property (strong, nonatomic) UILabel *hengLabel;
@end

@implementation StoreOperationMainTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.iconImageView = [[UIImageView alloc]init];
        [self.contentView addSubview:self.iconImageView];
        [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(20);
            make.centerY.equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(24, 21));
        }];
        
        self.namelabel = [[UILabel alloc]init];
        [self.contentView addSubview:self.namelabel];
        [self.namelabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.iconImageView.mas_right).offset(15);
            make.centerY.equalTo(self.contentView);
        }];
        self.namelabel.textColor = [UIColor blackColor];
        self.namelabel.textAlignment = 0;
        self.namelabel.font = kFont(16);
        self.namelabel.text = @"我是名称";
        
        UIImageView *rightImageView = [[UIImageView alloc]init];
        [self.contentView addSubview:rightImageView];
        [rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(-12);
            make.centerY.equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(8, 13));
        }];
        rightImageView.image = [UIImage imageNamed:@"store_operation_enter"];
        
        self.hengLabel = [[UILabel alloc]init];
        [self.contentView addSubview:self.hengLabel];
        [self.hengLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).offset(20);
            make.bottom.equalTo(self.contentView.mas_bottom);
            make.right.equalTo(self.contentView.mas_right).offset(-12);
            make.height.mas_equalTo(1);
        }];
        self.hengLabel.backgroundColor = COLORRGB(0xececec);
    }
    return self;
}

-(void) reloadUIWithImageName:(NSString *)imageName name:(NSString *)name isShowHeng:(BOOL)isShow
{
    if (!isShow)
    {
        self.hengLabel.hidden = YES;
    }
    self.iconImageView.image = [UIImage imageNamed:imageName];
    self.namelabel.text = name;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
