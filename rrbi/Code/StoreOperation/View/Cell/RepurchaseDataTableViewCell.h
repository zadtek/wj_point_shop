//
//  RepurchaseDataTableViewCell.h
//  rrbi
//
//  Created by 徐浩 on 2019/5/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RepurchaseDataTableViewCell : UITableViewCell

-(void) reloadUIWithName:(NSString *)name peopleNum:(NSString *)peopleNum peopleNum1:(NSString *)peopleNum1 peopleNumNew:(NSString *)peopleNumNew peopleNumNew1:(NSString *)peopleNumNew1 peopleNumOld:(NSString *)peopleNumOld peopleNumOld1:(NSString *)peopleNumOld1;

@end

NS_ASSUME_NONNULL_END
