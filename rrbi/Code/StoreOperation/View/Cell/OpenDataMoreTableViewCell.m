//
//  OpenDataMoreTableViewCell.m
//  rrbi
//
//  Created by MyMac on 2019/5/10.
//

#import "OpenDataMoreTableViewCell.h"

@implementation OpenDataMoreTableViewCell

- (void)updataWithModel:(FinancaialOrderModel *)model {
    
    // timeStampString 是服务器返回的13位时间戳
    NSString *timeStampString  = model.time;
    
    // iOS 生成的时间戳是10位
    NSTimeInterval interval    =[timeStampString doubleValue];
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *dateString       = [formatter stringFromDate: date];
    
    self.shijianLabel.text = [NSString stringWithFormat:@"%@",dateString];
    [self.yiwanChengButton setTitle:model.status forState:UIControlStateNormal];
    self.diandanBianHaoLabel.text = [NSString stringWithFormat:@"%@",model.order_number];
    
    self.liushuiLabel.text = [NSString stringWithFormat:@"%@",model.liushui];
    self.shangpinPriceLabel.text = [NSString stringWithFormat:@"%@",model.goods_price];
    self.baoZhuangFeiLabel.text = [NSString stringWithFormat:@"%@",model.packing_price];
    self.peisongFeiLabel.text = [NSString stringWithFormat:@"%@",model.delivery_price];
    self.yujiShouRuLabel.text = [NSString stringWithFormat:@"%@",model.estimated_revenue];
    self.pingtaiBuTieLabel.text = [NSString stringWithFormat:@"%@",model.platform_subsidy];
    self.shangjiaBULabel.text = [NSString stringWithFormat:@"%@",model.business_subsidy];
    self.dingdanShiChangLabel.text = [NSString stringWithFormat:@"%@",model.order_duration];
    self.wanChengTimeLabel.text = [NSString stringWithFormat:@"%@",model.completion_time];
    self.huodongTypeLabel.text = [NSString stringWithFormat:@"%@",model.activity_type];
    
}



- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
