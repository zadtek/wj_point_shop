//  用水查询——时间选择
//  SWYearTimeChoose.h
//  IntelligentWaterSystem
//
//  Created by geimin on 15/1/21.
//  Copyright (c) 2015年 Palmtrends. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWYearTimeChooseDelegate<NSObject>

-(void)clickInquireBackWithTime:(NSString *)timeStr;

@end

@interface SWYearTimeChoose : UIView

@property (nonatomic, weak) id<SWYearTimeChooseDelegate>SWYearTimeChooseDelegate;


@end
