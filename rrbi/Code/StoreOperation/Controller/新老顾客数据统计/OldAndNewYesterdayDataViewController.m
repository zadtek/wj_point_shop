//
//  OldAndNewYesterdayDataViewController.m
//  rrbi
//
//  Created by 徐浩 on 2019/5/5.
//

#import "OldAndNewYesterdayDataViewController.h"

@interface OldAndNewYesterdayDataViewController ()
//成交数
@property (strong, nonatomic) UILabel *transactionNumLabel;
//新顾客
@property (strong, nonatomic) UILabel *customerNewLabel;
//老顾客
@property (strong, nonatomic) UILabel *customerOldLabel;
//新顾客占比
@property (strong, nonatomic) UILabel *customerNewNumLabel;
//老顾客占比
@property (strong, nonatomic) UILabel *customerOldNumLabel;
@end

@implementation OldAndNewYesterdayDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    // Do any additional setup after loading the view.
}

- (void)createUI
{
    self.view.backgroundColor = COLORRGB(0xececec);
    
    UIView *topView = [[UIView alloc]init];
    [self.view addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.mas_topLayoutGuide).offset(5);
        make.height.mas_equalTo(52);
    }];
    topView.backgroundColor = [UIColor whiteColor];
    
    UILabel *transactionLabel = [[UILabel alloc]init];
    [topView addSubview:transactionLabel];
    [transactionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(topView);
        make.left.equalTo(topView.mas_left).offset(22);
    }];
    transactionLabel.textColor = COLORRGB(0x999999);
    transactionLabel.text = @"成交顾客数";
    transactionLabel.textAlignment = 0;
    transactionLabel.font = kFontNameSize(12);
    
    self.transactionNumLabel = [[UILabel alloc]init];
    [topView addSubview:self.transactionNumLabel];
    [self.transactionNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(transactionLabel.mas_right).offset(16);
        make.centerY.equalTo(topView);
    }];
    self.transactionNumLabel.textAlignment = 0;
    self.transactionNumLabel.textColor = [UIColor blackColor];
    self.transactionNumLabel.font = kFontNameSize(15);
    //self.transactionNumLabel.text = @"150";
    
    UIView *centerView = [[UIView alloc]init];
    [self.view addSubview:centerView];
    [centerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(36);
    }];
    
    UILabel *shuLanel = [[UILabel alloc]init];
    [centerView addSubview:shuLanel];
    [shuLanel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(centerView.mas_left).offset(11);
        make.centerY.equalTo(centerView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(4, 14));
    }];
    shuLanel.backgroundColor = COLORRGB(0x666666);
    
    UILabel *zhanbiLabel = [[UILabel alloc]init];
    [centerView addSubview:zhanbiLabel];
    [zhanbiLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shuLanel.mas_right).offset(6);
        make.centerY.equalTo(shuLanel.mas_centerY);
    }];
    zhanbiLabel.textColor = COLORRGB(0x666666);
    zhanbiLabel.text = @"新老客户占比";
    zhanbiLabel.textAlignment = 0;
    zhanbiLabel.font = kFontNameSize(13);
    
    UIView *bottomView = [[UIView alloc]init];
    [self.view addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(centerView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(105);
    }];
    bottomView.backgroundColor = [UIColor whiteColor];
    
    self.customerNewLabel = [[UILabel alloc]init];
    [bottomView addSubview:self.customerNewLabel];
    [self.customerNewLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bottomView.mas_top).offset(25);
        make.left.equalTo(bottomView.mas_left).offset(40);
        make.size.mas_equalTo(CGSizeMake(70, 20));
    }];
    self.customerNewLabel.textAlignment = NSTextAlignmentCenter;
    self.customerNewLabel.textColor = [UIColor blackColor];
    //self.customerNewLabel.text = @"30";
    self.customerNewLabel.font = kFontNameSize(15);
    
    self.customerNewNumLabel = [[UILabel alloc]init];
    [bottomView addSubview:self.customerNewNumLabel];
    [self.customerNewNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomView.mas_left).offset(40);
        make.top.equalTo(self.customerNewLabel.mas_bottom).offset(8);
        make.size.mas_equalTo(CGSizeMake(70, 40));
    }];
    self.customerNewNumLabel.textColor = COLORRGB(0x999999);
    self.customerNewNumLabel.textAlignment = NSTextAlignmentCenter;
    self.customerNewNumLabel.numberOfLines = 0;
    //self.customerNewNumLabel.text = @"新顾客数占比30%";
    self.customerNewNumLabel.font = kFontNameSize(11);
    
    self.customerOldLabel = [[UILabel alloc]init];
    [bottomView addSubview:self.customerOldLabel];
    [self.customerOldLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bottomView.mas_top).offset(25);
        make.right.equalTo(bottomView.mas_right).offset(-40);
        make.size.mas_equalTo(CGSizeMake(70, 20));
    }];
    self.customerOldLabel.textAlignment =NSTextAlignmentCenter;
    self.customerOldLabel.textColor = [UIColor blackColor];
    //self.customerOldLabel.text = @"70";
    self.customerOldLabel.font = kFontNameSize(15);
    
    self.customerOldNumLabel = [[UILabel alloc]init];
    [bottomView addSubview:self.customerOldNumLabel];
    [self.customerOldNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bottomView.mas_right).offset(-40);
        make.top.equalTo(self.customerOldLabel.mas_bottom).offset(8);
        make.size.mas_equalTo(CGSizeMake(70, 40));
    }];
    self.customerOldNumLabel.textColor = COLORRGB(0x999999);
    self.customerOldNumLabel.textAlignment = NSTextAlignmentCenter;
    self.customerOldNumLabel.numberOfLines = 0;
    //self.customerOldNumLabel.text = @"新顾客数占比70%";
    self.customerOldNumLabel.font = kFontNameSize(11);
    [self getData];
}
- (void)getData {
    
    
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{
                          @"ru_id":[UserModel sharedInstanced].ru_Id,
                          @"ince":@"new_old_count",
                          @"status":@"2",
                          };
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",BaseUrl,@"new_old_count"];
    [mgr POST:URL parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            self.transactionNumLabel.text = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"people"]];
            self.customerNewLabel.text = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"newpeople"]];
            self.customerNewNumLabel.text = [NSString stringWithFormat:@"新顾客数占比%@",[responseObject objectForKey:@"newpeopleratio"]];
            self.customerOldLabel.text = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"oldpeople"]];
            self.customerOldNumLabel.text = [NSString stringWithFormat:@"老顾客数占比%@",[responseObject objectForKey:@"oldpeopleratio"]];
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        NSLog(@"%@",error);
        [self hidHUD:@"网络异常"];
    }];
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
