//
//  OldAndNewDataMainViewController.m
//  rrbi
//
//  Created by 徐浩 on 2019/5/5.
//

#import "OldAndNewDataMainViewController.h"
#import <WMPageController.h>
#import "OldAndNewTodayDataViewController.h"
#import "OldAndNewYesterdayDataViewController.h"
#import "OldAndNewWeekViewController.h"
#import "OldAndNewMonthViewController.h"
#import "UICustomDatePicker.h"

//判断iPhoneX
#define IS_IPHONE_X ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPHoneXr
#define IS_IPHONE_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhoneXs
#define IS_IPHONE_Xs ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhoneXs Max
#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
#define Height_NavBar ((IS_IPHONE_X==YES || IS_IPHONE_Xr ==YES || IS_IPHONE_Xs== YES || IS_IPHONE_Xs_Max== YES) ? 88.0 : 64.0)

@interface OldAndNewDataMainViewController ()<WMPageControllerDelegate,WMPageControllerDataSource>
@property (nonatomic, strong) WMPageController *pageController;

@property (strong, nonatomic) UIView *dateView;

//成交数
@property (strong, nonatomic) UILabel *transactionNumLabel;
//新顾客
@property (strong, nonatomic) UILabel *customerNewLabel;
//老顾客
@property (strong, nonatomic) UILabel *customerOldLabel;
//新顾客占比
@property (strong, nonatomic) UILabel *customerNewNumLabel;
//老顾客占比
@property (strong, nonatomic) UILabel *customerOldNumLabel;

@end

@implementation OldAndNewDataMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"新老顾客数据统计";
    [self createUI];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"日期(1)"] style:UIBarButtonItemStylePlain target:self action:@selector(dateButtonClick)];
    // Do any additional setup after loading the view.
}

-(void)dateButtonClick
{
    [UICustomDatePicker showCustomDatePickerAtView:self.view choosedDateBlock:^(NSDate *date) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *strDate = [dateFormatter stringFromDate:date];
        [self requstDataWithDate:strDate];
    } cancelBlock:^{
        
    }];
}

- (WMPageController *)pageController {
    if (!_pageController) {
        _pageController = [[WMPageController alloc] initWithViewControllerClasses:@[[OldAndNewTodayDataViewController class],[OldAndNewYesterdayDataViewController class],[OldAndNewWeekViewController class],[OldAndNewMonthViewController class]] andTheirTitles:@[@"今日数据",@"昨日数据",@"本周累计",@"本月累计"]];
        _pageController.menuViewStyle = WMMenuViewStyleLine;
        _pageController.menuViewLayoutMode = WMMenuViewLayoutModeScatter;
        _pageController.menuItemWidth = 80;
        _pageController.menuHeight = 38;
        //        _pageController.progressWidth = 12;
        //        _pageController.progressHeight = 4;
        _pageController.titleSizeNormal = 14;
        _pageController.titleSizeSelected = 14;
        _pageController.titleColorNormal = COLORRGB(0x999999);
        _pageController.titleColorSelected = COLORRGB(0x999999);
        _pageController.progressColor = [UIColor blackColor];//设置下划线(或者边框)颜色
        _pageController.menuBGColor = COLORRGB(0xffffff);
        _pageController.delegate = self;
        _pageController.dataSource = self;
        _pageController.view.backgroundColor = COLORRGB(0xffffff);
    }
    return _pageController;
}

- (void)createUI {
    [self.view addSubview:self.pageController.view];
    self.pageController.viewFrame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - Height_NavBar);
    
    self.dateView = [[UIView alloc]init];
    [self.view addSubview:self.dateView];
    [self.dateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    self.dateView.backgroundColor = COLORRGB(0xececec);
    
    UIView *topView = [[UIView alloc]init];
    [self.dateView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.dateView);
        make.top.equalTo(self.mas_topLayoutGuide).offset(5);
        make.height.mas_equalTo(52);
    }];
    topView.backgroundColor = [UIColor whiteColor];

    UILabel *transactionLabel = [[UILabel alloc]init];
    [topView addSubview:transactionLabel];
    [transactionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(topView);
        make.left.equalTo(topView.mas_left).offset(22);
    }];
    transactionLabel.textColor = COLORRGB(0x999999);
    transactionLabel.text = @"成交顾客数";
    transactionLabel.textAlignment = 0;
    transactionLabel.font = kFontNameSize(12);

    self.transactionNumLabel = [[UILabel alloc]init];
    [topView addSubview:self.transactionNumLabel];
    [self.transactionNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(transactionLabel.mas_right).offset(16);
        make.centerY.equalTo(topView);
    }];
    self.transactionNumLabel.textAlignment = 0;
    self.transactionNumLabel.textColor = [UIColor blackColor];
    self.transactionNumLabel.font = kFontNameSize(15);
    self.transactionNumLabel.text = @"100";

    UIView *centerView = [[UIView alloc]init];
    [self.dateView addSubview:centerView];
    [centerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom);
        make.left.right.equalTo(self.dateView);
        make.height.mas_equalTo(36);
    }];

    UILabel *shuLanel = [[UILabel alloc]init];
    [centerView addSubview:shuLanel];
    [shuLanel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(centerView.mas_left).offset(11);
        make.centerY.equalTo(centerView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(4, 14));
    }];
    shuLanel.backgroundColor = COLORRGB(0x666666);

    UILabel *zhanbiLabel = [[UILabel alloc]init];
    [centerView addSubview:zhanbiLabel];
    [zhanbiLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shuLanel.mas_right).offset(6);
        make.centerY.equalTo(shuLanel.mas_centerY);
    }];
    zhanbiLabel.textColor = COLORRGB(0x666666);
    zhanbiLabel.text = @"新老客户占比";
    zhanbiLabel.textAlignment = 0;
    zhanbiLabel.font = kFontNameSize(13);

    UIView *bottomView = [[UIView alloc]init];
    [self.dateView addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(centerView.mas_bottom);
        make.left.right.equalTo(self.dateView);
        make.height.mas_equalTo(105);
    }];
    bottomView.backgroundColor = [UIColor whiteColor];

    self.customerNewLabel = [[UILabel alloc]init];
    [bottomView addSubview:self.customerNewLabel];
    [self.customerNewLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bottomView.mas_top).offset(32);
        make.left.equalTo(bottomView.mas_left).offset(55);
    }];
    self.customerNewLabel.textAlignment = 0;
    self.customerNewLabel.textColor = [UIColor blackColor];
    self.customerNewLabel.text = @"30";
    self.customerNewLabel.font = kFontNameSize(15);

    self.customerNewNumLabel = [[UILabel alloc]init];
    [bottomView addSubview:self.customerNewNumLabel];
    [self.customerNewNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomView.mas_left).offset(40);
        make.top.equalTo(self.customerNewLabel.mas_bottom).offset(8);
        make.size.mas_equalTo(CGSizeMake(50, 40));
    }];
    self.customerNewNumLabel.textColor = COLORRGB(0x999999);
    self.customerNewNumLabel.textAlignment = 1;
    self.customerNewNumLabel.numberOfLines = 0;
    self.customerNewNumLabel.text = @"新顾客数占比30%";
    self.customerNewNumLabel.font = kFontNameSize(11);

    self.customerOldLabel = [[UILabel alloc]init];
    [bottomView addSubview:self.customerOldLabel];
    [self.customerOldLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bottomView.mas_top).offset(32);
        make.right.equalTo(bottomView.mas_right).offset(-55);
    }];
    self.customerOldLabel.textAlignment =2;
    self.customerOldLabel.textColor = [UIColor blackColor];
    self.customerOldLabel.text = @"70";
    self.customerOldLabel.font = kFontNameSize(15);

    self.customerOldNumLabel = [[UILabel alloc]init];
    [bottomView addSubview:self.customerOldNumLabel];
    [self.customerOldNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bottomView.mas_right).offset(-40);
        make.top.equalTo(self.customerOldLabel.mas_bottom).offset(8);
        make.size.mas_equalTo(CGSizeMake(50, 40));
    }];
    self.customerOldNumLabel.textColor = COLORRGB(0x999999);
    self.customerOldNumLabel.textAlignment = 1;
    self.customerOldNumLabel.numberOfLines = 0;
    self.customerOldNumLabel.text = @"老顾客数占比0%";
    self.customerOldNumLabel.font = kFontNameSize(11);
    
    self.dateView.hidden = YES;
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController;
{
    return 4;
}


- (__kindof UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index
{
    if (index == 0) {
        OldAndNewTodayDataViewController *repurchaseDataWeekVC = [[OldAndNewTodayDataViewController alloc]init];
        return repurchaseDataWeekVC;
    }
    else if (index == 1)
    {
        OldAndNewYesterdayDataViewController *repurchaseDataMonthVC = [[OldAndNewYesterdayDataViewController alloc]init];
        return repurchaseDataMonthVC;
    }
    else if (index == 2)
    {
        OldAndNewWeekViewController *repurchaseDataMonthVC = [[OldAndNewWeekViewController alloc]init];
        return repurchaseDataMonthVC;
    }
    else
    {
        OldAndNewMonthViewController *repurchaseDataMonthVC = [[OldAndNewMonthViewController alloc]init];
        return repurchaseDataMonthVC;
    }
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index
{
    if (index == 0) {
        return @"今日数据";
    }
    else if (index == 1)
    {
        return @"昨日数据";
    }
    else if (index == 2)
    {
        return @"本周累计";
    }
    else
    {
        return @"本月累计";
    }
}



-(void)requstDataWithDate:(NSString *)dateStr
{
    MJWeakSelf;
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{
                          @"ru_id":[UserModel sharedInstanced].ru_Id,
                          @"ince":@"new_old_count",
                          @"status":@"",
                          @"time":dateStr
                          };
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",BaseUrl,@"new_old_count"];
    [mgr POST:URL parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            [weakSelf requestDataSuccessWith:responseObject];
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        NSLog(@"失败");
        [self hidHUD:@"网络异常"];
    }];
}

-(void) requestDataSuccessWith:(id)responseObject
{
    self.dateView.hidden = NO;
    self.transactionNumLabel.text = [[[Config alloc]init]isBlankString:[NSString stringWithFormat:@"%@",responseObject[@"people"]]] ? @"" : [NSString stringWithFormat:@"%@",responseObject[@"people"]];
    self.customerNewLabel.text = [[[Config alloc]init]isBlankString:[NSString stringWithFormat:@"%@",responseObject[@"newpeople"]]] ? @"" : [NSString stringWithFormat:@"%@",responseObject[@"newpeople"]];
    self.customerOldLabel.text = [[[Config alloc]init]isBlankString:[NSString stringWithFormat:@"%@",responseObject[@"oldpeople"]]] ? @"" : [NSString stringWithFormat:@"%@",responseObject[@"oldpeople"]];
    self.customerNewNumLabel.text = [[[Config alloc]init]isBlankString:[NSString stringWithFormat:@"%@",responseObject[@"newpeopleratio"]]] ? @"新顾客数占比" : [NSString stringWithFormat:@"新顾客数占比%@",responseObject[@"newpeopleratio"]];
    self.customerOldNumLabel.text = [[[Config alloc]init]isBlankString:[NSString stringWithFormat:@"%@",responseObject[@"oldpeopleratio"]]] ? @"老顾客占比" : [NSString stringWithFormat:@"老顾客占比%@",responseObject[@"oldpeopleratio"]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
