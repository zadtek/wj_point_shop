//
//  RepurchaseDataMonthViewController.m
//  rrbi
//
//  Created by 徐浩 on 2019/5/5.
//

#import "RepurchaseDataMonthViewController.h"
#import "RepurchaseDataTableViewCell.h"

static NSString *const cellID = @"RepurchaseDataTableViewCell";

@interface RepurchaseDataMonthViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic,strong) NSDictionary *dict;

@end

@implementation RepurchaseDataMonthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 33;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // Do any additional setup after loading the view.
    
    [self getData];
}

#pragma mark --- delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RepurchaseDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[RepurchaseDataTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    if (indexPath.row == 0)
    {
        [cell reloadUIWithName:@"复购人数" peopleNum:[NSString stringWithFormat:@"%@",[self.dict objectForKey:@"count"]] peopleNum1:@"复购人数" peopleNumNew:[NSString stringWithFormat:@"%@",[self.dict objectForKey:@"newcount"]] peopleNumNew1:@"复购新客" peopleNumOld:[NSString stringWithFormat:@"%@",[self.dict objectForKey:@"oldcount"]] peopleNumOld1:@"复购老客"];
    }
    else
    {
        [cell reloadUIWithName:@"复购率" peopleNum:[NSString stringWithFormat:@"%@",[self.dict objectForKey:@"fugourate"]] peopleNum1:@"复购率" peopleNumNew:[NSString stringWithFormat:@"%@",[self.dict objectForKey:@"newfugourate"]] peopleNumNew1:@"新客复购率" peopleNumOld:[NSString stringWithFormat:@"%@",[self.dict objectForKey:@"oldfugourate"]] peopleNumOld1:@"老客复购率"];
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}
- (void)getData {
    
    
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{
                          @"ru_id":[UserModel sharedInstanced].ru_Id,
                          @"ince":@"fugou_count",
                          @"status":@"2",
                          };
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",BaseUrl,@"fugou_count"];
    [mgr POST:URL parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            self.dict = responseObject;
            [self.tableView reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        NSLog(@"%@",error);
        [self hidHUD:@"网络异常"];
    }];
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
