//
//  RepurchaseDataMainViewController.m
//  rrbi
//
//  Created by 徐浩 on 2019/5/5.
//

#import "RepurchaseDataMainViewController.h"
#import <WMPageController.h>
#import "RepurchaseDataWeekViewController.h"
#import "RepurchaseDataMonthViewController.h"
#import "UICustomDatePicker.h"
#import "RepurchaseDataTableViewCell.h"

static NSString *const cellID = @"RepurchaseDataTableViewCell";

//判断iPhoneX
#define IS_IPHONE_X ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPHoneXr
#define IS_IPHONE_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhoneXs
#define IS_IPHONE_Xs ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhoneXs Max
#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
#define Height_NavBar ((IS_IPHONE_X==YES || IS_IPHONE_Xr ==YES || IS_IPHONE_Xs== YES || IS_IPHONE_Xs_Max== YES) ? 88.0 : 64.0)

@interface RepurchaseDataMainViewController ()<WMPageControllerDelegate,WMPageControllerDataSource,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) WMPageController *pageController;

@property (strong, nonatomic) UITableView *tableView;

@property (copy, nonatomic) NSDictionary *dataDic;

@end

@implementation RepurchaseDataMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"复购数据统计";
    [self createUI];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"日期(1)"] style:UIBarButtonItemStylePlain target:self action:@selector(dateButtonClick)];
    // Do any additional setup after loading the view.
}

-(void)dateButtonClick
{
    [UICustomDatePicker showCustomDatePickerAtView:self.view choosedDateBlock:^(NSDate *date) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *strDate = [dateFormatter stringFromDate:date];
        [self requstDataWithDate:strDate];
    } cancelBlock:^{
        
    }];
}


- (WMPageController *)pageController {
    if (!_pageController) {
        _pageController = [[WMPageController alloc] initWithViewControllerClasses:@[[RepurchaseDataWeekViewController class],[RepurchaseDataMonthViewController class]] andTheirTitles:@[@"本周累计",@"本月累计"]];
        _pageController.menuViewStyle = WMMenuViewStyleLine;
        _pageController.menuViewLayoutMode = WMMenuViewLayoutModeScatter;
        _pageController.menuItemWidth = 80;
        _pageController.menuHeight = 38;
//        _pageController.progressWidth = 12;
//        _pageController.progressHeight = 4;
        _pageController.titleSizeNormal = 14;
        _pageController.titleSizeSelected = 14;
        _pageController.titleColorNormal = COLORRGB(0x999999);
        _pageController.titleColorSelected = COLORRGB(0x999999);
        _pageController.progressColor = [UIColor blackColor];//设置下划线(或者边框)颜色
        _pageController.menuBGColor = COLORRGB(0xffffff);
        _pageController.delegate = self;
        _pageController.dataSource = self;
        _pageController.view.backgroundColor = COLORRGB(0xffffff);
    }
    return _pageController;
}

- (void)createUI {
    [self.view addSubview:self.pageController.view];
    self.pageController.viewFrame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - Height_NavBar);
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 33;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.hidden = YES;
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController;
{
    return 2;
}


- (__kindof UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index
{
    if (index == 0) {
        RepurchaseDataWeekViewController *repurchaseDataWeekVC = [[RepurchaseDataWeekViewController alloc]init];
        return repurchaseDataWeekVC;
    }
    else
    {
        RepurchaseDataMonthViewController *repurchaseDataMonthVC = [[RepurchaseDataMonthViewController alloc]init];
        return repurchaseDataMonthVC;
    }
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index
{
    if (index == 0) {
        return @"本周累计";
    }
    else
    {
        return @"本月累计";
    }
}

#pragma mark --- delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RepurchaseDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[RepurchaseDataTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    if (indexPath.row == 0)
    {
        [cell reloadUIWithName:@"复购人数" peopleNum:[[[Config alloc]init] isBlankString:[NSString stringWithFormat:@"%@",self.dataDic[@"count"]]] ? @"" : [NSString stringWithFormat:@"%@",self.dataDic[@"count"]] peopleNum1:@"复购人数" peopleNumNew:[[[Config alloc]init] isBlankString:[NSString stringWithFormat:@"%@",self.dataDic[@"newcount"]]] ? @"" : [NSString stringWithFormat:@"%@",self.dataDic[@"newcount"]] peopleNumNew1:@"复购新客" peopleNumOld:[[[Config alloc]init] isBlankString:[NSString stringWithFormat:@"%@",self.dataDic[@"oldcount"]]] ? @"" : [NSString stringWithFormat:@"%@",self.dataDic[@"oldcount"]] peopleNumOld1:@"复购老客"];
    }
    else
    {
        [cell reloadUIWithName:@"复购率" peopleNum:[[[Config alloc]init] isBlankString:[NSString stringWithFormat:@"%@",self.dataDic[@"fugourate"]]] ? @"" : [NSString stringWithFormat:@"%@",self.dataDic[@"fugourate"]] peopleNum1:@"复购率" peopleNumNew:[[[Config alloc]init] isBlankString:[NSString stringWithFormat:@"%@",self.dataDic[@"newfugourate"]]] ? @"" : [NSString stringWithFormat:@"%@",self.dataDic[@"newfugourate"]] peopleNumNew1:@"新客复购率" peopleNumOld:[[[Config alloc]init] isBlankString:[NSString stringWithFormat:@"%@",self.dataDic[@"oldfugourate"]]] ? @"" : [NSString stringWithFormat:@"%@",self.dataDic[@"oldfugourate"]] peopleNumOld1:@"老客复购率"];
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}

-(void)requstDataWithDate:(NSString *)dateStr
{
    MJWeakSelf;
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{
                          @"ru_id":[UserModel sharedInstanced].ru_Id,
                          @"ince":@"fugou_count",
                          @"status":@"",
                          @"time":dateStr
                          };
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",BaseUrl,@"fugou_count"];
    [mgr POST:URL parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            [weakSelf requestDataSuccessWith:responseObject];
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        NSLog(@"失败");
        [self hidHUD:@"网络异常"];
    }];
}

-(void) requestDataSuccessWith:(id)responseObject
{
    self.tableView.hidden = NO;
    self.dataDic = responseObject;
    [self.tableView reloadData];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
