//
//  OperatingDataViewController.m
//  rrbi
//
//  Created by 徐浩 on 2019/5/5.
//

#import "OperatingDataViewController.h"
#import <WMPageController.h>
#import "OpenDataViewController.h"
#import "GoodsDataViewController.h"

//判断iPhoneX
#define IS_IPHONE_X ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPHoneXr
#define IS_IPHONE_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhoneXs
#define IS_IPHONE_Xs ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhoneXs Max
#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
#define Height_NavBar ((IS_IPHONE_X==YES || IS_IPHONE_Xr ==YES || IS_IPHONE_Xs== YES || IS_IPHONE_Xs_Max== YES) ? 88.0 : 64.0)

@interface OperatingDataViewController ()<WMPageControllerDelegate,WMPageControllerDataSource>
@property (nonatomic, strong) WMPageController *pageController;

@end

@implementation OperatingDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"经营数据";
    [self createUI];
    // Do any additional setup after loading the view.
}
- (WMPageController *)pageController {
    if (!_pageController) {
        _pageController = [[WMPageController alloc] initWithViewControllerClasses:@[[OpenDataViewController class],[GoodsDataViewController class]] andTheirTitles:@[@"营业数据",@"商品数据"]];
        _pageController.menuViewStyle = WMMenuViewStyleLine;
        _pageController.menuViewLayoutMode = WMMenuViewLayoutModeScatter;
        _pageController.menuItemWidth = 80;
        _pageController.menuHeight = 38;
        //        _pageController.progressWidth = 12;
        //        _pageController.progressHeight = 4;
        _pageController.titleSizeNormal = 14;
        _pageController.titleSizeSelected = 14;
        _pageController.titleColorNormal = COLORRGB(0x999999);
        _pageController.titleColorSelected = COLORRGB(0x999999);
        _pageController.progressColor = COLORRGB(0x3bad6a);//设置下划线(或者边框)颜色
        _pageController.menuBGColor = COLORRGB(0xffffff);
        _pageController.delegate = self;
        _pageController.dataSource = self;
        _pageController.view.backgroundColor = COLORRGB(0xffffff);
    }
    return _pageController;
}

- (void)createUI {
    [self.view addSubview:self.pageController.view];
    self.pageController.viewFrame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - Height_NavBar);
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController;
{
    return 2;
}


- (__kindof UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index
{
    if (index == 0) {
        OpenDataViewController *repurchaseDataWeekVC = [[OpenDataViewController alloc]init];
        return repurchaseDataWeekVC;
    }
    else
    {
        GoodsDataViewController *repurchaseDataMonthVC = [[GoodsDataViewController alloc]init];
        return repurchaseDataMonthVC;
    }
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index
{
    if (index == 0) {
        return @"营业数据";
    }
    else
    {
        return @"商品数据";
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
