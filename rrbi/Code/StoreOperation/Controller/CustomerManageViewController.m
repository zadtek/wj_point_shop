//
//  CustomerManageViewController.m
//  rrbi
//
//  Created by 徐浩 on 2019/5/5.
//

#import "CustomerManageViewController.h"
#import "CustomerManageMainTableViewCell.h"
#import "RepurchaseDataMainViewController.h"
#import "OldAndNewDataMainViewController.h"
#import "CommentList.h"
#import "UsersStatisticalViewController.h"
#import "OldAndNewDataMainViewController.h"

static NSString *const cellID = @"CustomerManageMainTableViewCell";
@interface CustomerManageViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;

@end

@implementation CustomerManageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"顾客管理";
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:(UITableViewStyleGrouped)];
    _tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // Do any additional setup after loading the view.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomerManageMainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[CustomerManageMainTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0) {
            [cell reloadUIWithName:@"新老顾客数据统计" isShowHeng:YES];
        }
        else
        {
            [cell reloadUIWithName:@"复购数据统计" isShowHeng:NO];
        }
    }
    else
    {
        if (indexPath.row == 0) {
            [cell reloadUIWithName:@"用户评价" isShowHeng:YES];
        }
        else
        {
            [cell reloadUIWithName:@"用户统计" isShowHeng:NO];
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]init];
    
    headerView.backgroundColor = COLORRGB(0xececec);
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0) {
            NSLog(@"新老顾客数据统计");
            OldAndNewDataMainViewController *olaAndNewVC = [[OldAndNewDataMainViewController alloc]init];
            [self.navigationController pushViewController:olaAndNewVC animated:YES];
        }
        else
        {
            NSLog(@"复购数据统计");
            RepurchaseDataMainViewController *repurchaseDataMainVC = [[RepurchaseDataMainViewController alloc]init];
            [self.navigationController pushViewController:repurchaseDataMainVC animated:YES];
        }
    }
    else
    {
        if (indexPath.row == 0) {
            NSLog(@"用户评价");
            
            CommentList* controller = [[CommentList alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else
        {
            NSLog(@"用户统计");
            UsersStatisticalViewController* controller = [[UsersStatisticalViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
