//
//  StoreOperationMainViewController.m
//  rrbi
//
//  Created by 徐浩 on 2019/5/5.
//

#import "StoreOperationMainViewController.h"
#import "StoreOperationMainTableViewCell.h"
#import "ReconciliationViewController.h"
#import "OperatingDataViewController.h"
#import "CustomerManageViewController.h"
#import "FinancaialOrderMainViewController.h"
#import "GoodsList.h"
static NSString *const cellID = @"StoreOperationMainTableViewCell";
@interface StoreOperationMainViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;

@end

@implementation StoreOperationMainViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!self.Identity.userInfo.isLogin){
        UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:[[Login alloc]init]];
        [self presentViewController:nav animated:YES completion:nil];
    }else{
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:(UITableViewStyleGrouped)];
    _tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // Do any additional setup after loading the view.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    StoreOperationMainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[StoreOperationMainTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0) {
            [cell reloadUIWithImageName:@"store_operation_goods" name:@"商品管理" isShowHeng:YES];
        }
        else
        {
            [cell reloadUIWithImageName:@"store_operation_customer" name:@"顾客管理" isShowHeng:NO];
        }
    }
    else
    {
        if (indexPath.row == 0) {
            [cell reloadUIWithImageName:@"store_operation_finance" name:@"财务对账" isShowHeng:YES];
        }
        else
        {
            [cell reloadUIWithImageName:@"store_operation_data" name:@"经营数据" isShowHeng:NO];
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]init];
    
    headerView.backgroundColor = COLORRGB(0xececec);
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0) {
            NSLog(@"商品管理");
            GoodsList* controller =[[GoodsList alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
            
            
        }
        else
        {
            NSLog(@"顾客管理");
            CustomerManageViewController *customerManageVC = [[CustomerManageViewController alloc]init];
            [self.navigationController pushViewController:customerManageVC animated:YES];
        }
    }
    else
    {
        if (indexPath.row == 0) {
            NSLog(@"财务对账");
            
            FinancaialOrderMainViewController * controller = [[FinancaialOrderMainViewController alloc]init];
            controller.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:controller animated:YES];
        }
        else
        {
            NSLog(@"经营数据");
            OperatingDataViewController *operatingDataVC = [[OperatingDataViewController alloc]init];
            [self.navigationController pushViewController:operatingDataVC animated:YES];
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
