//
//  OpenDataMoreViewController.m
//  rrbi
//
//  Created by 徐浩 on 2019/5/5.
//

#import "OpenDataMoreViewController.h"
#import "OpenDataMoreTableViewCell.h"
#import "FinancaialOrderModel.h"

#import "UICustomDatePicker.h"
#import "TimeSectionView.h"
#import "zySheetPickerView.h"


@interface OpenDataMoreViewController ()<UITableViewDelegate,UITableViewDataSource,TimeSectionViewDelegate,UITextFieldDelegate>

@property (nonatomic, strong) UITextField *searchTextFiled;


@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIButton *searchButton;
@property (nonatomic, strong) NSMutableArray *mainArray;
@property (strong, nonatomic) UIButton *shijianBtn;
@property (strong, nonatomic) UIButton *dingdanBtn;
@property (strong, nonatomic) UIButton *shijianduanBtn;
@property (strong, nonatomic) UIButton *huodongBtn;

@property (nonatomic, copy) NSString *count;
@property (nonatomic, copy) NSString *yingYeEStr;
@property (nonatomic, copy) NSString *junPriceStr;

@property (nonatomic, strong) TimeSectionView *timeView;
@property (nonatomic, copy) NSString *time_ymd;
@property (nonatomic, copy) NSString *begTime_hour;
@property (nonatomic, copy) NSString *endTime_hour;
@property (nonatomic, copy) NSString *order_status;
@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *order_statusStr;
@property (nonatomic, copy) NSString *typeStr;


@end

@implementation OpenDataMoreViewController
- (NSMutableArray *)mainArray {
    if (!_mainArray) {
        _mainArray  = [NSMutableArray array];
    }
    return _mainArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self crateUI];
    // Do any additional setup after loading the view.
}


- (void)crateUI {
    NSDate *date =[NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy"];
    NSInteger currentYear=[[formatter stringFromDate:date] integerValue];
    [formatter setDateFormat:@"MM"];
    NSInteger currentMonth=[[formatter stringFromDate:date]integerValue];
    [formatter setDateFormat:@"dd"];
    NSInteger currentDay=[[formatter stringFromDate:date] integerValue];
    self.time_ymd = [NSString stringWithFormat:@"%ld-%ld-%ld",currentYear,currentMonth,currentDay];
    self.begTime_hour = @"";
    self.endTime_hour = @"时间段";
    self.order_statusStr = @"订单状态";
    self.typeStr = @"活动类型";
    self.order_status = @"";
    self.type = @"";
    
    [self creatCustomNavView];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    _tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 33;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([OpenDataMoreTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"openDataMoreTableViewCell"];
    
    self.timeView = [[TimeSectionView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-SafeAreaTopHeight)];
    self.timeView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.3];
    self.timeView.delegate = self;
    [self.view addSubview:self.timeView];
    self.timeView.hidden = YES;
    
    [self getShopData];
    
    
    
}

-(void)creatCustomNavView{
    //  顶部搜索
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
    self.navigationItem.titleView = topView;
    if (@available(iOS 11.0, *)) {
        [topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(240);
            make.height.mas_equalTo(44);
        }];
    }
    
    
    UIView *searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 4.5, 240, 35)];
    searchView.backgroundColor = getColor(@"F3F4F7");
    searchView.layer.cornerRadius = 5;
    searchView.layer.masksToBounds = YES;
    [topView addSubview:searchView];
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(topView.mas_centerX);
        make.centerY.equalTo(topView.mas_centerY);
        make.width.mas_equalTo(240 / WIDTH_6S_SCALE);
        make.height.mas_equalTo(35 / HEIGHT_6S_SCALE);
    }];
    // 搜索框
    [searchView addSubview:self.searchTextFiled];
    [self.searchTextFiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(searchView).offset(5);
        make.centerY.equalTo(searchView.mas_centerY);
        make.right.equalTo(searchView);
        make.height.equalTo(searchView);
    }];
    
}
-(void)navButtonClick:(UIButton *)btn
{
    NSLog(@"点击搜索");
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.mainArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 335;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OpenDataMoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"openDataMoreTableViewCell"];
    
    FinancaialOrderModel *model = self.mainArray[indexPath.row];
    [cell updataWithModel:model];
   
    return cell;
}


- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 80)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIView *topView = [[UIView alloc]init];
    [view addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(view);
        make.height.mas_equalTo(41);
    }];
    
    self.shijianBtn = [[UIButton alloc]init];
    [topView addSubview:self.shijianBtn];
    [self.shijianBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.top.equalTo(topView);
        make.width.mas_equalTo(SCREEN_WIDTH / 4);
    }];
    
    [self.shijianBtn setTitle:self.time_ymd forState:UIControlStateNormal];
    [self.shijianBtn setTitleColor:COLORRGB(0xc1c1c1) forState:(UIControlStateNormal)];
    [self.shijianBtn setTitleColor:COLORRGB(0x31ce32) forState:(UIControlStateSelected)];
    [self.shijianBtn setImage:[UIImage imageNamed:@"下拉拷贝2"] forState:UIControlStateNormal];
    [self.shijianBtn setImage:[UIImage imageNamed:@"下拉"] forState:UIControlStateSelected];
    self.shijianBtn.selected = YES;
    self.shijianBtn.titleLabel.font = kFont(10);
    [self.shijianBtn addTarget:self action:@selector(clickshijian:) forControlEvents:(UIControlEventTouchUpInside)];
    
    self.dingdanBtn = [[UIButton alloc]init];
    [topView addSubview:self.dingdanBtn];
    [self.dingdanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.top.equalTo(topView);
        make.width.mas_equalTo(SCREEN_WIDTH / 4);
        make.left.equalTo(self.shijianBtn.mas_right);
    }];
    [self.dingdanBtn setTitle:self.order_statusStr forState:(UIControlStateNormal)];
    [self.dingdanBtn setTitleColor:COLORRGB(0xc1c1c1) forState:(UIControlStateNormal)];
    [self.dingdanBtn setTitleColor:COLORRGB(0x31ce32) forState:(UIControlStateSelected)];
    [self.dingdanBtn setImage:[UIImage imageNamed:@"下拉拷贝2"] forState:UIControlStateNormal];
    [self.dingdanBtn setImage:[UIImage imageNamed:@"下拉"] forState:UIControlStateSelected];
    self.dingdanBtn.titleLabel.font = kFont(10);
    [self.dingdanBtn addTarget:self action:@selector(clickdingdan:) forControlEvents:(UIControlEventTouchUpInside)];
    
    self.shijianduanBtn = [[UIButton alloc]init];
    [topView addSubview:self.shijianduanBtn];
    [self.shijianduanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.top.equalTo(topView);
        make.width.mas_equalTo(SCREEN_WIDTH / 4);
        make.left.equalTo(self.dingdanBtn.mas_right);
    }];
    [self.shijianduanBtn setTitle:self.endTime_hour forState:(UIControlStateNormal)];
    [self.shijianduanBtn setTitleColor:COLORRGB(0xc1c1c1) forState:(UIControlStateNormal)];
    [self.shijianduanBtn setTitleColor:COLORRGB(0x31ce32) forState:(UIControlStateSelected)];
    [self.shijianduanBtn setImage:[UIImage imageNamed:@"下拉拷贝2"] forState:UIControlStateNormal];
    [self.shijianduanBtn setImage:[UIImage imageNamed:@"下拉"] forState:UIControlStateSelected];
    self.shijianduanBtn.titleLabel.font = kFont(10);
    [self.shijianduanBtn addTarget:self action:@selector(clickshijianduan:) forControlEvents:(UIControlEventTouchUpInside)];
    
    self.huodongBtn = [[UIButton alloc]init];
    [topView addSubview:self.huodongBtn];
    [self.huodongBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.top.equalTo(topView);
        make.width.mas_equalTo(SCREEN_WIDTH / 4);
        make.left.equalTo(self.shijianduanBtn.mas_right);
    }];
    [self.huodongBtn setTitle:self.typeStr forState:(UIControlStateNormal)];
    [self.huodongBtn setTitleColor:COLORRGB(0xc1c1c1) forState:(UIControlStateNormal)];
    [self.huodongBtn setTitleColor:COLORRGB(0x31ce32) forState:(UIControlStateSelected)];
    [self.huodongBtn setImage:[UIImage imageNamed:@"下拉拷贝2"] forState:UIControlStateNormal];
    [self.huodongBtn setImage:[UIImage imageNamed:@"下拉"] forState:UIControlStateSelected];
    self.huodongBtn.titleLabel.font = kFont(10);
    [self.huodongBtn addTarget:self action:@selector(clickhuodong:) forControlEvents:(UIControlEventTouchUpInside)];
    
    UIView *bottomView = [[UIView alloc]init];
    [view addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(view);
        make.top.equalTo(topView.mas_bottom);
    }];
    bottomView.backgroundColor = COLORRGB(0xececec);
    
    
    UILabel *danshuLabel = [[UILabel alloc]init];
    [bottomView addSubview:danshuLabel];
    [danshuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomView.mas_left).offset(10);
        make.centerY.equalTo(bottomView.mas_centerY);
    }];
    danshuLabel.text = [NSString stringWithFormat:@"%@单",self.count];
    danshuLabel.textColor = [UIColor blackColor];
    danshuLabel.textAlignment = 0;
    danshuLabel.font = kFontNameSize(15);
    
    UILabel *yingyeeLabel = [[UILabel alloc]init];
    [bottomView addSubview:yingyeeLabel];
    [yingyeeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(danshuLabel.mas_right).offset(18);
        make.centerY.equalTo(bottomView.mas_centerY);
    }];
    yingyeeLabel.text = [NSString stringWithFormat:@"营业额¥%@",self.yingYeEStr];
    yingyeeLabel.textColor = [UIColor blackColor];
    yingyeeLabel.textAlignment = 0;
    yingyeeLabel.font = kFontNameSize(15);
    
    UILabel *junjiaLabel = [[UILabel alloc]init];
    [bottomView addSubview:junjiaLabel];
    [junjiaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bottomView.mas_right).offset(-10);
        make.centerY.equalTo(bottomView.mas_centerY);
    }];
    junjiaLabel.text = [NSString stringWithFormat:@"单均价¥%@",self.junPriceStr];
    junjiaLabel.textColor = [UIColor blackColor];
    junjiaLabel.textAlignment = 2;
    junjiaLabel.font = kFontNameSize(15);
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 80;
}


-(void)clickshijian:(UIButton *)btn
{
    NSLog(@"点击");
    self.shijianBtn.selected = YES;
    self.dingdanBtn.selected = NO;
    self.shijianduanBtn.selected = NO;
    self.huodongBtn.selected = NO;
    
    
    __weak typeof(self) weakSelf = self;
    [UICustomDatePicker showCustomDatePickerAtView:self.view choosedDateBlock:^(NSDate *date) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *strDate = [dateFormatter stringFromDate:date];
        weakSelf.time_ymd = strDate;
        [btn setTitle:strDate forState:UIControlStateNormal];
        [self getShopData];
    } cancelBlock:^{
        
    }];
}

-(void)clickdingdan:(UIButton *)btn
{
    self.shijianBtn.selected = NO;
    self.dingdanBtn.selected = YES;
    self.shijianduanBtn.selected = NO;
    self.huodongBtn.selected = NO;
    
    
    NSArray * str  = @[@"已完成",@"未完成"];
    zySheetPickerView *pickerView = [zySheetPickerView ZYSheetStringPickerWithTitle:str andHeadTitle:@"订单状态" Andcall:^(zySheetPickerView *pickerView, NSString *choiceString) {
        //1已完成 2未完成 3留着备用
        
        if ([choiceString isEqualToString:@"已完成"]) {
            self.order_status = @"1";
        } else if ([choiceString isEqualToString:@"未完成"]){
            self.order_status = @"2";
        }
        self.order_statusStr = choiceString;
        
        [self getShopData];
        [pickerView dismissPicker];
    }];
    [pickerView show];
}

-(void)clickshijianduan:(UIButton *)btn
{
    self.shijianBtn.selected = NO;
    self.dingdanBtn.selected = NO;
    self.shijianduanBtn.selected = YES;
    self.huodongBtn.selected = NO;
    
    
    self.timeView.hidden  = NO;
}

-(void)clickhuodong:(UIButton *)btn
{
    self.shijianBtn.selected = NO;
    self.dingdanBtn.selected = NO;
    self.shijianduanBtn.selected = NO;
    self.huodongBtn.selected = YES;
    
    NSArray * str  = @[@"满减",@"减免配送费",@"折扣",@"新客立减",@"满赠",@"第二份半价",@"买赠",@"满减运费",@"下单返券"];
    zySheetPickerView *pickerView = [zySheetPickerView ZYSheetStringPickerWithTitle:str andHeadTitle:@"订单状态" Andcall:^(zySheetPickerView *pickerView, NSString *choiceString) {
        //0满减 1减免配送费 2折扣 3新客立减 4满赠 5第二份半价 6买赠 7满减运费 8下单返券
        
        if ([choiceString isEqualToString:@"满减"]) {
            self.type = @"0";
        } else if ([choiceString isEqualToString:@"减免配送费"]){
            self.type = @"1";
        } else if ([choiceString isEqualToString:@"折扣"]){
            self.type = @"2";
        } else if ([choiceString isEqualToString:@"新客立减"]){
            self.type = @"3";
        } else if ([choiceString isEqualToString:@"满赠"]){
            self.type = @"4";
        } else if ([choiceString isEqualToString:@"第二份半价"]){
            self.type = @"5";
        } else if ([choiceString isEqualToString:@"买赠"]){
            self.type = @"6";
        } else if ([choiceString isEqualToString:@"满减运费"]){
            self.type = @"7";
        } else if ([choiceString isEqualToString:@"下单返券"]){
            self.type = @"8";
        }
        self.typeStr = choiceString;
        [self getShopData];
        [pickerView dismissPicker];
    }];
    [pickerView show];
    
}


- (void)pickerStaTime:(NSString *)staTime endTime:(NSString *)endTime {
    
    self.begTime_hour = staTime;
    self.endTime_hour = endTime;
    [self.shijianduanBtn setTitle:staTime forState:UIControlStateNormal];
    [self getShopData];
}


#pragma mark -UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];//关闭键盘
    
    [self getShopData];
    
    
    return YES;
}
#pragma mark - 数据请求
- (void)getShopData{
    
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{
                          @"ru_id":[UserModel sharedInstanced].ru_Id,
                          @"ince":@"busine_data_list",
                          @"search_key":self.searchTextFiled.text,
                          @"time_ymd":self.time_ymd,
                          @"time_hour_start":self.begTime_hour,
                          @"order_status":self.order_status,//订单状态 1已完成 2未完成 3留着备用
                          @"time_hour_end":self.endTime_hour,
                          @"type":self.type,//活动类型 0满减 1减免配送费 2折扣 3新客立减 4满赠 5第二份半价 6买赠 7满减运费 8下单返券
                          };
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",BaseUrl,@"busine_data_list"];
    [mgr POST:URL parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            [self.mainArray removeAllObjects];
            
            self.count = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"count"]];
            self.yingYeEStr = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"turnover"]];
            self.junPriceStr = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"math"]];
            
            self.mainArray = [FinancaialOrderModel mj_objectArrayWithKeyValuesArray:[responseObject objectForKey:@"list"]];
            [self.tableView reloadData];
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        [self hidHUD:@"网络异常"];
    }];
    
}

#pragma mark - init
- (UITextField *)searchTextFiled
{
    if (!_searchTextFiled) {
        _searchTextFiled = [[UITextField alloc]init];
        _searchTextFiled.backgroundColor = [UIColor clearColor];
        //        _searchTextFiled.backgroundColor = getColor(@"F3F4F7");
        _searchTextFiled.font = [UIFont systemFontOfSize:14];
        _searchTextFiled.textColor = getColor(textColor);
        _searchTextFiled.textAlignment = NSTextAlignmentLeft;
        _searchTextFiled.placeholder = @"搜索商品名、商品编号、商品订单号";
        _searchTextFiled.delegate = self;
        _searchTextFiled.leftViewMode = UITextFieldViewModeAlways;
        _searchTextFiled.leftView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_search_small.png"]];
        _searchTextFiled.keyboardType = UIKeyboardTypeWebSearch;
    }
    return _searchTextFiled;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
