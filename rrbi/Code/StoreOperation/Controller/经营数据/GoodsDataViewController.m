//
//  GoodsDataViewController.m
//  rrbi
//
//  Created by 徐浩 on 2019/5/5.
//

#import "GoodsDataViewController.h"
#import "ProductDataListViewController.h"
#import "UIView+VC.h"
@interface GoodsDataViewController ()
//数量
@property (strong, nonatomic) UILabel *todayNumLabel;
//数量
@property (strong, nonatomic) UILabel *yesterdayNumLabel;
//金额
@property (strong, nonatomic) UILabel *todayMoneyLabel;
//金额
@property (strong, nonatomic) UILabel *yesterdayMoneyLabel;
//盈利
@property (strong, nonatomic) UILabel *todayProfitLabel;
//盈利
@property (strong, nonatomic) UILabel *yesterdayProfitLabel;
@end

@implementation GoodsDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getData];
    
}
-(void) createUI
{
    self.view.backgroundColor = COLORRGB(0xececec);
    
    UIView *topView = [[UIView alloc]init];
    [self.view addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.view);
        make.height.mas_equalTo(102);
    }];
    topView.backgroundColor = COLORRGB(0x3bad6a);
    
    UILabel *todayNum = [[UILabel alloc]init];
    [topView addSubview:todayNum];
    [todayNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(topView);
        make.top.equalTo(topView.mas_top).offset(25);
        make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
    }];
    todayNum.textColor = COLORRGB(0xffffff);
    todayNum.font = kFontNameSize(12);
    todayNum.textAlignment = 1;
    todayNum.text = @"今日销售数量";
    
    self.todayNumLabel = [[UILabel alloc]init];
    [topView addSubview:self.todayNumLabel];
    [self.todayNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(topView);
        make.top.equalTo(todayNum.mas_bottom);
        make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
    }];
    self.todayNumLabel.textColor = COLORRGB(0xffffff);
    self.todayNumLabel.font = kFontNameSize(12);
    self.todayNumLabel.textAlignment = 1;
    self.todayNumLabel.text = @"60件商品";
    
    self.yesterdayNumLabel = [[UILabel alloc]init];
    [topView addSubview:self.yesterdayNumLabel];
    [self.yesterdayNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(topView);
        make.top.equalTo(self.todayNumLabel.mas_bottom).offset(9);
        make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
    }];
    self.yesterdayNumLabel.textColor = COLORRGB(0xcfcfcf);
    self.yesterdayNumLabel.font = kFontNameSize(11);
    self.yesterdayNumLabel.textAlignment = 1;
    self.yesterdayNumLabel.text = @"昨日80件";
    
    UILabel *shuLabel = [[UILabel alloc]init];
    [topView addSubview:shuLabel];
    [shuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.todayNumLabel.mas_right);
        make.top.equalTo(todayNum);
        make.bottom.equalTo(self.yesterdayNumLabel.mas_bottom);
        make.width.mas_equalTo(1);
    }];
    shuLabel.backgroundColor = COLORRGB(0x666666);
    
    UILabel *todayMoney = [[UILabel alloc]init];
    [topView addSubview:todayMoney];
    [todayMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shuLabel.mas_right);
        make.top.equalTo(topView.mas_top).offset(25);
        make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
    }];
    todayMoney.textColor = COLORRGB(0xffffff);
    todayMoney.font = kFontNameSize(12);
    todayMoney.textAlignment = 1;
    todayMoney.text = @"今日销售金额";
    
    self.todayMoneyLabel = [[UILabel alloc]init];
    [topView addSubview:self.todayMoneyLabel];
    [self.todayMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shuLabel.mas_right);
        make.top.equalTo(todayMoney.mas_bottom);
        make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
    }];
    self.todayMoneyLabel.textColor = COLORRGB(0xffffff);
    self.todayMoneyLabel.font = kFontNameSize(12);
    self.todayMoneyLabel.textAlignment = 1;
    self.todayMoneyLabel.text = @"332.00";
    
    self.yesterdayMoneyLabel = [[UILabel alloc]init];
    [topView addSubview:self.yesterdayMoneyLabel];
    [self.yesterdayMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shuLabel.mas_right);
        make.top.equalTo(self.todayMoneyLabel.mas_bottom).offset(9);
        make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
    }];
    self.yesterdayMoneyLabel.textColor = COLORRGB(0xcfcfcf);
    self.yesterdayMoneyLabel.font = kFontNameSize(11);
    self.yesterdayMoneyLabel.textAlignment = 1;
    self.yesterdayMoneyLabel.text = @"昨日111.00";
    
    UILabel *shuLabel1 = [[UILabel alloc]init];
    [topView addSubview:shuLabel1];
    [shuLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.todayMoneyLabel.mas_right);
        make.top.equalTo(todayMoney);
        make.bottom.equalTo(self.yesterdayNumLabel.mas_bottom);
        make.width.mas_equalTo(1);
    }];
    shuLabel1.backgroundColor = COLORRGB(0x666666);
    
    UILabel *todayProfit = [[UILabel alloc]init];
    [topView addSubview:todayProfit];
    [todayProfit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shuLabel1.mas_right);
        make.top.equalTo(topView.mas_top).offset(25);
        make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
    }];
    todayProfit.textColor = COLORRGB(0xffffff);
    todayProfit.font = kFontNameSize(12);
    todayProfit.textAlignment = 1;
    todayProfit.text = @"今日利润";
    
    self.todayProfitLabel = [[UILabel alloc]init];
    [topView addSubview:self.todayProfitLabel];
    [self.todayProfitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shuLabel1.mas_right);
        make.top.equalTo(todayProfit.mas_bottom);
        make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
    }];
    self.todayProfitLabel.textColor = COLORRGB(0xffffff);
    self.todayProfitLabel.font = kFontNameSize(12);
    self.todayProfitLabel.textAlignment = 1;
    self.todayProfitLabel.text = @"332.00";
    
    self.yesterdayProfitLabel = [[UILabel alloc]init];
    [topView addSubview:self.yesterdayProfitLabel];
    [self.yesterdayProfitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shuLabel1.mas_right);
        make.top.equalTo(self.todayProfitLabel.mas_bottom).offset(9);
        make.width.mas_equalTo(SCREEN_WIDTH / 3 - 1);
    }];
    self.yesterdayProfitLabel.textColor = COLORRGB(0xcfcfcf);
    self.yesterdayProfitLabel.font = kFontNameSize(11);
    self.yesterdayProfitLabel.textAlignment = 1;
    self.yesterdayProfitLabel.text = @"昨日111.00";
    
    
    UIButton *moreBtn = [[UIButton alloc]init];
    [self.view addSubview:moreBtn];
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-11);
        make.top.equalTo(topView.mas_bottom).offset(22);
        make.size.mas_equalTo(CGSizeMake(80, 20));
    }];
    [moreBtn setTitle:@"更多数据" forState:(UIControlStateNormal)];
    [moreBtn setTitleColor:COLORRGB(0x666666) forState:(UIControlStateNormal)];
    moreBtn.titleLabel.font = kFont(14);
    [moreBtn setImage:[UIImage imageNamed:@"store_operation_enter"] forState:(UIControlStateNormal)];
    CGFloat leftlab = [@"更多数据" sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]}].width;
    [moreBtn setImageEdgeInsets:UIEdgeInsetsMake(0, leftlab, 0, 0)];
    [moreBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -40, 0, 0)];
    [moreBtn addTarget:self action:@selector(clickMoreBtn:) forControlEvents:(UIControlEventTouchUpInside)];
    
    [self getData];
}

-(void)clickMoreBtn:(UIButton *)btn
{
    ProductDataListViewController  * controller = [[ProductDataListViewController alloc]init];
    controller.hidesBottomBarWhenPushed = YES;
    [self.view.superview.superview.superview.viewController.navigationController pushViewController:controller animated:YES];
    NSLog(@"点击更多数据");
}
- (void)getData {
    
    
    AFHTTPSessionManager *mgr=[AFHTTPSessionManager manager];
    NSDictionary* arg = @{
                          @"ru_id":[UserModel sharedInstanced].ru_Id,
                          @"ince":@"management_data",
                          @"status":@"2",
                          };
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",BaseUrl,@"management_data"];
    [mgr POST:URL parameters:arg progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [WMHelper outPutJsonString:responseObject];
        
        NSInteger flag = [[responseObject objectForKey:@"ret"]integerValue];
        if(flag == 200){
            self.todayNumLabel.text = [NSString stringWithFormat:@"%@件商品",[responseObject objectForKey:@"todayGoodsnumber"]];
            self.yesterdayNumLabel.text = [NSString stringWithFormat:@"昨日%@件",[responseObject objectForKey:@"yesterGoodsnumber"]];
            self.todayMoneyLabel.text = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"todayMoney"]];
            self.yesterdayMoneyLabel.text = [NSString stringWithFormat:@"昨日%@",[responseObject objectForKey:@"yesterMoney"]];
            self.todayProfitLabel.text = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"todayProfit"]];
            self.yesterdayProfitLabel.text = [NSString stringWithFormat:@"昨日%@",[responseObject objectForKey:@"yesterProfit"]];
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)  {
        NSLog(@"%@",error);
        [self hidHUD:@"网络异常"];
    }];
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
