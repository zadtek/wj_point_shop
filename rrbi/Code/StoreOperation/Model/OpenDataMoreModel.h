//
//  OpenDataMoreModel.h
//  rrbi
//
//  Created by 徐浩 on 2019/5/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenDataMoreModel : NSObject
//时间
@property (copy, nonatomic) NSString *time;
//状态
@property (copy, nonatomic) NSString *status;
//商品价格
@property (copy, nonatomic) NSString *goods_price;
//包装
@property (copy, nonatomic) NSString *packing_price;
//配送
@property (copy, nonatomic) NSString *delivery_price;
//预计收入
@property (copy, nonatomic) NSString *estimated_revenue;
//平台补贴
@property (copy, nonatomic) NSString *platform_subsidy;
//商家补贴
@property (copy, nonatomic) NSString *business_subsidy;
//订单时长
@property (copy, nonatomic) NSString *order_duration;
//完成时间
@property (copy, nonatomic) NSString *completion_time;
//活动类型
@property (copy, nonatomic) NSString *activity_type;
//订单编号
@property (assign, nonatomic) long long order_number;
//流水号
@property (assign, nonatomic) long long liushui;
@end

NS_ASSUME_NONNULL_END
