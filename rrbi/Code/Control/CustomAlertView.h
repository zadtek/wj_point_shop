//
//  CustomAlertView.h
//  LlkjProject
//
//  Created by Liuliankeji on 2018/7/10.
//  Copyright © 2018年 llkj. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^AlertResult)(NSString *fullPrice,NSString *cutPrice);

typedef void(^AlertTextViewResult)(NSString *textStr);

@interface CustomAlertView : UIView


@property (nonatomic,copy) AlertResult resultIndex;

@property (nonatomic,copy) AlertTextViewResult textBlock;

@property (nonatomic, copy) NSString * titleString;

- (instancetype)initAlert;


- (instancetype)initTextViewAlert;

- (instancetype)initMoneyAlert;

- (void)showAlertView;



@end
