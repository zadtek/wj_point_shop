//
//  CustomAlertView.m
//  LlkjProject
//
//  Created by Liuliankeji on 2018/7/10.
//  Copyright © 2018年 llkj. All rights reserved.
//

#import "CustomAlertView.h"

///alertView  宽
#define AlertW 280/WIDTH_6S_SCALE
#define AlertH 170/HEIGHT_6S_SCALE
///各个栏目之间的距离
#define XLSpace 25/WIDTH_6S_SCALE
#define BtnSpace 20/WIDTH_6S_SCALE
#define TF_W (AlertW - XLSpace*3)/2



@interface CustomAlertView()<UITextFieldDelegate,UITextViewDelegate>

//弹窗
@property (nonatomic,strong) UIView *alertView;

@property (nonatomic,strong) UIImageView *redImageView;

//title
@property (nonatomic,strong) UILabel *titleLbl;

@property (nonatomic,strong) UILabel *messageLabel;

//确认按钮
@property (nonatomic,strong) UIButton *sureBtn;
//取消按钮
@property (nonatomic,strong) UIButton *cancleBtn;

@property (nonatomic, strong) UIButton *off_btn;

@property (nonatomic,strong) UITextField *priceTF;

@property (nonatomic,strong) UITextField *priceCutTF;


@property (nonatomic,strong) UITextView *textView;

@property (nonatomic,assign) NSInteger showType;
@property (nonatomic,assign) BOOL isHaveDian;
@end


@implementation CustomAlertView

- (instancetype)initAlert{
    if (self == [super init]) {
        
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
        
        self.alertView = [[UIView alloc] init];
        self.alertView.backgroundColor = [UIColor whiteColor];
        self.alertView.layer.cornerRadius = 10.0;
        [self addSubview:self.alertView];
//        self.alertView.frame = CGRectMake(0, 0, AlertW, AlertH);
//        self.alertView.layer.position = self.center;
        [self.alertView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(AlertW, AlertH));
        }];
        
        
        self.redImageView = [[UIImageView alloc]init ];
        self.redImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.redImageView.image = [UIImage imageNamed:@"icon_lzh_people"];
        [self.alertView addSubview:self.redImageView];
        
        [self.redImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.alertView).offset(-35);
            make.left.equalTo(self.alertView).offset(-18);
            make.size.mas_equalTo(CGSizeMake(66, 80));
        }];
        
        
        self.off_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.off_btn setImage:[UIImage imageNamed:@"icon_lzh_off"] forState:(UIControlStateNormal)];
        [self.alertView addSubview:self.off_btn];
        [self.off_btn addTarget:self action:@selector(singleTapAction) forControlEvents:(UIControlEventTouchUpInside)];
        [self.off_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.equalTo(self.alertView);
            make.size.mas_equalTo(CGSizeMake(40, 40));
        }];
        
        
        self.titleLbl = [[UILabel alloc] init];
        self.titleLbl.text = @"满";
        self.titleLbl.textAlignment = NSTextAlignmentCenter;
        self.titleLbl.font = kFontNameSize(14);
        [self.alertView addSubview:self.titleLbl];
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.alertView).mas_offset(-20);
            make.height.mas_equalTo(40);
            make.left.equalTo(self.alertView);
            make.width.mas_equalTo(XLSpace);
            
        }];
        
        
        _priceTF = [[UITextField alloc] init];
        [_priceTF setFont:kFontNameSize(14)];
        _priceTF.placeholder=@"金额";
        _priceTF.borderStyle = UITextBorderStyleRoundedRect;
        _priceTF.delegate = self;
        _priceTF.textAlignment = NSTextAlignmentCenter;
        _priceTF.keyboardType = UIKeyboardTypeNumberPad;
        _priceTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self.alertView addSubview:_priceTF];
        [self.priceTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.height.equalTo(self.titleLbl);
            make.width.mas_equalTo(TF_W);
            make.left.equalTo(self.titleLbl.mas_right);

        }];

       
        
        
       
        
        self.messageLabel = [[UILabel alloc] init];
        self.messageLabel.text = @"减";
        self.messageLabel.textAlignment = NSTextAlignmentCenter;
        self.messageLabel.font = kFontNameSize(14);
        [self.alertView addSubview:self.messageLabel];
        [self.messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.width.equalTo(self.titleLbl);
            make.left.equalTo(self.priceTF.mas_right);
            
        }];
        
        _priceCutTF = [[UITextField alloc] init];
        _priceCutTF.borderStyle = UITextBorderStyleRoundedRect;
        [_priceCutTF setFont:kFontNameSize(14)];
        _priceCutTF.placeholder=@"金额";
        _priceCutTF.delegate = self;
        _priceCutTF.textAlignment = NSTextAlignmentCenter;
        _priceCutTF.keyboardType = UIKeyboardTypeNumberPad;
        _priceCutTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self.alertView addSubview:_priceCutTF];
        [self.priceCutTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.width.equalTo(self.priceTF);
            make.left.equalTo(self.messageLabel.mas_right);

        }];
        
    

        self.sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.sureBtn.backgroundColor = getColor(greenBackColor);
        [self.sureBtn setTitle:@"确认" forState:UIControlStateNormal];
        self.sureBtn.titleLabel.font = kFontNameSize(14);
        [self.sureBtn addTarget:self action:@selector(buttonEvent:) forControlEvents:UIControlEventTouchUpInside];
        self.sureBtn.adjustsImageWhenHighlighted = NO;
        self.sureBtn.layer.cornerRadius = 5.0;
        [self.alertView addSubview:self.sureBtn];




        //两个按钮
        self.cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.cancleBtn setTitle:@"取消" forState:UIControlStateNormal];
        self.cancleBtn.backgroundColor =[UIColor lightGrayColor];
//        [self.cancleBtn setTitleColor:  forState:UIControlStateNormal];
        self.cancleBtn.titleLabel.font = kFontNameSize(14);
        [self.cancleBtn addTarget:self action:@selector(buttonEvent:) forControlEvents:UIControlEventTouchUpInside];
        self.cancleBtn.adjustsImageWhenHighlighted = NO;
        self.cancleBtn.layer.cornerRadius = 5.0;
        [self.alertView addSubview:self.cancleBtn];


        [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.width.equalTo(self.priceTF);
            make.height.mas_equalTo(30);
            make.bottom.equalTo(self.alertView.mas_bottom).mas_offset(-BtnSpace);
            
        }];
        
        [self.cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.width.equalTo(self.sureBtn);
            make.left.equalTo(self.priceCutTF);
            
        }];
    
        self.sureBtn.tag = 1;
        self.cancleBtn.tag = 2;
        self.showType = 1;
    }
    
    return self;
}

- (instancetype)initTextViewAlert{
    if (self == [super init]) {
        
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
        
        self.alertView = [[UIView alloc] init];
        self.alertView.backgroundColor = [UIColor whiteColor];
        self.alertView.layer.cornerRadius = 10.0;
        [self addSubview:self.alertView];
        [self.alertView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(AlertW, 220/HEIGHT_6S_SCALE));
        }];
        
        
        self.redImageView = [[UIImageView alloc]init ];
        self.redImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.redImageView.image = [UIImage imageNamed:@"icon_lzh_people"];
        [self.alertView addSubview:self.redImageView];
        
        [self.redImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.alertView).offset(-35);
            make.left.equalTo(self.alertView).offset(-18);
            make.size.mas_equalTo(CGSizeMake(66, 80));
        }];
        
        
        self.off_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.off_btn setImage:[UIImage imageNamed:@"icon_lzh_off"] forState:(UIControlStateNormal)];
        [self.alertView addSubview:self.off_btn];
        [self.off_btn addTarget:self action:@selector(singleTapAction) forControlEvents:(UIControlEventTouchUpInside)];
        [self.off_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.equalTo(self.alertView);
            make.size.mas_equalTo(CGSizeMake(40, 40));
        }];
        
        
        self.titleLbl = [[UILabel alloc] init];
        self.titleLbl.textAlignment = NSTextAlignmentCenter;
        self.titleLbl.font = kFontNameSize(16);
        self.titleLbl.text = @"请输入拒绝理由";
        [self.alertView addSubview:self.titleLbl];
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.alertView);
            make.height.mas_equalTo(40);
            make.left.right.equalTo(self.alertView);
            
        }];
        
        
        
        
        _textView = [[UITextView alloc] init]; //初始化大小并自动释放
        _textView.textColor = [UIColor blackColor];//设置字体颜色
        _textView.font =  kFontNameSize(13);;//设置 字体 和 大小
        _textView.delegate = self;// 设置控制器为 textView 的代理方法
        _textView.backgroundColor = [UIColor groupTableViewBackgroundColor];//设置它的背景颜色
        _textView.layer.cornerRadius = 5;
        [self.alertView addSubview:_textView];
               
        
        
        
        self.sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.sureBtn.backgroundColor = getColor(greenBackColor);
        [self.sureBtn setTitle:@"确认" forState:UIControlStateNormal];
        self.sureBtn.titleLabel.font = kFontNameSize(14);
        [self.sureBtn addTarget:self action:@selector(textViewbuttonEvent:) forControlEvents:UIControlEventTouchUpInside];
        self.sureBtn.adjustsImageWhenHighlighted = NO;
        self.sureBtn.layer.cornerRadius = 5.0;
        [self.alertView addSubview:self.sureBtn];
        
        
        
        
        //两个按钮
        self.cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.cancleBtn setTitle:@"取消" forState:UIControlStateNormal];
        self.cancleBtn.backgroundColor =[UIColor lightGrayColor];
        //        [self.cancleBtn setTitleColor:  forState:UIControlStateNormal];
        self.cancleBtn.titleLabel.font = kFontNameSize(14);
        [self.cancleBtn addTarget:self action:@selector(textViewbuttonEvent:) forControlEvents:UIControlEventTouchUpInside];
        self.cancleBtn.adjustsImageWhenHighlighted = NO;
        self.cancleBtn.layer.cornerRadius = 5.0;
        [self.alertView addSubview:self.cancleBtn];
        
        float btn_w = (AlertW- 60)/2;
        [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.alertView).offset(20);
            make.width.mas_equalTo(btn_w);
            make.height.mas_equalTo(30);
            make.bottom.equalTo(self.alertView.mas_bottom).mas_offset(-BtnSpace);
            
        }];
        
        [self.cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.width.equalTo(self.sureBtn);
            make.right.equalTo(self.alertView).offset(-20);
            
        }];
        
        [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLbl.mas_bottom);
            make.bottom.equalTo(self.sureBtn.mas_top).offset(-20);
            make.left.equalTo(self.sureBtn);
            make.right.equalTo(self.cancleBtn);

        }];
        
        
        
        self.sureBtn.tag = 1;
        self.cancleBtn.tag = 2;
        
    }
    
    return self;
    
}

- (instancetype)initMoneyAlert{
    if (self == [super init]) {
        
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
        
        self.alertView = [[UIView alloc] init];
        self.alertView.backgroundColor = [UIColor whiteColor];
        self.alertView.layer.cornerRadius = 10.0;
        [self addSubview:self.alertView];
        //        self.alertView.frame = CGRectMake(0, 0, AlertW, AlertH);
        //        self.alertView.layer.position = self.center;
        [self.alertView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(AlertW, AlertH));
        }];
        
        
        self.redImageView = [[UIImageView alloc]init ];
        self.redImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.redImageView.image = [UIImage imageNamed:@"icon_lzh_people"];
        [self.alertView addSubview:self.redImageView];
        
        [self.redImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.alertView).offset(-35);
            make.left.equalTo(self.alertView).offset(-18);
            make.size.mas_equalTo(CGSizeMake(66, 80));
        }];
        
        
        self.off_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.off_btn setImage:[UIImage imageNamed:@"icon_lzh_off"] forState:(UIControlStateNormal)];
        [self.alertView addSubview:self.off_btn];
        [self.off_btn addTarget:self action:@selector(singleTapAction) forControlEvents:(UIControlEventTouchUpInside)];
        [self.off_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.equalTo(self.alertView);
            make.size.mas_equalTo(CGSizeMake(40, 40));
        }];
        
        self.titleLbl = [[UILabel alloc] init];
        self.titleLbl.text = @"退款金额";
        self.titleLbl.textAlignment = NSTextAlignmentCenter;
        self.titleLbl.font = kFontNameSize(14);
        [self.alertView addSubview:self.titleLbl];
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.alertView).mas_offset(-20);
            make.height.mas_equalTo(40);
            make.left.equalTo(self.alertView).mas_offset(20);
//            make.width.mas_equalTo(XLSpace);

        }];
        
        _priceTF = [[UITextField alloc] init];
        [_priceTF setFont:kFontNameSize(14)];
        _priceTF.placeholder=@"请输入退款金额";
        _priceTF.borderStyle = UITextBorderStyleRoundedRect;
        _priceTF.delegate = self;
        _priceTF.textAlignment = NSTextAlignmentLeft;
        _priceTF.keyboardType = UIKeyboardTypeDecimalPad;
        _priceTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self.alertView addSubview:_priceTF];
        [self.priceTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.height.equalTo(self.titleLbl);
//            make.width.mas_equalTo(TF_W);
            make.left.equalTo(self.titleLbl.mas_right).mas_offset(10);
            make.right.equalTo(self.alertView).mas_offset(-20);
        }];
        
        self.sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.sureBtn.backgroundColor = getColor(greenBackColor);
        [self.sureBtn setTitle:@"确认" forState:UIControlStateNormal];
        self.sureBtn.titleLabel.font = kFontNameSize(14);
        [self.sureBtn addTarget:self action:@selector(moneyButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
        self.sureBtn.adjustsImageWhenHighlighted = NO;
        self.sureBtn.layer.cornerRadius = 5.0;
        [self.alertView addSubview:self.sureBtn];
        
        
        
        
        //两个按钮
        self.cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.cancleBtn setTitle:@"取消" forState:UIControlStateNormal];
        self.cancleBtn.backgroundColor =[UIColor lightGrayColor];
        //        [self.cancleBtn setTitleColor:  forState:UIControlStateNormal];
        self.cancleBtn.titleLabel.font = kFontNameSize(14);
        [self.cancleBtn addTarget:self action:@selector(moneyButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
        self.cancleBtn.adjustsImageWhenHighlighted = NO;
        self.cancleBtn.layer.cornerRadius = 5.0;
        [self.alertView addSubview:self.cancleBtn];
        
        
        [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.alertView).mas_offset(20);
            make.width.mas_offset(TF_W);
            make.height.mas_equalTo(30);
            make.bottom.equalTo(self.alertView.mas_bottom).mas_offset(-BtnSpace);
            
        }];
        
        [self.cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.width.equalTo(self.sureBtn);
            make.right.equalTo(self.alertView).mas_offset(-20);
//            make.left.equalTo(self.priceCutTF);
            
        }];
        
        self.sureBtn.tag = 1;
        self.cancleBtn.tag = 2;
        
    }
    
    return self;
}

- (void)setTitleString:(NSString *)titleString{
    _titleString = titleString;
    self.titleLbl.text = titleString;
}

#pragma mark - 弹出 -
- (void)showAlertView
{
    UIWindow *rootWindow = [UIApplication sharedApplication].keyWindow;
    [rootWindow addSubview:self];
    [self creatShowAnimation];
}

- (void)creatShowAnimation
{
    self.alertView.layer.position = self.center;
    self.alertView.transform = CGAffineTransformMakeScale(0.90, 0.90);
    [UIView animateWithDuration:0.25 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
        self.alertView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    } completion:^(BOOL finished) {
    }];
    
}

//点击事件
-(void)singleTapAction
{
    //具体的实现
    [self removeFromSuperview];
}

#pragma mark - 回调 -设置只有1  -- > 确定才回调
- (void)moneyButtonEvent:(UIButton *)sender
{
    
    if (sender.tag == 1) {
        
        if (self.textBlock) {
            self.textBlock(self.priceTF.text);
        }
        [self removeFromSuperview];
        
    }else{
        
        [self removeFromSuperview];
        
    }
    
}

#pragma mark - 回调 -设置只有1  -- > 确定才回调
- (void)textViewbuttonEvent:(UIButton *)sender
{
    
    if (sender.tag == 1) {
        
        if (self.textBlock) {
            self.textBlock(self.textView.text);
        }
        [self removeFromSuperview];
        
    }else{
        
        [self removeFromSuperview];
        
    }
    
}
#pragma mark - 回调 -设置只有1  -- > 确定才回调
- (void)buttonEvent:(UIButton *)sender
{
    
    if (sender.tag == 1) {
        
        if (self.priceTF.text.length !=0 && self.priceCutTF.text.length !=0) {
            if (self.resultIndex) {
                self.resultIndex(self.priceTF.text,self.priceCutTF.text);
            }
            [self removeFromSuperview];

        }else{
            [self makeToast:@"满减数值不能为空" duration:1 position:CSToastPositionCenter];
        }
      
    }else{
        
        [self removeFromSuperview];

    }
    
}

//限制输入
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (string.length != 0 ){
        //限制输入
        if (self.showType == 1) {
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
            NSString*filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            BOOL basicTest = [string isEqualToString:filtered];
            if (!basicTest) {
                return NO;
            }else{
                return YES;
            }
        }else{
            // 判断是否有小数点
            if ([textField.text containsString:@"."]) {
                self.isHaveDian = YES;
            }else{
                self.isHaveDian = NO;
            }
            if (string.length > 0) {
                //当前输入的字符
                unichar single = [string characterAtIndex:0];
                // 不能输入.0-9以外的字符
                if (!((single >= '0' && single <= '9') || single == '.')) return NO;
                // 只能有一个小数点
                if (self.isHaveDian && single == '.') return NO;
                // 如果第一位是.则前面加上0.
                if ((textField.text.length == 0) && (single == '.')) {
                    textField.text = @"0";
                }
                // 如果第一位是0则后面必须输入点，否则不能输入。
                if ([textField.text hasPrefix:@"0"]) {
                    if (textField.text.length > 1) {
                        NSString *secondStr = [textField.text substringWithRange:NSMakeRange(1, 1)];
                        if (![secondStr isEqualToString:@"."]) return NO;
                    }else{
                        if (![string isEqualToString:@"."]) return NO;
                    }
                }
                // 小数点后最多能输入两位
                if (self.isHaveDian) {
                    NSRange ran = [textField.text rangeOfString:@"."];
                    // 由于range.location是NSUInteger类型的，所以这里不能通过(range.location - ran.location)>2来判断
                    if (range.location > ran.location) {
                        if ([textField.text pathExtension].length > 1) return NO;
                    }
                }
            }
        }
    }
    return YES;
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if([text isEqualToString:@"\n"]){
        
        [textView resignFirstResponder];
        
        return NO;
    }
    
    return YES;

}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
