//
//  Config.h
//  TianjinBoHai
//
//  Created by 李莹 on 15/1/16.
//  Copyright (c) 2015年 Binky Lee. All rights reserved.
//

#import <UIKit/UIKit.h>


#pragma mark --------log

#define VOTETEXTCOLOR [UIColor colorWithRed:0.47 green:0.47 blue:0.47 alpha:1]
// 颜色
#define kMyColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
//通用字号
#define DEF_FontSize_26 [UIFont fontSize:26]
#define DEF_FontSize_18 [UIFont fontSize:18]
#define DEF_FontSize_17 [UIFont fontSize:17]
#define DEF_FontSize_16 [UIFont fontSize:16]
#define DEF_FontSize_15 [UIFont fontSize:15]
#define DEF_FontSize_14 [UIFont fontSize:14]
#define DEF_FontSize_13 [UIFont fontSize:13]
#define DEF_FontSize_12 [UIFont fontSize:12]
#define DEF_FontSize_11 [UIFont fontSize:11]
#define DEF_FontSize_10 [UIFont fontSize:10]
#define DEF_FontSize_9 [UIFont fontSize:9]
#define DEF_FontSize_8 [UIFont fontSize:8]

//字体
#define kFont(fontSize)           [UIFont systemFontOfSize:fontSize]
#define kFontNameSize(fontNameSize)      [UIFont fontWithName:@"PingFang-SC-Regular" size:fontNameSize]



#define Strong @property (nonatomic, strong)
#define Assign @property (nonatomic, assign)
#define Copy @property (nonatomic, copy)
//显示msg
#define ShowMessage(msg) [self.view makeToast:msg duration:1 position:CSToastPositionCenter]
#define ShowViewMessage(msg) [self  makeToast:msg duration:1 position:CSToastPositionCenter]

//屏幕宽高

//#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
//#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
/**
 根据屏幕高度 判断是否是iphoneX
 */
// 顶部宏 iPhone X：状态栏44  导航栏44  其他：状态栏20  导航栏44
#define STATUS_HEIGHT (SCREEN_HEIGHT == 812.0 ? 44 : 20)
// 顶部宏 状态栏44或者20  导航栏44
#define SafeAreaTopHeight (SCREEN_HEIGHT == 812.0 ? 88 : 64)
// 底部宏 （没有tarbar的情况） iPhone X 下面多34高度
#define SafeAreaBottomHeight (SCREEN_HEIGHT == 812.0 ? 34 : 0)
// 底部宏 （有tarbar的情况）iPhone X ：tarbar总高度 49 + 34   其他：49
#define TABBAR_HEIGHT (SCREEN_HEIGHT == 812.0 ? 83 : 49)
/**
 根据状态栏判断 iPhone X
 导航栏和状态栏的总高度
 */
#define NAVGATIONBAR_HEIGHT (CGRectGetHeight([UIApplication sharedApplication].statusBarFrame) == 20 ? 64 : 88)


#define BOUND_HEIGHT self.bounds.size.height
#define BOUND_WIDTH self.bounds.size.width

//5S宽高比例

#define ScreenBounds [UIScreen mainScreen].bounds
#define WIDTH_5S_SCALE 320.0 * [UIScreen mainScreen].bounds.size.width
#define HEIGHT_5S_SCALE 568.0 * [UIScreen mainScreen].bounds.size.height

#define WIDTH_6S_SCALE 375.0 * [UIScreen mainScreen].bounds.size.width
#define HEIGHT_6S_SCALE 667.0 * [UIScreen mainScreen].bounds.size.height
//弱应用
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

#define  minstr(a) [NSString stringWithFormat:@"%@",a]
/** 比例*/
#define kScale SCREEN_WIDTH/414.0

#define kSizeRatio(value) value/2*kScale //全局缩放比
/**
 * 一般分割线颜色
 */
extern NSString *dividerColor;

/**
 * 字体颜色
 */
extern NSString *textColor;

/**
 * 一般背景颜色
 */
extern NSString *bgColor;

/**
 *  文字按钮  白色
 */
extern NSString *whiteColor;

/**
 *  主颜色
 */
extern NSString *mainColor;

/**
 *  文字\按钮  蓝色
 */
extern NSString *blueColor;

/**
 *  文字\按钮  黑色
 */
extern NSString *blackColor;

/**
 *  背景绿色
 */
extern NSString *greenBackColor;

extern NSString *lightTextColor;


@interface Config : NSObject

UIColor* getColor(NSString * hexColor);

- (BOOL) isBlankString:(NSString *)string;
// 拼接字符串
- (NSString *) connectionString:(NSString *)connectionUrl;
+ (UIColor *)colorWithHexString: (NSString *)color WithAlpha:(float)alpha;
@end
