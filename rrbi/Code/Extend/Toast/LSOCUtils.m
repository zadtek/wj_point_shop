//
//  LSOCUtils.m
//  QFProject
//
//  Created by analysys on 16/2/16.
//  Copyright © 2016年 analysys. All rights reserved.
//

#import "LSOCUtils.h"

#define SYSTEM_FONT(fontSize) [UIFont fontWithName:@"Heiti SC" size:fontSize]

@interface LSOCUtils ()
{

}
@end

@implementation LSOCUtils



+(void)draw:(NSString*)text frame:(CGRect)rect color:(UIColor*)mycolor font:(CGFloat)myfont pargraph:(NSMutableParagraphStyle*)mypargraph
{
    [text drawInRect:rect withAttributes:[NSDictionary dictionaryWithObjectsAndKeys:SYSTEM_FONT(myfont),NSFontAttributeName ,mycolor,NSForegroundColorAttributeName,mypargraph,NSParagraphStyleAttributeName,nil]];
}


//获取当前的时间戳(秒)
+(NSString *)getNowTimeTimestamp{
    
    
    
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    
    NSTimeInterval a=[dat timeIntervalSince1970];
    
    NSString*timeString = [NSString stringWithFormat:@"%0.f", a];//转为字符型
    
    ;
    
    return timeString;
    
}
+(void)drawArrays:(NSArray*)titleArray withFrameArray:(NSArray*)frameArray color:(UIColor*)mycolor font:(CGFloat)myfont pargraph:(NSMutableParagraphStyle*)mypargraph
{
    for(int i=0;i<frameArray.count;i++)
    {
        [LSOCUtils draw:titleArray[i] frame:[frameArray[i] CGRectValue] color:mycolor font:myfont pargraph:mypargraph];
    }
}


//获取当前屏幕显示的viewcontroller
+ (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;//((QFBaseTabbarViewController*)nextResponder).selectedViewController;
    else
        result = nextResponder;//((QFBaseTabbarViewController*)window.rootViewController).selectedViewController;
    
    return result;
}

+(id) dictionaryValue:(NSDictionary*)dic forKey:(NSString*)key
{
    id value = [dic valueForKey:key];
    if(!value) return @"";
    if(value == [NSNull null]) return @"";
    if([value isKindOfClass:[NSString class]] && ([value isEqualToString:@"null"] || [value isEqualToString:@"(null)"]||[value isEqualToString:@""]) )  return @"";
    return value;
}

+(id) dictionaryNotString:(NSDictionary*)dic forKey:(NSString*)key
{
    id value = [dic valueForKey:key];
    if(value == [NSNull null]) return nil;
    if([value isKindOfClass:[NSString class]] && ([value isEqualToString:@"null"] || [value isEqualToString:@"(null)"]||[value isEqualToString:@""]) )  return nil;
    return value;
}
+(BOOL)judgeTel:(NSString*)telnumber
{
    NSString *str = @"(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", str];
    BOOL isTrue = NO;
    if([predicate evaluateWithObject:telnumber])
    {
        isTrue = YES;
    }
    else
    {
        isTrue = NO;
    }
    return isTrue;
}



+(NSString*)getSubjectsPlist:(NSInteger)type withGrade:(NSInteger)gradeTag
{
    NSString *gradeStr = @"" ;
    switch (type) {
        case 0:
        {
            NSArray *arr = @[@"一年级上",@"一年级下", @"二年级上",@"二年级下",@"三年级上",@"三年级下",@"四年级上",@"四年级下",@"五年级上",@"五年级下",@"六年级上",@"六年级下"];
            gradeStr = [NSString stringWithString:arr[gradeTag]];
            
            break;
        }
        case 1:
        {
            NSArray *arr    = @[@"初一上学期",@"初一下学期", @"初二上学期",@"初二下学期",@"初三上学期",@"初三下学期",@"初四上学期",@"初四下学期"];
            gradeStr = [NSString stringWithString:arr[gradeTag]];
            
            break;
        }
        case 2:
        {
            NSArray *arr    = @[@"高一上学期",@"高一下学期", @"高二上学期",@"高二下学期",@"高三上学期",@"高三下学期"];
            gradeStr = [NSString stringWithString:arr[gradeTag]];
            
            
        }
        default:
            break;
    }
    
    
    NSString *plistStr =[[NSBundle mainBundle]pathForResource:@"grade" ofType:@"plist"];
    //    NSArray *arr1 = [NSArray arrayWithContentsOfFile:plistStr];
    NSDictionary *ddd = [NSDictionary dictionaryWithContentsOfFile:plistStr];
    NSString *tempId = [ddd objectForKey:gradeStr];
    return tempId;
}

//+(NSMutableArray*)getAllModelPlist
//{
//    NSString *plistStr =[[NSBundle mainBundle]pathForResource:@"LSItemPlist" ofType:@"plist"];
//    NSDictionary *plistDict = [NSDictionary dictionaryWithContentsOfFile:plistStr];
//
//    NSMutableArray *array = [NSMutableArray array];
//    for(NSString *oneStr in plistDict)
//    {
//        NSDictionary *oneDict = plistDict[oneStr];
//        LSHomeModel *mm = [[LSHomeModel alloc]init];
//        mm.name = oneDict[@"title"];
//        mm.colorStr = oneDict[@"color"];
//        mm.imageName = oneDict[@"image"];
//        mm.urlString = oneDict[@"link"];
//        mm.tagId = [NSString stringWithFormat:@"%@",oneStr];
//        [array addObject:mm];
//    }
//    return array;
//}

+(unsigned long)stringColorToLong:(NSString*)string
{
    unsigned long color16 = strtoul([string UTF8String],0,16);
    return color16;
}
+(void)setNullLoctaionData
{
//    
//    [UserInfor getInstanceUser].isLogin = NO;
//    [UserInfor getInstanceUser].is_agentStr = @"";
//    [UserInfor getInstanceUser].member_citycodeStr = @"";
//    [UserInfor getInstanceUser].member_idStr = @"";
//    [UserInfor getInstanceUser].member_nameStr =@"";
//    [UserInfor getInstanceUser].member_phoneStr = @"";
//    [UserInfor getInstanceUser].member_photoStr = @"";
//    [UserInfor getInstanceUser].member_plate_full_idStr = @"";
//    [UserInfor getInstanceUser].member_sexStr = @"";
//    [UserInfor getInstanceUser].member_ty_sidStr = @"";
//    [UserInfor getInstanceUser].member_yxy_sidStr =@"";
//    [UserInfor getInstanceUser].tokenStr = @"";
//    [UserInfor getInstanceUser].is_teacherStr = @"";
//    [UserInfor getInstanceUser].grade = @"";
//    [UserInfor getInstanceUser].user_idStr = @"";
//    
//    NSLog(@"%@",[UserInfor getInstanceUser].member_yxy_sidStr);
}



//+(void)showAleart:(NSString*)message
//{
//    double version = [[UIDevice currentDevice].systemVersion doubleValue];
//    if(version>=8.0)
//    {
//         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
//        [[LSOCUtils getCurrentVC] presentViewController:alertController animated:YES completion:nil];
//    }
//    else
//    {
//
//    }
//
//
//}


+(void)show:(UIView*)view withText:(NSString*)text
{
    [view makeToast:text duration:1.0f position:CSToastPositionCenter];
}



+(void)draw:(NSString*)text frame:(CGRect)rect color:(UIColor*)mycolor font:(CGFloat)myfont withTextAlignment:(NSTextAlignment)alignment
{
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc]init];
    paragraph.alignment = alignment;
    
    [text drawInRect:rect withAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:myfont],NSFontAttributeName ,mycolor,NSForegroundColorAttributeName,paragraph,NSParagraphStyleAttributeName,nil]];
    
    
    
}

+ (UIImage *) imageCompressFitSizeScale:(UIImage *)sourceImage targetSize:(CGSize)size{
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = size.width;
    CGFloat targetHeight = size.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
    
    if(CGSizeEqualToSize(imageSize, size) == NO){
        
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if(widthFactor > heightFactor){
            scaleFactor = widthFactor;
            
        }
        else{
            
            scaleFactor = heightFactor;
        }
        scaledWidth = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        if(widthFactor > heightFactor){
            
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }else if(widthFactor < heightFactor){
            
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    UIGraphicsBeginImageContext(size);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil){
        NSLog(@"scale image fail");
    }
    
    UIGraphicsEndImageContext();
    return newImage;
}

+(void)drawBlod:(NSString*)text frame:(CGRect)rect color:(UIColor*)mycolor font:(CGFloat)myfont withTextAlignment:(NSTextAlignment)alignment
{
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc]init];
    paragraph.alignment = alignment;
    [text drawInRect:rect withAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:myfont],NSFontAttributeName ,mycolor,NSForegroundColorAttributeName,paragraph,NSParagraphStyleAttributeName,nil]];
}


+(void)setShandow:(UIView*)someview inBGView:(UIView*)bgview
{
    someview.layer.cornerRadius=8.f;
    someview.layer.masksToBounds=YES;
    
    CALayer *subLayer=[CALayer layer];
    
    CGRect fixframe=someview.layer.frame;
    
    fixframe.size.width=SCREEN_WIDTH-20;
    
    subLayer.frame=fixframe;
    
    subLayer.cornerRadius=8;
    
    subLayer.backgroundColor=[[UIColor grayColor] colorWithAlphaComponent:0.5].CGColor;
    
    subLayer.masksToBounds=NO;
    
    subLayer.shadowColor=[UIColor grayColor].CGColor;
    
    subLayer.shadowOffset=CGSizeMake(0,0);
    
    subLayer.shadowOpacity=0.5;
    
    subLayer.shadowRadius=8;
    
    [bgview.layer insertSublayer:subLayer below:someview.layer];
}



@end

