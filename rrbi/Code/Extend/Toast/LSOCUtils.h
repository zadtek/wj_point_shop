//
//  LSOCUtils.h
//
//
//  Created by  on 16/2/16.
//  Copyright © 2016年 analysys. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface LSOCUtils : NSObject




+(void)draw:(NSString*)text frame:(CGRect)rect color:(UIColor*)mycolor font:(CGFloat)myfont withTextAlignment:(NSTextAlignment)alignment;
+(void)drawBlod:(NSString*)text frame:(CGRect)rect color:(UIColor*)mycolor font:(CGFloat)myfont withTextAlignment:(NSTextAlignment)alignment;

/**
 *  绘制文字
 *
 *  @param text       文字
 *  @param rect       frame
 *  @param mycolor    颜色
 *  @param myfont     字号
 *  @param mypargraph 修饰
 */
+(void)draw:(NSString*)text frame:(CGRect)rect color:(UIColor*)mycolor font:(CGFloat)myfont pargraph:(NSMutableParagraphStyle*)mypargraph;

/*
 
 获取当前时间戳
 
 
 */

+(NSString *)getNowTimeTimestamp;

/**
 *  绘制很多行字
 *
 *  @param titleArray 文字数组
 *  @param frameArray frame数组
 *  @param mycolor    颜色
 *  @param myfont     字号
 *  @param mypargraph 修饰
 */
+(void)drawArrays:(NSArray*)titleArray withFrameArray:(NSArray*)frameArray color:(UIColor*)mycolor font:(CGFloat)myfont pargraph:(NSMutableParagraphStyle*)mypargraph;


/**
 *  获取当前屏幕显示的viewcontroller
 *
 *  @return <#return value description#>
 */
+ (UIViewController *)getCurrentVC;


/**
 *  解析字典得到的是NSString类型
 *
 *  @param dic <#dic description#>
 *  @param key <#key description#>
 *
 *  @return value
 */
+(id) dictionaryValue:(NSDictionary*)dic forKey:(NSString*)key;


/**
 *  解析字典得到的不是nsstring类型
 *
 *  @param dic <#dic description#>
 *  @param key <#key description#>
 *
 *  @return value
 */
+(id) dictionaryNotString:(NSDictionary*)dic forKey:(NSString*)key;

/**
 *  判断登录注册时输入的手机号
 *
 *  @param telnumber 手机号string
 *
 *  @return 电话号是否成立
 */
+(BOOL)judgeTel:(NSString*)telnumber;

//+(NSMutableArray*)getAllModelPlist;

+(NSString*)getSubjectsPlist:(NSInteger)type withGrade:(NSInteger)gradeTag;


+(unsigned long)stringColorToLong:(NSString*)string;
+(void)setNullLoctaionData;

//显示黑框提示语
+(void)show:(UIView*)view withText:(NSString*)text;

//验证码接口需要旧代码接口获取
+ (NSMutableDictionary *)requestOldWithDic:(NSMutableDictionary *)mDic;

//处理图片
+ (UIImage *) imageCompressFitSizeScale:(UIImage *)sourceImage targetSize:(CGSize)size;
//增加阴影
+(void)setShandow:(UIView*)someview inBGView:(UIView*)bgview;
+ (void)initialize;
//显示菊花
+(void)showLoading;
+(void)hideLoading;
+(void)showChuTiLoading;
+(void)hideChuTiLoading;
+(void)showChuTiFenXILoading;
+(void)hideChuTiFenXILoading;
+(UIImage*)getImageFromName:(NSString*)str;
@end
