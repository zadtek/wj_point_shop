//
//  UIViewController+Category.h
//  RRQS
//
//  Created by kyjun on 16/3/23.
//
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFNetworkReachabilityManager.h>
#import "MSingle.h"

@interface UIViewController (Category)<MBProgressHUDDelegate>

@property(nonatomic,strong) MBProgressHUD* HUD;

@property(nonatomic,strong,readonly) MSingle* Identity;

@property(nonatomic,assign) BOOL firstLoad;

-(void)showHUD;
-(void)showHUD:(NSString*)message;

-(void)hidHUD;
-(void)hidHUD:(NSString*)message;
-(void)hidHUD:(NSString*)message success:(BOOL)success;
-(void)hidHUD:(NSString*)message success:(BOOL)success complete:(dispatch_block_t) complete;

-(void)alertHUD:(NSString*)message;
-(void)alertHUD:(NSString*)message complete:(dispatch_block_t) complete;
-(void)alertHUD:(NSString *)message delay:(NSTimeInterval)delay;

-(void)checkNetWorkState:(void (^)(AFNetworkReachabilityStatus netWorkStatus))complete;

@end
