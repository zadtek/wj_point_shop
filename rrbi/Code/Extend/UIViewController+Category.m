//
//  UIViewController+Category.m
//  RRQS
//
//  Created by kyjun on 16/3/23.
//
//

#import "UIViewController+Category.h"
#import <objc/runtime.h>

static char const * const rrHUD =  "HUD";
static char const * const rrFirstLoad =  "firstLoad";

@implementation UIViewController (Category)

@dynamic HUD,Identity;

-(MBProgressHUD *)HUD{
    return objc_getAssociatedObject(self, rrHUD);
}

-(void)setHUD:(MBProgressHUD *)HUD{
    objc_setAssociatedObject(self, rrHUD, HUD, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)firstLoad{
    return [objc_getAssociatedObject(self, rrFirstLoad) boolValue];
}

-(void)setFirstLoad:(BOOL)firstLoad{
    objc_setAssociatedObject(self, rrFirstLoad, [NSNumber numberWithBool:firstLoad], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(MSingle *)Identity{
    return [MSingle shareAuhtorization];
}


-(void)showHUD{
    if (self.HUD) {
        [self.HUD removeFromSuperview];
        self.HUD = nil;
    }
    self.HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:self.HUD];
    self.HUD.delegate = self;
    self.HUD.minSize = CGSizeMake(135.f, 135.f);
    self.HUD.label.font = [UIFont systemFontOfSize:14.f];
    [self.HUD showAnimated:YES];
}
-(void)showHUD:(NSString*)message{
    if (self.HUD) {
        [self.HUD removeFromSuperview];
        self.HUD = nil;
    }
    self.HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:self.HUD];
    self.HUD.delegate = self;
    self.HUD.minSize = CGSizeMake(135.f, 135.f);
    self.HUD.label.font = [UIFont systemFontOfSize:14.f];
    self.HUD.label.text = message;
    [self.HUD showAnimated:YES];
}

-(void)hidHUD{
    if (self.HUD) {
        [self.HUD hideAnimated:YES];
        self.HUD=nil;
    }
}
-(void)hidHUD:(NSString*)message{
    self.HUD.mode = MBProgressHUDModeCustomView;
    self.HUD.label.text  = message;
    [self.HUD hideAnimated:YES afterDelay:1];
}
-(void)hidHUD:(NSString*)message success:(BOOL)success{
    if (success) {
        self.HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"success"]];
    }else{
        self.HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"error"]];
    }
    self.HUD.mode = MBProgressHUDModeCustomView;
    self.HUD.label.text  = message;
    [self.HUD hideAnimated:YES afterDelay:1];
}

-(void)hidHUD:(NSString*)message success:(BOOL)success complete:(dispatch_block_t) complete{
    if (success) {
        self.HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"success"]];
    }else{
        self.HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"error"]];
    }
    self.HUD.mode = MBProgressHUDModeCustomView;
    self.HUD.label.text  = message;
    [self.HUD hideAnimated:YES afterDelay:1];
    complete();
}

-(void)alertHUD:(NSString*)message{
    self.HUD = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    self.HUD.mode = MBProgressHUDModeCustomView;
    self.HUD.label.text  = message;
    self.HUD.label.font = [UIFont systemFontOfSize:12.f];
    self.HUD.removeFromSuperViewOnHide = YES;
    [self.HUD hideAnimated:YES afterDelay:1];
}

-(void)alertHUD:(NSString*)message complete:(dispatch_block_t) complete{
    self.HUD = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    self.HUD.mode = MBProgressHUDModeCustomView;
    self.HUD.label.text  = message;
    self.HUD.label.font = [UIFont systemFontOfSize:12.f];
    self.HUD.removeFromSuperViewOnHide = YES;
    complete();
    [self.HUD hideAnimated:YES afterDelay:1];
}
-(void)alertHUD:(NSString *)message delay:(NSTimeInterval)delay{
    self.HUD = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    self.HUD.mode = MBProgressHUDModeCustomView;
    self.HUD.label.text  = message;
    self.HUD.label.font = [UIFont systemFontOfSize:12.f];
    self.HUD.removeFromSuperViewOnHide = YES;
    [self.HUD hideAnimated:YES afterDelay:delay];
}
- (void)hudWasHidden:(MBProgressHUD *)hud {
    [hud removeFromSuperview];
    hud = nil;
}
-(void)checkNetWorkState:(void (^)(AFNetworkReachabilityStatus netWorkStatus))complete{
    [[AFNetworkReachabilityManager sharedManager]startMonitoring];
    
    [[AFNetworkReachabilityManager sharedManager]setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if(status == AFNetworkReachabilityStatusNotReachable)
            [self alertHUD:@"网络异常!"];
        complete(status);
    }];
}

@end
