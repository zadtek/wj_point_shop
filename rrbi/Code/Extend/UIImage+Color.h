//
//  UIImage+Color.h
//  Test
//
//  Created by Binky Lee on 16/2/14.
//  Copyright © 2016年 Binky Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  根据颜色返回图片
 */
@interface UIImage (Color)
+ (UIImage*) imageWithColor:(UIColor*)color;
@end
